package com.dashihui.afford.ui.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dashihui.afford.AffordApp;
import com.dashihui.afford.R;
import com.dashihui.afford.common.constants.AffConstans;
import com.dashihui.afford.common.constants.CommConstans;
import com.dashihui.afford.sqlite.SqliteShoppingCart;
import com.dashihui.afford.thirdapi.greedsqlite.ShoppingCart;
import com.dashihui.afford.ui.activity.AtyShoppingCart;
import com.dashihui.afford.ui.activity.shop.AtyAffordShop;
import com.dashihui.afford.ui.activity.shop.AtyAffordShopDetail;
import com.dashihui.afford.ui.widget.WgtAlertDialog;
import com.dashihui.afford.util.list.UtilList;
import com.dashihui.afford.util.number.UtilNumber;
import com.dashihui.afford.util.preferences.UtilPreferences;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.List;

/**
 * Created by NiuFC on 2015/11/15.
 */
public class AdapterShoppingCart extends AdapterBase<ShoppingCart> {

    public final static String PRICE_XALL = "小计:￥";
    public final static String PRICE_HALL = "合计:￥";
    private boolean flag = false;


    private CheckBox mCheckAllBox, mDeleteAllAll;
    private TextView mTxtViewAllPrice;
    private TextView mTxtViewAllNum;
    private List<ShoppingCart> mListObject;


    @ViewInject(R.id.itemBoxAgree)
    private CheckBox mItemCheckBox;//是否选中
    private AtyShoppingCart mAtyShopCart;

    public AdapterShoppingCart(AtyShoppingCart atyShopCart, List<ShoppingCart> _List, CheckBox _checkBox, CheckBox _deletCheckbox, TextView _price, TextView _num) {
        super(atyShopCart, _List);
        mAtyShopCart=atyShopCart;
        this.mCheckAllBox = _checkBox;
        this.mDeleteAllAll = _deletCheckbox;
        this.mTxtViewAllPrice = _price;
        this.mTxtViewAllNum = _num;
        this.mListObject = _List;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        LayoutInflater inflater = mContext.getLayoutInflater();
        if (convertView == null) {
            LogUtils.e("getView=========初始化===refresh==>");
            convertView = inflater.inflate(R.layout.aty_shoppingcart_item, null);
            viewHolder = new ViewHolder(mAtyShopCart, mList, mCheckAllBox, mDeleteAllAll, mTxtViewAllPrice, mTxtViewAllNum, position);
            //依赖注入初始化
            ViewUtils.inject(viewHolder, convertView);
            convertView.setTag(viewHolder);
            viewHolder.refresh(position);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.update(position, mList);
        }
        return convertView;
    }

    /**
     * 是否改变全选状态
     */
    public void refreshChanged() {

        List<ShoppingCart> shopCartList = SqliteShoppingCart.getInstance(mContext).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");

        if (!UtilList.isEmpty(shopCartList)) {
            boolean isAllCheck = true;
            for (int k = 0; k < shopCartList.size(); k++) {
                if (!shopCartList.get(k).getIschoose()) {
                    isAllCheck = false;
                }
            }
            mCheckAllBox.setChecked(isAllCheck);
            mDeleteAllAll.setChecked(isAllCheck);
        }
    }

    /**
     * @author p
     */
    class ViewHolder {
        @ViewInject(R.id.itemBoxAgree)
        private CheckBox mItemCheckBox;//是否选中
        @ViewInject(R.id.apps_item_img)
        private ImageView mImageView;//图片
        @ViewInject(R.id.apps_item_app_name_tv)
        private TextView mTextViewName;//商品名
        @ViewInject(R.id.apps_item_download_count_tv)
        private TextView mTextViewPrice;//商品现价
        @ViewInject(R.id.apps_item_size_tv)
        private TextView mTextViewOldPrice;//商品原价
        @ViewInject(R.id.trade_edit_num_input)
        private TextView mTextViewInputNums;//输入的数字

        @ViewInject(R.id.apps_item_desc_tv)
        private TextView mTextViewTotalPrice;//小计

        @ViewInject(R.id.trade_btn_num_reduce)
        private ImageButton mButReduce;//减少按钮
        @ViewInject(R.id.trade_btn_num_raise)
        private ImageButton mButRaise;//增加按钮
        @ViewInject(R.id.lyt_edit)
        private LinearLayout mLytEdit;
        @ViewInject(R.id.deleteAllBoxAgree)
        private CheckBox mDeleteAllAll;//全选

        private List<ShoppingCart> mListObject;

        private CheckBox m_CheckAllBox;//全选
        private TextView m_TxtViewAllPrice;//选择的价格
        private TextView m_TxtViewAllNum;//选择的数量
        private int mPosition;
        private AtyShoppingCart mAtyShopCart;

        public ViewHolder(AtyShoppingCart atyShopCart, List<ShoppingCart> listObject, CheckBox _checkBox, CheckBox _deleteCheckBox, TextView _price, TextView _num, int position) {
            this.mListObject = listObject;
            mAtyShopCart = atyShopCart;
            this.m_CheckAllBox = _checkBox;
            this.mDeleteAllAll = _deleteCheckBox;
            this.m_TxtViewAllPrice = _price;
            this.m_TxtViewAllNum = _num;
            mPosition = position;
        }

        /**
         * 更新最新数据
         *
         * @param _listMap
         */
        public void refreshList(List<ShoppingCart> _listMap) {
            mListObject = _listMap;
        }

        /**
         * 第一次刷新
         */
        public void refresh(final int position) {
            mPosition = position;
            if (!UtilList.isEmpty(mListObject)){
                mItemCheckBox.setChecked(mListObject.get(position).getIschoose());
                String uri = AffConstans.PUBLIC.ADDRESS_IMAGE + mListObject.get(position).getImgsrc() + "";
                LogUtils.e("uri===图片地址===========>" + uri);
                Glide.with(mAtyShopCart)
                        .load(uri)
                        .placeholder(R.drawable.default_list)
                        .error(R.drawable.default_list)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mImageView);
                mTextViewName.setText(mListObject.get(position).getName() + "");
                LogUtils.e("优惠后价格==============>" + mListObject.get(position).getSellprice());
                mTextViewPrice.setText(mListObject.get(position).getSellprice() + "");
                mTextViewOldPrice.setText("￥" + mListObject.get(position).getMarketprice() + "");
                mTextViewOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); //中划线
                mTextViewInputNums.setText(UtilNumber.IntegerValueOf(mListObject.get(position).getBuynum() + "") + "");

                double num = UtilNumber.DoubleValueOf(mListObject.get(position).getBuynum() + "");
                double price = UtilNumber.DoubleValueOf(mListObject.get(position).getSellprice() + "");
                double allPrice = UtilNumber.DoubleValueOf(num * price + "");
                mTextViewTotalPrice.setText(PRICE_XALL + allPrice + "");
            }else{
                LogUtils.e("refresh===Error=====null=========>" + mListObject + "");
            }


        }


        //查询当前item是否被选中
        public boolean isCheckChecked() {
            boolean isChecke = mListObject.get(mPosition).getIschoose();
            return isChecke;
        }

        @OnClick(R.id.itemBoxAgree)
        public void OnCheckClick(View v) {
            boolean isChecked = mItemCheckBox.isChecked();
            LogUtils.e("OnCheckClick====点击后触发==========>" + isChecked);
            //改变选择按钮的属性
            mListObject.get(mPosition).setIschoose(isChecked);

            LogUtils.e("OnCheckClick====点击后触发==修改数据库========>" + isChecked);
            double num;
            double price;
            double allPrice;
            //获取本地存储总数量和价格
            double preFerNumAll = UtilPreferences.getInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM);
            double preFerPriceAll = UtilNumber.DoubleValueOf(UtilPreferences.getString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE));

            double finalPrice = 0;
            double finalNum = 0;
            if (isChecked) {
                if ("0".equals(mTextViewInputNums.getText().toString())) {
                    mListObject.get(mPosition).setBuynum("1");
                    mTextViewInputNums.setText("1");
                }
                num = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getBuynum() + "");
                price = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getSellprice() + "");
                allPrice = UtilNumber.DoubleValueOf(num * price + "");
                //底部总的
                finalNum = UtilNumber.DoubleValueOf((preFerNumAll + num) + "");
                finalPrice = UtilNumber.DoubleValueOf((preFerPriceAll + allPrice) + "");
                mTextViewTotalPrice.setText(PRICE_XALL + allPrice + "");
                m_TxtViewAllNum.setText("去结算(" + UtilNumber.IntegerValueOf(finalNum + "") + ")");//改变底部结算数量
                m_TxtViewAllPrice.setText(PRICE_HALL + finalPrice + "");//改变底部合计

                UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_CHOOSE, isChecked + "");

                //判断是否改变全选的状态
                refreshChanged();
            } else {
                if (preFerNumAll != 0 || preFerPriceAll != 0) {
                    num = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getBuynum() + "");
                    price = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getSellprice() + "");
                    allPrice = UtilNumber.DoubleValueOf(num * price + "");

                    finalNum = UtilNumber.IntegerValueOf((preFerNumAll - num) + "");
                    finalPrice = UtilNumber.DoubleValueOf((preFerPriceAll - allPrice) + "");
                    m_TxtViewAllNum.setText("去结算(" + UtilNumber.IntegerValueOf(finalNum + "") + ")");//改变底部结算数量
                    m_TxtViewAllPrice.setText(PRICE_HALL + finalPrice + "");//改变底部合计
                }
                //全选的状态保存
                UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_CHOOSE, isChecked + "");
                m_CheckAllBox.setChecked(false);//改变底部全选状态
                mDeleteAllAll.setChecked(false);//改变底部全选状态
            }

            //保存到数据库
            SqliteShoppingCart.getInstance(mAtyShopCart).update(mListObject.get(mPosition).getID(), mListObject.get(mPosition));
            UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE, finalPrice + "");
            UtilPreferences.putInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM, UtilNumber.IntegerValueOf(finalNum + ""));


        }

        /**
         * 是否改变全选状态
         */
        private void refreshChanged() {
            List<ShoppingCart> shopCartList = SqliteShoppingCart.getInstance(mAtyShopCart).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");

            LogUtils.e("onCheckedChanged====改变状态时触发==========>" + shopCartList);
            if (!UtilList.isEmpty(shopCartList)) {
                boolean isAllCheck = true;
                LogUtils.e("onCheckedChanged====改变状态时触发==========>" + shopCartList.size());
                for (int k = 0; k < shopCartList.size(); k++) {
                    if (!shopCartList.get(k).getIschoose()) {
                        isAllCheck = false;
                    }
                }
                LogUtils.e("onCheckedChanged====改变状态时触发===ggg=======>" + isAllCheck);
                m_CheckAllBox.setChecked(isAllCheck);
                mDeleteAllAll.setChecked(isAllCheck);
            }
        }


        @OnClick(R.id.trade_btn_num_raise)
        public void onRaiseClick(View v) {//添加
            boolean isChecked = mItemCheckBox.isChecked();
            //数量
            String numEdit = mTextViewInputNums.getText().toString().trim();
            double price = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getSellprice() + "");
            if (UtilNumber.isNumeric(numEdit)) {
                double result = UtilNumber.DoubleValueOf(numEdit);
                if (result <= 0) {
                    mTextViewInputNums.setText(UtilNumber.IntegerValueOf(++result + "") + "");
                    mTextViewTotalPrice.setText(PRICE_XALL + price + "");
                } else {
                    LogUtils.e("结果为零==============>");
                    mTextViewInputNums.setText(UtilNumber.IntegerValueOf(++result + "") + "");
                    double allPrice = UtilNumber.DoubleValueOf(result * price + "");
                    mTextViewTotalPrice.setText(PRICE_XALL + allPrice + "");
                    //小计
                    if (isChecked) {//改变总合计和总数量
                        LogUtils.e("++改变总合计和总数量==========改变====>");
                        //获取本地存储总数量和价格
                        double preFerNumAll = UtilPreferences.getInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM);
                        double preFerPriceAll = UtilNumber.DoubleValueOf(UtilPreferences.getString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE));
                        double allNum = ++preFerNumAll;
                        double priceAll = UtilNumber.DoubleValueOf(preFerPriceAll + price + "");
                        m_TxtViewAllNum.setText("去结算(" + UtilNumber.IntegerValueOf(allNum + "") + ")");//改变底部结算数量
                        m_TxtViewAllPrice.setText(PRICE_HALL + priceAll + "");//改变底部合计
                        //存储到本地
                        UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE, priceAll + "");
                        UtilPreferences.putInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM, UtilNumber.IntegerValueOf(allNum + ""));
                    } else {
                        LogUtils.e("++不改变总合计和总数量==============>");
                    }
                }
                //改变数据库中此商品的数量
                mListObject.get(mPosition).setBuynum(result + "");
                SqliteShoppingCart.getInstance(mAtyShopCart).update(mListObject.get(mPosition).getID(), mListObject.get(mPosition));
            } else {
                mTextViewInputNums.setText("1");
                mTextViewTotalPrice.setText(PRICE_XALL + price + "");
            }

            //发送广播更新底部购物车显示
            sendShopChartBroadcast();
        }


        @OnClick(R.id.trade_btn_num_reduce)
        public void onReduceClick(View v) {//减少
            String numEdit = mTextViewInputNums.getText().toString().trim();

            if (UtilNumber.isNumeric(numEdit) && !UtilList.isEmpty(mListObject)) {
                double result = UtilNumber.DoubleValueOf(numEdit);
                double price = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getSellprice() + "");
                if (result <= 1) {
                    final WgtAlertDialog mAtDialog = new WgtAlertDialog();
                    mAtDialog.show(mAtyShopCart,
                            "取消", "确定",
                            "确认删除当前物品？",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mAtDialog.dismiss();
                                }
                            }, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //更新界面数据
                                    boolean isChoose = mListObject.get(mPosition).getIschoose();
                                    double price1 = UtilNumber.DoubleValueOf(mListObject.get(mPosition).getSellprice() + "");
                                    if (isChoose == true) {
                                        double preFerNumAll = UtilPreferences.getInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM);
                                        double preFerPriceAll = UtilNumber.DoubleValueOf(UtilPreferences.getString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE));
                                        double allNum = --preFerNumAll;
                                        double priceAll = UtilNumber.DoubleValueOf((preFerPriceAll - price1) + "");
                                        m_TxtViewAllNum.setText("去结算(" + UtilNumber.IntegerValueOf(allNum + "") + ")");//改变底部结算数量
                                        m_TxtViewAllPrice.setText(PRICE_HALL + priceAll + "");//改变底部合计
                                        //存储到本地
                                        UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE, priceAll + "");
                                        UtilPreferences.putInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM, UtilNumber.IntegerValueOf(allNum + ""));
                                    }
                                    //删除数据库中数据
                                    SqliteShoppingCart.getInstance(mAtyShopCart).deleteShoppingCart(mListObject.get(mPosition));
                                    mListObject.remove(mPosition);
                                    mListObject = SqliteShoppingCart.getInstance(mAtyShopCart).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");

                                    //刷新当前数据
                                    setList(mListObject);
                                    //购物车为空时显示为空的界面
                                    if (mListObject.size() <= 0) {
//                                        mAtyShopCart.onBackPressed();
                                        mAtyShopCart.visiableNoShopping();
                                    }
                                    notifyDataSetChanged();
                                    //发送广播更新底部购物车显示
                                    sendShopChartBroadcast();

                                    mAtDialog.dismiss();
                                }
                            }, false, false, 0, null);

                } else {
                    mTextViewInputNums.setText(UtilNumber.IntegerValueOf(--result + "") + "");
                    boolean isChecked = mItemCheckBox.isChecked();
                    /*****************/
                    if (result == 0) {

//                        mItemCheckBox.setChecked(false);
                        m_CheckAllBox.setChecked(false);
                        mDeleteAllAll.setChecked(false);
                        //改变数据库的数据
                        mListObject.get(mPosition).setIschoose(false);
                    } else {
                        LogUtils.e("数量不为为零===00===改变小计========>");
                    }
                    double allPrice = UtilNumber.DoubleValueOf(result * price + "");
                    mTextViewTotalPrice.setText(PRICE_XALL + allPrice + "");
                    //小计
                    if (isChecked) {
                        LogUtils.e("当此控件选中时==00===改变合计和总数量========>");
                        //获取本地存储总数量和价格
                        double preFerNumAll = UtilPreferences.getInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM);
                        double preFerPriceAll = UtilNumber.DoubleValueOf(UtilPreferences.getString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE));
                        double allNum = --preFerNumAll;
                        double priceAll = UtilNumber.DoubleValueOf((preFerPriceAll - price) + "");
                        m_TxtViewAllNum.setText("去结算(" + UtilNumber.IntegerValueOf(allNum + "") + ")");//改变底部结算数量
                        m_TxtViewAllPrice.setText(PRICE_HALL + priceAll + "");//改变底部合计
                        //存储到本地
                        UtilPreferences.putString(mAtyShopCart, AtyShoppingCart.SHOPCART_PRICE, priceAll + "");
                        UtilPreferences.putInt(mAtyShopCart, AtyShoppingCart.SHOPCART_NUM, UtilNumber.IntegerValueOf(allNum + ""));
                    } else {
                        LogUtils.e("--不改变总合计和总数量==============>");
                    }
                    //改变数据库中此商品的数量
                    mListObject.get(mPosition).setBuynum(result + "");
                    SqliteShoppingCart.getInstance(mAtyShopCart).update(mListObject.get(mPosition).getID(), mListObject.get(mPosition));
                }

            } else {
//                mTextViewInputNums.setText("0");
//                mItemCheckBox.setChecked(false);
//                mTextViewTotalPrice.setText(PRICE_XALL + "0.0");
            }

            //发送广播更新底部购物车显示
            sendShopChartBroadcast();
        }


        @OnClick(R.id.itemRLyt)
        public void mItemRLytClick(View view) {
            Intent mIntent = new Intent(mAtyShopCart, AtyAffordShopDetail.class);
            //便利店列表
            mIntent.putExtra(AtyAffordShop.INTENT_SHOP_ID, mListObject.get(mPosition).getID() + "");
            mAtyShopCart.startActivity(mIntent);
        }

        /**
         * 底部购物车显示
         */
        public void sendShopChartBroadcast() {
            int shopCartNum = 0;
            if (AffordApp.getInstance().getEntityLocation() != null && AffordApp.getInstance().getEntityLocation().getSTORE() != null) {
                List<ShoppingCart> shopCartList = SqliteShoppingCart.getInstance(mAtyShopCart).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");
                if (!UtilList.isEmpty(shopCartList)) {
                    for (int k = 0; k < shopCartList.size(); k++) {
                        shopCartNum += UtilNumber.IntegerValueOf(shopCartList.get(k).getBuynum());
                    }

                }
            }
            LogUtils.e("BroadcastReceiver===============>" + shopCartNum);
            Intent intent = new Intent();  //Itent就是我们要发送的内容
            intent.putExtra("data", shopCartNum);
            intent.setAction(CommConstans.REGISTER.BROADCAST_INTENT_ACTION);   //设置你这个广播的action，只有和这个action一样的接受者才能接受者才能接收广播
            mAtyShopCart.sendBroadcast(intent);   //发送广播
        }

        /**
         * 更新
         */
        public void update(final int position, List<ShoppingCart> _listMap) {
            refreshList(_listMap);
            refresh(position);
        }
    }

}
