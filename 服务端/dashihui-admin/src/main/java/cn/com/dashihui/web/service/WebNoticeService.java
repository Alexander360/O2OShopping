package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Page;

import cn.com.dashihui.web.dao.WebNotice;

public class WebNoticeService {
	
	public boolean addNotice(WebNotice newObject){
		return newObject.save();
	}
	
	public boolean delNotice(int id){
		return WebNotice.me().deleteById(id);
	}
	
	public boolean editNotice(WebNotice object){
		return object.update();
	}
	
	public WebNotice findById(int id){
		return WebNotice.me().findFirst("SELECT * FROM t_web_notice WHERE id=?",id);
	}
	
	public Page<WebNotice> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_web_notice A");
		sqlExcept.append(" ORDER BY A.createDate DESC");
		return WebNotice.me().paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString());
	}
}
