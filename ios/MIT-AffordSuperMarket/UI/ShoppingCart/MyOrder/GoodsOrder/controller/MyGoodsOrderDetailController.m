//
//  MyGoodsOrderDetailController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderDetailController.h"
//controller
#import "WeiXinPayController.h"
#import "MyGoodsOderTrackingController.h"
#import "MyGoodsTradeFinishedController.h"
#import "Evaluate_Store_Controller.h"
#import "MyGoodsOrderController.h"
#import "SureOrderController.h"
//view
#import "MyGoodsOrderDetailHeadCell.h"
#import "MyGoodsDetailAddressCell.h"
#import "MyGoodsOrderDetailCell.h"
#import "MyGoodsDetailBottomView.h"
#import "MyOrderDetailBottomCell.h"
//other
#import "ManagerHttpBase.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
#import "ACETelPrompt.h"
#import "MTA.h"
@interface MyGoodsOrderDetailController ()<UITableViewDataSource,UITableViewDelegate,MyGoodsDetailBottomViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)MyGoodsDetailBottomView *bottomView;//底部栏
@property (nonatomic,strong)NSMutableArray *dataSource;//商品数据源
@property (nonatomic,strong)NSMutableDictionary *dataDic;//数据源
//总价格
@property (nonatomic,strong)UILabel *totalPrice;
@property (nonatomic)BOOL payStateIsYes;
@end

@implementation MyGoodsOrderDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化控件
    [self setSubView];
    self.title =@"订单详情";
    self.dataSource = [[NSMutableArray alloc]init];
    self.dataDic =[[NSMutableDictionary alloc]init];
    self.payStateIsYes =NO;
     [self addRightNavItemWith:[UIImage imageNamed:@"tail_btn_telephone"]];
    //添加返回按钮
    [self addBackNavItem];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        [self initOrderDetailArray];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
    [self.bottomView timesStop];

}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"商品订单详情"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"商品订单详情"];
}
#pragma mark -- 请求数据
//数据源
-(void)initOrderDetailArray
{
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..." inView:self.view];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/detail" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf.dataDic removeAllObjects];
                [weakSelf.dataSource removeAllObjects];
                [weakSelf.dataDic setValuesForKeysWithDictionary:[dataDic objectForKey:@"OBJECT"]];
                [weakSelf.dataSource addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"GOODSLIST"]];
                [weakSelf.bottomView setButtonWithDictionary:weakSelf.dataDic];
                NSString *isSelf = [NSString stringTransformObject:[self.dataDic objectForKey:@"ISSELF"]];
                if ([isSelf isEqualToString:@"1"]) {
                    weakSelf.navigationItem.rightBarButtonItem =nil;
                }
                [weakSelf.tableView reloadData];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];
    
}
//请求签收
- (void)loadOrderQianShouData {
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/receive" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                MyGoodsTradeFinishedController *tradeFinishedVC = [[MyGoodsTradeFinishedController alloc]init];
                tradeFinishedVC.goodsOrderInfoDic = self.dataDic;
                [self.navigationController pushViewController:tradeFinishedVC animated:YES];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
        
    }];
}
//取消订单
- (void)loadCancelOrderData {
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:@"order/cancel" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
        NSString *msg = [dataDic objectForKey:@"MSG"];
        if ([state isEqualToString:@"0"]) {//请求成功
            [weakSelf initOrderDetailArray];
        } else {
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
        }
        
    } failure:^(bool isFailure) {
        
    }];
}
//请求催单数据
- (void)loadCuiOrderData{
   
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:mode.token forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/urge" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                msg = @"催单成功";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {
        
        
    }];
}

//删除订单

-(void)loaddeleteOrderOrderData
{
    /*
     SIGNATURE			设备识别码
     TOKEN			    用户签名
     ORDERNUM			订单号
     **/
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:@"order/delete" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
//        NSLog(@"%@",[dataDic objectForKey:@"MSG"]);
        if ([state isEqualToString:@"0"]) {//请求成功
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"删除成功" inView:weakSelf.view];
            [weakSelf performSelector:@selector(backUserViewController) withObject:nil afterDelay:2.0f];
        }
        
    } failure:^(bool isFailure) {
        
    }];
    
}
-(void)backUserViewController
{
    
    BOOL isContainTradeFinishedVC = NO;
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[MyGoodsTradeFinishedController class]]) {//从订单完成push到订单详情
            isContainTradeFinishedVC = YES;
        }
    }

    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[MyGoodsOrderController class]] && isContainTradeFinishedVC) {
            [self.navigationController popToViewController:VC animated:YES];
        }
    }
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[SureOrderController class]] ) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    if (!isContainTradeFinishedVC) {
         [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark --MyGoodsDetailBottomViewDelegate
-(void)bottomButtonSelectedWithButtonTitle:(NSString *)title
{
    if([title isEqualToString:@"取消订单"])
    {
        NSString *str =@"";
        {
            if (self.payStateIsYes) {
                str=@"取消订单后,您的退款进度请进入\n“我的-我的订单-退款”查看";
            }else{
                str =@"取消订单后,您的优惠也将一并取消!\n是否仍继续？";
            }
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:str delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=1;
        [alert show];
    } else if ([title isEqualToString:@"去支付"]) {
        WeiXinPayController *weixinpay=[[WeiXinPayController alloc]init];
        weixinpay.orderNum=self.orderNum;
        weixinpay.totalPrice=[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[self.dataDic objectForKey:@"AMOUNT"]] floatValue]];
        weixinpay.requestDataType =@"1";
        weixinpay.goodIDs=@"";
        [self.navigationController pushViewController:weixinpay animated:YES];
    }else if([title isEqualToString:@"再次购买"]){
        //    SureOrderController *sureOrderVC = [[SureOrderController alloc]init];
        //    [self.navigationController pushViewController:sureOrderVC animated:YES];
        
    } else if ([title isEqualToString:@"确认收货"]||[title isEqualToString:@"确认取货"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"是否%@？",title] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=2;
        [alert show];
        
    }else if ([title isEqualToString:@"删除订单"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除订单?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=3;
        [alert show];
        
    }else if ([title isEqualToString:@"去评价"]){
        Evaluate_Store_Controller *evalVc =[[Evaluate_Store_Controller alloc]init];
        evalVc.orderNum =self.orderNum;
        [self.navigationController pushViewController:evalVc animated:YES];
    }else if ([title isEqualToString:@"催单"]){
        [self loadCuiOrderData];
    }
}


#pragma mark--UIAlertViewdelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag ==1){//取消订单
        if (buttonIndex == 1){
            [self loadCancelOrderData];
        }
    }else if(alertView.tag ==2){//确认签收
        if (buttonIndex == 1){
            [self loadOrderQianShouData];
        }
    }else if(alertView.tag == 3)//删除订单
    {
        if (buttonIndex == 1){
            [self loaddeleteOrderOrderData];
        }
    }
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0 || section ==1) {
        return 1;
    }else if(section ==2)
    {
        return self.dataSource.count +2;
    }else if (section == 3)
    {
        NSString *isSelf = [NSString stringTransformObject:[self.dataDic objectForKey:@"ISSELF"]];
        if ([isSelf isEqualToString:@"1"]) {
           return 3;
        }else{
           return 4;
        }
    }
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section ==0) {
        static NSString *cellID =@"goodOrderCell";
        MyGoodsOrderDetailHeadCell*cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if(!cell) {
            cell=[[MyGoodsOrderDetailHeadCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
            //取消cell点击状态
           cell.backgroundColor =[UIColor whiteColor];
        }
        [cell setSubViewDateWithOredrNum:[NSString stringTransformObject:[self.dataDic objectForKey:@"ORDERNUM"]] OrderState:[NSString stringWithFormat:@"%@ >",[self orderStateWith:self.dataDic]]];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:FontSize(16)];
        return cell;

    }else if(indexPath.section ==1){
        static NSString *cellID =@"goodOrderAddressCell";
        MyGoodsDetailAddressCell*cell=[tableView dequeueReusableCellWithIdentifier:cellID];
        if(!cell) {
            cell=[[MyGoodsDetailAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
            //取消cell点击状态
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor =[UIColor whiteColor];
        }
             [cell setSubViewDateWithDictionary:self.dataDic];
        //4.返回cell
        return cell;
    }else if(indexPath.section ==2)
    {
        if (indexPath.row==0) {
            NSString *ID =@"title";
            UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:ID];
            if (cell==nil) {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
            }
            NSString *isSelf = [NSString stringTransformObject:[_dataDic objectForKey:@"ISSELF"]];
            if ([isSelf isEqualToString:@"1"]) {
                 cell.textLabel.text=kDaShiHuiStoreName;
                
            }else{
                XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager]getxiaoQuAndStoreInfo];
                 cell.textLabel.text=model.storeName;
            }
          
            cell.textLabel.textColor=[UIColor colorWithHexString:@"#000000"];
            cell.textLabel.font=[UIFont systemFontOfSize:HightScalar(18)];
            return cell;
        }else if(indexPath.row ==self.dataSource.count+1){
            
            NSString *ID =@"MyOrderDetailBottomCell";
            MyOrderDetailBottomCell *cell =[tableView dequeueReusableCellWithIdentifier:ID];
            if (cell==nil) {
                cell=[[MyOrderDetailBottomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
                
            }
            [cell upSubViewWithData:self.dataDic];
          return cell;
        }else {
            
            NSString *ID = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
            MyGoodsOrderDetailCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
            if(!cell) {
                cell=[[MyGoodsOrderDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
            }
             [cell retSubViewWithData:[self.dataSource objectAtIndex:indexPath.row-1]];
            
            return cell;
        }

    }else{
        if (indexPath.row==0) {
            static NSString *identfy = @"payType";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
            }
            NSString *str=[NSString stringTransformObject:[self.dataDic objectForKey:@"PAYTYPE"]];
            NSString *paytype=@"";
            if ([str isEqualToString:@"1"]) {
                paytype=@"在线支付";
            }else
            {
              paytype=@"货到付款";
            }
            cell.textLabel.text =[NSString stringWithFormat:@"支付方式: %@",paytype];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            cell.textLabel.textColor =[UIColor colorWithHexString:@"#555555"];
            
            return cell;

            
        }else if(indexPath.row ==1){
            static NSString *identfy = @"xiadantime";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                cell.backgroundColor =[UIColor whiteColor];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.text =[NSString stringWithFormat:@"下单时间: %@",[NSString stringTransformObject:[self.dataDic objectForKey:@"STARTDATE"]]];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            cell.textLabel.textColor =[UIColor colorWithHexString:@"#555555"];
            
            return cell;
        }else if(indexPath.row ==2){
            static NSString *identfy = @"peisongtype";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                 cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];
            }
       
            NSString *taketype=@"";

            NSString *isSelf = [NSString stringTransformObject:[self.dataDic objectForKey:@"ISSELF"]];
            if ([isSelf isEqualToString:@"1"]) {
                taketype=@"第三方配送";
            }else{
            NSString *str=[NSString stringTransformObject:[self.dataDic objectForKey:@"TAKETYPE"]];
                if ([str isEqualToString:@"1"]) {
                    taketype=@"门店配送";
                }else
                {
                    taketype=@"上门自取";
                }
            }
            cell.textLabel.text =[NSString stringWithFormat:@"配送方式: %@",taketype];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            cell.textLabel.textColor =[UIColor colorWithHexString:@"#555555"];
            return cell;
        }else{
            XiaoQuAndStoreInfoModel *model =[[ManagerGlobeUntil sharedManager]getxiaoQuAndStoreInfo];
            static NSString *identfy = @"peisongshuoming";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.backgroundColor =[UIColor whiteColor];

            }
            NSString *str=[NSString stringTransformObject:[self.dataDic objectForKey:@"TAKETYPE"]];
           
            if ([str isEqualToString:@"1"]) {
              cell.textLabel.text =[NSString stringWithFormat:@"配送说明: %@",model.storePeiSongIntroduce];
            }else
            {
               cell.textLabel.text =[NSString stringWithFormat:@"营业时间: %@",model.storeYingYeTime];
            }
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            cell.textLabel.textColor =[UIColor colorWithHexString:@"#555555"];
            return cell;

       }
    }
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section ==0 ||indexPath.section ==3 ) {
        return HightScalar(46);
    }else if(indexPath.section ==1){
        return HightScalar(106);
    }else{
        if (indexPath.row==self.dataSource.count+1) {
          
            return HightScalar(130);
        }else if(indexPath.row == 0)
        {
            return HightScalar(46);
        }else{
            return HightScalar(110);
        }
    }
    
}
//黑条效果
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HightScalar(12);
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view =[[UIView alloc]init];
    view.backgroundColor =[UIColor colorWithHexString:@"efefef"];
    [view.layer setBorderWidth:0.7];
    [view.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    return view;
}
//去掉table的Header的悬浮效果
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = HightScalar(12);
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}
//选中cell点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //订单跟踪
    if(indexPath.section == 0){
         [tableView deselectRowAtIndexPath:indexPath animated:YES];
        MyGoodsOderTrackingController *orderTrackingVC = [[MyGoodsOderTrackingController alloc]init];
        
        orderTrackingVC.orderNumber = [NSString stringTransformObject:[self.dataDic objectForKey:@"ORDERNUM"]];
        orderTrackingVC.payType = [NSString transitionPayType:[NSString stringTransformObject:[self.dataDic objectForKey:@"PAYTYPE"]]];
        orderTrackingVC.peiSongType = [NSString transitionPeiSongType:[NSString stringTransformObject:[self.dataDic objectForKey:@"TAKETYPE"]]];
        [self.navigationController pushViewController:orderTrackingVC animated:YES];
    }
}
-(NSString *)orderStateWith:(NSMutableDictionary *)dataDic
{
    
    
        NSString *PayType=[NSString stringTransformObject:[dataDic objectForKey:@"PAYTYPE"]];
        NSString *DeliveType=[NSString stringTransformObject:[dataDic objectForKey:@"TAKETYPE"]];
        NSString *PayState=[NSString stringTransformObject:[dataDic objectForKey:@"PAYSTATE"]];
        NSString *DeliveState=[NSString stringTransformObject:[dataDic objectForKey:@"DELIVERSTATE"]];
        NSString *OrderState=[NSString stringTransformObject:[dataDic objectForKey:@"ORDERSTATE"]];
        NSString *packState=[NSString stringTransformObject:[dataDic objectForKey:@"PACKSTATE"]];
//        NSString *eavlState=[NSString stringTransformObject:[dataDic objectForKey:@"EVALSTATE"]];
   
    if ([OrderState isEqualToString:KOrderStateNormal] ){//订单正常  支付状态为 未支付
        if ([PayType isEqualToString:KOrderPayTypeIsPayPal] && [DeliveType isEqualToString:KOrderDeliverTypeIsDelivery])//在线付款+送货上门
        {
            if ([PayState isEqualToString:KOrderPayStateIsNo]) {
                return @"待付款";
            }else if([PayState isEqualToString:KOrderPayStateIsYes] &&[DeliveState isEqualToString:KOrderDeliverStateIsNo])//订单正常  支付状态为 已支付
            {
                self.payStateIsYes =YES;

                if ([packState isEqualToString:KOrderPackState1]) {
                                        return @"付款成功";
                }else if([packState isEqualToString:KOrderPackState2]){
                     return @"打包中";
                }else if([packState isEqualToString:KOrderPackState3]){
                    return @"打包中";
                }
                else if([packState isEqualToString:KOrderPackState4]){
                    return @"待配送";
                }
            }else if([DeliveState isEqualToString:KOrderDeliverStateIsYes]){//订单正常  收货状态为 已发货
               return @"配送中";;
            }
        }else if([PayType isEqualToString:KOrderPayTypeIsPayPal]&& [DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]){//自取 +在线付款
            
            if([PayState isEqualToString:KOrderPayStateIsNo]){
               return @"待付款";
                
            }else if([PayState isEqualToString:KOrderPayStateIsYes])
            {
                self.payStateIsYes =YES;
                if ([packState isEqualToString:KOrderPackState1]) {
                    
                    return @"付款成功";
                }else if([packState isEqualToString:KOrderPackState2]){
                    return @"打包中";
                }else if([packState isEqualToString:KOrderPackState3]){
                    return @"打包中";
                }
                else if([packState isEqualToString:KOrderPackState4]){
                    return @"待取货";
                }
            }
        }else if([PayType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsDelivery]) //货到付款 + 送货上门
        {
            
            if([DeliveState isEqualToString:KOrderDeliverStateIsNo]){
                if ([packState isEqualToString:KOrderPackState1]) {
                    return @"处理中";
                }else if([packState isEqualToString:KOrderPackState2]){
                    return @"打包中";
                }
                else if([packState isEqualToString:KOrderPackState3]){
                    return @"打包中";
                }
                else if([packState isEqualToString:KOrderPackState4]){
                   return @"待配送";
                }
            }else if ([DeliveState isEqualToString:KOrderDeliverStateIsYes])//2
            {
                return @"配送中";
            }
            
        }else if([PayType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]) //货到付款 + 自取
        {
            if ([packState isEqualToString:KOrderPackState1]) {
                return @"处理中";
            }else if([packState isEqualToString:KOrderPackState2]){
                return @"打包中";
            }
            else if([packState isEqualToString:KOrderPackState3]){
                return @"打包中";
            }
            else if([packState isEqualToString:KOrderPackState4]){
                return @"待取货";
            }

        }
    }else if([OrderState isEqualToString:KOrderStateFinish])//订单状态为已完成
    {
         return @"交易完成";
        
    }else if ([OrderState isEqualToString:KOrderStateCancel])//订单状态为已取消
    {
        return @"已取消";

    } else if ([OrderState isEqualToString:KOrderStateBeOverdue])//订单状态为已过期
    {
        return @"已过期";

    }
    return @"";
}
#pragma mark --初始化视图控件
-(void)setSubView
{
    //初始化tableView
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT-64-HightScalar(65)) style:UITableViewStylePlain];
    _tableView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
    _tableView.delegate =self;
    _tableView.dataSource =self;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    UIView *footView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 1)];
    _tableView.tableFooterView =footView;
    [self.view addSubview:_tableView];
    //底部栏
    _bottomView =[[MyGoodsDetailBottomView alloc]initWithFrame:CGRectMake(0, VIEW_HEIGHT-HightScalar(65)-64, VIEW_WIDTH, HightScalar(65))];
    _bottomView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
    _bottomView.delegate =self;
    [self.view addSubview:_bottomView];
}
//返回按钮
-(void)backBtnAction:(id)sender
{
    BOOL isContainTradeFinishedVC = NO;
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[MyGoodsTradeFinishedController class]]) {//从订单完成push到订单详情
            isContainTradeFinishedVC = YES;
        }
    }
    
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[MyGoodsOrderController class]] && isContainTradeFinishedVC) {
            [self.navigationController popToViewController:VC animated:YES];
        }
    }
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[SureOrderController class]] ) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    
    if (!isContainTradeFinishedVC) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)rigthBtnAction:(id)sender
{
    XiaoQuAndStoreInfoModel *model =[[ManagerGlobeUntil sharedManager]getxiaoQuAndStoreInfo];
    [ACETelPrompt callPhoneNumber:model.storePhone
                             call:^(NSTimeInterval duration) {
                                 
                             } cancel:^{
                                 
                             }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
