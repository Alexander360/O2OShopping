//
//  BaiDuLocationCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>
@interface BaiDuLocationCell : UITableViewCell
- (void)updateViewWithData:(BMKCloudPOIInfo*)poiInfo;//定位页面
- (void)updateStoreListCellWithDataDic:(NSDictionary*)dataDic;//便利店列表
@end
