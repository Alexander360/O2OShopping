package cn.com.dashihui.wx.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import cn.com.dashihui.wx.kit.DoubleKit;

/**
 * 购物车商品操作工具类<br/>
 * 在session中以店铺ID为key，将各店铺的购物车商品列表进行保存，理论上一个用户可以切换不同店铺进行商品选择
 */
@SuppressWarnings("unchecked")
public class CartKit {
	private static final String SESSION_CART = "CART_LIST";
	
	/**
	 * 验证购物车是否存在
	 * @param session
	 * @param storeid 店铺ID
	 * @return
	 */
	public static boolean hasCart(HttpSession session, int storeid){
		Object temp = session.getAttribute(SESSION_CART);
		Map<String,Cart> cartMap = null;
		Cart cart = null;
		if(temp==null){
			return false;
		}else{
			cartMap = (Map<String,Cart>)temp;
		}
		cart = cartMap.get(Cart.key(storeid));
		if(cart==null){
			return false;
		}
		return true;
	}
	
	/**
	 * 获取购物车
	 * @param session
	 * @param storeid 店铺ID
	 * @return
	 */
	public static Cart getCart(HttpSession session, int storeid){
		Object temp = session.getAttribute(SESSION_CART);
		Map<String,Cart> cartMap = null;
		Cart cart = null;
		if(temp==null){
			cartMap = new HashMap<String,Cart>();
		}else{
			cartMap = (Map<String,Cart>)temp;
		}
		cart = cartMap.get(Cart.key(storeid));
		if(cart==null){
			cart = new Cart(storeid);
			cartMap.put(Cart.key(storeid), cart);
			session.setAttribute("CART_LIST", cartMap);
		}
		return cart;
	}
	
	/**
	 * 移除购物车
	 */
	public static void removeCart(HttpSession session, int storeid){
		Object temp = session.getAttribute(SESSION_CART);
		Map<String,Cart> cartMap = null;
		if(temp!=null){
			cartMap = (Map<String,Cart>)temp;
			if(cartMap.containsKey(Cart.key(storeid))){
				//移除指定店铺的购物车
				cartMap.remove(Cart.key(storeid));
			}
		}
	}
	
	/**
	 * 向购物车中添加某商品
	 * @param session
	 * @param aGoods 要添加的商品信息对象
	 */
	public static Cart add(HttpSession session, int storeid, CartGoods aGoods){
		//获取店铺的购物车
		Cart cart = getCart(session,storeid);
		//添加商品
		cart.add(aGoods);
		//返回购物车
		return cart;
	}
	
	/**
	 * 修改购物车中某商品的数量及金额
	 * @param session
	 * @param goodsid 要修改数量的商品ID
	 * @param operate 1：增加数量，0：减少数量
	 * @param num 要修改的数量
	 */
	public static Cart update(HttpSession session, int storeid, String goodsid, int operate, int num){
		//获取店铺的购物车
		Cart cart = getCart(session,storeid);
		//更新商品
		cart.update(goodsid, operate, num);
		//更新完商品后，判断如果购物车中商品数量为0，则移除购物车
		if(cart.getCounter()==0){
			removeCart(session, storeid);
		}
		//返回购物车
		return cart;
	}
	
	/**
	 * 删除购物车中某些商品
	 * @param session
	 * @param goodsids 要删除的商品ID数组
	 */
	public static Cart delete(HttpSession session, int storeid, String[] goodsids){
		//获取店铺的购物车
		Cart cart = getCart(session,storeid);
		//删除商品
		cart.delete(goodsids);
		//删除完商品后，判断如果购物车中商品数量为0，则移除购物车
		if(cart.getCounter()==0){
			removeCart(session, storeid);
		}
		//返回购物车
		return cart;
	}
	
	/**
	 * 购物车信息对象
	 */
	public static class Cart implements Serializable{
		private static final long serialVersionUID = 1L;
		private int storeid;
		private Map<String,CartGoods> list;
		private int counter;
		private double total;
		
		public Cart(int storeid) {
			this.storeid = storeid;
			this.list = new LinkedHashMap<String,CartGoods>();
		}
		
		public static String key(int storeid){
			return "store"+storeid;
		}
		
		/**
		 * 向购物车中添加某商品
		 * @param aGoods 要添加的商品信息对象
		 */
		public void add(CartGoods aGoods){
			//获取购物车中对应商品
			CartGoods sGoods = list.get(CartGoods.key(aGoods.getId()));
			if(sGoods==null){
				sGoods = aGoods;
			}else{
				//数量加1
				sGoods.setCounter(sGoods.getCounter()+aGoods.getCounter());
				sGoods.setTotal(DoubleKit.add(sGoods.getTotal(),aGoods.getTotal()));
			}
			//保存
			list.put(CartGoods.key(aGoods.getId()), sGoods);
			//修改购物车总量
			this.counter = this.counter + +aGoods.getCounter();
			//修改购物车总金额
			this.total = DoubleKit.add(this.total,aGoods.getTotal());
		}
		
		/**
		 * 查询购物车中是否有某商品
		 * @param goodsid 商品ID
		 */
		public boolean hasGoods(String goodsid){
			return list.containsKey(CartGoods.key(goodsid));
		}
		
		/**
		 * 获取购物车中指定ID的商品
		 * @param goodsid 商品ID
		 */
		public CartGoods getGoods(String goodsid){
			if(hasGoods(goodsid)){
				return list.get(CartGoods.key(goodsid));
			}
			return null;
		}
		
		/**
		 * 修改购物车中某商品的数量及金额
		 * @param goodsid 要修改数量的商品ID
		 * @param operate 1：增加数量，0：减少数量
		 * @param num 要修改的数量
		 */
		public void update(String goodsid,int operate,int num){
			String key = CartGoods.key(goodsid);
			if(list.containsKey(key)){
				//获取购物车中对应商品
				CartGoods sGoods = list.get(CartGoods.key(goodsid));
				if(sGoods==null){
					return;
				}
				if(operate==1){
					//增加相应商品数量及总金额
					sGoods.setCounter(sGoods.getCounter()+num);
					sGoods.setTotal(DoubleKit.add(sGoods.getTotal(),DoubleKit.mul(sGoods.getSellPrice(),num)));
					//保存以更新该商品
					list.put(CartGoods.key(goodsid), sGoods);
					//修改购物车总量
					this.counter = this.counter + num;
					//修改购物车总金额
					this.total = DoubleKit.add(this.total,DoubleKit.mul(sGoods.getSellPrice(),num));
				}else if(operate==0){
					//减去相应商品数量及总金额
					sGoods.setCounter(sGoods.getCounter()-num);
					sGoods.setTotal(DoubleKit.sub(sGoods.getTotal(),DoubleKit.mul(sGoods.getSellPrice(),num)));
					if(sGoods.getCounter()<=0){
						//商品数量为0，从购物车中移除
						list.remove(CartGoods.key(goodsid));
					}else{
						//保存以更新该商品
						list.put(CartGoods.key(goodsid),sGoods);
					}
					//修改购物车总量
					this.counter = this.counter - num;
					//修改购物车总金额
					this.total = DoubleKit.sub(this.total,DoubleKit.mul(sGoods.getSellPrice(),num));
				}
			}
		}
		
		/**
		 * 删除购物车中某个商品
		 * @param goodsids 要删除的商品ID数组
		 */
		public void delete(String goodsid){
			String key = CartGoods.key(goodsid);
			if(list.containsKey(key)){
				//要删除的商品
				CartGoods sGoods = list.get(key);
				if(sGoods!=null){
					int num = sGoods.getCounter();
					//修改购物车总量
					this.counter = this.counter - num;
					//修改购物车总金额
					this.total = DoubleKit.sub(this.total,DoubleKit.mul(sGoods.getSellPrice(),num));
					//从购物车中移除商品
					list.remove(CartGoods.key(goodsid));
				}
			}
		}
		
		/**
		 * 删除购物车中某些商品
		 * @param goodsids 要删除的商品ID数组
		 */
		public void delete(String[] goodsids){
			for(String goodsid : goodsids){
				delete(goodsid);
			}
		}
		
		/**
		 * 获取商品列表
		 */
		public List<CartGoods> getList(){
			List<CartGoods> goodsList = new ArrayList<CartGoods>();
			for(CartGoods goods :((LinkedHashMap<String, CartGoods>)list).values()){
				goodsList.add(goods);
			}
			//反转
			Collections.reverse(goodsList);
			return goodsList;
		}
		
		public int getStoreid() {
			return storeid;
		}
		
		public int getCounter() {
			return counter;
		}

		public double getTotal() {
			return total;
		}
	}
	
	/**
	 * 购物车中商品信息对象
	 */
	public static class CartGoods implements Serializable{
		private static final long serialVersionUID = 1L;
		private String id;
		private int isSelf;
		private String name;
		private String spec;
		private String thumb;
		private double sellPrice;
		private double marketPrice;
		private int counter = 0;
		private double total = 0;
		
		public CartGoods(String id) {
			this.id = id;
		}
		
		public CartGoods(String id,int isSelf,String name,String spec,String thumb,double sellPrice,double marketPrice) {
			this.id = id;
			this.isSelf = isSelf;
			this.name = name;
			this.spec = spec;
			this.thumb = thumb;
			this.sellPrice = sellPrice;
			this.marketPrice = marketPrice;
			this.counter = 1;
			this.total = sellPrice;
		}
		
		public static String key(String goodsid){
			return "goods"+goodsid;
		}
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public int getIsSelf() {
			return isSelf;
		}

		public void setIsSelf(int isSelf) {
			this.isSelf = isSelf;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSpec() {
			return spec;
		}

		public void setSpec(String spec) {
			this.spec = spec;
		}

		public String getThumb() {
			return thumb;
		}

		public void setThumb(String thumb) {
			this.thumb = thumb;
		}

		public double getSellPrice() {
			return sellPrice;
		}

		public void setSellPrice(double sellPrice) {
			this.sellPrice = sellPrice;
		}

		public double getMarketPrice() {
			return marketPrice;
		}

		public void setMarketPrice(double marketPrice) {
			this.marketPrice = marketPrice;
		}

		public int getCounter() {
			return counter;
		}

		public void setCounter(int counter) {
			this.counter = counter;
		}

		public double getTotal() {
			return total;
		}

		public void setTotal(double total) {
			this.total = total;
		}
	}
}
