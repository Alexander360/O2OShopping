<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.web.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>订单管理</title>
    <jsp:include page="../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">全部订单</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<label class="search-label">订单编号：</label><input type="text" id="sOrderNum" value="" class="form-control search-input width200" maxlength="21">
				<label class="search-label">下单时间：</label><input type="text" id="sBeginDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<label class="search-label">至</label><input type="text" id="sEndDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-offset-2 col-lg-9">
				<label class="search-label">收货地址：</label><input type="text" id="sAddress" value="" class="form-control search-input width200" maxlength="100">
				<label class="search-label">买家电话：</label><input type="text" id="sTel" value="" class="form-control search-input width120" maxlength="13">
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-offset-2 col-lg-9 m-t-10">
				<label class="search-label">订单状态：</label>
				<div class="search-input"><select id="sState" class="selectpicker" data-width="150">
					<option value="0">全部</option>
					<option value="1">待付款</option>
					<option value="2">待发货</option>
					<option value="3">待签收</option>
					<option value="4">待取货</option>
					<option value="5">已签收</option>
					<option value="6">已取货</option>
					<option value="7">被催单</option>
					<option value="8">已取消</option>
					<option value="9">已过期</option>
					<option value="10">已删除</option>
				</select></div>
				<label class="search-label">配送方式：</label>
				<div class="search-input"><select id="sTakeType" class="selectpicker" data-width="150">
					<option value="0">全部</option>
					<option value="1">门店配送</option>
					<option value="2">上门自取</option>
				</select></div>
				<label class="search-label">支付方式：</label>
				<div class="search-input"><select id="sPayType" class="selectpicker" data-width="150">
					<option value="0">全部</option>
					<option value="1">在线支付</option>
					<option value="2">货到付款</option>
				</select></div>
				<button class="btn btn-success search-btn" onclick="query();">确定</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<div id="dataList" class="orderPage"></div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<div class="panel panel-default" id="item{{item.orderNum}}" data-id="{{item.orderNum}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-2"><label>订单号：</label>{{item.orderNum}}</div>
				<div class="col-sm-6 col-md-4 col-lg-2"><label>下单时间：</label>{{item.startDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>
				{{if item.payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}<div class="col-sm-6 col-md-4 col-lg-2"><label>付款时间：</label>{{item.payDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
				{{if item.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}<div class="col-sm-6 col-md-4 col-lg-2"><label>发货时间：</label>{{item.deliverDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
				{{if item.orderState==<%=OrderCode.OrderState.FINISH%>}}<div class="col-sm-6 col-md-4 col-lg-2"><label>完成时间：</label>{{item.signDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
			</div>
		</div>
		<table class="table table-noellipsis table-bordered">
			<thead>
				<tr>
					<th width="26%">订单详情</th>
					<th width="6%">单价（元）</th>
					<th width="5%">数量</th>
					<th width="8%">实收款（元）</th>
					<th width="8%">收货人</th>
					<th width="12%">配送方式及地址</th>
					<th width="6%">订单状态</th>
					<th width="8%">配送人员</th>
					<th width="10%">备注</th>
					<th width="6%">操作</th>
				</tr>
			</thead>
			<tbody>
				{{each item.goodsList as goods indx}}
				<tr>
					<td>
						<table>
						<tr><td><a href="javascript:void(0)" onclick="Kit.photo('${FTP_PATH}{{goods.thumb}}')" class="center-block pull-left thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}{{goods.thumb}}"></a>
						<td class="p-l-5"><span>{{goods.name}}</span></td></tr>
						</table>
					</td>
					<td>{{goods.price}}</td>
					<td>{{goods.count}}</td>
					{{if indx==0}}
					<td rowspan="{{item.goodsList.length}}">
						￥ {{item.amount}}<br/>
						{{if item.payType==<%=OrderCode.OrderPayType.ON_LINE%>}}
							{{if item.payState==<%=OrderCode.OrderPayState.HAD_PAY%>}}
								{{item.payMethod | flagTransform:<%=OrderCode.OrderPayMethod.ALIPAY%>,'支付宝支付',<%=OrderCode.OrderPayMethod.WEIXIN%>,'微信支付'}}
							{{/if}}
						{{else}}
						货到付款
						{{/if}}
					</td>
					<td rowspan="{{item.goodsList.length}}">
						{{item.linkName}} {{item.sex}}<br/>
						{{item.tel}}
					</td>
					<td rowspan="{{item.goodsList.length}}">
						{{item.takeType | flagTransform:<%=OrderCode.OrderTakeType.DELIVER%>,'门店配送',<%=OrderCode.OrderTakeType.TAKE_SELF%>,'上门自取'}}<br/>
						{{item.address}}
					</td>
					{{if item.orderState==<%=OrderCode.OrderState.EXPIRE%>}}
					<td rowspan="{{item.goodsList.length}}">已过期</td>
					{{else if item.orderState==<%=OrderCode.OrderState.CANCEL%>}}
					<td rowspan="{{item.goodsList.length}}">已取消</td>
					{{else if item.orderState==<%=OrderCode.OrderState.FINISH%>}}
					<td rowspan="{{item.goodsList.length}}">交易完成</td>
					{{else if item.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&item.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
					<!-- 状态3：在线支付+送货上门 -->
						{{if item.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
						<td rowspan="{{item.goodsList.length}}">等待付款</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">付款成功</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">已接单</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
						<td rowspan="{{item.goodsList.length}}">打包中</td>
						{{else if item.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
						<td rowspan="{{item.goodsList.length}}">待配送</td>
						{{else if item.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
						<td rowspan="{{item.goodsList.length}}">配送中</td>
						{{/if}}
					{{else if item.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&item.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
					<!-- 状态4：在线支付+门店自取 -->
						{{if item.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
						<td rowspan="{{item.goodsList.length}}">等待付款</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">付款成功</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">已接单</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
						<td rowspan="{{item.goodsList.length}}">打包中</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
						<td rowspan="{{item.goodsList.length}}">等待取货</td>
						{{/if}}
					{{else if item.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&item.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
					<!-- 状态5：货到付款+送货上门 -->
						{{if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">处理中</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">已接单</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
						<td rowspan="{{item.goodsList.length}}">打包中</td>
						{{else if item.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
						<td rowspan="{{item.goodsList.length}}">待配送</td>
						{{else if item.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
						<td rowspan="{{item.goodsList.length}}">配送中</td>
						{{/if}}
					{{else if item.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&item.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
					<!-- 状态5：货到付款+门店自取 -->
						{{if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">处理中</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
						<td rowspan="{{item.goodsList.length}}">已接单</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
						<td rowspan="{{item.goodsList.length}}">打包中</td>
						{{else if item.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
						<td rowspan="{{item.goodsList.length}}">等待取货</td>
						{{/if}}
					{{/if}}
					<td rowspan="{{item.goodsList.length}}">
						{{item.sellerName}}<br/>{{item.sellerTel}}
					</td>
					<td rowspan="{{item.goodsList.length}}">
						{{item.describe}}
					</td>
					<td rowspan="{{item.goodsList.length}}">
						<button type="button" class="btn btn-sm btn-default" onclick="showOrder('{{item.orderNum}}')">查看订单</button><br/>

						{{if item.orderState==<%=OrderCode.OrderState.EXPIRE%>}}
						{{else if item.orderState==<%=OrderCode.OrderState.CANCEL%>}}
						{{else if item.orderState==<%=OrderCode.OrderState.FINISH%>}}
						{{else if item.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&item.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
						<!-- 状态3：在线支付+送货上门 -->
							{{if item.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
							<!--<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doPay('{{item.orderNum}}')">确认支付</button><br/>-->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="toLinkSeller('{{item.orderNum}}')">安排配送</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="startPack('{{item.orderNum}}')">开始打包</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="packFinish('{{item.orderNum}}')">打包完成</button>
							{{else if item.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doSend('{{item.orderNum}}')">立即送货</button>
							{{else if item.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doSign('{{item.orderNum}}')">确认收货</button>
							{{/if}}
						{{else if item.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&item.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
						<!-- 状态4：在线支付+门店自取 -->
							{{if item.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
							<!--<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doPay('{{item.orderNum}}')">确认支付</button><br/>-->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="toLinkSeller('{{item.orderNum}}')">安排配送</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="startPack('{{item.orderNum}}')">开始打包</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="packFinish('{{item.orderNum}}')">打包完成</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doTake('{{item.orderNum}}')">确认取货</button>
							{{/if}}
						{{else if item.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&item.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
						<!-- 状态5：货到付款+送货上门 -->
							{{if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="toLinkSeller('{{item.orderNum}}')">安排配送</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="startPack('{{item.orderNum}}')">开始打包</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="packFinish('{{item.orderNum}}')">打包完成</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doSend('{{item.orderNum}}')">立即送货</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doSign('{{item.orderNum}}')">确认收货</button>
							{{/if}}
						{{else if item.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&item.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
						<!-- 状态5：货到付款+门店自取 -->
							{{if item.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="toLinkSeller('{{item.orderNum}}');">安排配送</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="startPack('{{item.orderNum}}')">开始打包</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACKING%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="packFinish('{{item.orderNum}}')">打包完成</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{else if item.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="doTake('{{item.orderNum}}')">确认取货</button>
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="doCancel('{{item.orderNum}}')">取消订单</button>
							{{/if}}
						{{/if}}
					</td>
					{{/if}}
				</tr>
   				{{/each}}
			</tbody>
		</table>
		{{if item.orderState==<%=OrderCode.OrderState.NORMAL%>&&item.urgeTimes!=0}}
		<div class="panel-footer">
			<span class="text-danger pull-right">催单{{item.urgeTimes}}次，最后一次催单时间：{{item.urgeLastTime | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</span>
			<div class="clearfix"></div>
		</div>
		{{/if}}
	</div>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(query);
function query(){
	var params = {
		pageSize:10,
		orderNum:$("#sOrderNum").val(),
		beginDate:$("#sBeginDate").val(),
		endDate:$("#sEndDate").val(),
		address:$("#sAddress").val(),
		tel:$("#sTel").val(),
		state:$("#sState").val(),
		takeType:$("#sTakeType").val(),
		payType:$("#sPayType").val()
	};
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/order/page",params,function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var showOrderDialog;
function showOrder(orderNum){
	showOrderDialog = Kit.dialog("查看订单","${BASE_PATH}/order/detail?orderNum="+orderNum,{size:'size-wide',closable:true}).open();
}
function doPay(orderNum){
	Kit.confirm("提示","确定用户已付款吗？",function(ref){
		$.post("${BASE_PATH}/order/doPay",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
function doCancel(orderNum){
	Kit.confirm("提示","确定要取消订单吗？",function(ref){
		$.post("${BASE_PATH}/order/doCancel",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
var setSellerDialog;
function toLinkSeller(orderNum){
	setSellerDialog = Kit.dialog("配送","${BASE_PATH}/order/toSetSeller?orderNum="+orderNum,{closable:true}).open();
}
function onSellerChoosed(orderNum,id,name){
	Kit.confirm("提示","确定安排【"+name+"】吗？",function(ref){
		$.post("${BASE_PATH}/order/doSetSeller",{'orderNum':orderNum,'sellerid':id},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
function startPack(orderNum){
	$.post("${BASE_PATH}/order/doStartPack",{'orderNum':orderNum},function(result){
		if(result.flag==0){
			$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
		}else{
			Kit.alert("操作失败");return;
		}
		
	});
}
function packFinish(orderNum){
	$.post("${BASE_PATH}/order/doPackFinish",{'orderNum':orderNum},function(result){
		if(result.flag==0){
			$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
		}else{
			Kit.alert("操作失败");return;
		}
		
	});
}
function doSend(orderNum){
	Kit.confirm("提示","确定要发货吗？",function(ref){
		$.post("${BASE_PATH}/order/doSend",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
function doSign(orderNum,payType){
	Kit.confirm("提示",(payType==1?"确定已签收吗？":"确定已收款并签收吗？"),function(ref){
		$.post("${BASE_PATH}/order/doSign",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
function doTake(orderNum,payType){
	Kit.confirm("提示",(payType==1?"确定已取货吗？":"确定已收款并取货吗？"),function(ref){
		$.post("${BASE_PATH}/order/doSign",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
</script>
</body>
</html>