//
//  ReSetNicknameController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "ReSetNicknameController.h"
#import "UIColor+Hex.h"
#import "UITextField+Space.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
#import "MTA.h"
@interface ReSetNicknameController ()
@property (strong, nonatomic) NSString *myTextValue;
@property (nonatomic, strong) UITextField *textField;
@end

@implementation ReSetNicknameController
+ (instancetype)settingTextVCWithTitle:(NSString *)title textValue:(NSString *)textValue doneBlock:(void(^)(NSString *textValue))block{
    ReSetNicknameController *vc = [[ReSetNicknameController alloc] init];
    vc.title = title;
    vc.textValue = textValue? textValue : @"";
    vc.doneBlock = block;
    return vc;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnClicked:)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 40, CGRectGetWidth(self.view.bounds), 44)];
    _textField.text = self.textValue;
    _textField.backgroundColor = [UIColor whiteColor];
    _textField.font = [UIFont systemFontOfSize:16];
    _textField.textColor = [UIColor blackColor];
    _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_textField setDirection:kLeftEdge edgeSpace:15];
    [_textField becomeFirstResponder];
    _textField.placeholder = @"未填写";
    [_textField addTarget:self action:@selector(textValueChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:_textField];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"修改昵称"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"修改昵称"];
}
#pragma mark doneBtn
- (void)textValueChange:(UITextField *)sender{
    self.myTextValue = sender.text;
    if (![self.myTextValue isEqualToString:self.textValue]) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
}

- (void)doneBtnClicked:(id)sender{
    if (self.doneBlock) {
        self.doneBlock(_myTextValue);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
