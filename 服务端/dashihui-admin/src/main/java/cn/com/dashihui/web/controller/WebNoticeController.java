package cn.com.dashihui.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.upload.UploadFile;

import cn.com.dashihui.kit.DirKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.WebNotice;
import cn.com.dashihui.web.service.WebNoticeService;

@RequiresAuthentication
public class WebNoticeController extends BaseController{
	private static final Logger logger = Logger.getLogger(WebNoticeController.class);
	private WebNoticeService service = new WebNoticeService();

	@RequiresPermissions("web:notice:index")
    public void index(){
        render("index.jsp");
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize));
	}

	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：标题为空，2：标题过长，3：内容为空，4：图片上传失败
	 */
	public void doAdd(){
		//缩略图
		UploadFile thumb = getFile("thumb");
		//类型
		int type = getParaToInt("type",0);
		//标题
		String title = getPara("title");
		//内容
		String context = getPara("context");
		if(StrKit.isBlank(title)){
			renderResult(1);
			return;
		}else if(title.length()>100){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(context)){
			renderResult(3);
			return;
		}else{
			//保存
			WebNotice notice = new WebNotice()
				.set("type", type)
				.set("title", title)
				.set("context", context);
			//如果上传了图片，则上传至FTP，并记录图片文件名
			if(thumb!=null){
				String thumbFileName;
				String dir = DirKit.getDir(DirKit.NOTICE);
				try {
					thumbFileName = uploadToFtp(dir,thumb);
				} catch (IOException e) {
					e.printStackTrace();
					renderResult(4);
					return;
				}
				notice.set("thumb", dir.concat(thumbFileName));
			}
			if(service.addNotice(notice)){
				renderSuccess(service.findById(notice.getInt("id")));
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：标题为空，2：标题过长，3：内容为空，4：图片上传失败
	 */
	public void doEdit(){
		//缩略图
		UploadFile thumb = getFile("thumb");
		//ID
		String noticeid = getPara("noticeid");
		//类型
		int type = getParaToInt("type",0);
		//标题
		String title = getPara("title");
		//内容
		String context = getPara("context");
		//旧图片
		String thumbOld = getPara("thumbOld");
		if(StrKit.isBlank(noticeid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(title)){
			renderResult(1);
			return;
		}else if(title.length()>100){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(context)){
			renderResult(3);
			return;
		}else{
			//更新
			WebNotice notice = new WebNotice()
				.set("id", noticeid)
				.set("type", type)
				.set("title", title)
				.set("context", context);
			if(thumb!=null){
				//如果修改了图片，则上传新图片，删除旧图片
				String thumbFileName;
				String dir = DirKit.getDir(DirKit.NOTICE);
				try {
					thumbFileName = uploadToFtp(dir,thumb);
				} catch (IOException e) {
					e.printStackTrace();
					renderResult(4);
					return;
				}
				notice.set("thumb", dir.concat(thumbFileName));
				if(service.editNotice(notice)){
					try {
						deleteFromFtp(thumbOld);
					} catch (IOException e) {
						logger.error("更新时，旧图片"+thumbOld+"删除失败！");
						e.printStackTrace();
					}
					renderSuccess(service.findById(Integer.valueOf(noticeid)));
					return;
				}
			}else if(service.editNotice(notice)){
				renderSuccess(service.findById(Integer.valueOf(noticeid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0){
			WebNotice notice = service.findById(id);
			//先判断如果上传了图片，则删除图片
			if(!StrKit.isBlank(notice.getStr("thumb"))){
				try {
					deleteFromFtp(notice.getStr("thumb"));
				} catch (IOException e) {
					logger.error("删除时，相关图片"+notice.getStr("thumb")+"删除失败！");
					e.printStackTrace();
				}
			}
			//然后再对记录进行删除
			notice.delete();
			logger.info("删除公告【"+id+"】，及其图片");
			renderSuccess();
			return;
		}
		renderFailed();
	}
	
	/**
	 * 文本编辑中图片上传
	 */
	public void uploadimg(){
		UploadFile thumb = getFile();
		Map<String,Object> map = new HashMap<String,Object>();  
		if(thumb!=null){
			String thumbFileName;
			String dir = DirKit.getDir(DirKit.NOTICE);
			try {
				thumbFileName = uploadToFtp(dir,thumb);
				map.put("error", 0);  
				map.put("url",PropKit.get("constants.ftppath").concat(dir).concat(thumbFileName));  
				renderJson(map);
		        return;
			} catch (IOException e) {
				e.printStackTrace();
				logger.debug("上传图片失败");
				map.put("error", 1);  
				map.put("message","图片上传失败");  
				renderJson(map);
			}
		}else{
			map.put("error", 1);  
			map.put("message","图片为空");  
			renderJson(map);
		}
	}
}
