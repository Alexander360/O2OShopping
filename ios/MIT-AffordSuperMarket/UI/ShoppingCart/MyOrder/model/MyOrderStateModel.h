//
//  MyOrderStateModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderStateModel : NSObject
//主标题
@property (nonatomic,strong)NSString *Title;
//副标题
@property (nonatomic,strong)NSString *Subtitle;
//时间
@property (nonatomic,strong)NSString *time;

-(instancetype)initWithDict:(NSDictionary *)dict;
+(instancetype)orderStateModelWithDict:(NSDictionary *)dict;

@end
