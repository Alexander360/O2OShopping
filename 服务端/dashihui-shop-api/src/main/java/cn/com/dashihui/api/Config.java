package cn.com.dashihui.api;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

import cn.com.dashihui.api.controller.SerCommonController;
import cn.com.dashihui.api.controller.SerOrderController;
import cn.com.dashihui.api.controller.SerShopController;
import cn.com.dashihui.api.dao.SerFeedback;
import cn.com.dashihui.api.dao.SerOrder;
import cn.com.dashihui.api.dao.SerShop;
import cn.com.dashihui.api.dao.SerShopPush;
import cn.com.dashihui.api.dao.SerVersion;

public class Config extends JFinalConfig {
	
	public void configConstant(Constants me) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadPropertyFile("config.properties");
		//设置视图根目录
		me.setBaseViewPath("/WEB-INF/page");
		//设置字符集
		me.setEncoding("UTF-8");
		me.setViewType(ViewType.JSP);
		//调试模式，会打印详细日志
		me.setDevMode(getPropertyToBoolean("constants.devMode", false));
		//上传文件配置
		//注：此处的目录是上级目录，真正在Controller中获取上传的文件时，需要指定（也可以不指定）要将文件移到什么目录下（）相对于此处指定的目录
		//比如在此设置目录为<E:/>，而在Controller中<getFile("fileName","upload")>，则会将文件移至<E:/upload>目录中，如果不指定则移至<E:/>目录中
		//OreillyCos.init(PathKit.getWebRootPath()+File.separator+"upload", 10*1024*1024, "UTF-8");
	}
	
	public void configRoute(Routes me) {
		me.add("/shop",SerShopController.class);
		me.add("/order", SerOrderController.class);
		me.add("/common", SerCommonController.class);
	}
	
	public void configPlugin(Plugins me) {
		//缓存
		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("db.jdbcUrl"), getProperty("db.username"), getProperty("db.password"), getProperty("db.jdbcDriver"));
		c3p0Plugin.setMaxPoolSize(getPropertyToInt("db.maxPoolSize"));
		c3p0Plugin.setMinPoolSize(getPropertyToInt("db.minPoolSize"));
		c3p0Plugin.setInitialPoolSize(getPropertyToInt("db.initialPoolSize"));
		c3p0Plugin.setMaxIdleTime(getPropertyToInt("db.maxIdleTime"));
		c3p0Plugin.setAcquireIncrement(getPropertyToInt("db.acquireIncrement"));
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new MysqlDialect());
		//字段名大写
		arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.setShowSql(true);
		me.add(arp);
		//添加model映射
		arp.addMapping("t_bus_ser_shop", SerShop.class);
		arp.addMapping("t_bus_ser_order", SerOrder.class);
		arp.addMapping("t_sys_version_ser", SerVersion.class);
		arp.addMapping("t_sys_feedback_ser", SerFeedback.class);
		arp.addMapping("t_bus_ser_shop_push", SerShopPush.class);
	}
	
	public void configInterceptor(Interceptors me) {
	}
	
	public void configHandler(Handlers me) {
		//可在此设置context_path，解决http://ip:port/context_path的问题
		//因为测试时是在jetty下，所以默认没有context_path，如果部署在tomcat下，会自动加上项目名，所以会用到该配置
		//可自定义context_path，默认下是CONTEXT_PATH，使用如：${CONTEXT_PATH}
		me.add(new ContextPathHandler("BASE_PATH"));
	}
}
