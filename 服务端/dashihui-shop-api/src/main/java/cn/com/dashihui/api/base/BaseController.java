package cn.com.dashihui.api.base;

import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

import cn.com.dashihui.api.common.Result;
import cn.com.dashihui.api.common.ResultState;

public class BaseController extends Controller{
	protected Logger logger = Logger.getLogger(this.getClass());
	
	
	/**
	 * 返回json格式内容
	 */
	public void renderAPIResult(Result result){
		String json = result.toString();
		logger.info("响应："+json);
		this.renderText(json);
	}
	
	public void renderResult(String state){
		this.renderAPIResult(new Result(state));
	}
	
	public void renderResult(String state,String message){
		this.renderAPIResult(new Result(state,message));
	}
	
	public void renderResult(String state,Object object){
		this.renderAPIResult(new Result(state,object));
	}
	
	public void renderSuccess(){
		renderResult(ResultState.STATE_SUCCESS);
	}
	
	public void renderSuccess(Object object){
		renderResult(ResultState.STATE_SUCCESS,object);
	}
	
	public void renderFailed(){
		renderResult(ResultState.STATE_FAILED);
	}
	
	public void renderFailed(String message){
		renderResult(ResultState.STATE_FAILED,message);
	}
	
	/**
	 * 日志
	 */
	public void error(String message,int userid,String clientid){
		logger.error(message.concat("\n用户【"+userid+"】，设备【"+clientid+"】"));
	}
	
	public void error(String message){
		logger.error(message);
	}
	
	public void debug(String message,int userid,String clientid){
		logger.debug(message.concat("\n用户【"+userid+"】，设备【"+clientid+"】"));
	}
	
	public void debug(String message){
		logger.debug(message);
	}
}
