//
//  MyGoodSOrderRefundModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodSOrderRefundModel.h"

@implementation MyGoodSOrderRefundModel
+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{
             @"orderNum" : @"ORDERNUM",
             @"orderMount" : @"ORDERAMOUNT",
             @"amount" : @"AMOUNT",
             @"orderListID" : @"ORDERLISTID",
             @"refundNum" : @"REFUNDNUM",
             @"createDate" : @"CREATEDATE",
             @"state" : @"STATE",
             @"refuseReason" : @"REFUSEREASON",
             @"upDateDate" : @"UPDATEDATE",
             @"ID" : @"ID",
             @"type" : @"TYPE",
             @"goodsList" : @"GOODSLIST",
             @"stateMent" : @"STATEMENT",
             };
}
@end
