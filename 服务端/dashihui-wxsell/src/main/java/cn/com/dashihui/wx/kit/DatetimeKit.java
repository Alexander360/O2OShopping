package cn.com.dashihui.wx.kit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.jfinal.kit.StrKit;

public class DatetimeKit {
	/**
	 * 日期格式化
	 */
	public static String getFormatDate(String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	/**
	 * 日期格式化
	 */
	public static String getFormatDate(Date date,String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	/**
	* 字符串转换成日期
	* @param str 格式为yyyy-MM-dd HH:mm:ss   
	* @return date
	*/
	public static Date StrToDate(String str) {
	  
	   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	   return date;
	}
	
	/**
	 * 提前或者推迟后的日期(只能是天数)
	 * @param preDay 提前或者推迟的天数  比如-1就是前一天  1是明天
	 * @param format 格式为yyyy-MM-dd HH:mm:ss  
	 * @return
	 */
	public static String getPNtime(int preDay,String format) {
		if(format ==null || format.equals("")){
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();  
        c.add(Calendar.DATE, preDay); 
        Date day = c.getTime();
        return  sdf.format(day);
	} 
	
	/**
	 * 提前或者推迟后的日期在本周的星期数(只能推迟天数)
	 * @param preDay 提前或者推迟的天数  比如-1就是前一天  1是明天
	 * @param String[] weeks 需要返回的的日期格式  比如{"周日","周一","周二","周三","周四","周五","周六"}
	 * 						 注意要从周日的索引为0依次类推
	 */
	public static String getPNDayWeek(int preDay,String[] weeks) {
		Calendar c = Calendar.getInstance();  
		c.add(Calendar.DATE, preDay); 
		return weeks[c.get(Calendar.DAY_OF_WEEK)-1];
	} 
	
	/**
	 * 获得间隔后的格式化后的时间
	 * @param intervalTime 时间间隔 单位秒(比如30分钟 30*60*1000)
	 * @param format 时间格式 （yyyy-MM-dd HH:mm:ss）
	 */
	public static String getIntervalFormatDate(int intervalTime,String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		long currentTime = System.currentTimeMillis() + intervalTime;
		Date date = new Date(currentTime);
		DateFormat df = new SimpleDateFormat(format);
		String nowTime=df.format(date);
		return nowTime;
	}
	/**
	 * 比较与当前时间大小
	 * @param data 
	 * @return 1 传入时间大于当前时间 ，-1 传入时间小于当前时间，0：传入时间等于当前时间
	 */
	public static int compareDate(Date data){
        if (data.getTime() > new Date().getTime()) {
            return 1;
        } else if (data.getTime() < new Date().getTime()) {
            return -1;
        } else {//相等
            return 0;
        }
	}
	
	/**
	 * 获取时间戳
	 * @return
	 */
	public static String getTimestamp() {
		return Long.toString(new Date().getTime() / 1000);
	}
}
