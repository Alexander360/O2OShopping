<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">热搜关键字</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="<c:url value="/dict/keyword/doSort"/>" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                    <th width="55%">关键字</th>
			                    <th width="15%">搜索量</th>
			                    <th width="15%">排序</th>
			                    <th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
			        </form>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.keyword}}</td>
		<td>{{item.amount}}</td>
		<td><input name="orderNo{{item.id}}" value="{{item.orderNo}}" size="3" class="order form-control width50" required></td>
		<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button> <button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button></td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/keyword/page",{"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/keyword/toAdd").open();
}
function onAddSuccess(newObject){
	$("#dataList").prepend(template("dataTpl",newObject));
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/keyword/toEdit/"+id).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/keyword/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>