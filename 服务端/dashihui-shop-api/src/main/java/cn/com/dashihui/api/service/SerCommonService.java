package cn.com.dashihui.api.service;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.api.dao.SerShopPush;
import cn.com.dashihui.api.dao.SerVersion;

public class SerCommonService{
	
	/**
	 * 查询出指定平台的，最新的版本信息
	 */
	public SerVersion findLastVersion(String type){
		return SerVersion.me().findFirst("SELECT * FROM t_sys_version_ser WHERE TYPE=? ORDER BY releaseDate DESC",type);
	}
	
    /**
     * 查询t_bus_ser_shop_push表中此终端注册的记录
     */
	public SerShopPush findShopPush(int shopid,String clientid,String  deviceid){
		return SerShopPush.me().findFirst("SELECT * FROM t_bus_ser_shop_push WHERE shopid=? AND clientid=? AND deviceid=? ",shopid,clientid, deviceid);
	}
	
	/**
     * 注册t_bus_ser_shop_push表中此终端注册的记录
     */
	public boolean addShopPush(SerShopPush shopPush){
		return shopPush.save();
	}
	
	/**
     * 删除t_bus_ser_shop_push表中此终端注册的记录
     */
	public boolean delShopPush(int shopid,String clientid,String  deviceid){
		String sql = "delete from  t_bus_ser_shop_push WHERE shopid=? AND clientid=? AND deviceid=? ";
		
		return Db.update(sql,shopid,clientid,deviceid)==1;
	}
	
	
}
