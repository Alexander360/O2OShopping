//
//  Strore_Root_Top_ItemView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/25.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ItemViewBtnName) {
    kStroreGoodSGroup = 100,//分类
    kStroreSalesVolume, //销量
    kStroreProprietaryBtn ,//自营
    kStrorePriceBtn  //价格
};

@protocol Strore_Root_Top_ItemViewDelegate <NSObject>

-(void)kStroreGoodSGroupClickWithSelectedState:(BOOL)selectedState;//分类
-(void)kStroreSalesVolumeClick;//销量
-(void)kStroreProprietaryBtnWithSelectedState:(BOOL)selectedState;//自营
-(void)kStrorePriceBtnClickWithSort:(NSString *)sort;// 1是价格从低到高 2是价格从高到低
@end
@interface Strore_Root_Top_ItemView : UIView
@property(nonatomic)ItemViewBtnName ItemViewBtnName;
@property(nonatomic,assign)id<Strore_Root_Top_ItemViewDelegate>delegate;
@end
