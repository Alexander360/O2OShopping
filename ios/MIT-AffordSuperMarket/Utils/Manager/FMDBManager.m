//
//  FMDBManager.m
//  sqlite
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "FMDBManager.h"
#import "FMDatabase.h"
#import "Global.h"
#import"FMDatabaseAdditions.h"
@interface FMDBManager()
@property (nonatomic, strong) FMDatabase *db;
@end
@implementation FMDBManager

+ (instancetype)sharedManager
{
    
    static FMDBManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[FMDBManager alloc] init];
    });
    return manager;
}
-(void)initWithCreateSqlite
{
    NSString *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    // 拼接文件名
    NSString *filePath = [cachePath stringByAppendingPathComponent:@"dashihui.sqlite"];
    

    //获取系统版本号
    
    // 创建一个数据库的实例,仅仅在创建一个实例，并会打开数据库
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    _db = db;
    
   

    
}

#pragma mark ---history表
-(void)initWithHistoryCreateTable
{
//    NSFileManager * fileManager = [NSFileManager defaultManager];
//    if ([fileManager fileExistsAtPath:self.dbPath]==NO) {
    // 打开数据库
      [_db open];
        if ([_db open]) {
            NSString *sql=@"create table if not exists t_history(id integer primary key autoincrement,shopID text,goodID text,picture blob, name text, price text,oldprice text, serviceshopID ,serciceshopdetail,sercvicenum,isProprietary);";
            
            BOOL res=[_db executeUpdate:sql];
            if (!res) {
                NSLog(@"error when creating db table");
            }else{
            
                NSLog(@"succ to creating db table");
                if ([self.db columnExists:@"isProprietary" inTableWithName:@"t_history"]) {
                    NSLog(@"已存在自营字段");
                }else{
                    NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ text",@"t_history",@"isProprietary"];
                    BOOL res=[_db executeUpdate:sql];
                    if (!res) {
                        NSLog(@"插入新列自营列失败");
                    }else{
                        NSLog(@"插入新列自营列成功");
                    }
                }
            }
            [_db close];
        }else{
            NSLog(@"error when open db");
        }
//    }
    
}
//插入
-(void)HistoryInsertWithshopID:(NSString *)shopID goodID:(NSString *)goodID Picture:(NSData *)picture name:(NSString *)name price:(NSString *)price oldprice:(NSString *)oldprice serviceshopID:(NSString *)serviceshopID serciceshopdetail:(NSString *)serciceshopdetail sercvicenum:(NSString *)sercvicenum isProprietary:(NSString *)isProprietary
{
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
         NSString * sql = @"insert into t_history(shopID,goodID,picture,name,price,oldprice,serviceshopID,serciceshopdetail,sercvicenum,isProprietary) values(?,?,?,?,?,?,?,?,?,?);";
        BOOL res = [_db executeUpdate:sql,shopID,goodID,picture,name,price,oldprice,serviceshopID,serciceshopdetail,sercvicenum,isProprietary];
        if (!res) {
            NSLog(@"error to insert data");
        } else {
            NSLog(@"succ to insert data");
        }
        [_db close];
    }

}
//查询
-(id)HistoryQueryDataWithShopID:(NSString *)ShopID
{
    NSMutableArray* HistoryAry=[[NSMutableArray alloc]init];
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"select * from t_history where shopID ='%@';",ShopID];
        FMResultSet * rs = [_db executeQuery:sql];
        while ([rs next]) {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
            [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
            [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
            [dic setObject:[rs dataForColumn:@"picture"] forKey:@"picture"];
            [dic setObject:[rs stringForColumn:@"name"] forKey:@"name"];
            [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
            [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
            [dic setObject:[rs stringForColumn:@"serviceshopID"] forKey:@"serviceshopID"];
            [dic setObject:[rs stringForColumn:@"serciceshopdetail"] forKey:@"serciceshopdetail"];
            [dic setObject:[rs stringForColumn:@"sercvicenum"] forKey:@"sercvicenum"];
            if([rs stringForColumn:@"isProprietary"].length!=0){
            [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                
            }
//
            [HistoryAry addObject:dic];
     
        }
        [_db close];
    }
    

    HistoryAry = (NSMutableArray *)[[HistoryAry reverseObjectEnumerator] allObjects];

    return HistoryAry;
}
//查询单个数据
-(id)HistoryQueryDataWithShopID:(NSString *)ShopID goodID:(NSString *)goodID serviceshopID:(NSString *)serviceshopID
{
   NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        if (goodID.length!=0) {
            NSString * sql = [NSString stringWithFormat:@"select * from t_history where (shopID='%@'AND goodID ='%@');",ShopID,goodID];
            FMResultSet * rs = [_db executeQuery:sql];
            while ([rs next]) {
                [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
                [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
                [dic setObject:[rs dataForColumn:@"picture"] forKey:@"picture"];
                [dic setObject:[rs stringForColumn:@"name"] forKey:@"name"];
                [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
                [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
                [dic setObject:[rs stringForColumn:@"serviceshopID"] forKey:@"serviceshopID"];
                [dic setObject:[rs stringForColumn:@"serciceshopdetail"] forKey:@"serciceshopdetail"];
                [dic setObject:[rs stringForColumn:@"sercvicenum"] forKey:@"sercvicenum"];
                if([rs stringForColumn:@"isProprietary"].length!=0){
                    [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                    
                }
            }
        }else if(serviceshopID.length !=0)
        {
            NSString * sql = [NSString stringWithFormat:@"select * from t_history where (shopID='%@'AND serviceshopID ='%@');",ShopID,serviceshopID];
            FMResultSet * rs = [_db executeQuery:sql];
            while ([rs next]) {
                [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
                [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
                [dic setObject:[rs dataForColumn:@"picture"] forKey:@"picture"];
                [dic setObject:[rs stringForColumn:@"name"] forKey:@"name"];
                [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
                [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
                [dic setObject:[rs stringForColumn:@"serviceshopID"] forKey:@"serviceshopID"];
                [dic setObject:[rs stringForColumn:@"serciceshopdetail"] forKey:@"serciceshopdetail"];
                [dic setObject:[rs stringForColumn:@"sercvicenum"] forKey:@"sercvicenum"];
            }
        }
               [_db close];
    }
    
    return dic;
}
//更新数据
-(void)HistorymodifyDataWithShopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name price:(NSString *)price oldprice:(NSString *)oldprice serviceshopID:(NSString *)serviceshopID serciceshopdetail:(NSString *)serciceshopdetail sercvicenum:(NSString *)sercvicenum isProprietary:(NSString *)isProprietary
{
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        if (serviceshopID.length!=0) {
            NSString *updateSql = [NSString stringWithFormat:
                                   @"UPDATE t_history SET name ='%@',price='%@',oldprice='%@',serviceshopID='%@' ,serciceshopdetail='%@' ,sercvicenum='%@' WHERE (shopID = '%@'AND serviceshopID ='%@');",
                                   name,price,oldprice,goodID,serciceshopdetail,sercvicenum,shopID,serviceshopID];
            BOOL res = [_db executeUpdate:updateSql];
            if (!res) {
                NSLog(@"error when update db table");
            } else {
                NSLog(@"success to update db table");
            }

        }else if (goodID.length !=0)
        {
            NSString *updateSql = [NSString stringWithFormat:
                                   @"UPDATE t_history SET name ='%@',price='%@',oldprice='%@',serviceshopID='%@' ,serciceshopdetail='%@' ,sercvicenum='%@',isProprietary='%@' WHERE (shopID = '%@'AND goodID ='%@');",
                                  name,price,oldprice,serviceshopID,serciceshopdetail,sercvicenum,isProprietary,shopID,goodID];
            BOOL res = [_db executeUpdate:updateSql];
            if (!res) {
                NSLog(@"error when update db table");
            } else {
                NSLog(@"success to update db table");
            }

        }
                [_db close];
        
    }
}
#pragma mark--shoppingCart
-(void)initWithShoppingCartTable
{
    // 打开数据库
    [_db open];
        if ([_db open]) {
            NSString *sql=@"create table if not exists t_shopCar(id integer primary key autoincrement,shopID, goodID,name,picture, type,price,oldprice,buynum, ischoose,isProprietary);";
            BOOL res=[_db executeUpdate:sql];
            if (!res) {
                NSLog(@"创建shopcar表失败");
            }else{
                NSLog(@"创建shopcar表成功");

               if ([self.db columnExists:@"isProprietary" inTableWithName:@"t_shopCar"]) {
                    NSLog(@"已存在自营字段");
                }else{
                    NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ text",@"t_shopCar",@"isProprietary"];
                    BOOL res=[_db executeUpdate:sql];
                    if (!res) {
                        NSLog(@"插入新列自营列失败");
                    }else{
                        NSLog(@"插入新列自营列成功");
                    }
                }
                        
                    }
            [_db close];
        }else{
            NSLog(@"error when open db");
        }
//    }
}
//增
-(void)InsertWithshopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name Picture:(NSData *)picture type:(NSString *)type price:(NSString *)price oldprice:(NSString *)oldprice buynum:(NSString *)buynum ischoose:(NSString *)ischoose isProprietary:(NSString *)isProprietary
{
    [_db open];
    if ([_db open]) {
    
      
        
            NSString * sql = @"insert into t_shopCar( shopID,goodID,name,picture,type,price,oldprice,buynum,ischoose,isProprietary) values(?,?,?,?,?,?,?,?,?,?);";
            BOOL res = [_db executeUpdate:sql,shopID,goodID,name,picture,type,price,oldprice,buynum,ischoose,isProprietary];
            if (!res) {
                NSLog(@"shopcar表添加数据失败");
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
                NSLog(@"shopcar表添加数据成功");
            }
            [_db close];
    
    }
}
//查所有数据 包括（自营）
-(NSMutableArray *)QueryDataWithshopID:(NSString *)shopID
{
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
            NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where shopID = '%@' OR isProprietary = '1';",shopID];
            FMResultSet * rs = [_db executeQuery:sql];
            
            while ([rs next]) {
                NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
                //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
                [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
                [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
                [dic setObject:[rs stringForColumn:@"name"]   forKey:@"name"];
                [dic setObject:[rs dataForColumn: @"picture"] forKey:@"picture"];
                [dic setObject:[rs stringForColumn:@"type"] forKey:@"type"];
                [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
                [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
                [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"buynum"];
                [dic setObject:[rs stringForColumn:@"ischoose"] forKey:@"ischoose"];
                if([rs stringForColumn:@"isProprietary"].length!=0){
                    [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                    
                }
                [goods addObject:dic];
            }

       
        }
          [_db close];
    goods = (NSMutableArray *)[[goods reverseObjectEnumerator] allObjects];
    
    return goods;
}
// 查询非自营商品
-(NSMutableArray *)QueryIsNoProprietaryDataWithShopID:(NSString *)shopID
{
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where shopID = '%@';",shopID];
        FMResultSet * rs = [_db executeQuery:sql];
        
        while ([rs next]) {
            NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
            //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
            [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
            [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
            [dic setObject:[rs stringForColumn:@"name"]   forKey:@"name"];
            [dic setObject:[rs dataForColumn: @"picture"] forKey:@"picture"];
            [dic setObject:[rs stringForColumn:@"type"] forKey:@"type"];
            [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
            [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
            [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"buynum"];
            [dic setObject:[rs stringForColumn:@"ischoose"] forKey:@"ischoose"];
            if([rs stringForColumn:@"isProprietary"].length!=0){
                [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                
            }
            [goods addObject:dic];
        }
        
        
    }
    [_db close];
    goods = (NSMutableArray *)[[goods reverseObjectEnumerator] allObjects];
    
    return goods;
}

// 查询自营商品
-(NSMutableArray *)QueryDataWithIsProprietary:(NSString *)isProprietary
{
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where isProprietary ='%@';",isProprietary];
        FMResultSet * rs = [_db executeQuery:sql];
        
        while ([rs next]) {
            NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
            //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
            [dic setObject:[rs stringForColumn:@"shopID"] forKey:@"shopID"];
            [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"goodID"];
            [dic setObject:[rs stringForColumn:@"name"]   forKey:@"name"];
            [dic setObject:[rs dataForColumn: @"picture"] forKey:@"picture"];
            [dic setObject:[rs stringForColumn:@"type"] forKey:@"type"];
            [dic setObject:[rs stringForColumn:@"price"] forKey:@"price"];
            [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"oldprice"];
            [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"buynum"];
            [dic setObject:[rs stringForColumn:@"ischoose"] forKey:@"ischoose"];
            if([rs stringForColumn:@"isProprietary"].length!=0){
                [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                
            }
            [goods addObject:dic];
        }
        
        
    }
    [_db close];
    goods = (NSMutableArray *)[[goods reverseObjectEnumerator] allObjects];
    
    return goods;
}
//查询所有选中状态的商品
-(NSMutableArray *)QueryIsChoosedDataWithShopID:(NSString *)ShopID 
{
    
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
                   NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where (shopID = '%@' OR isProprietary = '1') AND ischoose = '1';",ShopID];
            FMResultSet * rs = [_db executeQuery:sql];
            while ([rs next]) {
                NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
                //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
                [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"ID"];
                [dic setObject:[rs stringForColumn:@"name"]   forKey:@"NAME"];
                [dic setObject:[rs stringForColumn:@"price"] forKey:@"SELLPRICE"];
                [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"MARKETPRICE"];
                [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"BUYNUM"];
                if([rs stringForColumn:@"isProprietary"].length!=0){
                    [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                    
                }
                [goods addObject:dic];
            }

                     [_db close];
    }
    
    return goods;
}
//查询自营的选中商品
-(NSMutableArray *)QueryisProprietaryIsChoosedData
{
    
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where (isProprietary ='1' AND ischoose ='1');"];
        FMResultSet * rs = [_db executeQuery:sql];
        while ([rs next]) {
            NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
            //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
            [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"ID"];
            [dic setObject:[rs stringForColumn:@"name"]   forKey:@"NAME"];
            [dic setObject:[rs stringForColumn:@"price"] forKey:@"SELLPRICE"];
            [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"MARKETPRICE"];
            [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"BUYNUM"];
            [goods addObject:dic];
        }
        
        [_db close];
    }
    
    return goods;
}
//根据便利店选中商品
-(NSMutableArray *)QueryIsChoosedDataInShopWithShopID:(NSString *)ShopID{
    
    NSMutableArray *goods=[[NSMutableArray alloc]init];
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where (shopID ='%@' AND ischoose ='1');",ShopID];
        FMResultSet * rs = [_db executeQuery:sql];
        while ([rs next]) {
            NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
            //shopID,goodID,name,unit,shortinfo,picture,purchaseamount,type,price,oldprice,buynum,ischoose
            [dic setObject:[rs stringForColumn:@"goodID"] forKey:@"ID"];
            [dic setObject:[rs stringForColumn:@"name"]   forKey:@"NAME"];
            [dic setObject:[rs stringForColumn:@"price"] forKey:@"SELLPRICE"];
            [dic setObject:[rs stringForColumn:@"oldprice"] forKey:@"MARKETPRICE"];
            [dic setObject:[rs stringForColumn:@"buynum"] forKey:@"BUYNUM"];
            [goods addObject:dic];
        }
        
        [_db close];
    }
    
    return goods;
}
//查询单个数据
-(id)QueryDataWithShopID:(NSString *)ShopID goodID:(NSString *)goodID
{
     NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        
            NSString * sql = [NSString stringWithFormat:@"select * from t_shopCar where (shopID='%@'AND goodID ='%@');",ShopID,goodID];
            FMResultSet * rs = [_db executeQuery:sql];
            
            while ([rs next]) {
                
                [dic setObject:[rs stringForColumn:@"shopID"]    forKey:@"shopID"];
                [dic setObject:[rs stringForColumn:@"goodID"]    forKey:@"goodID"];
                [dic setObject:[rs stringForColumn:@"name"]      forKey:@"name"];
                [dic setObject:[rs dataForColumn:@"picture"]     forKey:@"picture"];
                [dic setObject:[rs stringForColumn:@"type"]      forKey:@"type"];
                [dic setObject:[rs stringForColumn:@"price"]     forKey:@"price"];
                [dic setObject:[rs stringForColumn:@"oldprice"]  forKey:@"oldprice"];
                [dic setObject:[rs stringForColumn:@"buynum"]    forKey:@"buynum"];
                [dic setObject:[rs stringForColumn:@"ischoose"]  forKey:@"ischoose"];
                if([rs stringForColumn:@"isProprietary"].length!=0){
                    [dic setObject:[rs stringForColumn:@"isProprietary"] forKey:@"isProprietary"];
                    
                }
            }

        }
        [_db close];
    
    return dic;
}
//更新数据
-(void)modifyShopCarDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID name:(NSString *)name type:(NSString *)type price:(NSString *)price oldprice:(NSString *)oldprice
{
    // 打开数据库
    [_db open];
    if ([_db open]) {
            NSString *updateSql = [NSString stringWithFormat:@"UPDATE t_shopCar SET name ='%@',type='%@',price='%@',oldprice='%@' WHERE (shopID = '%@'AND goodID ='%@');",name,type,price,oldprice,shopID,goodID];
            BOOL res = [_db executeUpdate:updateSql];
        if (!res) {
                NSLog(@"shopcar更新数据失败");
            } else {
                NSLog(@"shopcar更新数据成功");
            }

        }
    [_db close];
    
}
//更新数量
-(void)modifyDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID buynum:(NSString *)buynum;
{
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
            NSString *updateSql = [NSString stringWithFormat:
                                   @"UPDATE t_shopCar SET buynum = '%@'  WHERE (shopID = '%@' AND goodID='%@');",
                                   buynum,shopID,goodID];
            BOOL res = [_db executeUpdate:updateSql];
            if (!res) {
                NSLog(@"shopcar更新数量失败");
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
                NSLog(@"shopcar更新数量成功");
            }

        }
           [_db close];
}
//自营更新数量
-(void)modifyDataWithgoodID:(NSString *)goodID buynum:(NSString *)buynum isProprietary:(NSString *)isProprietary
{
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString *updateSql = [NSString stringWithFormat:
                               @"UPDATE t_shopCar SET buynum = '%@'  WHERE (isProprietary = '%@' AND goodID='%@');",
                               buynum,isProprietary,goodID];
        BOOL res = [_db executeUpdate:updateSql];
        if (!res) {
            NSLog(@"shopcar更新数量失败");
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
            NSLog(@"shopcar更新数量成功");
        }
        
    }
    [_db close];
}
//更新选中状态
-(void)modifyDataWithshopID:(NSString *)shopID goodID:(NSString *)goodID  ischoose:(NSString *)ischoose
{
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
            NSString *updateSql = [NSString stringWithFormat:
                                   @"UPDATE t_shopCar SET ischoose = '%@'  WHERE (shopID = '%@' AND goodID='%@');",
                                   ischoose,shopID,goodID];
            BOOL res = [_db executeUpdate:updateSql];
            if (!res) {
                NSLog(@"shopcar更新选中状态失败");
            } else {
                NSLog(@"shopcar更新选中状态成功");
            }
            [_db close];
        }
}
//自营更新选中状态
-(void)modifyDataWithgoodID:(NSString *)goodID ischoose:(NSString *)ischoose isProprietary:(NSString *)isProprietary
{
    //    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString *updateSql = [NSString stringWithFormat:
                               @"UPDATE t_shopCar SET ischoose = '%@'  WHERE (isProprietary = '%@' AND goodID='%@');",
                               ischoose,isProprietary,goodID];
        BOOL res = [_db executeUpdate:updateSql];
        if (!res) {
            NSLog(@"shopcar更新选中状态失败");
        } else {
            NSLog(@"shopcar更新选中状态成功");
        }
        [_db close];
    }
}

//删除全部
-(void)deleteAll:(NSString *)tableName
{
//    FMDatabase * db = [FMDatabase databaseWithPath:self.dbPath];
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString * sql = [NSString stringWithFormat:@"delete from %@;",tableName];
        BOOL res = [_db executeUpdate:sql];
        if (!res) {
            NSLog(@"error to delete db data");
        } else {
            if ([tableName isEqualToString:@"t_shopCar"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
            }
            NSLog(@"succ to deleta db data");
        }
        [_db close];
    }
}
//单条删除
-(void)deleteOneDataWithShopID:(NSString *)shopID goodID:(NSString *)goodID
{

    // 打开数据库
    [_db open];
    if ([_db open]) {
            NSString *sql =[NSString stringWithFormat:@"DELETE FROM t_shopCar WHERE (shopID = '%@' AND goodID='%@');",shopID,goodID];
            BOOL res = [_db executeUpdate:sql];
            if (!res) {
                NSLog(@"shopcar单条删除数据失败");
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
                NSLog(@"shopcar单条删除数据成功");
            }
        }
        [_db close];

}
//自营单条删除
-(void)deleteOneDataWithgoodID:(NSString *)goodID
{
    
    // 打开数据库
    [_db open];
    if ([_db open]) {
        NSString *sql =[NSString stringWithFormat:@"DELETE FROM t_shopCar WHERE (isProprietary = '1' AND goodID='%@');",goodID];
        BOOL res = [_db executeUpdate:sql];
        if (!res) {
            NSLog(@"shopcar单条删除数据失败");
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:PRODUCTCOUNTCHANGE object:nil];
            NSLog(@"shopcar单条删除数据成功");
        }
    }
    [_db close];
    
}

@end


