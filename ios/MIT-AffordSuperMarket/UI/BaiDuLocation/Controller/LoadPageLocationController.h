//
//  LoadPageLocationController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/27.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： LoadPageLocationController
 Created_Date： 20160127
 Created_People： GT
 Function_description：每次启动应用开启定位（除第一次安装），加载页消失后进入该页面
 ***************************************/
#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>
@interface LoadPageLocationController : UIViewController<BMKLocationServiceDelegate,BMKCloudSearchDelegate>
{
    BMKCloudSearch* _search;
    BMKLocationService* _locService;
}
@end
