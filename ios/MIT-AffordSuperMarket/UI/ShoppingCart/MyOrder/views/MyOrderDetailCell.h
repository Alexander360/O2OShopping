//
//  MyOrderDetailCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MyOrderDetailCell : UITableViewCell

-(void)retSubViewWithData:(NSDictionary *)dictionary requestDataType:(NSString *)requestDataType homeMakeType:(NSString*)homeMakeType;
@end
