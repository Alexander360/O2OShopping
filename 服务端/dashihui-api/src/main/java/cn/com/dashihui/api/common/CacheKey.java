package cn.com.dashihui.api.common;

public class CacheKey {
	public static final String CACHE_REGIST_SMS_VALID_CODE_KEY = "registSMSValidCode";
	//找回密码 发送短信
	public static final String CACHE_RESETPWD_SMS_VALID_CODE_KEY = "resetPwdSMSValidCode";
}
