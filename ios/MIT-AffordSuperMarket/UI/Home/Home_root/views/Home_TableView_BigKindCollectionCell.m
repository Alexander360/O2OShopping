//
//  Home_TableView_BigKindCollectionCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#import "Global.h"
#import "Home_TableView_BigKindCollectionCell.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Conversion.h"
#import "UIColor+Hex.h"
#define ktopSpace 7
#define kbottomSpace 6

@interface Home_TableView_BigKindCollectionCell()
//@property(nonatomic, strong)UIButton *KindButton;
@property(nonatomic,strong)UILabel *bigKindTitle;
@property(nonatomic,strong)UIImageView *bigkindImage;
@end
@implementation Home_TableView_BigKindCollectionCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setupSubView];
        
    }
    
    return self;
}
#pragma makr -- 初始化子视图
- (void)setupSubView {
    _bigkindImage =[[UIImageView alloc]initWithFrame:CGRectMake((self.bounds.size.width - HightScalar(60))/2
                                                                , ktopSpace, HightScalar(60), HightScalar(60))];
    [self.contentView addSubview:_bigkindImage];
    _bigKindTitle =[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bigkindImage.frame)
                                                            , self.bounds.size.width
                                                            , CGRectGetHeight(self.bounds) - ktopSpace - kbottomSpace - CGRectGetHeight(self.bigkindImage.bounds))];
    _bigKindTitle.textColor =[UIColor colorWithHexString:@"#000000"];
    _bigKindTitle.font =[UIFont systemFontOfSize:FontSize(14)];
    _bigKindTitle.textAlignment =NSTextAlignmentCenter;
    _bigKindTitle.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_bigKindTitle];

}

- (void)updateViewWithData:(NSDictionary*)dataDic
{
    _bigKindTitle.text =[dataDic objectForKey:@"title"];
    _bigkindImage.image =[UIImage imageNamed:[dataDic objectForKey:@"imageName"]];
}

- (void)setTitleColor:(NSString*)colorName {
    self.bigKindTitle.textColor = [UIColor colorWithHexString:colorName];
}
//服务页大类展示
- (void)updateServiceViewWithData:(NSDictionary*)dataDic {
    [_bigkindImage setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dataDic objectForKey:@"ICON"]]]]placeholderImage:[UIImage imageNamed:@"place_image"]];
    _bigKindTitle.text = [NSString stringISNull:[dataDic objectForKey:@"NAME"]];
}
@end
