//
//  UserCenter_FeedbackAndSuggestionController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_FeedbackAndSuggestionController
 Created_Date： 20151123
 Created_People： GT
 Function_description： 添加备注或反馈建议
 ***************************************/

#import "BaseViewController.h"

@interface UserCenter_FeedbackAndSuggestionController : BaseViewController
typedef NS_ENUM(NSInteger, FeedbackOrRemarksType) {
    kFeedbackType = 0,//反馈建议
    kRemarksType //备注
};
@property(nonatomic, strong)NSString *hadSavedMarkInfo;//用户已经填写的备注信息
@property(nonatomic)FeedbackOrRemarksType feedbackOrRemarksType;
@end
