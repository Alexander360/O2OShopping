//
//  MyOrderDetailCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyOrderDetailCell.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+Conversion.h"
@interface MyOrderDetailCell()
//商品名称
@property (nonatomic,strong)UILabel *goodName;
//商品数量
@property (nonatomic,strong)UILabel *goodNumber;
//商品价格
@property (nonatomic,strong)UILabel *goodMoney;

@property (nonatomic,strong)UILabel *homeMake;

@end
@implementation MyOrderDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}
#pragma mark---初始化视图
-(void)setUpSuberView
{
    //家政服务 标题名字
    _homeMake =[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, HightScalar(50))];
    _homeMake.textColor=[UIColor colorWithHexString:@"#555555"];
    _homeMake.font=[UIFont systemFontOfSize:FontSize(16)];
    _homeMake.hidden=YES;
    [self.contentView addSubview:self.homeMake];
    
    //商品名字
    _goodName =[[UILabel alloc]initWithFrame:CGRectMake(15, 0, VIEW_WIDTH/2+30, HightScalar(50))];
    _goodName.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodName.font=[UIFont systemFontOfSize:FontSize(16)];
//    _goodName.adjustsFontSizeToFitWidth =YES;
//    _goodMoney.minimumScaleFactor = 0.8;
    [self.contentView addSubview:self.goodName];
    
    
    
    //商品价格
    _goodMoney =[[UILabel alloc]initWithFrame:CGRectMake(VIEW_WIDTH-85, 0, 70,CGRectGetHeight(self.goodName.bounds))];
    _goodMoney.font =[UIFont systemFontOfSize:FontSize(16)];
  
    _goodMoney.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodMoney.textAlignment=NSTextAlignmentRight;
    [self.contentView addSubview:self.goodMoney];
    //商品数量
    _goodNumber =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.goodMoney.frame)-40, CGRectGetMinY(self.goodName.frame), 30,CGRectGetHeight(self.goodName.bounds))];
    _goodNumber.font =[UIFont systemFontOfSize:FontSize(16)];
    _goodNumber.textColor=[UIColor colorWithHexString:@"#555555"];
    _goodNumber.textAlignment=NSTextAlignmentRight;
    [self.contentView addSubview:self.goodNumber];
    }
 //requestDataType  商品订单 是1 服务订单是 0
-(void)retSubViewWithData:(NSDictionary *)dictionary requestDataType:(NSString *)requestDataType homeMakeType:(NSString *)homeMakeType
{
  NSString *name=@"";
    NSString *money=@"";
    if ([requestDataType isEqualToString:@"1"]) {
        name=[NSString stringTransformObject:[dictionary objectForKey:@"NAME"]];
        money=[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"PRICE"]] floatValue]];
        self.goodNumber.hidden=NO;
        CGSize maxSize = CGSizeMake(VIEW_WIDTH, CGRectGetHeight(self.goodName.bounds));
        CGSize curSize = [money boundingRectWithSize:maxSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                          attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:FontSize(16)] }
                                             context:nil].size;
        CGRect frame=CGRectMake(VIEW_WIDTH-curSize.width-15, CGRectGetMinY(self.goodName.frame),curSize.width,CGRectGetHeight(self.goodName.bounds));
        self.goodMoney.frame=frame;
        
    }else if([requestDataType isEqualToString:@"0"])
    {
        name=[NSString stringTransformObject:[dictionary objectForKey:@"SERVICETITLE"]];
        money=[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"AMOUNT"]] floatValue]];
        self.goodNumber.hidden=YES;
        CGSize maxSize = CGSizeMake(VIEW_WIDTH, CGRectGetHeight(self.goodName.bounds));
        CGSize curSize = [money boundingRectWithSize:maxSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                          attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:FontSize(16)] }
                                             context:nil].size;
        CGRect frame=CGRectMake(VIEW_WIDTH-curSize.width-15, CGRectGetMinY(self.goodName.frame),curSize.width,CGRectGetHeight(self.goodName.bounds));
        self.goodMoney.frame=frame;
    }
    if (homeMakeType.length !=0) {
        _homeMake.hidden=NO;
        if ([homeMakeType isEqualToString:@"1"]) {
            _homeMake.text=@"日常保洁";
        }else{
            _homeMake.text=@"深度保洁";
        }
        self.goodName.frame= CGRectMake(CGRectGetMaxX(self.homeMake.frame)+20, CGRectGetMinY(self.homeMake.frame), VIEW_WIDTH/2+50, 20);
        name =[NSString stringTransformObject:[dictionary objectForKey:@"TITLE"]];
        money=[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"AMOUNT"]] floatValue]];
        CGSize maxSize = CGSizeMake(VIEW_WIDTH, CGRectGetHeight(self.goodName.bounds));
        CGSize curSize = [money boundingRectWithSize:maxSize
                                              options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                           attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:FontSize(16)] }
                                              context:nil].size;
        CGRect frame=CGRectMake(VIEW_WIDTH-curSize.width-15, CGRectGetMinY(self.goodName.frame),curSize.width,CGRectGetHeight(self.goodName.bounds));
        self.goodMoney.frame=frame;
    }
    
    self.goodName.text=name;
    self.goodNumber.text=[NSString stringWithFormat:@"X%@",[NSString stringTransformObject:[dictionary objectForKey:@"COUNT"]]];
    self.goodMoney.text=money;
    
}


@end
