//
//  ReceivedAddressOBJECT.h
//
//  Created by apple  on 16/1/25
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ReceivedAddressOBJECT : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *lINKNAME;
@property (nonatomic, assign) double sTOREID;
@property (nonatomic, strong) NSString *aDDRESS;
@property (nonatomic, strong) NSString *tEL;
@property (nonatomic, assign) double sEX;
@property (nonatomic, assign) double iSDEFAULT;
@property (nonatomic, strong) NSString *sTORETITLE;
@property (nonatomic, assign) double iDProperty;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
