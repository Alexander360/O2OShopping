package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.WebJoin;

public class WebJoinService {
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_web_join wj");
		sqlExcept.append(" ORDER BY wj.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT wj.* ", sqlExcept.toString());
	}
	
	public boolean delJoin(int id){
		return WebJoin.me().deleteById(id);
	}
}
