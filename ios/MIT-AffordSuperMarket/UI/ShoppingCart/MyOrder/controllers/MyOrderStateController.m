//
//  MyOrderStateController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyOrderStateController.h"
//view
#import "MyOrderStateCell.h"
//model
#import "MyOrderStateModel.h"
//untils
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "UserInfoModel.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
//controller
#import "WeiXinPayController.h"
#import "MTA.h"
@interface MyOrderStateController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString *_totalPrice;
    NSString *_orderNum;//单号
    NSString *_orderDate;//下单日期
    NSString *_cuiDanCount;//催单次数
    NSString *_otherCuiDanDate;//其他催单日期
}

@property (nonatomic,strong)UITableView *orderStateTableView;
@property (nonatomic, strong)NSMutableArray *dataSource;//数据源
@property(nonatomic, strong)NSMutableArray *buttonArr;//存放buton数组

@end

@implementation MyOrderStateController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource = [[NSMutableArray alloc]init];
    _buttonArr = [[NSMutableArray alloc]init];
    [self setUpSuberView];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        [self initWithorderStatrArray];
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//    [appDelegate.tabBarController setTabBarHidden:NO animated:NO];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"订单状态"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"订单状态"];
}
#pragma mark -- request Data
//请求签收
- (void)loadOrderQianShouData {
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/receive":@"service/order/receive";
    if (self.homeMakeType.length !=0) {
        requertKey =@"ser/order/sign";
    }
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf initWithorderStatrArray];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
        
    }];
}
//请求催单数据
- (void)loadCuiOrderDataWithOrderNumber:(NSString*)orderNum {
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/urge":@"service/order/urge";
    if (self.homeMakeType.length !=0) {
        requertKey =@"ser/order/urge";
    }
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:mode.token forKey:@"TOKEN"];
    [parameter setObject:orderNum forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                msg = @"催单成功";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
                _otherCuiDanDate = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"DATETIME"]];
                _cuiDanCount = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"TIMES"]];
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {
        
        
    }];
}
//取消订单
- (void)loadCancelOrderData {
     NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/cancel":@"service/order/cancel";
    
    if (self.homeMakeType.length !=0) {
        requertKey =@"ser/order/cancel";
    }
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
         NSString *msg = [dataDic objectForKey:@"MSG"];
        if ([state isEqualToString:@"0"]) {//请求成功
            
            [weakSelf initWithorderStatrArray];
        } else {
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
        }
        
    } failure:^(bool isFailure) {
        
    }];
}
#pragma mark--设置显示状态
-(void)initWithorderStatrArray
{
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
     NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/detail":@"service/order/detail";
    if (self.homeMakeType.length !=0) {
        requertKey =@"ser/order/detail";
    }
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            [weakSelf.dataSource removeAllObjects];
            _totalPrice = @"";
            if ([state isEqualToString:@"0"]) {//请求成功
                _totalPrice = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"AMOUNT"]];
                [weakSelf updateDataWithDataDic:[dataDic objectForKey:@"OBJECT"]];
                [_orderStateTableView reloadData];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
    } failure:^(bool isFailure) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];
   
}
//设置数据
- (void)updateDataWithDataDic:(NSDictionary*)datadic{
    NSString *payType = [NSString stringTransformObject:[datadic objectForKey:@"PAYTYPE"]];//支付方式 1：在线支付，2：货到付款
    NSString *takeType = [NSString stringTransformObject:[datadic objectForKey:@"TAKETYPE"]];//收货方式 1：送货，2：自取
    //服务订单不用判断 收货方式 设置默认为1
    if ([self.requestDataType isEqualToString:@"0"]) {
        takeType=@"1";
    }
    NSString *payState = [NSString stringTransformObject:[datadic objectForKey:@"PAYSTATE"]];//支付状态 1：待支付，2：已支付
    NSString *takeState = [NSString stringTransformObject:[datadic objectForKey:@"DELIVERSTATE"]];//收货状态 1：待发货，2：已发货 3：已签收
    NSString *orderState = [NSString stringTransformObject:[datadic objectForKey:@"ORDERSTATE"]];//订单状态
    _orderNum = [NSString stringTransformObject:[datadic objectForKey:@"ORDERNUM"]];
    _orderDate = [NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]];
     _cuiDanCount = [NSString stringTransformObject:[datadic objectForKey:@"URGETIMES"]];
    if ([_cuiDanCount isEqualToString:@"0"]) {
        _otherCuiDanDate = @"";
    } else {
      _otherCuiDanDate = [NSString stringTransformObject:[datadic objectForKey:@"URGELASTTIME"]];
    }
   NSString *serviceType =@"";
    if (self.homeMakeType.length !=0) {
        serviceType = @"1";
    }else{
        serviceType =@"2";
    }
    [self setupBottomViewWithPayType:payType DeliveType:takeType PayState:payState DeliveState:takeState OrderState:orderState servicetype:serviceType];

    
    //支付方式  1：在线支付，2：货到付款
    //收货方式 1：送货，2：自取
    //支付状态 1：待支付，2：已支付
    //收货状态 1：待发货，2：已发货 3：已签收
    // 订单状态 1:正常，2：已完成，3：取消，4：删除，5 :订单已过期
    
    if ([orderState isEqualToString:@"1"]) {//正常
        if ([payType isEqualToString:@"1"]) {//在线支付
            if ([takeType isEqualToString:@"1"]) {//送货
                //支付状态 1：待支付，2：已支付
                if ([payState isEqualToString:@"1"]) {//待支付
                    //订单已提交
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"订单已提交" forKey:@"title"];
                    [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                    [self.dataSource addObject:dic];
                    //待支付
                    dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"待支付" forKey:@"title"];
                    [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                    [dic setValue:@"" forKey:@"time"];
                    [self.dataSource addObject:dic];
                } else if([payState isEqualToString:@"2"]) {//已支付
                    //收货状态 1：待发货，2：已发货 3：已签收
                    if ([takeState isEqualToString:@"1"]) {//待发货
                        //订单已提交
                        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"订单已提交" forKey:@"title"];
                        [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                        [self.dataSource addObject:dic];
                        //待支付
                        dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"待支付" forKey:@"title"];
                        [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                        [dic setValue:@"" forKey:@"time"];
                        [self.dataSource addObject:dic];
                        //已支付
                        dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"已支付" forKey:@"title"];
                        [dic setValue:@"等待门店确认" forKey:@"subTitle"];
                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
                        [self.dataSource addObject:dic];
                    } else if ([takeState isEqualToString:@"2"]) {//已发货
                        //订单已提交
                        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"订单已提交" forKey:@"title"];
                        [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                        [self.dataSource addObject:dic];
                        //待支付
                        dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"待支付" forKey:@"title"];
                        [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                        [dic setValue:@"" forKey:@"time"];
                        [self.dataSource addObject:dic];
                        //已支付
                        dic = [[NSMutableDictionary alloc]init];
                        [dic setValue:@"已支付" forKey:@"title"];
                        [dic setValue:@"等待门店确认" forKey:@"subTitle"];
                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
                        [self.dataSource addObject:dic];
                        //已发货
                        dic = [[NSMutableDictionary alloc]init];
                        NSString *title=@"";
                        if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                            title =@"已发货";
                        }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                        {
                            title=@"成功派单";
                        }
                        [dic setValue:title forKey:@"title"];
                        [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
                        [self.dataSource addObject:dic];
                    }
//                    else if ([takeState isEqualToString:@"3"]) {//已签收
//                        //订单已提交
//                        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"订单已提交" forKey:@"title"];
//                        [dic setValue:@"请尽快支付" forKey:@"subTitle"];
//                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                        //待支付
//                        dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"待支付" forKey:@"title"];
//                        [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
//                        [dic setValue:@"" forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                        //已支付
//                        dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"已支付" forKey:@"title"];
//                        [dic setValue:@"等待门店确认" forKey:@"subTitle"];
//                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                        //已发货
//                        dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"已发货" forKey:@"title"];
//                        [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
//                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                        //订单完成
//                        dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"订单已完成" forKey:@"title"];
//                        [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
//                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                    }
                }
                
            } else if ([takeType isEqualToString:@"2"]) {//自取
                //支付状态 1：待支付，2：已支付
                if ([payState isEqualToString:@"1"]) {//待支付
                    //订单已提交
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"订单已提交" forKey:@"title"];
                    [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                    [self.dataSource addObject:dic];
                    //待支付
                    dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"待支付" forKey:@"title"];
                    [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                    [dic setValue:@"" forKey:@"time"];
                    [self.dataSource addObject:dic];
                } else if ([payState isEqualToString:@"2"]) {//已支付
                    //收货状态 1：待发货，2：已发货 3：已签收
                    //订单已提交
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"订单已提交" forKey:@"title"];
                    [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                    [self.dataSource addObject:dic];
                    //待支付
                    dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"待支付" forKey:@"title"];
                    [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                    [dic setValue:@"" forKey:@"time"];
                    [self.dataSource addObject:dic];
                    //已支付
                    dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"已支付" forKey:@"title"];
                    [dic setValue:@"等待门店确认" forKey:@"subTitle"];
                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
                    [self.dataSource addObject:dic];
                    
//                    if ([takeState isEqualToString:@"3"]) {//已签收
//                        //订单完成
//                        dic = [[NSMutableDictionary alloc]init];
//                        [dic setValue:@"订单已完成" forKey:@"title"];
//                        [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
//                        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
//                        [self.dataSource addObject:dic];
//                    }
                }
            }
        } else if([payType isEqualToString:@"2"]) {//货到付款
            
            if ([takeType isEqualToString:@"1"]) {//送货
                //收货状态 1：待发货，2：已发货 3：已签收
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请耐心等待商家确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                if ([takeState isEqualToString:@"2"]) {//已发货
                    //已发货
                    dic = [[NSMutableDictionary alloc]init];
                    [dic setValue:@"已发货" forKey:@"title"];
                    [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
                    [self.dataSource addObject:dic];
                }
//                else if ([takeState isEqualToString:@"3"]) {//已签收
//                    //已发货
//                    dic = [[NSMutableDictionary alloc]init];
//                    [dic setValue:@"已发货" forKey:@"title"];
//                    [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
//                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
//                    [self.dataSource addObject:dic];
//                    //订单完成
//                    dic = [[NSMutableDictionary alloc]init];
//                    [dic setValue:@"订单已完成" forKey:@"title"];
//                    [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
//                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
//                    [self.dataSource addObject:dic];
//                }
               
            } else if ([takeType isEqualToString:@"2"]) {//自取
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请耐心等待商家确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
//                if ([takeState isEqualToString:@"3"]) {//已签收
//                    //订单完成
//                    dic = [[NSMutableDictionary alloc]init];
//                    [dic setValue:@"订单已完成" forKey:@"title"];
//                    [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
//                    [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
//                    [self.dataSource addObject:dic];
//                }
            }
        }
    }else if ([orderState isEqualToString:@"2"]) {//已完成
        if ([payType isEqualToString:@"1"]) {//在线支付
            if ([takeType isEqualToString:@"1"]) {//送货
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //待支付
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"待支付" forKey:@"title"];
                [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                [dic setValue:@"" forKey:@"time"];
                [self.dataSource addObject:dic];
                //已支付
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"已支付" forKey:@"title"];
                [dic setValue:@"等待门店确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //已发货
                dic = [[NSMutableDictionary alloc]init];
                NSString *title=@"";
                if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                    title =@"已发货";
                }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                {
                    title=@"成功派单";
                }
                [dic setValue:title forKey:@"title"];
                [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //订单完成
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已完成" forKey:@"title"];
                [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
            } else if ([takeType isEqualToString:@"2"]) {//自取
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请尽快支付" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //待支付
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"待支付" forKey:@"title"];
                [dic setValue:@"请在30分钟内完成支付" forKey:@"subTitle"];
                [dic setValue:@"" forKey:@"time"];
                [self.dataSource addObject:dic];
                //已支付
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"已支付" forKey:@"title"];
                [dic setValue:@"等待门店确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"PAYDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //订单完成
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已完成" forKey:@"title"];
                [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
            }
        }else if([payType isEqualToString:@"2"]) {//货到付款
            if ([takeType isEqualToString:@"1"]) {//送货
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请耐心等待商家确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //已发货
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"已发货" forKey:@"title"];
                [dic setValue:@"商品已在路上，请耐心等待" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"DELIVERDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //订单完成
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已完成" forKey:@"title"];
                [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
            }else if ([takeType isEqualToString:@"2"]) {//自取
                //订单已提交
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已提交" forKey:@"title"];
                [dic setValue:@"请耐心等待商家确认" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"STARTDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
                //订单完成
                dic = [[NSMutableDictionary alloc]init];
                [dic setValue:@"订单已完成" forKey:@"title"];
                [dic setValue:@"任何意见和吐槽，请联系我们" forKey:@"subTitle"];
                [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"SIGNFORDATE"]] forKey:@"time"];
                [self.dataSource addObject:dic];
            }
        }
    }else if([orderState isEqualToString:@"3"]) {//取消
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setValue:@"该订单已取消" forKey:@"title"];
        [dic setValue:@"快去逛逛心仪商品或再次购买" forKey:@"subTitle"];
        [dic setValue:@"" forKey:@"time"];
        [self.dataSource addObject:dic];
    } else if([orderState isEqualToString:@"5"]) {//订单已过期
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setValue:@"该订单已过期" forKey:@"title"];
        [dic setValue:@"快去逛逛心仪商品或再次购买" forKey:@"subTitle"];
        [dic setValue:[NSString stringTransformObject:[datadic objectForKey:@"EXPIREDATE"]] forKey:@"time"];
        [self.dataSource addObject:dic];
    }
    [self.orderStateTableView reloadData];
    //[self selectedRowWithPayType:payType takeType:takeType payState:payState takeState:takeState];
    
}
//- (void)selectedRowWithPayType:(NSString*)payType takeType:(NSString*)takeType payState:(NSString*)payState takeState:(NSString*)takeState {
//    NSInteger selecteRowIndex = 0;
//    if ([payType isEqualToString:@"1"]) {//在线支付
//        if ([takeType isEqualToString:@"1"]) {//送货
//            //支付状态
//            if ([payState isEqualToString:@"1"]) {//待支付
//                selecteRowIndex = 1;
//            } else if ([payState isEqualToString:@"2"]) {//已支付
//                //收货状态
//                if ([takeState isEqualToString:@"1"]) {//待发货
//                    selecteRowIndex = 2;
//                } else if ([takeState isEqualToString:@"2"]) {//已发货
//                     selecteRowIndex = 3;
//                } else if ([takeState isEqualToString:@"3"]) {//已签收
//                    selecteRowIndex = 4;
//                }
//            }
//        } else if ([takeType isEqualToString:@"2"]) {//自取
//            //支付状态
//            if ([payState isEqualToString:@"1"]) {//待支付
//                selecteRowIndex = 1;
//            } else if ([payState isEqualToString:@"2"]) {//已支付
//                //收货状态
//                selecteRowIndex = 2;
//                if ([takeState isEqualToString:@"2"]) {//已签收
//                    selecteRowIndex = 3;
//                }
//            }
//        }
//    } else if ([payType isEqualToString:@"2"]) {//货到付款
//        if ([takeType isEqualToString:@"1"]) {////送货
//            selecteRowIndex = 0;
//            if ([takeState isEqualToString:@"2"]) {//已发货
//                selecteRowIndex = 1;
//            } else if ([takeState isEqualToString:@"3"]) {//已签收
//                selecteRowIndex = 2;
//            }
//            
//        } else if ([takeType isEqualToString:@"2"]) {//自取
//            selecteRowIndex = 0;
//            if ([takeState isEqualToString:@"3"]) {
//                 selecteRowIndex = 1;
//            }
//        }
//    }
//    [self.orderStateTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selecteRowIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
//}
//OrderState:(NSString*)OrderState PayState:(NSString *)PayState DeliveState:(NSString *)DeliveState PayType:(NSString *)payType DeliveType:(NSString *)DeliveType OrderType:(OrderType)OrderType servicetype:(NSString *)serviceType
- (void)setupBottomViewWithPayType:(NSString*)payType DeliveType:(NSString*)DeliveType PayState:(NSString*)PayState DeliveState:(NSString*)DeliveState OrderState:(NSString*)OrderState servicetype:(NSString *)serviceType{
    for (UIButton *btn in self.buttonArr) {
        [btn removeFromSuperview];
    }
    NSString *firstButtonTitle = @"";
    NSString *secondButtonTitle = @"";
    if ([OrderState isEqualToString:KOrderStateNormal] ){//订单正常  支付状态为 未支付
           if ([payType isEqualToString:KOrderPayTypeIsPayPal] && [DeliveType isEqualToString:KOrderDeliverTypeIsDelivery])//在线付款+送货上门
        {
            if ([PayState isEqualToString:KOrderPayStateIsNo]) {
                firstButtonTitle = @"取消订单";
                secondButtonTitle = @"去支付";
            }else if([PayState isEqualToString:KOrderPayStateIsYes])//订单正常  支付状态为 已支付
            {
                if ([DeliveState isEqualToString:KOrderDeliverStateIsNo]) {
                    firstButtonTitle = @"催单";
                }else if([DeliveState isEqualToString:KOrderDeliverStateIsYes]){//订单正常  收货状态为 已发货
                    if ([serviceType isEqualToString:@"1"]) {
                        firstButtonTitle = @"催单";
                    }else{
                        NSString *title=@"";
                        if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                            title =@"确认收货";
                        }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                        {
                            title=@"确认服务";
                        }
                        firstButtonTitle = title;
                    }
                }else if([DeliveState isEqualToString:@"3"]){//家政店铺已派单
                    firstButtonTitle = @"催单";
                }else if([DeliveState isEqualToString:@"4"])//家政店铺确认订单
                   firstButtonTitle = @"确认服务";
            }
        }else if([payType isEqualToString:KOrderPayTypeIsPayPal]&& [DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]){//自取 +在线付款
            
                if([PayState isEqualToString:KOrderPayStateIsNo]){
                    firstButtonTitle = @"取消订单";
                    secondButtonTitle = @"去支付";
                    
                }else if([PayState isEqualToString:KOrderPayStateIsYes]) //如果是 家政订单 type才有值
                {
                    firstButtonTitle = @"确认取货";
                    
                }
        }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsDelivery]) //货到付款 + 送货上门
        {
                    if([DeliveState isEqualToString:KOrderDeliverStateIsNo]){
                        firstButtonTitle = @"取消订单";
                        secondButtonTitle = @"催单";
                        
                    }else if ([DeliveState isEqualToString:KOrderDeliverStateIsYes])//2
                    {
                        if ([serviceType isEqualToString:@"1"]) {
                            firstButtonTitle = @"取消订单";
                            secondButtonTitle = @"催单";
                        }else{
                            NSString *title=@"";
                            if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                                title =@"确认收货";
                            }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                            {
                                title=@"确认服务";
                            }
                            firstButtonTitle = title;
                        }
                        
                    }else if ([DeliveState isEqualToString:@"3"])//家政店铺已派单
                    {
                        firstButtonTitle = @"取消订单";
                        secondButtonTitle = @"催单";

                        
                    }else if([DeliveState isEqualToString:@"4"])//家政店铺已派单
                    {
                       firstButtonTitle = @"确认服务";
                    }
                    
          }else if([payType isEqualToString:KOrderPayTypeIsCOD]&&[DeliveType isEqualToString:KOrderDeliverTypeIsSelfUp]) //货到付款 + 自取
          {
            
            firstButtonTitle = @"取消订单";
            secondButtonTitle = @"催单";
          }
        } else if([OrderState isEqualToString:KOrderStateFinish])//订单状态为已完成
        {
            firstButtonTitle = @"";
           
        }else if ([OrderState isEqualToString:KOrderStateCancel])//订单状态为已取消
        {
            firstButtonTitle = @"";
        } else if ([OrderState isEqualToString:KOrderStateBeOverdue])//订单状态为已过期
        {
            firstButtonTitle = @"";
        }else if ([serviceType isEqualToString:@"1"]&&[OrderState isEqualToString:@"4"]) //拒单
        {
            firstButtonTitle = @"催单";
            
        }else if ([serviceType isEqualToString:@"1"]&&[OrderState isEqualToString:@"6"]) //家政已过期
        {
              firstButtonTitle = @"";
        }


//     // 订单状态 1:正常，2：已完成，3：取消，4：删除，5 :订单已过期
//    if ([orderState isEqualToString:KOrderStateNormal]) {//1:正常
//        //支付方式  1：在线支付，2：货到付款
//        if ([payType isEqualToString:KOrderPayTypeIsPayPal]) {//在线支付
//            //收货方式 1：送货，2：自取
//            if ([takeType isEqualToString:KOrderDeliverTypeIsDelivery]) {//送货
//                //支付状态 1：待支付，2：已支付
//                if ([payState isEqualToString:KOrderPayStateIsNo]) {//待支付
//                    firstButtonTitle = @"取消订单";
//                    secondButtonTitle = @"去支付";
//                } else if ([payState isEqualToString:KOrderPayStateIsYes]) {//已支付
//                    //收货状态 1：待发货，2：已发货 3：已签收
//                    if ([takeState isEqualToString:KOrderDeliverStateIsNo]) {//待发货
//                        firstButtonTitle = @"催单";
//                    } else if ([takeState isEqualToString:KOrderDeliverStateIsYes]||[takeState isEqualToString:@"3"]) {//已发货
//                        NSString *title=@"";
//                        if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
//                            title =@"确认收货";
//                        }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
//                        {
//                            title=@"确认服务";
//                        }
//                        firstButtonTitle = title;
//                    }
//                }
//            } else if ([takeType isEqualToString:KOrderDeliverTypeIsSelfUp]) {//自取
//                //支付状态
//                if ([payState isEqualToString:KOrderPayStateIsNo]) {//待支付
//                    firstButtonTitle = @"取消订单";
//                    secondButtonTitle = @"去支付";
//                } else if ([payState isEqualToString:KOrderPayStateIsYes]) {//已支付
//                    //收货状态
//                    firstButtonTitle = @"确认取货";
//                }
//            }
//        } else if ([payType isEqualToString:KOrderPayTypeIsCOD]) {//货到付款
//            
//            if ([takeType isEqualToString:KOrderDeliverTypeIsDelivery]) {////送货
//                 if ([takeState isEqualToString:KOrderDeliverStateIsNo]){
//                         firstButtonTitle = @"取消订单";
//                         secondButtonTitle = @"催单";
//                     }else if ([takeState isEqualToString:KOrderDeliverStateIsYes]||[takeState isEqualToString:@"3"]) {//已发货
//                   
//                    NSString *title=@"";
//                    if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
//                        title =@"确认收货";
//                    }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
//                    {
//                        title=@"确认服务";
//                    }
//                    firstButtonTitle = title;
//
//                }
//                
//            
//        }else if ([takeType isEqualToString:KOrderDeliverTypeIsSelfUp]) {//自取
//                firstButtonTitle = @"取消订单";
//                secondButtonTitle = @"确认取货";
//            }
//        }
//    } else if([orderState isEqualToString:@"2"]) {//已完成(已签收，已取货)
//         firstButtonTitle = @"";
//        //firstButtonTitle = @"再次购买";
//        //secondButtonTitle = @"去评价";
//    }else if([orderState isEqualToString:@"3"]) {//取消
//         //firstButtonTitle = @"再次购买";
//        firstButtonTitle = @"";
//    }else if([orderState isEqualToString:@"5"]) {//订单已过期
//         //firstButtonTitle = @"再次购买";
//         firstButtonTitle = @"";
//    }

    CGFloat leftSpace = 0;
    CGFloat hSpace  = 0;
    NSArray *titles = nil;
    if (firstButtonTitle.length != 0) {
        if (firstButtonTitle.length != 0 && secondButtonTitle.length != 0) {//两个button
            leftSpace = 15;
            hSpace  = 30;
            titles = @[firstButtonTitle,secondButtonTitle];
        } else {//一个button
            leftSpace = 60;
            titles = @[firstButtonTitle];
        }
        CGFloat itemWidth = (VIEW_WIDTH - hSpace - 2*leftSpace)/titles.count;
        UIImage *deleteOrderBackgroundImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#c52720"]
                                                                      frame:CGRectMake(0, 0, itemWidth,44)];
        for (NSInteger i = 0; i < titles.count; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(leftSpace +(itemWidth + hSpace)*i,CGRectGetMaxY(self.orderStateTableView.frame) + 10, itemWidth, 44);
            [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            [button setBackgroundImage:deleteOrderBackgroundImage forState:UIControlStateNormal];
            [button.layer setMasksToBounds:YES];
            [button.layer setCornerRadius:4];
            [button setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
            [button addTarget:self action:@selector(operationMyMarkclick:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.view addSubview:button];
            [_buttonArr addObject:button];
        }
    }
    
    
}
#pragma mark --UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HightScalar(50);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.dataSource.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identfy =@"orderState";
    MyOrderStateCell *cell =[tableView dequeueReusableCellWithIdentifier:identfy];
    if (cell==nil) {
        cell =[[MyOrderStateCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identfy];
        
        
    }
    
    NSDictionary *dataDic = [[self.dataSource objectAtIndex:indexPath.row] copy];
    [cell updateData:dataDic];
    
    
    return cell;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        if (buttonIndex == 1){
       [self loadCancelOrderData];
        }
    }
}
#pragma mark -- button Action

- (void)operationMyMarkclick:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    if ([button.titleLabel.text isEqualToString:@"取消订单"]) {//取消订单
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否取消订单?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=1;
        [alert show];
       
    } else if([button.titleLabel.text isEqualToString:@"去支付"]) {//去支付
        WeiXinPayController *weiXinPayVC = [[WeiXinPayController alloc]init];
        weiXinPayVC.orderNum = [NSString stringISNull:self.orderNum];
        weiXinPayVC.totalPrice = [NSString stringISNull:_totalPrice];
        weiXinPayVC.goodIDs=@"";
        weiXinPayVC.requestDataType =self.requestDataType;
        weiXinPayVC.homeMakeType =self.homeMakeType;
        [self.navigationController pushViewController:weiXinPayVC animated:YES];
        
    }else if([button.titleLabel.text isEqualToString:@"催单"]){//催单
        [self handleCuiDanEvent];
    }else if([button.titleLabel.text isEqualToString:@"确认收货"]||[button.titleLabel.text isEqualToString:@"确认服务"]){//确认送达
        [self loadOrderQianShouData];
    }else if([button.titleLabel.text isEqualToString:@"确认取货"]){//已取货
        [self loadOrderQianShouData];
    }
//    else if([button.titleLabel.text isEqualToString:@"再次购买"]){//再来一单
//        
//    }else if([button.titleLabel.text isEqualToString:@"去评价"]){//去评价
//        
//    }
}

- (void)handleCuiDanEvent {
//    NSDate *date = nil;
//    if([_cuiDanCount isEqualToString:@"0"]) {
//        date = [self dateWithString:_orderDate];
//    } else {
//         date = [self dateWithString:_otherCuiDanDate];
//    }
//    [date timeIntervalSinceDate:[NSDate date]];
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    unsigned int unitFlags = NSSecondCalendarUnit;//年、月、日、时、分、秒、周等等都可以
//    NSDateComponents *comps = [gregorian components:unitFlags fromDate:date toDate:[NSDate date] options:0];
//    if ([_cuiDanCount isEqualToString:@"0"]) {//第一次催单
//        if ([comps second] > 900) {
//            [self loadCuiOrderDataWithOrderNumber:_orderNum];
//        } else {
//            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"下单后15分钟后方可催单" inView:self.view];
//        }
//    } else {//不是第一次催单
//        if ([comps second] > 300) {
//            [self loadCuiOrderDataWithOrderNumber:_orderNum];
//        } else {
//            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"催单间隔为5分钟" inView:self.view];
//        }
//    }
     [self loadCuiOrderDataWithOrderNumber:_orderNum];
}
//- (NSDate*)dateWithString:(NSString*)str {
//    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
//    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
//    return  [dateFormat dateFromString:str];
//}
#pragma mark--初始化视图
-(void)setUpSuberView
{
    _orderStateTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, CGRectGetHeight(self.view.bounds)-44-64 -64) style:UITableViewStylePlain];
    [self.view addSubview:_orderStateTableView];
    _orderStateTableView.delegate=self;
    _orderStateTableView.dataSource=self;
    _orderStateTableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    _orderStateTableView.backgroundColor=[UIColor colorWithRed:238/250.0 green:238/250.0 blue:238/250.0 alpha:1];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, HightScalar(50))];
    view.backgroundColor = [UIColor colorWithRed:238/250.0 green:238/250.0 blue:238/250.0 alpha:1];
    UILabel * label =[[UILabel alloc]initWithFrame:CGRectMake(10, 0, view.bounds.size.width, view.bounds.size.height)];
    label.text=@"配送信息";
    label.textColor=[UIColor colorWithHexString:@"#555555"];
    label.font=[UIFont systemFontOfSize:FontSize(18)];
    [view addSubview:label];
    _orderStateTableView.tableHeaderView = view;
    
}





@end
