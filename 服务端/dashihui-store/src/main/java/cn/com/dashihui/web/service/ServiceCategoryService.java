package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.ServiceCategory;

public class ServiceCategoryService {
	
	public boolean sortCategory(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_service_category SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addCategory(ServiceCategory newObject){
		return newObject.save();
	}
	
	public ServiceCategory findByCode(int storeid,String code){
		return ServiceCategory.me().findFirst("SELECT * FROM t_bus_service_category WHERE storeid=? AND code=?",storeid,code);
	}
	
	@Before(Tx.class)
	public boolean delCategory(String code){
		Db.update("DELETE FROM t_bus_service_category WHERE code=?",code);
		Db.update("DELETE FROM t_bus_service_category_rel WHERE categorycode=?",code);
		return true;
	}
	
	public boolean editCategory(ServiceCategory object){
		return object.update();
	}
	
	public ServiceCategory findById(int id){
		return ServiceCategory.me().findFirst("SELECT a.* FROM t_bus_service_category a WHERE a.id=?",id);
	}
	
	public Page<Record> findByPage(int pageNum, int pageSize, int storeid){
		StringBuffer sqlExcept = new StringBuffer("FROM t_bus_service_category A WHERE A.storeid=? ORDER BY A.orderNo");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sqlExcept.toString(),storeid);
	}
}
