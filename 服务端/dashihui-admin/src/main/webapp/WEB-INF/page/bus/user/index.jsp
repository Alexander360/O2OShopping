<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">用户</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-hover">
			            <thead>
			                <tr>
			                    <th width="10%">头像</th>
			                    <th width="20%">用户名</th>
			                    <th width="20%">昵称</th>
			                    <th width="10%">性别</th>
			                    <th width="30%">住所</th>
			                    <th width="10%">注册时间</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.avator}}</td>
		<td>{{item.username}}</td>
		<td>{{item.nickName}}</td>
		<td>{{item.sex | flagTransform:1,'男',2,'女'}}</td>
		<td>{{item.communityName}}-{{item.buildName}}-{{item.unitName}}-{{item.roomName}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd'}}</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/bus/user/page",{"pageSize":20},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/bus/user/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/bus/user/toEdit/"+id).open();
}
function onEditSuccess(id,newObject){
	$("#item"+id).replaceWith(template("list-tpl",{"list":[newObject]}));
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/bus/user/doDelete/"+id,function(result){
			$("#item"+id).remove();
		});
	});
}
</script>
</body>
</html>