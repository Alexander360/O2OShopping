//
//  MyOrderController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyOrderController
 Created_Date： 20151107
 Created_People：JSQ
 Function_description： 我的订单
 ***************************************/

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, MyOrderType) {
    kallOrderType = 0,//所有订单
    kwaitPayMoneyType = 1,//待付款
    kwaitSendProductType = 2,//待发货
    kwaitGetProductType  //待收货
};
@interface MyOrderController : BaseViewController
@property(nonatomic)MyOrderType myOrderType;
@property(nonatomic,strong)NSString *requestDataType;//商品订单 是1 服务订单是 0
@property(nonatomic,strong)NSString *homeMakeType;  //家政服务 有数据 说明是家政服务
//@property(nonatomic)enum orderTypeWithgoodS orderTypeWithGoods;
//@property(nonatomic)enum serviceType serviceTyoe;
@end
