<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_feedback.css" />
	</head>
<body>
	<form class="am-form">
		<fieldset>
			<div class="am-form-group">
	      		<textarea maxlength="500" rows="10" id="content" placeholder="请输入您的意见或建议(500字以内)" onkeydown="javascript:validate();" onkeyup="javascript:validate();"></textarea>
	    	</div>
	    	<div class="am-form-group">
		      <input id="contact" type="text" class="am-form-field am-radius" placeholder="输入电子邮件或手机号码" maxlength="30">
		    </div>
	    	<div class="app-addr-btn">
				<a href="javascript:feedback();" class="app-submit">提交</a>
			</div>
		</fieldset>
	</form>
	<div class="app-service">
		客服电话：<span style="color:red;">0371-86563519</span> &nbsp;&nbsp;&nbsp;&nbsp; 服务时间：9:00-17:30
	</div>
	<script type="text/javascript">
		$(function(){
			validate();
		})
		
		function validate() {
			var content = $("#content").val();
			if(content == null || content == '') {
				$(".app-submit").removeAttr("href").attr("style", "background-color:#E29CA7;");
			} else {
				$(".app-submit").attr("href", "javascript:feedback();").removeAttr("style");
			}
		}
		
		function feedback(){
			var contact = $("#contact").val();
			var content = $("#content").val();
			if(content == null || content == '') {
				Kit.ui.alert("请输入您的建议");
			} else {
				Kit.ajax.post("${BASE_PATH}/feedback?CONTEXT=context&CONTACT=contact",function(result){
					if(result.flag == 0){
						location.href="${BASE_PATH}/index";
					}
				});
			}
		}
	</script>
</body>
</html>