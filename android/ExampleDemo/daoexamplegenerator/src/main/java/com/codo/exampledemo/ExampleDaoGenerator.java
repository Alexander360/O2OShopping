package com.codo.exampledemo;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class ExampleDaoGenerator {
    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(6, "com.codo.example");

        addShopping(schema);
        addSearch(schema);
        browseHistory(schema);
        addServer(schema);
        new DaoGenerator().generateAll(schema, "E:/workspace/ExampleDemo/app/src/main/java");
    }



//    private Long id;
//    /** Not-null value. */
//    private String spID;//商品主键ID
//    private String name;//    NAME	商品名称
//    private String unit;//    UNIT	单位
//    private String shortinfo;//    SHORTINFO	商品文字短介绍
//    private String price;//    PRICE	商品价格（原价）
//    private String imgsrc;//    IMGSRC	图片地址
//    private String purchaseamount;//    PURCHASEAMOUNT	限购数量
//    private String type;//    TYPE	优惠类型，1：普通，2：推荐，3：限量，4：一元购
//    private String favprice;// 现在的价格
//    private String buynum;// 买的数量
//    private Boolean ischoose;// 购物车中是否选中

    /**
     * 购物车数据库
     * @param schema
     */
    private static void addShopping(Schema schema){
        Entity buyGoods = schema.addEntity("ShoppingCart");
        buyGoods.addIdProperty().autoincrement();
        buyGoods.addStringProperty("ID").notNull();//商品主键ID
        buyGoods.addStringProperty("ShopID").notNull();//店铺ID
        buyGoods.addStringProperty("brandname");//    BRANDNAME	商品品牌名称
        buyGoods.addStringProperty("name");//    NAME	商品名称
        buyGoods.addStringProperty("spec");//    规格
        buyGoods.addStringProperty("shortinfo");//    SHORTINFO	商品文字短介绍

        buyGoods.addStringProperty("marketprice");//    marketprice	市场价格（原价）
        buyGoods.addStringProperty("imgsrc");//    IMGSRC	图片地址
        buyGoods.addStringProperty("urv");//    URV	限购数量
        buyGoods.addStringProperty("type");//    TYPE	优惠类型，1：普通，2：推荐，3：限量，4：一元购

        buyGoods.addStringProperty("sellprice");// 现在的价格

        buyGoods.addStringProperty("buynum");// 买的数量
        buyGoods.addStringProperty("shoptype");// 商品类型  0：普通，1：自营，3：其他
        buyGoods.addBooleanProperty("ischoose");// 购物车中是否选中
    }


    /**
     * 服务数据
     * @param schema
     */
    private static void addServer(Schema schema){
        Entity server = schema.addEntity("Server");
        server.addIdProperty().autoincrement();
        server.addStringProperty("ID").notNull();//商品主键ID
        server.addStringProperty("ShopID").notNull();//店铺ID
        server.addStringProperty("brandname");//    BRANDNAME	商品品牌名称
        server.addStringProperty("name");//    NAME	商品名称
        server.addStringProperty("thumb");//    THUMB	图片地址
        server.addStringProperty("comment");//    商家说明，或价格说明

        server.addStringProperty("marketprice");//    marketprice	市场价格（原价）
        server.addStringProperty("sellprice");// 现在的价格
        server.addStringProperty("title");//   服务项目标题，如：“家具清洁1小时 30元”
        server.addStringProperty("serverID");//    服务项对象ID

        server.addStringProperty("shortinfo");//    SHORTINFO	商品文字短介绍
        server.addStringProperty("buynum");// 买的数量

        server.addBooleanProperty("ischoose");// 购物车中是否选中
    }

    /**
     * 商品搜索历史
     * @param schema
     */
    private static void addSearch(Schema schema){
        Entity buyGoods = schema.addEntity("Search");
        buyGoods.addIdProperty().autoincrement();
        buyGoods.addLongProperty("ID").notNull();//主键ID
        buyGoods.addStringProperty("query").notNull();//
        buyGoods.addLongProperty("date");//   时间

    }
    /**
     * 浏览历史
     * @param schema
     */
    private static void browseHistory(Schema schema){
        Entity historyGoods = schema.addEntity("BrowseHistory");
        historyGoods.addIdProperty().autoincrement();
        historyGoods.addStringProperty("ID").notNull();//商品主键ID
        historyGoods.addStringProperty("ShopID").notNull();//店铺ID
        historyGoods.addStringProperty("brandname");//    BRANDNAME	商品品牌名称
        historyGoods.addStringProperty("name");//    NAME	商品名称
        historyGoods.addStringProperty("spec");//    规格
        historyGoods.addStringProperty("shortinfo");//    SHORTINFO	商品文字短介绍

        historyGoods.addStringProperty("marketprice");//    marketprice	市场价格（原价）
        historyGoods.addStringProperty("imgsrc");//    IMGSRC	图片地址
        historyGoods.addStringProperty("urv");//    URV	限购数量
        historyGoods.addStringProperty("type");//    TYPE	优惠类型，1：普通，2：推荐，3：限量，4：一元购

        historyGoods.addStringProperty("sellprice");// 现在的价格

        historyGoods.addStringProperty("historynum");// 查看的次数

        historyGoods.addStringProperty("shoptype");// 商品类型  0：普通，1：自营，3：其他

    }



//    private static void addCustomerOrder(Schema schema) {
//        Entity customer = schema.addEntity("Customer");
//        customer.addIdProperty();
//        customer.addStringProperty("name").notNull();
//
//        Entity order = schema.addEntity("Order");
//        order.setTableName("ORDERS"); // "ORDER" is a reserved keyword
//        order.addIdProperty();
//        Property orderDate = order.addDateProperty("date").getProperty();
//        Property customerId = order.addLongProperty("customerId").notNull().getProperty();
//        order.addToOne(customer, customerId);
//
//        ToMany customerToOrders = customer.addToMany(order, customerId);
//        customerToOrders.setName("orders");
//        customerToOrders.orderAsc(orderDate);
//    }

}

