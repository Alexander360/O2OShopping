//
//  SelectServiceTimeContentView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SelectServiceTimeContentView
 Created_Date： 20151224
 Created_People：gt
 Function_description： 选择服务时间内容视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol SelectServiceTimeContentViewDelegate <NSObject>
@optional
- (void)selectedListTtemAtIndex:(NSInteger)index title:(NSString*)title;//点击按钮触发
@end
@interface SelectServiceTimeContentView : UIView
@property(nonatomic)id<SelectServiceTimeContentViewDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame withDataArr:(NSArray*)dataArr columnNums:(NSInteger)columns;
- (void)reloadViewWithDateSelectedIndex:(NSInteger)selectedIndex;
@end
