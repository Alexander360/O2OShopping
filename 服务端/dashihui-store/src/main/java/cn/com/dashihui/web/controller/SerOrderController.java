package cn.com.dashihui.web.controller;

import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.kit.PushKit;
import cn.com.dashihui.kit.SMSKit;
import cn.com.dashihui.kit.ValidateKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.common.SerOrderCode;
import cn.com.dashihui.web.dao.SerOrder;
import cn.com.dashihui.web.service.SerOrderService;

/**
 * （家政）服务订单管理<br/>
 * 查询的是t_bus_ser_order表
 */
public class SerOrderController extends BaseController {
	//使用Duang.duang进行封装，使普通类具有事务的功能
	private SerOrderService service = Duang.duang(SerOrderService.class);

	/**
	 * 订单列表页面
	 */
	public void index(){
		render("index.jsp");
	}
	/**
	 * 获得订单列表数据
	 */
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//订单编号
		String orderNum = getPara("orderNum");
		//下单时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		//订单状态
		int state = getParaToInt("state",0);
		//买家地址
		String address = getPara("address");
		//买家电话
		String tel = getPara("tel");
		//支付方式
		int payType=getParaToInt("payType",0);
		renderResult(0,service.findByPage(pageNum,pageSize,getStoreid(),orderNum,beginDate,endDate,state,address,tel,payType));
	}
	/**
	 * 查询订单详情
	 */
	public void detail(){
		String orderNum = getPara("orderNum");
		SerOrder order = service.getOrderByOrderNum(orderNum);
		if(StrKit.notNull(order)){
			setAttr("obj", order);
			setAttr("logList", service.getLogListByOrderNum(orderNum));
		}
		render("detail.jsp");
	}

	/**
	 * 把订单设为为无效
	 */
	public void doWast(){
		String orderNum = getPara("orderNum");
		String reason = getPara("reason");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许设为无效");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“未付款”时可设为无效
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.NO_PAY){
						renderFailed("此订单当前不允许设为无效");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“店铺待接单”时可设为无效
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT){
						renderFailed("此订单当前不允许设为无效");
						return;
					}
				}
				//3.订单设为无效操作，并记录日志
				order.set("orderState", SerOrderCode.OrderState.WASTE).set("wasteDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(service.update(order)){
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "设订单为无效", "订单号："+orderNum+"，操作成功！备注："+reason);
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单设为无效失败");
				return;
			}
		}
	}
	
	/**
	 * 店铺接单
	 */
	public void doStroeAccept(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许接单");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“店铺待接单”时可接单
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.NO_PAY||deliverState!=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT){
						renderFailed("此订单当前不允许接单");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“店铺待接单”时可接单
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT){
						renderFailed("此订单当前不允许接单");
						return;
					}
				}
				//3.订单接单操作，并记录日志
				order.set("deliverState", SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT);
				if(service.update(order)){
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "店铺接单", "订单号："+orderNum+"，接单操作成功！");
					//向用户发送短信通知，需要判断如果用户的电话是手机号码
					if(ValidateKit.Mobile(order.getStr("tel"))){
						SMSKit.onStoreAcceptToCustomer(order.getStr("tel"), DatetimeKit.getFormatDate(order.getDate("serTime"), "yyyy年MM月dd日HH时mm分"), order.getStr("address"));
					}
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单接单失败");
				return;
			}
		}
	}
	
	/**
	 * 查询当前店铺下所有的服务商家，供派单选择
	 */
	public void toDeliver(){
		String orderNum = getPara("orderNum");
		if(!StrKit.isBlank(orderNum)){
			setAttr("shopList", service.getShopList(getStoreid(),orderNum));
			setAttr("orderNum",orderNum);
		}
		render("deliverShop.jsp");
	}
	
	/**
	 * 店铺派单 
	 */
	public void doDeliver(){
		String orderNum = getPara("orderNum");
		String proSeridStr = getPara("proSerid");
		String proSerName = getPara("proSerName");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(proSeridStr)||StrKit.isBlank(proSerName)){
			renderResult(2);
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(3);
				return;
			}else{
				//1.判断订单状态是否为“正常”或“拒单”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL&&orderState!=SerOrderCode.OrderState.REFUSE){
					renderFailed("此订单当前不允许派单");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“店铺已接单”时可接单
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.NO_PAY||deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT){
						renderFailed("此订单当前不允许派单");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“店铺已接单”时可派单
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT){
						renderFailed("此订单当前不允许派单");
						return;
					}
				}
				//3.订单派单操作，并记录日志
				//注：此处重新修改订单状态为“正常”
				int proSerid = Integer.valueOf(proSeridStr).intValue();
				order.set("orderState", SerOrderCode.OrderState.NORMAL)
					.set("deliverState", SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH)
					.set("proSerid", proSerid)
					.set("proSerName", proSerName)
					.set("storeDeliverDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(service.update(order)){
					//向商家客户端发起通知消息
					push(proSerid,"您有新的订单哟!请刷新接单列表");
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "店铺派单", "订单号："+orderNum+"，派单操作成功！备注：派单给店铺<"+proSerName+">");
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单派单失败");
				return;
			}
		}
	}
	
	/**
	 * 用户确认服务完成派单 
	 */
	public void doConfirm(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许接单");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“商家已确认”时可确认服务完成
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.NO_PAY||deliverState!=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT){
						renderFailed("此订单当前不允许确认服务完成");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“商家已确认”时可确认服务完成
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT){
						renderFailed("此订单当前不允许确认服务完成");
						return;
					}
				}
				//3.订单确认服务完成操作，并记录日志
				String datetime = DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss");
				order.set("orderState", SerOrderCode.OrderState.FINISH).set("signDate", datetime);
				if(service.update(order)){
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "订单确认服务完成", "订单号："+orderNum+"，确认服务完成操作成功！");
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("确认失败");
				return;
			}
		}
	}
	
	/**
	 * 向商家所登录的客户端推送消息提醒
	 * @param shopid
	 */
	private void push(int shopid,String text){
		//增加通知
		PushKit.pushToMany(service.getClientId(shopid), "大实惠商家", text);
	}
}
