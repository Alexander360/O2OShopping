package cn.com.dashihui.api.controller;

import com.jfinal.aop.Clear;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultField;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.common.ResultState;
import cn.com.dashihui.api.dao.ApiClient;
import cn.com.dashihui.api.dao.Community;
import cn.com.dashihui.api.dao.Store;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.dao.Version;
import cn.com.dashihui.api.interceptor.AuthClientTokenInterceptor;
import cn.com.dashihui.api.service.CommonService;
import cn.com.dashihui.kit.CommonKit;

public class CommonController extends BaseController{
	private CommonService service = new CommonService();
	
	/**
	 * 测试链接
	 * 用于客户端测试服务器状态，以判断服务器是否可以正常响应，直接返回成功即可
	 * @return 0：正常
	 */
	@Clear
	public void test(){
		renderSuccess();
	}
    
	/**
	 * 终端注册
	 * 用户第一次使用客户端时，客户端向服务器提交该终端的相关信息
	 * @param BRAND 品牌
	 * @param MODEL 型号
	 * @param OS 操作系统
	 * @param OSVERSION 系统版本
	 * @param SCREEN 屏幕尺寸
	 * @param RESOLUTION 分辨率
	 * @param MIDU 屏幕密度
	 * @param UQID 设备识别号，Android对应IMEI，iPhone对应设备号
	 * @param OSID 系统ID号，Android专用
	 * @param TVERSION 终端版本号，客户端当前使用的版本号
	 * @param TYPE 终端类型，A.Android B.iPhone C.iPad
	 * @return SIGNATURE 设备识别码，针对系统该设备的身份标示
	 */
	@Clear(AuthClientTokenInterceptor.class)
    public void regist(){
    	//判断入参设备识别号已经记录过，则直接使用其设备识别码
    	ApiClient client = service.findClientByUqid(getPara("UQID"));
    	if(client!=null){
    		//更新使用客户端版本号
    		client.set("tversion", getPara("TVERSION")).update();
    		renderSuccess(ResultMap.newInstance().put(ResultField.FIELD_CLIENT_TOKEN,client.get("ID")));
    		return;
    	}
    	//如果没有，则新生成设备识别码，并创建新记录
    	String clientid = CommonKit.getUUID().concat(CommonKit.randomNum(4));
    	if(new ApiClient()
    			.set("id", clientid)
    			.set("brand", getPara("BRAND"))
    			.set("model", getPara("MODEL"))
    			.set("os", getPara("OS"))
    			.set("osversion", getPara("OSVERSION"))
    			.set("screen", getPara("SCREEN"))
    			.set("resolution", getPara("RESOLUTION"))
    			.set("midu", getPara("MIDU"))
    			.set("uqid", getPara("UQID"))
    			.set("osid", getPara("OSID"))
    			.set("tversion", getPara("TVERSION"))
    			.set("type", getPara("TYPE"))
    			.save()){
    		renderSuccess(ResultMap.newInstance().put(ResultField.FIELD_CLIENT_TOKEN,clientid));
    		return;
    	}
    	renderResult(ResultState.STATE_CLIENT_REGIST_FAILED,"终端注册失败");
    }
    
    /**
     * 检查版本更新，判断是否有比入参最新的版本信息
     * @param TYPE 平台，A.Android B.iPhone C.iPad
     * @param VERSION 当前客户端版本号
     * @return 
     */
    public void checkVersion(){
    	String typeStr = getPara("TYPE");
    	String versionStr = getPara("VERSION");
    	if(StrKit.isBlank(typeStr)){
    		renderFailed("参数TYPE不能为空");
    		return;
    	}else if(!typeStr.equals("A")&&!typeStr.equals("B")&&!typeStr.equals("C")){
    		renderFailed("参数TYPE不正确");
    		return;
    	}else if(StrKit.isBlank(versionStr)){
    		renderFailed("参数VERSION不能为空");
    		return;
    	}else if(!versionStr.matches("[1-9]{1,3}\\.\\d{1,3}\\.\\d{1,5}")){
    		renderFailed("参数VERSION格式不正确");
    		return;
    	}
    	String[] versionArr = versionStr.split("\\.");
    	int iVersionPart1 = Integer.valueOf(versionArr[0]), iVersionPart2 = Integer.valueOf(versionArr[1]), iVersionPart3 = Integer.valueOf(versionArr[2]);
    	//查询指定平台客户端的最新版本信息
    	Version lastVersion = service.findLastVersion(typeStr);
    	if(lastVersion!=null){
    		int lVersionPart1 = lastVersion.getInt("versionPart1"), lVersionPart2 = lastVersion.getInt("versionPart2"), lVersionPart3 = lastVersion.getInt("versionPart3");
    		//将数据库中的最新版本信息的版本号三个部分，与入参的版本号三个部分进行比较，判断数据库中的最新版本信息是否比入参版本号要大
    		if(lVersionPart1>iVersionPart1
    				||(lVersionPart1==iVersionPart1&&lVersionPart2>iVersionPart2)
    				||(lVersionPart1==iVersionPart1&&lVersionPart2==iVersionPart2&&lVersionPart3>iVersionPart3)){
    			renderSuccess(ResultMap.newInstance()
    					.put("UPDATEFLG",lastVersion.getInt("updateFlag")==1?"C":"B")
    					.put("CURVER", CommonKit.join(".",lVersionPart1,lVersionPart2,lVersionPart3))
    					.put("DOWNURL", lastVersion.get("downurl"))
    					.put("UPDATELOG", CommonKit.ifNull(lastVersion.get("updatelog")))
    					.put("RELEASEDATE", lastVersion.getDate("releaseDate")));
    			return;
    		}
    	}
		renderSuccess(ResultMap.newInstance().put("UPDATEFLG","A"));
    }
    
    /**
	 * 获取默认社区的百度位置主键（IOS应付上线专用）
	 * @return SIGNATURE 设备识别码，针对系统该设备的身份标示
	 */
    public void defLocation(){
    	Prop prop = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
		renderSuccess(ResultMap.newInstance().put("BAIDUKEY",prop.get("defLocation",null)));
    }
    
    /**
     * 查询指定数量的店铺信息，供客户端展示（考虑到客户端上有些用户禁用定位）
     * @param KEYWORD 关键字
     */
    public void lbs(){
		String keyword = getPara("KEYWORD");
		//返回
		renderSuccess(service.findAllLBSData(keyword));
    }
    
    /**
     * 根据百度地图的社区KEY，获取到相应的社区信息及便利店信息
     * @param BAIDUKEY 百度地图的社区KEY
     */
    public void location(){
    	String baidukey = getPara("BAIDUKEY");
    	if(StrKit.isBlank(baidukey)){
    		renderFailed("参数BAIDUKEY不能为空");
    		return;
    	}else{
    		Community comm = service.findCommunityByBaidukey(baidukey);
    		if(comm==null){
        		renderFailed("社区信息不存在");
        		return;
    		}
    		Store store = service.findStoreById(comm.getInt("storeid"));
    		if(store==null){
        		renderFailed("店铺信息不存在");
        		return;
    		}
    		//返回
    		renderSuccess(ResultMap.newInstance().put("COMMUNITY",comm).put("STORE", store));
    	}
    }
    
    /**
     * 根据社区ID，获取到相应的社区信息及便利店信息
     * @param COMMUNITYID 社区ID
     */
    public void getCommunity(){
    	String communityidStr = getPara("COMMUNITYID");
    	if(StrKit.isBlank(communityidStr)){
    		renderFailed("参数COMMUNITYID不能为空");
    		return;
    	}else{
    		Community comm = service.findCommunityById(Integer.valueOf(communityidStr));
    		if(comm==null){
        		renderFailed("社区信息不存在");
        		return;
    		}
    		Store store = service.findStoreById(comm.getInt("storeid"));
    		if(store==null){
        		renderFailed("店铺信息不存在");
        		return;
    		}
    		//返回
    		renderSuccess(ResultMap.newInstance().put("COMMUNITY",comm).put("STORE", store));
    	}
    }
    
    /**
     * 根据社区ID查询社区下所有的楼号、单元号、房间门牌号
     * @param COMMUNITYID 社区ID
     */
    @Clear
    public void communityDetail(){
    	String communityid = getPara("COMMUNITYID");
    	if(StrKit.isBlank(communityid)){
    		renderFailed("参数COMMUNITYID不能为空");
    		return;
    	}
		renderSuccess(service.findAllCommunityDetails(Integer.valueOf(communityid)));
    }
    
    /**
     * 查询所有分类
     */
    public void category(){
		String storeidStr = getPara("STOREID");
		int isSelf = getParaToInt("ISSELF",0);
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		renderSuccess(service.findAllCategory(Integer.valueOf(storeidStr).intValue(), isSelf));
    }
    
    /**
     * 意见反馈
     * @param CONTEXT 反馈内容
     */
    public void feedback(){
    	String context = getPara("CONTEXT");
    	if(StrKit.isBlank(context)){
    		renderFailed("反馈内容不能为空");
    		return;
    	}else if(context.length()>500){
    		renderFailed("反馈内容太长了，请精简您的内容在500字以内");
    		return;
    	}
    	//如果当前有登录用户，则同时记录用户ID
    	User user = getCurrentUser();
    	if(service.feedback(context, (user!=null?user.getInt("id"):0))){
    		//返回
    		renderSuccess();
    		return;
    	}
		renderFailed("提交反馈失败");
    }
}
