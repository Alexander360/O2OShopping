<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
<!--
var setGoodsDataPaginator;
function doRefresh(){
	var c1 = $("#categoryonid").val(), c2 = $("#categorytwid").val(), c3 = $("#categorythid").val(), c4 = $("#categoryfoid").val(), k = $("#keyword").val();
	if(setGoodsDataPaginator){
		setGoodsDataPaginator.destroy();
	}
	setGoodsDataPaginator = Kit.pagination("#setGoodsDataPagination","${BASE_PATH}/api/goodsPage",{c1:c1,c2:c2,c3:c3,c4:c4,k:k,"pageSize":10},function(result){
		$("#setGoodsDataList").empty().append(template("setGoodsDataTpl",result.object));
	});
}
function doSetGoods(id,title){
	onGoodsChoosed(id,title);
	setGoodsDialog.close();
}
//-->
</script>
<script type="text/html" id="setGoodsDataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.name}}</td>
		<td><button type="button" class="btn btn-sm btn-primary" onclick="doSetGoods('{{item.id}}','{{item.name}}')" title="选择">选择</button></td>
	</tr>
	{{/each}}
</script>
<div class="row">
	<div class="col-lg-12">
		<select id="categoryonid" class="selectpicker pull-left" data-url="${BASE_PATH}/api/category" data-width="17%" data-isfirst="true" data-next="#categorytwid" data-key="id:name" data-def-value="0"></select>
		<select id="categorytwid" class="selectpicker pull-left p-l-5" data-url="${BASE_PATH}/api/category/{value}" data-width="17%" data-next="#categorythid" data-key="id:name" data-def-value="0"></select>
		<select id="categorythid" class="selectpicker pull-left p-l-5" data-url="${BASE_PATH}/api/category/{value}" data-width="17%" data-next="#categoryfoid" data-key="id:name" data-def-value="0"></select>
		<select id="categoryfoid" class="selectpicker pull-left p-l-5" data-url="${BASE_PATH}/api/category/{value}" data-width="17%" data-key="id:name" data-def-value="0"></select>
		<input type="text" id="keyword" value="" class="form-control pull-left m-l-5 width200" placeholder="搜索关键字" maxlength="100">
		<button type="button" class="btn btn-success pull-left m-l-5" onclick="doRefresh()">查询</button>
		<div class="clearfix"></div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12" style="min-height:300px;">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th width="85%">商品名称</th>
	                    <th width="15%">操作</th>
	                </tr>
	            </thead>
	            <tbody id="setGoodsDataList"></tbody>
	        </table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<ul id="setGoodsDataPagination" class="pagination-sm pull-right"></ul>
	</div>
</div>