<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
<head>
	<title>大实惠云商服务平台 社区o2o平台</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
	<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
	<link rel="stylesheet" href="${BASE_PATH }/static/css/others.css" />
    <link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/foot.css">
    <link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/banner.css">
    <link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
	<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
	<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/template/template.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/template/template-ext.js"></script>
	<style>
		.ab_left li a:link {
			font-size: 16px;
			color: #666 !important;
		}
		
		.ab_left {
			width: 80%;
			border: 0;
			background: none;
			box-shadow: none;
			padding-left: 4%;
		}
		
		.ab_left li {
			float: left;
			width: 20%;
			border: 1px solid #ddd;
			background: #fff /*#e94a26*/;
			text-align: center;
			margin: 0 0 2% 2%;
			border-radius: 5px;
			color: #E94A26;
		}
		
		.ab_right {
			width: 80%;
		}
		
		.ab {
			padding: 1% 0 0 15%;
			background: none;
		}
</style>
</head>
<body>
	<center class="ab_ct">
		<header class="header">
			<div class="header_left">
				<a name="coop_new"><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li><a href="${BASE_PATH }/" class="button wobble-horizontal">网站首页</a></li>
					<li><a href="${BASE_PATH }/cooperate" class="button wobble-horizontal">合作加盟</a></li>
					<li class="nav-active"><a class="button wobble-horizontal">关于我们</a></li>
					<li><a href="${BASE_PATH }/notice" class="button wobble-horizontal">新闻动态</a></li>
					<li><a href="${BASE_PATH }/contact" class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<div class="banner">		
		    <img class="moving-bg" src="${BASE_PATH }/static/img/banner_a.jpg" alt="deepengine">
		    <div class="banner-mask"></div>
		    <div class="banner-box">
		        <h2></h2>
		        <p class="info"></p>		        
		        <a href="#" class="btn btn-curve">		        
		        
		        </a>
			</div>
    	</div>        
		<section class="ab">	
			<div class="ab_left">
				<ul>
					<li onclick="changeData('1');" id="tab_1"><a class="button wobble-horizontal">我们是谁</a></li>
					<li onclick="changeData('2');" id="tab_2"><a class="button wobble-horizontal">企业核心</a></li>
					<li onclick="changeData('3');" id="tab_3"><a class="button wobble-horizontal">项目优势</a></li>
					<li onclick="changeData('4');" id="tab_4"><a class="button wobble-horizontal">未来布局</a></li>
				</ul>
			</div>
			<div class="ab_right" id="showData"></div>
		</section>
		<!--在线留言-->
		<footer style="height: 450px;">
			<div style="width:55%; float: left;margin: 1% 0 0 2%;">
			<h1 style="">在线留言</h1>
			<h3>请您填写以下相关信息</h3>
			</div>
			<div style="width:40%;float: left;margin-top: 3%;">
				<img src="${BASE_PATH }/static/img/images/c_34.png" style="width:60% ;" />
			</div>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form action="http://www.91dashihui.com/cooperate;jsessionid=546C1F14B3C702877DA1F8B841AEFF93#">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt><img src="${BASE_PATH }/static/img/wscm.png" /></dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt><img src="${BASE_PATH }/static/img/a.png" /></dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;豫ICP备15031706号 
			</p>
		</footer>
	</center>
	<div style="background: url(${BASE_PATH }/static/img/images/c_33.png) repeat-x;width: 100%; float: left;">
		<div style="width:188px;margin:0 auto;">
			<a href="#coop_new" style="display:block;">
				<img src="${BASE_PATH }/static/img/images/c_32.png" style="margin:0;float: left;" />
			</a>
		</div>
	</div>
	<script type="text/html" id="type1">
		<div class="ab_right_p">	
			<p>大实惠云商自成立以来发展迅猛，短短半年时间已在河南省内签约地市加盟商 7 家，社区连锁便利店 200 家，并已建设1万平的大型省级物流配送中心。作为省内知名的电子商务服务企业，大实惠云商不仅可以为便利店经营者提供经营管理方案，还能为平台入驻商户提供后台支持、物流配送、经营策划方案等一系列服务，此外，还可以为物业公司提供信息管理平台。大实惠云商汇聚了一流的 IT 技术团队，在国内率先开发出集微商城、云服务、社交、公益等为一体的新型社区生活服务系统，这一切都离不开先进技术的支持。</p>
			<p>我们汇聚了一流的IT技术团队，成功开发出第二代社区服务云平台。我们抛弃了“烧钱引流量”“头重脚轻”的老毛病老思路，率先创造出线下社区便利店和线上云商城无缝连接、交相辉映的发展新模式，打出了线上线下双盈利的新创举。</p>
        </div>
		<img src="${BASE_PATH }/static/img/wom.png" style="padding-top:2%;width:100%;height: auto;">
	</script>
	<script type="text/html" id="type2">
		<p>随着经济和互联网科技的发展，越来越多的老百姓希望通过更为便捷安全的渠道买到物美价廉的商品、享受到专业舒适的服务，希望让繁琐无趣的家事更少的影响自己，希望留下更多的时间享受生活、陪伴家人。</p>
		<p>中国生活服务O2O已进入高速发展期,2015年市场规模将超过4000亿元人民币（易观智库）我们为“老百姓的智慧社区生活”而生！为“用云改变生活，让实惠走近你我”的使命而生！哪里有需求哪里就有市场，因此不管是生鲜、餐饮、洗衣、洗车、家政还是美妆、保健、旅游、理财、出行等各式各样的生活APP如雨后春笋般次第而生。他们的发展模式大同小异，多数是为了快速的提高会员数和订单量、为了快速的提高估值而疯狂的做广告、疯狂的布点，然后 陷入引钱烧钱的恶性循环。他们多数只是做了信息导引，而忽视监督、忽视服务质量。多数是为了电商而电商，多数是昙花一现。</p>
		<p>我们大实惠云商另辟蹊径，深入社区、狠抓服务，用一个云服务APP和一个生活E站解决老百姓的多样生活需求。我们用真心打动客户，用行动证明自我。</p>
	</script>
	<script type="text/html" id="type3">
		<p>风口      互联网+社区便利店掀起社区O2O行业热潮，新模式引领新蓝海</p>
		<p>机遇     全民创新，万众创业，国家领导人鼓励 历史机遇。</p>
		<p>资本   互联网社区O2O行业融资总额超过2000亿。</p>
		<p>市场   全国超过1万户社区已经超过百万个，市场消费规模超过万亿年增长超过40%，2015年线上O2O市场超过7500亿。</p>
		<p>照搬成熟模式，快速启动，快速复制，快速扩展。</p>
		<p>大实惠云商  开启全新社区生活！</p>
		<p>一、让创业者把便利店开在成熟社区里，借助大实惠云商平台为社区居民提供便利生活服务。</p>
		<p>二、以便利店平价生活消费品和免费送货上门为切入点，引入快递代收发、家政、干洗、家电维修、产品预售、社区金融等服务。</p>
		<p>三、成为社区居民的全能生活管家！同时成为加盟商的聚宝盆。</p>
		<p>我们汇聚了一流的IT技术团队，成功开发出第二代社区服务云平台。我们抛弃了“烧钱引流量”“头重脚轻”的老毛病老思路，率先创造出线下社区便利店和线上云商城无缝连接、交相辉映的发展新模式，打出了线上线下双盈利的新创举。</p>
	</script>
	<script type="text/html" id="type4">
		<p>我们如何帮助加盟商打开市场？</p>
		<p>快速启动，快速复制，快速扩张</p>
		<p>1.超低投入，快速启动  （投入要求低，一个店面，万元投入，就可快速启动，半个月就可开张。</p>  
		<p>2.成熟模式 高速复制   （运营模式成熟，总部共享成功案例，指导经营，保证加盟商开一家成一家。</p>
		<p>3. 服务多元化，竞争力强 （高频低价策略免费上面服务，对手无法模仿）</p>
		<p>加盟商如何赚钱？（有短期回报，更有长期收益！）</p>
		<p>市级子公司加盟</p>
		<p>根据城市规模、GDP总量、人口数量以及所辖区县数量我们将市级子公司分为A、B、C三个级别。</p>
		<p>超高盈利，保证加盟商迅速打开当地市场！</p>
		<p>二、加盟商收益</p>
		<p>合作伙伴	盈利项目	经营利润	备注</p>
		<p>地市子公司	区域内加盟费提成、直营区县营收、广告费、新品汇分润、加盟推介收益。	100万/月	</p>
		<p>县市加盟商	直营便利店、广告费、新品汇分润、加盟推介。	70万/月	</p>
		<p>社区店面	商品收益、服务返佣、广告费、展位费、新品汇分润。	4万/月	</p>
		<p>总部三大资源支持、六大扶持政策、让生意更好做
		<p>城市商业人脉+厂家资源+大数据平台（社区便利店就是渠道，渠道就是金钱。掌控社区便利店，将为你长期带来滚滚财源，更将助你登上本地商业先驱宝座。</p>
		<p>六大扶持政策：
		<p>1.整体加盟创业指导 （针对加盟商所代理区域进行整体项目方案规划）</p>
		<p>2.全面的营销策划  （您运营中所需要的所有营销资料，总部给予提供）</p>
		<p>3.专业运营培训 （总部拥有专业运营团队为您做全方位的帮助和指导）</p>
		<p>4.严格的区域保护 （加盟商全权负责该区域所有业务，其他人无权开发）</p>
		<p>5.平台免费升级 （强大的技术开发团队，对平台定期更新升级，确保平台稳定运营）</p>
		<p>6.在您的运营过程中，大实惠云商为您提供全程指导和咨询服务，您的成长有我们的陪伴）</p>
	</script>
	<script type="text/javascript">
		$(function(){
			changeData('1');
		});
		function changeData(type) {
			$("#tab_"+type).addClass("curr");
			$("#showData").empty().append(template("type"+type));
			for(var i = 1; i <= 4; i++) {
				if(i != type){
					$("#tab_"+i).removeClass("curr");
				}
			}
		}
	</script>
</body>
</html>