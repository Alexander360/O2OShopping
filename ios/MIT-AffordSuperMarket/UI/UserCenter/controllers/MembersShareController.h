//
//  MembersShareController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyWalletViewController
 Created_Date： 20160405
 Created_People： JSQ
 Function_description： 会员分享
 ***************************************/
#import "BaseViewController.h"

@interface MembersShareController : BaseViewController

@end
