package cn.com.dashihui.api.service;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.api.common.SerOrderCode;
import cn.com.dashihui.api.dao.SerOrder;

public class SerOrderService {

	/**
	 * 保存服务订单
	 */
	public boolean save(SerOrder order){
		return order.save();
	}
	
	/**
	 * <red>注：2016/02/25，停止该方法在客户端的调用，家政类服务订单的统计接口，统一在UserController的state方法中返回，本方法不删除只是为了适应旧版本客户端</red>
	 * 统计用户 待付款 待服务 完成的订单
	 */
	public long getCountOrder(int userid,int flag){
		
		if(flag==1){
			
			//1:待付款（“未支付”、“正常”）
			String sql = "SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND payState=? AND orderState=?";
			return Db.queryLong(sql,userid,SerOrderCode.OrderPayState.NO_PAY,SerOrderCode.OrderState.NORMAL);
		}else if(flag==2){
			
			//2：待服务("正常订单 下在线支付已完成   服务方没有派单")
			String sql = "SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND payState=? AND orderState=?";
			return Db.queryLong(sql,userid,SerOrderCode.OrderPayState.HAD_PAY,SerOrderCode.OrderState.NORMAL);
		}else {
			
			//3：完成订单（“订单状态已完成”）
			String sql = "SELECT COUNT(*) FROM t_bus_ser_order WHERE userid=? AND orderState=?";
			return Db.queryLong(sql,userid,SerOrderCode.OrderState.FINISH);
		}
	}
	
	/**
	 * <red>注：2016/02/25，停止该方法在客户端的调用，家政类服务订单列表与其他类服务订单列表，统一调用UserController的serviceOrderList，本方法不删除只是为了适应旧版本客户端</red>
	 * 用户查询各种状态的订单列表
	 */
	public Page<Record> findByPage(int userid,int flag,int pageNum,int pageSize){
		if(flag==1){
			//1:待付款（“未支付”、“正常”）
			String sql = "FROM t_bus_ser_order WHERE userid=? AND payState=? AND orderState=? ORDER BY createDate DESC";
			return Db.paginate(pageNum, pageSize, "SELECT * ",sql,userid,SerOrderCode.OrderPayState.NO_PAY,SerOrderCode.OrderState.NORMAL);
			
		}else if(flag==2){
			
			//2：待服务("正常订单 下在线支付已完成   服务方没有派单")
			String sql = "FROM t_bus_ser_order WHERE userid=? AND payState=? AND orderState=? ORDER BY createDate DESC";
			return Db.paginate(pageNum, pageSize, "SELECT * ",sql,userid,SerOrderCode.OrderPayState.HAD_PAY,SerOrderCode.OrderState.NORMAL);
			
		}else if(flag==3){
			
			//3：完成订单（“订单状态已完成”）
			String sql = "FROM t_bus_ser_order WHERE userid=? AND orderState=? ORDER BY createDate DESC";
			return Db.paginate(pageNum, pageSize, "SELECT * ",sql,userid,SerOrderCode.OrderState.FINISH);
			
		}else{
			
			//0：全部订单（“删除订单除外”）
			String sql = "FROM t_bus_ser_order WHERE userid=? AND orderState!=? ORDER BY createDate DESC";
			return Db.paginate(pageNum, pageSize, "SELECT * ",sql,userid,SerOrderCode.OrderState.DELETE);
		}
	}
	
	/**
	 * 根据订单编号查询出订单
	 * 排除已删除、已过期的订单
	 */
	public SerOrder getOrderByOrderNum(int userid,String orderNum){
		return SerOrder.me().findFirst("SELECT * FROM t_bus_ser_order WHERE userid=? AND orderNum=? AND orderState!=? AND orderState!=?",userid,orderNum,SerOrderCode.OrderState.DELETE,SerOrderCode.OrderState.EXPIRE);
	}
	
	/**
	 * 根据订单编号查询出订单
	 */
	public SerOrder getOrderByOrderNum(String orderNum){
		return SerOrder.me().findFirst("SELECT * FROM t_bus_ser_order WHERE orderNum=? AND orderState!=? AND orderState!=?",orderNum,SerOrderCode.OrderState.DELETE,SerOrderCode.OrderState.EXPIRE);
	}
	
	/**
	 * 用户催单
	 */
	@Before(Tx.class)
	public SerOrder urgeOrder(int userid,String orderNum) throws Exception{
		if(Db.update("UPDATE t_bus_ser_order SET urgeTimes=urgeTimes+1,urgeLastTime=now() WHERE userid=? AND orderNum=?",userid,orderNum)==1){
			return getOrderByOrderNum(orderNum);
		}
		throw new Exception("订单不存在");
	}
	
	/**
	 * 记录操作日志
	 */
	public void log(String orderNum, String user, String action, String content){
		Db.update("INSERT INTO t_bus_ser_order_log(orderNum,user,action,content) VALUES(?,?,?,?)",orderNum,user,action,content);
	}
}
