//
//  MyOrderCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, OrderType) {
    kgoodOrderType = 0,//商品详情底部栏
    kserviceOrderType //服务详情底部栏
};
@protocol MyOrderCellDelegate<NSObject>
//去支付 或 再次购买
-(void)selectedFirstItemWithTag:(NSInteger)tag title:(NSString *)title homeMakeType:(NSString *)homeMakeType;
//删除订单
-(void)selectedSecondItemWithTag:(NSInteger)tag title:(NSString *)title homeMakeType:(NSString *)homeMakeType
;
@end

@interface MyOrderCell : UITableViewCell
// 删除订单
@property (nonatomic,strong)UIButton *deleteOrder;
//去支付
@property (nonatomic,strong)UIButton *goPay;

@property(nonatomic,unsafe_unretained)id<MyOrderCellDelegate>delegate;
- (void)updateViewwithData:(NSDictionary*)dataDic OrderType:(OrderType)OrderType;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier OrderType:(OrderType)OrderType;

@end
