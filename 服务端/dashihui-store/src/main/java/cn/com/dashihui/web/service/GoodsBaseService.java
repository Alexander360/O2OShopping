package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.web.dao.Goods;
import cn.com.dashihui.web.dao.GoodsBase;
import cn.com.dashihui.web.dao.GoodsBaseImages;

public class GoodsBaseService {
	/**
	 * 分页查找基础产品
	 * @param pageNum
	 * @param pageSize
	 * @param storeid 店铺ID
	 * @param c1,c2,c3,c4 四级分类
	 * @param keyword 搜索关键字
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,int storeid,int c1,int c2,int c3,int c4,String keyword){
		StringBuffer sBuffer = new StringBuffer("FROM t_bus_goods_base A WHERE A.state=1 AND NOT EXISTS (SELECT 1 FROM t_bus_goods B WHERE A.id=B.goodsid AND B.storeid=?)");
		List<Object> params = new ArrayList<Object>();
		params.add(storeid);
		if(c4!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=? AND A.categoryfoid=?");
			params.add(c1);params.add(c2);params.add(c3);params.add(c4);
		}else if(c3!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=? AND A.categorythid=?");
			params.add(c1);params.add(c2);params.add(c3);
		}else if(c2!=0){
			sBuffer.append(" AND A.categoryonid=? AND A.categorytwid=?");
			params.add(c1);params.add(c2);
		}else if(c1!=0){
			sBuffer.append(" AND A.categoryonid=?");
			params.add(c1);
		}
		if(!StrKit.isBlank(keyword)){
			sBuffer.append(" AND A.name LIKE ?");
			params.add("%"+keyword+"%");
		}
		sBuffer.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.id,A.name,A.marketPrice,A.sellPrice,A.thumb,A.createDate", sBuffer.toString(), params.toArray());
	}
	/**
	 * 查找商品
	 */
	public GoodsBase findById(int id){
		return GoodsBase.me().findFirst("SELECT"
				+" a.*,"
				+ "c1.categoryName categoryonName,c2.categoryName categorytwName,c3.categoryName categorythName,c4.categoryName categoryfoName,"
				+ "b.name brandName"
				+" FROM t_bus_goods_base a"
				+" LEFT JOIN t_dict_category c1 ON a.categoryonid=c1.categoryId"
				+" LEFT JOIN t_dict_category c2 ON a.categorytwid=c2.categoryId"
				+" LEFT JOIN t_dict_category c3 ON a.categorythid=c3.categoryId"
				+" LEFT JOIN t_dict_category c4 ON a.categoryfoid=c4.categoryId"
				+" LEFT JOIN t_dict_brand b ON a.brandid=b.id"
				+ " WHERE a.state=1 AND a.id=?",id);
	}
	/**
	 * 查询商品图片集
	 */
	public List<GoodsBaseImages> findImages(int goodsid){
		return GoodsBaseImages.me().find("SELECT * FROM t_bus_goods_base_images WHERE goodsid=?",goodsid);
	}
	/**
	 * 复制商品
	 */
	@Before(Tx.class)
	public boolean copyTo(int goodsid,int storeid){
		//1.查询基础商品信息
		GoodsBase baseGoods = GoodsBase.me().findById(goodsid);
		if(baseGoods!=null){
			//复制给店铺
			Goods storeGoods = new Goods()
					.set("goodsid",baseGoods.get("id"))
					.set("storeid",storeid)
					.set("name",baseGoods.get("name"))
					.set("brandid",baseGoods.get("brandid"))
					.set("thumb",baseGoods.get("thumb"))
					.set("categoryonid",baseGoods.get("categoryonid"))
					.set("categorytwid",baseGoods.get("categorytwid"))
					.set("categorythid",baseGoods.get("categorythid"))
					.set("categoryfoid",baseGoods.get("categoryfoid"))
					.set("spec",baseGoods.get("spec"))
					.set("shortInfo",baseGoods.get("shortInfo"))
					.set("marketPrice",baseGoods.get("marketPrice"))
					.set("sellPrice",baseGoods.get("sellPrice"))
					.set("describe",baseGoods.get("describe"))
					.set("state",1)
					.set("type",1);
			if(storeGoods.save()){
				//2.商品信息复制成功后，将商品的图片信息也进行复制
				//可能基础商品没有图片集，所以update结果可能为0，所以在此不作判断
				Db.update("INSERT INTO t_bus_goods_images(goodsid,thumb,orderNo) SELECT ?,thumb,orderNo FROM t_bus_goods_base_images WHERE goodsid=?",storeGoods.getInt("id"),goodsid);
				return true;
			}
		}
		return false;
	}
}