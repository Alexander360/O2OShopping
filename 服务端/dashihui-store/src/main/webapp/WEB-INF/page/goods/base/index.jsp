<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">可复制商品</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-9">
				<select id="sCategoryonid" class="selectpicker pull-left" data-width="10%" data-url="${BASE_PATH}/api/category" data-isfirst="true" data-next="#sCategorytwid" data-key="id:name"></select>
				<select id="sCategorytwid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-next="#sCategorythid" data-key="id:name"></select>
				<select id="sCategorythid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-next="#sCategoryfoid" data-key="id:name"></select>
				<select id="sCategoryfoid" class="selectpicker pull-left p-l-5" data-width="10%" data-url="${BASE_PATH}/api/category/{value}" data-key="id:name"></select>
				<input type="text" id="sKeyword" value="" class="form-control pull-left m-l-5 width200" placeholder="搜索关键字" maxlength="100">
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-hover">
			            <thead>
			                <tr>
			                	<th width="5%"></th>
								<th>商品名称</th>
								<th width="8%">市场价(元)</th>
								<th width="8%">销售价(元)</th>
								<th width="10%">添加时间</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td><a href="javascript:void(0)" onclick="Kit.photo('${FTP_PATH}{{item.thumb}}')" class="center-block thumbnail thumbnail-none-margin width50"><img src="${FTP_PATH}{{item.thumb}}"></a></td>
		<td>{{item.name}}</td>
		<td>{{item.marketPrice}}</td>
		<td>{{item.sellPrice}}</td>
		<td>{{item.createDate | dateFormat:'yyyy-MM-dd hh:mm'}}</td>
		<td>
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toDetail('{{item.id}}')" title="查看"><span class="fa fa-eye fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-success btn-simple" onclick="doCopy('{{item.id}}',1)" title="复制"><span class="fa fa-copy fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(doSearch);
function doSearch(){
	var c1 = $("#sCategoryonid").val(), c2 = $("#sCategorytwid").val(), c3 = $("#sCategorythid").val(), c4 = $("#sCategoryfoid").val(), k = $("#sKeyword").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/goods/base/page",{c1:c1,c2:c2,c3:c3,c4:c4,k:k,"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var detailDialog;
function toDetail(id){
	detailDialog = Kit.dialog("查看","${BASE_PATH}/goods/base/toDetail/"+id,{closable:true}).open();
}
function doCopy(id,runFrom){
	Kit.confirm("提示","您确定要复制该商品？",function(){
		$.post("${BASE_PATH}/goods/base/doCopy/"+id,function(result){
			if(result.flag==0){
				$("#item"+id).remove();
				if(runFrom&&runFrom==2)detailDialog.close();
			}else{
				Kit.alert("复制失败");return;
			}
		});
	});
}
</script>
</body>
</html>