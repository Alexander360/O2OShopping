//
//  NSString+Conversion.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/28.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "NSString+Conversion.h"
#import "Global.h"
@implementation NSString (Conversion)
//把空字符串转换成 @“”
+ (NSString*)stringISNull:(NSString*)_str {
    if (_str.length == 0 || _str == nil || _str == NULL  ) {
        return @"";
    }
    return _str;
}

//判断字符串是否可用(可能是整型)
+ (NSString*)stringTransformObject:(id)object {
    NSString *str = nil;
    if ([object isKindOfClass:[NSNumber class]]) {
        str = [object stringValue];
    } else if([object isKindOfClass:[NSNull class]]){
        str = @"";
    } else {
        str = (NSString*)object;
    }
    return [self stringISNull:str];
}

//判断小数点后数字是否以0结尾
+ (NSString*)marketLastTwoByteOfStringIsZero:(NSString*)_str
{
    if ([_str doubleValue] == 0|| [_str isEqualToString:@"0"] || [_str isEqualToString:@"0.0"] || [_str isEqualToString:@"0.00"])
    {
        return @"0";
    }
    
    NSRange range = [_str rangeOfString:@"."];
    
    if (range.length == 0 || range.location == 0)//没有小数点的str
    {
        return _str;
    }
    
    NSInteger lastOfStringLocation = range.location+1;//小数点后面第一位数下标
    NSInteger subLength = _str.length - lastOfStringLocation;//小数点后长度
    
    //小数点后字符串
    NSString * lastOfString = [_str substringWithRange:NSMakeRange(lastOfStringLocation, subLength)];
    
    NSInteger cubLength = 0;//计算小数点最后出现O的位置
    for (NSInteger i = subLength -1; i>=0; i--) {
        NSString *subStr = [lastOfString substringWithRange:NSMakeRange(i, 1)];
        if ([subStr isEqualToString:@"0"])
        {
            cubLength ++;
        }
        else
        {
            break;
        }
    }
    NSString *newString = nil;
    if (cubLength !=0)
    {
        if(subLength== cubLength){
            newString = [_str substringWithRange:NSMakeRange(0, _str.length-cubLength-1)];
        }else{
            newString = [_str substringWithRange:NSMakeRange(0, _str.length-cubLength)];
        }
        
        return newString;
    }
    
    return _str;
    
}

//根据标题获取模式
+ (NSString*)productTypeWithItemTitle:(NSString*)title {
    if ([title isEqualToString:@"生鲜蔬果"]) {
        return @"010000000";
    } else if ([title isEqualToString:@"生活百货"]) {
        return @"030000000";
    } else if ([title isEqualToString:@"粮油调料"]) {
        return @"040000000";
    } else if ([title isEqualToString:@"酒水饮料"]) {
        return @"020000000";
    } else if ([title isEqualToString:@"营养早餐"]) {
        return @"050000000";
    }
    return @"";
    
}
//根据模式获取标题
+ (NSString*)itemTitleWithProductType:(NSString*)ProductType
{
    if ([ProductType isEqualToString:@"010000000"]) {
        return @"生鲜蔬果";
    } else if ([ProductType isEqualToString:@"030000000"]) {
        return @"生活百货";
    } else if ([ProductType isEqualToString:@"040000000"]) {
        return @"粮油调料";
    } else if ([ProductType isEqualToString:@"020000000"]) {
        return @"酒水饮料";
    } else if ([ProductType isEqualToString:@"050000000"]) {
        return @"营养早餐";
    }
    return @"";
    
}
//拼接图片url
+ (NSString*)appendImageUrlWithServerUrl:(NSString*)urlStr {
    return [NSString stringWithFormat:@"%@%@",baseImageUrl,[self stringISNull:urlStr]];
}

//支付类型
+ (NSString*)transitionPayType:(NSString*)payType {
    if ([payType isEqualToString:KOrderPayTypeIsPayPal]) {
        return @"在线支付";
    } else if ([payType isEqualToString:KOrderPayTypeIsCOD]){
        return @"货到付款";
    }
    return @"";
}

//配送类型
+ (NSString*)transitionPeiSongType:(NSString*)peiSongType {
    if ([peiSongType isEqualToString:KOrderDeliverTypeIsDelivery]) {
        return @"门店配送";
    } else if ([peiSongType isEqualToString:KOrderDeliverTypeIsSelfUp]){
        return @"上门自取";
    }
    return @"";
}
@end
