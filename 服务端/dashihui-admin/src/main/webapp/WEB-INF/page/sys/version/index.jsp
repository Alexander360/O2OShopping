<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">版本信息</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-2 col-lg-10">
				<select id="kind" name="kind" class="selectpicker pull-left p-l-5" data-width="10%">
					<option value="">--请选择--</option>
					<option value="1">用户版</option>
					<option value="2">商家版</option>
				</select>
				<button type="button" class="btn btn-success pull-left m-l-5" onclick="doSearch()">查询</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div><p></p></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
					<div class="col-lg-6">
						<div class="pull-right">
						<button type="button" class="btn btn-link" onclick="toAdd()"><span class="fa fa-plus"></span> 添加</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table id="dataTable" class="table table-striped table-hover">
			            <thead>
			                <tr>
			                   <th width="8%">类别</th>
								<th width="12%">版本号</th>
								<th width="10%">种类</th>
								<th width="44%">下载地址</th>
								<th width="10%">强制升级</th>
								<th width="11%">发布时间</th>
								<th width="15%">操作</th>
			                </tr>
			            </thead>
			            <tbody id="dataList"></tbody>
			        </table>
				</div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<tr id="item{{item.id}}" data-id="{{item.id}}">
		<td>{{item.type | flagTransform:'A','安卓','B','iPhone','C','iPad'}}</td>
		<td>{{item.versionPart1}}.{{item.versionPart2}}.{{item.versionPart3}}</td>
		<td>
			{{if item.kind == 1}}商家版{{/if}}
			{{if item.kind == 2}}用户版{{/if}}
		</td>
		<td>{{item.downurl}}</td>
		<td>{{item.updateFlag | flagTransform:'1','是','0','否'}}</td>
		<td>{{item.releaseDate | dateFormat:'yyyy-MM-dd'}}</td>
		<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{item.id}}')" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{item.id}}')" title="删除"><span class="fa fa-remove fa-lg"></span></button> 
		</td>
	</tr>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/sys/version/page",{"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var addDialog;
function toAdd(){
	addDialog = Kit.dialog("添加","${BASE_PATH}/sys/version/toAdd").open();
}
var editDialog;
function toEdit(id){
	editDialog = Kit.dialog("修改","${BASE_PATH}/sys/version/toEdit/"+id).open();
}
function toDelete(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/sys/version/doDelete/"+id,function(result){
			if(result.flag==0){
				$("#item"+id).remove();
			}else{
				Kit.alert("删除失败");return;
			}
			
		});
	});
}
function doSearch(){
	var kind = $("#kind").val(),type = $("#type").val();
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/sys/version/page",{type:type,kind:kind,pageSize:10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		alert(JSON.stringify(result.object));
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
</script>
</body>
</html>