<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${goods.name}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_goods_detail.css" />
	</head>
	<body class="app-navbar-body">
		<c:choose>
		<c:when test="${goods.images!=null&&fn:length(goods.images)!=0}">
		<div class="am-slider am-slider-c3">
			<ul class="am-slides">
				<c:forEach items="${goods.images}" var="image" varStatus="status">
					<li>
						<img src="${FTP_PATH}${image.thumb}">
						<div class="am-slider-desc"><div class="am-slider-counter"><span class="am-active">${status.count}</span>/${fn:length(goods.images)}</div></div>
					</li>
				</c:forEach>
			</ul>
		</div>
		</c:when>
		<c:otherwise>
		<div class="app-image-default"><img src="${BASE_PATH}/static/app/img/default.jpg"></div>
		</c:otherwise>
		</c:choose>
		<div class="app-detail-item">
			<h4>
				<c:if test="${goods.isSelf == 1}">
					<img src="${BASE_PATH}/static/app/img/goodsdetail_self.png" style="height:1.1em;margin-top:-3px;"/>
				</c:if>
				<c:if test="${goods.isRebate == 1}">
					<img src="${BASE_PATH}/static/app/img/goodsdetail_rebate.png" style="height:1.1em;margin-top:-3px;"/>
				</c:if>
				${goods.name}
			</h4>
			
			<div class="app-detail-item-text">${goods.shortInfo}</div>
			
			月销售<span id="saleCount">${goods.saleCount}</span>份
			关注<span id="collectedCount">${goods.collectedCount}</span><br>
			<span class="app-detail-price-normal"><i class="am-icon-rmb"></i> ${goods.sellPrice}</span>
  			<span class="app-detail-price-del"><i class="am-icon-rmb"></i> ${goods.marketPrice}</span>
			<hr/>
			
			<div onclick="toRule()">
				<c:if test="${goods.isRebate == 1}">
				<div class="app-text-small-font am-fl">
					<div>
						<img src="${BASE_PATH }/static/app/img/goodsdetail_recommend_icon.png" style="height:1.2em;margin-top:-4px;"/>
						白金会员在线购买，返现${goods.percent1}%，购物即存钱
					</div>
					<div>
						<img src="${BASE_PATH }/static/app/img/goodsdetail_recommend_icon.png" style="height:1.2em;margin-top:-4px;"/>
						现在推荐好友购买，返现${goods.percent2}%，购物即理财
					</div>
				</div>
				<img src="${BASE_PATH }/static/app/img/icon_turn.png" style="height:1.2em;margin-top:0.7em;" class="am-fr"/>
				</c:if>
				<c:if test="${goods.isRebate == 0}">
				<div class="app-text-small-font am-fl" style="height:30px;line-height:30px;">
					<img src="${BASE_PATH }/static/app/img/goodsdetail_recommend_icon.png" style="height:1.2em;margin-top:-4px;"/>
					推荐好友购买大实惠直营商品，最高返现15%，去推荐
				</div>
				<img src="${BASE_PATH }/static/app/img/icon_turn.png" style="height:1.2em;margin-top:0.4em;" class="am-fr"/>
				</c:if>
				<div class="am-cf"></div>
			</div>
			
		</div>
		
		<!-- 暂时隐藏，2016-02-25修改 -->
		<div class="app-detail-item app-m-t-10" style="display:none;">
			<label>品牌：</label>
			<span class="app-detail-item-text">${goods.brandName}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label>规格：</label>
			<span class="app-detail-item-text">${goods.spec}</span>
		</div>
		
		<c:if test="${goods.hasDescribe==1}">
		<div id="describeLoader" class="app-detail-item app-m-t-10">
			<a href="javascript:loadDescribe();" class="am-text-center">
				<span><i class="am-header-icon am-icon-chevron-up"></i> 点击查看图文详情</span>
			</a>
		</div>
		
		<style type="text/css">
			.app-goods-describe{
				font-size:1.4rem;
				padding:10px;
				background-color:#fff;
			}
			.app-goods-describe img{
				width:100%;
			}
			/*去掉label、h和p标签的margin*/
			.app-goods-describe label,.app-goods-describe h1, .app-goods-describe h2, .app-goods-describe h3, .app-goods-describe h4, .app-goods-describe h5, .app-goods-describe h6, .app-goods-describe p{
				margin:0;
			}
		</style>
		<div id="describePanel" class="app-goods-describe app-m-t-10" style="display:none;"></div>
		</c:if>
		
		<div class="am-navbar am-cf am-no-layout app-navbar" style="z-index:1;">
			<div class="am-navbar-nav am-cf am-g">
				<div class="am-u-sm-3 am-u-md-4  am-u-lg-5">
					<a id="collectBtn" href="javascript:doCollect();">
						<c:choose>
						<c:when test="${goods.isCollected==1}">
						<i class="am-header-icon am-icon-heart" style="color: #AA1C03;"></i>
						</c:when>
						<c:otherwise>
						<i class="am-header-icon am-icon-heart-o"></i>
						</c:otherwise>
						</c:choose>
						<span class="am-navbar-label">关注</span>
					</a>
				</div>
				<div class="am-u-sm-3 am-u-md-4  am-u-lg-5">
					<a id="cartBtn" href="${BASE_PATH}/cart">
						<i class="am-header-icon am-icon-shopping-cart"></i>
						<span class="am-navbar-label">购物车</span>
						<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
					</a>
				</div>
				<div class="am-u-sm-6 am-u-md-4  am-u-lg-2">
					<a id="addCartBtn" href="javascript:addCart();">
						<span class="am-navbar-label">加入购物车</span>
					</a>
				</div>
			</div>
		</div>
		<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
		<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:42%;bottom:20px;z-index:2;display:none;">+1</span>

		<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/history.js"></script>
		<script type="text/javascript">
			var goods = {
				"id":"${goods.id}",
				"isSelf":"${goods.isSelf}",
				"name":"${goods.name}",
				"spec":"${goods.spec}",
				"thumb":"${goods.thumb}",
				"sellPrice":"${goods.sellPrice}",
				"marketPrice":"${goods.marketPrice}"
			};
			$(function(){
				History.save(goods);
				//轮播图
				$('.am-slider').flexslider({directionNav:false});
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			function doCollect(){
				Kit.ajax.post("${BASE_PATH}/my/doCollect",{goodsid:goods.id,isSelf:goods.isSelf},function(result){
					if(result.flag==0){
						var collectedCount = parseInt($("#collectedCount").text());
						if(result.object.isCancel==1){
							$("#collectBtn i").removeClass("am-icon-heart").addClass("am-icon-heart-o");
							$("#collectBtn i").removeAttr("style");
							$("#collectedCount").html(collectedCount-1);
						}else{
							$("#collectBtn i").removeClass("am-icon-heart-o").addClass("am-icon-heart");
							$("#collectBtn i").attr("style","color: #AA1C03;");
							$("#collectedCount").html(collectedCount+1);
						}
					}
				});
			}
			function addCart(){
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":20+"px","opacity":100}).hide();
					});
				});
			}
			function toRule(){
				Kit.render.redirect("${BASE_PATH}/my/rule");
			}
		</script>
		<c:if test="${goods.hasDescribe==1}">
		<script type="text/javascript">
			function loadDescribe(){
				Kit.ajax.get("${BASE_PATH}/goods/describe/${goods.id}",function(result){
					if(result.flag==0&&!Kit.validate.isBlank(result.object.describe)){
						//展示商品详情描述
						$("#describePanel").append(result.object.describe);
					} else {
						//没有详情描述时，展示宣传图片
						$("#describePanel").append("<img src='${BASE_PATH}/static/app/img/goods_no_describe.jpg'/>");
					}
					//为详情描述中的图片添加延迟加载
					$("img","#describePanel").each(function(index,img){
						$(img).attr("data-original",$(img).attr("src")).addClass("lazyload").removeAttr("src");
					}).lazyload({
						failurelimit : 20,
						effect : "show"
					});
					$("#describePanel").show();
					$("#describeLoader").remove();
				});
			}
		</script>
		</c:if>
	</body>
</html>