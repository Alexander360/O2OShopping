package cn.com.dashihui.seller.service;

import cn.com.dashihui.seller.dao.Seller;

public class UserService {
	
	/**
	 * 查询指定用户名的用户信息，用于登录密码验证或验证指定用户名用户是否存在，所以只查询用户名和密码字段
	 */
	public Seller findByUsername(String username){
		return Seller.me().findFirst("SELECT * FROM t_dict_store_seller WHERE username=?",username);
	}
	
	/**
	 * 用户信息更新
	 */
	public boolean update(Seller user){
		return user.update();
	}
}
