//
//  MyShiHuiBiHeadView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyShiHuiBiHeadView : UIView
-(void)setSubViewDataWithMoneyMun:(NSString *)money;
@end
