package com.dashihui.distribution.common.base;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.dashihui.distribution.common.AffRequestCallBack;
import com.dashihui.distribution.ui.widget.WdtProDialog;
import com.dashihui.distribution.utils.network.ConnectivityReceiver;
import com.dashihui.distribution.utils.preferences.UtilPreferences;
import com.dashihui.distribution.utils.string.UtilString;
import com.gprinter.aidl.GpService;
import com.gprinter.io.PortParameters;
import com.gprinter.save.PortParamDataBase;


public abstract class BaseActivity extends Activity implements AffRequestCallBack {
	protected GpService mGpService = null;
	private PrinterServiceConnection conn = null;
	private String DEBUG_TAG = "BaseActivity";
	protected WdtProDialog mProDialog =null;
	private final static String TAG = "BaseActivity";
	public static ConnectivityReceiver mConnReceiver = null;
	protected PortParameters mPortParam[] = new PortParameters[1];

	private class PrinterServiceConnection implements ServiceConnection {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.i("ServiceConnection", "onServiceDisconnected() called");
			mGpService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mGpService = GpService.Stub.asInterface(service);
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//连接服务
		connection();
		initPortParam();
	}
	protected void connection() {
		conn = new PrinterServiceConnection();
		Intent intent = new Intent();
		intent.setAction("com.gprinter.aidl.GpPrintService");
		intent.setPackage(getPackageName());
		this.bindService(intent, conn, Context.BIND_AUTO_CREATE); // bindService
		startService(intent);
	}
	/**
	 * 初始化打印设备
	 */
	private void initPortParam() {
			PortParamDataBase database = new PortParamDataBase(this);
			mPortParam[0] = new PortParameters();
			mPortParam[0] = database.queryPortParamDataBase("" + 0);
			mPortParam[0].setPortOpenState(false);  //只使用第0个打印机
			mPortParam[0].setPortType(PortParameters.BLUETOOTH);
		String address = UtilPreferences.getString(this, Contracts.BLUETOOTH_ADDRESS,null);
		if(!UtilString.isEmpty(address)) {
			mPortParam[0].setBluetoothAddr(address);
		}
		}
	/**
	 * 显示过渡加载框
	 * @param context
	 */
	protected void showProDialog(Context context){
		if (mProDialog != null && mProDialog.isShowing()) {
			return;
		}
		if (mProDialog == null){
			mProDialog = WdtProDialog.createDialog(context);
		}
		mProDialog.show();
	}

	protected void dissProDialog(){
		if (mProDialog!=null && mProDialog.isShowing()){
			mProDialog.dismiss();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// TODO 腾讯统计
//		StatService.onResume(this);

		if(mConnReceiver==null){
			mConnReceiver = new ConnectivityReceiver(this);
			mConnReceiver.setOnNetworkAvailableListener(new ConnectivityReceiver.OnNetworkAvailableListener() {
				
				@Override
				public void onNetworkUnavailable() {
					// TODO Auto-generated method stub
					Log.i(TAG, "======>已断开与互联网的连接");
//					Toast.makeText(getApplicationContext(), R.string.home_no_net, Toast.LENGTH_SHORT).show();
				}
				
				@Override
				public void onNetworkAvailable() {
					// TODO Auto-generated method stub
					//Toast.makeText(getApplicationContext(), "0网络恢复正常啦", Toast.LENGTH_LONG).show();
					Log.i(TAG, "======>网络恢复正常啦");
//					onResume();
				}
			});
		}
		mConnReceiver.bind(this);
	}


		@Override
        protected void onPause() {
            // TODO Auto-generated method stub
            if (mConnReceiver != null) {
                mConnReceiver.unbind(this);
                mConnReceiver = null;
            }
            		super.onPause();

        }
	@Override
	protected void onDestroy() {
		if (conn != null) {
			Log.i(DEBUG_TAG, "关闭服务");
			unbindService(conn); // unBindService
		}

		super.onDestroy();
	}

}
