//
//  MyGoodsOderTrackingController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsOderTrackingController
 Created_Date： 20160315
 Created_People：GT
 Function_description： 商品订单追踪视图
 ***************************************/

#import "BaseViewController.h"

@interface MyGoodsOderTrackingController : BaseViewController
@property(nonatomic, strong)NSString *orderNumber;//订单号
@property(nonatomic, strong)NSString *payType;
@property(nonatomic, strong)NSString *peiSongType;
@end
