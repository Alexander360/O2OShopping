package com.dashihui.afford.alipay;

public class PayConstants {


  // 商户PID
  public static final String PARTNER = "2088121362816064";
  // 商户收款账号
  public static final String SELLER = "dashihui_IT@163.com";
  // 商户私钥，pkcs8格式
  public static final String RSA_PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMj7KJ98JGB0D5lxd6o7HL8aJrHbGyOjxKexq8DOI+5Oo8QwbhXxExZLO0e7NQdxxT1q/F3zhkQ+uWIS3VHgha7I0jO54XjoNhVu1gNNYQXYkuxgSfRD5MdUlIUl/6WazSLXsOnbX5ndMHUHvmGVXvBpxrGshMSYid5gR1QKyeVJAgMBAAECgYA6FTu8O6hXp7MKaVxn+gVoxSoBpIzHVAD1Zcd5RRnlORzjCgwGk1Es1DkhldzRdfPGuDCPBl37QxVYXpNPok9/m+f/xkXVTNS6I+9kZKvxo3G/NgMgf1HXirwwyX5aqnake/FTPqmWzmJGNQc/7ex1a2wUl2sH5Q+FnnbFVMBsAQJBAP1scbA8ZnQ61ZtHfaND/nbLCL8gMBTsuG/1ofAehYZ2PeecAgPN7+3H6lspAFqjVrU/Ql//fHr/hnm8T4rTPCkCQQDLBjqE1poROwGkZBOIDC7pflMB9np1i/8m9m0N/Z2bwXi3xmneo+sHxvxv9NiywL7ZiypDmtz01kTlkIj3eoQhAkBUsjygyN7483zWY6NCwIO4Eg0/SzOmmqLdkDtp89luYfA4ivRFbztDve++PcrHgBWHh7fYO5vPw7yezOfMnuJZAkANJCARjRp31wvqKhghjDhVILoavGt8xE0Fppm0eCAFfuZjbtJXeOCzpiucy4pjN5qi1iEsMw/uQRhAi7Rx4FpBAkEAhD/OK3TKn5ExitxPzkzifuAD0nufwiYNFFse5C7zymtRvdxxHVuDco/FvXzI+Q30QmNxZdhqwyA+lT8m9Hcfwg==";
  // 支付宝公钥
//  public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
  public static final int SDK_PAY_FLAG = 1;
  public static final int SDK_CHECK_FLAG = 2;

}
