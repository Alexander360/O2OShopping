package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.OrderEvalService;

@RequiresAuthentication
public class OrderEvalController extends BaseController{
	private OrderEvalService service = new OrderEvalService();
    
    public void index(){
        render("index.jsp");
    }
    
    public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(getStoreid(), pageNum, pageSize));
	}
}
