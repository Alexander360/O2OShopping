<%@page import="cn.com.dashihui.wx.common.UserCode"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>会员奖励</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_rule.css" />
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">会员奖励</h1>
		</header>
		<div class="app-part">
			<div class="app-part-title app-m-b-10">一、成为白金会员可享受购物返现</div>
			<div class="app-part-content app-m-b-10">
				成为白金会员条件：购买大实惠直营商品累计达到${level2Line}元，系统会自动将您升级到白金会员，
				成为白金会员后，您即可享受购物返现活动。
			</div>
			<div class="app-part-title app-m-b-10">二、白金会员奖励有哪些</div>
			<div class="app-part-content app-m-b-10">
				白金会员购买直营商品，最高可返现15%，推荐好友购买直营商品，最高可返现10%，购物即存钱，购物即理财。
			</div>
			<div class="app-part-title app-m-b-10">三、答谢老会员，推荐好友购物也可享受返现</div>
			<div class="app-part-content app-m-b-10">
				当您成为白金会员后，点击分享给好友，好友通过你的链接邀请码注册成功后，每次购买直营商品，您都可以得到返现，推荐的好友越多，
				您的返现也就越多，不购物也能返现，边消费边理财，轻松成为持家小能手。
			</div>
			<div class="app-part-title app-m-b-10">四、举例说明</div>
			<div class="app-part-content app-m-b-10">
				<div>某白金会员购物100元，返现如果是10%，确认收货后该会员可获得100元×10%=10元的返现。</div>
				<div>注：实惠币将在您确认收货后，自动计入我的钱包，1实惠币等于1元。</div>
			</div>
			<div class="app-part-title app-m-b-10">五、会员等级标准</div>
			<div class="app-part-content app-m-b-10">
				<div>-普通用户：注册大实惠；</div>
				<div>-白金会员：直营商品累计有效消费${level2Line}元；</div>
				<div>注：运费、退货费，退换差价费，不包括在累计购物金额中。非直营返现商品参与活动，仅支持在线支付。</div>
			</div>
			<div class="app-copyright">
				本次活动最终解释权归大实惠所有
			</div>
		</div>
		
		<div style="width:100%;padding:5px;">
			<a href="javascript:toShare();" class="am-btn am-btn-success app-m-t-10 am-radius" style="width:100%;">查看分享页，邀请好友注册</a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript">
		function toShare(){
			if('${currentUser.level}'=='<%=UserCode.UserLevel.LEVEL2%>'){
				Kit.render.redirect("${BASE_PATH}/my/share");
			}else{
				Kit.ui.alert("抱歉，您暂不满足条件");
			}
		}
		</script>
	</body>
</html>