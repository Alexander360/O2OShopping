package cn.com.dashihui.wx.interceptor;

import com.jfinal.aop.Invocation;

import cn.com.dashihui.wx.common.Constants;
import cn.com.dashihui.wx.controller.BaseController;
import cn.com.dashihui.wx.dao.Store;

/**
 * 拦截用户请求，判断如果是首次访问，则重定向至微信用户授权
 */
public class TestInterceptor extends AjaxInterceptor{
	
	@Override
	public void onAjax(Invocation inv) {
		onAjaxNope(inv);
	}
	
	@Override
	public void onAjaxNope(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		Object inited = c.getSessionAttr("inited");
		if(inited==null){
//			c.setSessionAttr(Constants.WX_OAUTH, 1);
			c.setSessionAttr(Constants.STORE, Store.me().findFirst("SELECT * FROM t_dict_store WHERE id=11"));
			c.setSessionAttr("inited",true);
		}
		inv.invoke();
	}
}
