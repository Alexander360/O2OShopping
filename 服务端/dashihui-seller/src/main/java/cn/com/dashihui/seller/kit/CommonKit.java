package cn.com.dashihui.seller.kit;

import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;

import com.jfinal.kit.StrKit;

public class CommonKit {
	
	private static final String UNKNOWN = "unknown";
	private static final String LOCALHOST = "0:0:0:0:0:0:0:1";
	/**
	 * 根据请求获取客户端真实ip
	 * @param request
	 * @return
	 */
	public static String getClientIp(HttpServletRequest request){
		String ip = request.getHeader("x-forwarded-for");
		if(StrKit.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)){
			ip = request.getHeader("Proxy-Client-IP");
		}
		if(StrKit.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)){
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if(StrKit.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)){
			ip = request.getHeader("HTTP_CLIENT_IP");		
		}
		if(StrKit.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)){
			ip = request.getHeader("X_FORWARDED_FOR");		
		}
		if(StrKit.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)){
			ip = request.getRemoteAddr();
		}
		if(LOCALHOST.equals(ip)){
			return "127.0.0.1";
		}else{
			int index = ip.indexOf(",");
			if(index >= 0){
				ip = ip.split(",")[0];
			}
			return ip;
		}
	}
	
	/**
	 * 生成指定位数的随机数字
	 */
	public static String randomNum(int len){
		StringBuffer random = new StringBuffer(len);
		for(int i = 0; i < len; i++){
			random.append((int)(Math.random() * 10));
		}
		return random.toString();
	}
	
	/**
	 * 生成4位随机数字
	 */
	public static String randomNum(){
		return randomNum(4);
	}
	
	/**
	 * 获取UUID
	 */
	public static String getUUID(){
		String s = UUID.randomUUID().toString(); 
        //去掉“-”符号 
        return s.substring(0,8)+s.substring(9,13)+s.substring(14,18)+s.substring(19,23)+s.substring(24); 
	}
	
	/**
	 * 判断入参是否为空，如果为空，则返回空串，如果不为空，则返回本身，模仿oracle中的nvl函数，或mysql中的ifnull函数
	 */
	public static String ifNull(String value){
		return ifNull(value,"");
	}
	
	/**
	 * 判断入参是否为空，如果为空，则返回设定的串，如果不为空，则返回本身，模仿oracle中的nvl函数，或mysql中的ifnull函数
	 */
	public static String ifNull(String value,String nullStr){
		if(StrKit.isBlank(value)){
			return nullStr;
		}
		return value;
	}
	
	/**
	 * 将入参strs以字符串split为分隔进行拼接
	 */
	public static String join(String split,String... strs){
		if(strs!=null&&strs.length!=0){
			StringBuffer sBuffer = new StringBuffer();
			for(String str : strs){
				sBuffer.append(str).append(split);
			}
			sBuffer.deleteCharAt(sBuffer.length()-1);
			return sBuffer.toString();
		}
		return null;
	}
	
	public static String join(String split,int... ints){
		if(ints!=null&&ints.length!=0){
			StringBuffer sBuffer = new StringBuffer();
			for(int str : ints){
				sBuffer.append(str).append(split);
			}
			sBuffer.deleteCharAt(sBuffer.length()-1);
			return sBuffer.toString();
		}
		return null;
	}
	
	/**
	 * 密码加密
	 */
	public static String encryptPassword(String passwordStr){
		return DigestUtils.md5Hex(passwordStr);
	}
	
	/**
	 * 密码对比
	 */
	public static boolean passwordsMatch(String passwordStr, String encrypted){
		if(!StrKit.isBlank(passwordStr)&&!StrKit.isBlank(encrypted)){
			return encryptPassword(passwordStr).equalsIgnoreCase(encrypted);
		}
		return false;
	}
	
	/**
	 * 处理手机号码，隐藏中间四位数字
	 */
	public static String hideMsisdn(String msisdn){
		if(!StrKit.isBlank(msisdn)&&ValidateKit.Mobile(msisdn)){
			return msisdn.substring(0,3)+"****"+msisdn.substring(7);
		}
		return null;
	}
	
	 /**
     * 获取一定长度的随机字符串
     * @param length 指定字符串长度
     * @return 一定长度的字符串
     */
    public static String getRandomStringByLength(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
    
    /**
     * url encode
     */
    public static String encodeURL(String src){
    	if(StrKit.isBlank(src)){
    		return "";
    	}
    	try {
    		return URLEncoder.encode(src, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return src;
    }
}
