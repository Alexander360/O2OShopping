package cn.com.dashihui.api.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultField;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.service.UserService;

/**
 * 验证请求中是否携带有用户登录标识，如果有，则查询出对应的用户信息，供接下来controller使用，如果没有，则不处理
 */
public class WhenLoginInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		String sessionid = c.getPara(ResultField.FIELD_USER_LOGIN_TOKEN);
		if(!StrKit.isBlank(sessionid)&&sessionid.length()==36){
			//根据SESSIONID查询出相应会话的登录用户基本信息
			User currUser = new UserService().findUserInfoBySessionid(sessionid);
			if(currUser!=null){
				//判断正确后，放入Controller的request中，以方便Controller中取出来用
				c.setAttr("sessionid", sessionid);
				c.setAttr(PropKit.get("constants.currentUserKey"), currUser);
			}
		}
		inv.invoke();
	}
}
