<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-offset-6 col-lg-6">
				<div class="pull-right">
					<button type="button" id="toAddGoodsImagesBtn" class="btn btn-link" onclick="toAddImages()"><span class="fa fa-plus"></span> 添加</button>
					<button type="button" class="btn btn-link" onclick="doSortImages()"><span class="fa fa-save"></span> 保存排序</button>
				</div>
			</div>
		</div>
		<hr/>
		<form id="sortImagesForm" action="<c:url value="/bus/goods/store/doImageSort"/>" method="post">
		<input type="hidden" name="sortKey" value="orderNo">
		<div id="imagesDataList" class="row" style="min-height:300px;"></div>
		</form>
		<hr/>
		<span class="p-t-5 text-success">*详情页顶部图片，建议最多上传5张，尺寸为正方形，设置为封面的图片不可删除，请先设置其他图片为LOGO再删除</span>
	</div>
</div>
<script type="text/html" id="imagesDataTpl">
	<div id="item{{id}}" class="col-lg-4 col-md-4 col-sm-4">
		<div class="thumbnail">
			{{if !link}}
			<a href="#"><img src="${FTP_PATH}{{thumb}}"></a>
			{{else}}
			<a href="{{link}}" target="_blank"><img src="${FTP_PATH}{{thumb}}"></a>
			{{/if}}
			<div class="caption">
				<input name="orderNo{{id}}" value="{{orderNo}}" size="3" title="排序" class="order form-control width50 pull-left" required>
				{{if isLogo==0}}
				<button type="button" class="btn btn-sm btn-danger pull-right" onclick="toDeleteImages('{{id}}')" title="删除"><span class="fa fa-remove fa-lg"></span> 删除</button>
				<button type="button" class="btn btn-sm btn-success pull-right m-r-10" onclick="doSetImageLogo('{{id}}')" title="设置为商品LOGO"><span class="fa fa-cog fa-lg"></span> 封面</button>
				{{/if}}
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</script>
<script type="text/javascript">
$(loadImages);
var imagesSize = 0;
function loadImages(){
	$("#imagesDataList").empty();;
	$.post("${BASE_PATH}/bus/goods/store/imagePage/${goodsid}",function(result){
		if(result.flag==0){
			$(result.object.reverse()).each(function(i,o){
				onAddImagesSuccess(o);
			});
		}
	},"json");
}
var addImagesDialog;
function toAddImages(){
	addImagesDialog = Kit.dialog("添加","${BASE_PATH}/bus/goods/store/toImageAdd/${goodsid}").open();
}
function onAddImagesSuccess(newObject){
	imagesSize++;
	$("#imagesDataList").prepend(template("imagesDataTpl",newObject));
	if(imagesSize>=5){
		$("#toAddGoodsImagesBtn").prop("disabled",true);
	}
}
function doSetImageLogo(id){
	Kit.confirm("提示","您确定要设置为商品LOGO？",function(){
		$.post("${BASE_PATH}/bus/goods/store/doSetImageLogo/"+id,{goodsid:"${goodsid}"},function(result){
			//更新商品列表中的商品LOGO
			onLogoSet('${goodsid}',$("#item"+id+" img").attr("src"));
			loadImages();
		});
	});
}
function toDeleteImages(id){
	Kit.confirm("提示","您确定要删除这条数据吗？",function(){
		$.post("${BASE_PATH}/bus/goods/store/doImageDelete/"+id,function(result){
			$("#item"+id).remove();
			imagesSize--;
			$("#toAddGoodsImagesBtn").prop("disabled",false);
		});
	});
}
function doSortImages(){
	$("#sortImagesForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				loadImages();
			}
		}
	});
}
</script>