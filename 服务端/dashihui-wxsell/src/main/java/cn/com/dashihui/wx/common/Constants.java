package cn.com.dashihui.wx.common;

public class Constants {
	//当前登录的用户信息
	public static String USER = "USER";
	//当前访问微商城的微信用户信息（信息来自微信）
	public static String WX_USER = "WX_USER";
	//首次访问时会去请求用户进行授权，请求过后会设置标识，以表示已经请求过
	public static String WX_OAUTH = "WX_OAUTH";
	//授权过程中使用的验证码
	public static String WX_OAUTH_VALIDCODE = "WX_OAUTH_VALIDCODE";
	
	//当前选择或定位的店铺
	public static String STORE = "STORE";
}
