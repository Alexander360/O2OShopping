<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		messages:{
			title:    {required: "请输入名称"},
			baidukey: {required: "请输入百度定位代码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 3:
						Kit.alert("请选择地市");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editDialog.close();
						return;
					}
				}
			});
		}
	});
});
</script>
<form id="editForm" action="${BASE_PATH}/dict/community/doEdit" method="post" class="form-horizontal" >
	<div class="form-group">
	    <label class="col-lg-2 control-label">名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="title" value="${object.title}" class="form-control" placeholder="请输入社区名称" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label">地址</label>
	    <div class="col-lg-9">
    		<select id="province" name="province" class="selectpicker" data-width="33%" data-url="${BASE_PATH}/api/province" data-val="${object.province}" data-isfirst="true" data-next="#city" data-key="id:name" required></select>
			<select id="city" name="city" class="selectpicker" data-width="33%" data-url="${BASE_PATH}/api/city/{value}" data-val="${object.city}" data-next="#area" data-key="id:name" required></select>
			<select id="area" name="area" class="selectpicker" data-width="32%" data-url="${BASE_PATH}/api/area/{value}" data-val="${object.area}" data-key="id:name" required></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label"></label>
	    <div class="col-lg-9">
        	<input type="text" name="address" value="${object.address}" class="form-control" placeholder="请输入详细地址" maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">定位代码</label>
	    <div class="col-lg-9">
        	<input type="text" name="baidukey" value="${object.baidukey}" class="form-control" placeholder="请输入百度定位代码" required>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="communityid" value="${object.id}"/>
</form>