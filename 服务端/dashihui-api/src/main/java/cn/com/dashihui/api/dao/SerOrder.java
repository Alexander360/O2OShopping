package cn.com.dashihui.api.dao;

import com.jfinal.plugin.activerecord.Model;

/**
 * 家政类服务订单对象
 */
public class SerOrder  extends Model<SerOrder>{
	private static final long serialVersionUID = 1L;
	private static SerOrder me = new SerOrder();
	public static SerOrder me(){
		return me;
	}
}
