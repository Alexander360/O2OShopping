javascript里有一个事件是滚动事件，只要拖动滚动条，就会触发事件。

用jquery的话，这个事件scroll 可以查看jquery api ：http://api.jquery.com/scroll/

但scroll 事件有一个缺陷，就是只能判断滚动条滚动，而不能监控滚动条停止滚动时的事件。

用法：
(function(){
    jQuery(window).bind('scrollstart', function(){
        console.log("start");
    });
 
    jQuery(window).bind('scrollstop', function(e){
        console.log("end");
    });
 
})();