//
//  AboutUsController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//


#import "AboutUsController.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
#import "MTA.h"
#import "Global.h"
@interface AboutUsController ()

@end

@implementation AboutUsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于大实惠";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    [self setUpSuberView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction)];
    tap.numberOfTapsRequired = 9;
    [self.view addGestureRecognizer:tap];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"关于大实惠"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"关于大实惠"];
}
- (void)tapAction
{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:baseServerUrl
                                                  delegate:self
                                         cancelButtonTitle:@"确定"
                                         otherButtonTitles:nil];
    [alert show];
    
}
-(void)setUpSuberView {
    self.contentLabel.textColor = [UIColor colorWithHexString:@"#161616"];
    self.versionLabel.textColor = [UIColor colorWithHexString:@"#161616"];
    [self.servetItemsBtn setTitleColor:[UIColor colorWithHexString:@"#161616"] forState:UIControlStateNormal];
    [self.yinDaoYeBtn setTitleColor:[UIColor colorWithHexString:@"#161616"] forState:UIControlStateNormal];
    self.companyLabel.textColor = [UIColor colorWithHexString:@"#8d8d8d"];
    self.banQuanLabel.textColor = [UIColor colorWithHexString:@"#8d8d8d"];
    [self.versionLabel.layer setMasksToBounds:YES];
    [self.versionLabel.layer setBorderWidth:1];
    [self.versionLabel.layer setBorderColor:[UIColor colorWithHexString:@"#e1e1e1"].CGColor];
    [self.versionLabel.layer setCornerRadius:16];
    self.versionLabel.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    self.versionLabel.text = [NSString stringWithFormat:@"V %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
#pragma mark -- button Action
- (IBAction)lookupServerItemsEvent:(id)sender {
    
}

- (IBAction)lookWelcomPageEvent:(id)sender {
    
}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
