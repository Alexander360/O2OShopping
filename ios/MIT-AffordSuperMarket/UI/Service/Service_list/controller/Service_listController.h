//
//  Service_listController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： Service_listController
 Created_Date： 20151103
 Created_People： JSQ
 Function_description：服务列表
 ***************************************/
#import "BaseViewController.h"

@interface Service_listController : BaseViewController
@property (nonatomic,strong)NSString *code;
@property (nonatomic,strong)NSString *titles;
@end
