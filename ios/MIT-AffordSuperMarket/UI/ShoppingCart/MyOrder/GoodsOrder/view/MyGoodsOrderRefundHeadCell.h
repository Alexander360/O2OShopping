//
//  MyGoodsOrderRefundHeadCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyGoodSOrderRefundModel.h"
@interface MyGoodsOrderRefundHeadCell : UITableViewCell
- (void)updateViewWithData:(MyGoodSOrderRefundModel*)refundModel;
@end
