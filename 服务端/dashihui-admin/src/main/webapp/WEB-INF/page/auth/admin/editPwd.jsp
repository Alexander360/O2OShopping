<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editPwdForm").validate({
		rules:{
			passwordConfirm: {
				equalTo: "#passwordOld"
			}
		},
		messages:{
			password: {required: "请输入密码"},
			passwordConfirm: {required: "请再次输入密码",equalTo: "两次密码输入不相同"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editPwdDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editPwdForm" action="${BASE_PATH}/auth/admin/doEditPwd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="${object.username}" class="form-control" disabled="disabled">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordOld" name="password" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">确认密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordConfirm" name="passwordConfirm" value="" class="form-control" placeholder="请再次输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editPwdDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="userid" value="${object.id}"/>
</form>