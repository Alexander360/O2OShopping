//
//  Service_Root_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： Service_Root_Controller
 Created_Date： 20151103
 Created_People： JSQ
 Function_description：服务root页
 ***************************************/
#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, ServiceEmptyPageType) {
    knoBigClass = 1,//无商品大类
    knoBusinessList = 2 ,//无商家列表
    knoShowEmptyPage //有数据
};
@interface Service_Root_Controller : BaseViewController

@end
