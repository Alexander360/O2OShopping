//
//  UserCenter_FeedbackAndSuggestionController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#import "UserCenter_FeedbackAndSuggestionController.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+Conversion.h"
//vendor
#import "UIPlaceHolderTextView.h"
#import "UITextField+Space.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
#import "ManagerHttpBase.h"
#import "ManagerGlobeUntil.h"
#import "MTA.h"
//controller
#import "SureOrderController.h"
#import "MyGoodsSureOrderController.h"

@interface UserCenter_FeedbackAndSuggestionController ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UITextViewDelegate>
@property (nonatomic,strong)UIPlaceHolderTextView *suggestionTextView;
@property (nonatomic,strong)UITextField *contactInformation;//输入框
@property (nonatomic, strong) NSString *placeStr; //文本框 提示信息
@end

@implementation UserCenter_FeedbackAndSuggestionController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    [self addRightNavItemWithTitle:@"提交"];
    [self setUpSuberView];
    [self setTabGesture];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"反馈和建议"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"反馈和建议"];
}

#pragma mark -- request Data
//反馈建议
-(void)loadFeedbackData {
    __weak typeof(self)weakSelf = self;
    //请求参数说明： SIGNATURE  设备识别码
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager]getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    [parameter setObject:self.suggestionTextView.text forKey:@"CONTEXT"];
    [manager parameters:parameter customPOST:@"common/feedback" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"感谢您的建议~" inView:weakSelf.view];
                
                [weakSelf performSelector:@selector(backUserViewController) withObject:nil afterDelay:2.0f];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {

    }];

}
-(void)backUserViewController
{
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[SureOrderController class]] || [VC isKindOfClass:[MyGoodsSureOrderController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark--初始化控件
-(void)setUpSuberView
{
    _suggestionTextView =[[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, HightScalar(140))];
    _suggestionTextView.textColor=[UIColor blackColor];
    
    
    self.suggestionTextView.delegate=self;
    self.suggestionTextView.text = self.hadSavedMarkInfo;
    self.suggestionTextView.backgroundColor=[UIColor whiteColor];
    self.suggestionTextView.returnKeyType=UIReturnKeyDefault;
    self.suggestionTextView.keyboardType=UIKeyboardTypeDefault;
    [self.suggestionTextView.layer setMasksToBounds:YES];
    self.suggestionTextView.font = [UIFont systemFontOfSize:FontSize(17)];

    _suggestionTextView.placeholder=self.placeStr;
    [self.view addSubview: self.suggestionTextView];

    
    if (self.feedbackOrRemarksType == kFeedbackType) {
        _contactInformation=[[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.suggestionTextView.frame), CGRectGetMaxY(self.suggestionTextView.frame)+10, VIEW_WIDTH, HightScalar(46))];
        _contactInformation.placeholder=@"输入您的邮箱或电话";
        _contactInformation.clearButtonMode = UITextFieldViewModeWhileEditing;
        _contactInformation.delegate = self;
        _contactInformation.backgroundColor=[UIColor whiteColor];
        _contactInformation.font = [UIFont systemFontOfSize:15];
//        [_contactInformation.layer setMasksToBounds:YES];
//        [_contactInformation.layer setBorderColor:[UIColor grayColor].CGColor];
//        [_contactInformation.layer setBorderWidth:1];
//        [_contactInformation.layer setCornerRadius:2];
        [_contactInformation setDirection:kLeftEdge edgeSpace:5];

        [self.view addSubview:_contactInformation];
    }
    UILabel *phonenumber=[[UILabel alloc]initWithFrame:CGRectMake(5,VIEW_HEIGHT-40-64, VIEW_WIDTH/2, HightScalar(20))];
    phonenumber.font=[UIFont systemFontOfSize:FontSize(13)];
    NSString *contentStr = @"客服电话：0371-86563519";
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:contentStr];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#000000"] range:NSMakeRange(0, 5)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#c52720"] range:NSMakeRange(5, str.length-5)];
    phonenumber.attributedText=str;
    phonenumber.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:phonenumber];
   
    UILabel *time=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(phonenumber.frame),CGRectGetMinY(phonenumber.frame), VIEW_WIDTH/2,  HightScalar(20))];
    time.text=@"服务时间：9:00-17:30";
    time.font=[UIFont systemFontOfSize:FontSize(13)];
    time.textColor=[UIColor colorWithHexString:@"#555555"];
    time.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:time];
   
}
#pragma mark -- buttonAction
- (void)backBtnAction:(id)sender{
    if (self.feedbackOrRemarksType == kRemarksType) {//添加备注
        for (UIViewController*VC in self.navigationController.childViewControllers) {
            if ([VC isKindOfClass:[SureOrderController class]] ) {
                ((SureOrderController*)VC).markContent = self.suggestionTextView.text;
                [self.navigationController popViewControllerAnimated:YES];
            } else if ([VC isKindOfClass:[MyGoodsSureOrderController class]]) {
                ((MyGoodsSureOrderController*)VC).markContent = self.suggestionTextView.text;;
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        
    } else if(self.feedbackOrRemarksType == kFeedbackType) {//反馈建议
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (void)rigthBtnAction:(id)sender {
    [self hideKeyboard];
    if (self.feedbackOrRemarksType == kRemarksType) {//添加备注
        for (UIViewController*VC in self.navigationController.childViewControllers) {
            if ([VC isKindOfClass:[SureOrderController class]]) {
                ((SureOrderController*)VC).markContent = self.suggestionTextView.text;
                [self.navigationController popViewControllerAnimated:YES];
            } else if ([VC isKindOfClass:[MyGoodsSureOrderController class]]) {
                ((MyGoodsSureOrderController*)VC).markContent = self.suggestionTextView.text;
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        
    } else if(self.feedbackOrRemarksType == kFeedbackType) {//反馈建议
        if (self.suggestionTextView.text.length != 0) {
            [self loadFeedbackData];
        }
    }
    
}

#pragma mark -- 键盘手势
-(void)setTabGesture
{
    //隐藏键盘手势
    UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tabGesture.delegate = self;
    [self.view addGestureRecognizer:tabGesture];
}
#pragma mark -- 隐藏键盘
//隐藏键盘
- (void)hideKeyboard {
    if ([self.suggestionTextView isFirstResponder])
    {
        [self.suggestionTextView resignFirstResponder];
    }
    if ([self.contactInformation isFirstResponder])
    {
        [self.contactInformation resignFirstResponder];
    }
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.suggestionTextView.isFirstResponder || self.contactInformation.isFirstResponder) {
        return YES;
    }
    return NO;
}
#pragma mark --根据 需要的页面 设置文本框提示信息;
- (void)setFeedbackOrRemarksType:(FeedbackOrRemarksType)feedbackOrRemarksType {
    NSString *navigationItemTitle = @"";
    if (feedbackOrRemarksType == kFeedbackType) {//反馈建议
        navigationItemTitle = @"服务反馈";
        _placeStr = @"请输入您的意见(500字以内)";
    } else if (feedbackOrRemarksType == kRemarksType) {//备注
        navigationItemTitle = @"添加备注";
        _placeStr = @"给店家留言，请输入您的需求~";
        
    }
    self.title = navigationItemTitle;
    _feedbackOrRemarksType = feedbackOrRemarksType;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
