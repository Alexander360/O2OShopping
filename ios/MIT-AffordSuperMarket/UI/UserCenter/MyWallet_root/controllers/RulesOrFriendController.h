//
//  RulesOrFriendController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/10.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//


#import "BaseViewController.h"

typedef NS_ENUM(NSInteger,RulesOrFriendType) {
    kRulesType = 0,//消费细则
    kFriendType //如何分享好友
};
@interface RulesOrFriendController : BaseViewController
@property(nonatomic)RulesOrFriendType rulesOrFriendType;

@end
