package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.kit.DoubleKit;
import cn.com.dashihui.kit.InviteCodeKit;
import cn.com.dashihui.kit.ListKit;
import cn.com.dashihui.web.common.DispatchState;
import cn.com.dashihui.web.common.OrderCode;
import cn.com.dashihui.web.common.SysConfig;
import cn.com.dashihui.web.common.UserCode;
import cn.com.dashihui.web.dao.Admin;
import cn.com.dashihui.web.dao.Order;
import cn.com.dashihui.web.dao.OrderRefund;
import cn.com.dashihui.web.dao.User;

public class OrderService {

	/**
	 * 分页查找订单信息
	 * @param orderNum 订单编号
	 * @param beginDate 下单时间
	 * @param endDate 成交时间
	 * @param state 订单状态，0：全部，1：待付款，2：待发货，3：待签收，4：待取货，5：已签收，6：已取货，7：被催单
	 * @param address 收货地址
	 * @param tel 买家电话
	 * @param takeType 配送方式
	 * @param payType 支付方式
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,String orderNum,String beginDate,String endDate,int state,String address,String tel,int takeType,int payType){
		StringBuffer sBuffer = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sBuffer.append("FROM t_bus_order A LEFT JOIN t_dict_logistics B ON A.logisticsid=B.id WHERE A.isSelf=1");
		if(!StrKit.isBlank(orderNum)){
			sBuffer.append(" AND A.orderNum=?");
			params.add(orderNum);
		}
		if(!StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND A.startDate BETWEEN ? AND ?");
			params.add(beginDate);
			params.add(endDate);
		}else if(!StrKit.isBlank(beginDate)&&StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')>=?");
			params.add(beginDate);
		}else if(StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')<=?");
			params.add(endDate);
		}
		switch (state) {
			case 1://待付款（“在线支付”、“未支付”、“正常”）
				sBuffer.append(" AND payType=? AND payState=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.NO_PAY);
				params.add(OrderCode.OrderState.NORMAL);
				break;
			case 2://待发货（1：“在线支付+送货上门”、“已支付”、“未发货”、“正常”，2：“货到付款+送货上门”、“未发货”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND takeType=? AND deliverState=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.HAD_PAY);
				params.add(OrderCode.OrderPayType.ON_DELIVERY);
				params.add(OrderCode.OrderTakeType.DELIVER);
				params.add(OrderCode.OrderDeliverState.NO_DELIVER);
				params.add(OrderCode.OrderState.NORMAL);
				break;
			case 3://待签收（1：“在线支付+送货上门”、“已支付”、“已发货”、“正常”，2：“货到付款+送货上门”、“已发货”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND takeType=? AND deliverState=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.HAD_PAY);
				params.add(OrderCode.OrderPayType.ON_DELIVERY);
				params.add(OrderCode.OrderTakeType.DELIVER);
				params.add(OrderCode.OrderDeliverState.HAD_DELIVER);
				params.add(OrderCode.OrderState.NORMAL);
				break;
			case 4://待取货（1：“在线支付+门店自取”、“已支付”、“正常”，2：“货到付款+门店自取”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND takeType=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.HAD_PAY);
				params.add(OrderCode.OrderPayType.ON_DELIVERY);
				params.add(OrderCode.OrderTakeType.TAKE_SELF);
				params.add(OrderCode.OrderState.NORMAL);
				break;
			case 5://已签收（1：“在线支付+送货上门”、“已支付”、“已发货”、“完成”，2：“货到付款+送货上门”、“已发货”、“完成”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND takeType=? AND deliverState=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.HAD_PAY);
				params.add(OrderCode.OrderPayType.ON_DELIVERY);
				params.add(OrderCode.OrderTakeType.DELIVER);
				params.add(OrderCode.OrderDeliverState.HAD_DELIVER);
				params.add(OrderCode.OrderState.FINISH);
				break;
			case 6://已取货（1：“在线支付+门店自取”、“已支付”、“完成”，2：“货到付款+门店自取”、“完成”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND takeType=? AND orderState=?");
				params.add(OrderCode.OrderPayType.ON_LINE);
				params.add(OrderCode.OrderPayState.HAD_PAY);
				params.add(OrderCode.OrderPayType.ON_DELIVERY);
				params.add(OrderCode.OrderTakeType.TAKE_SELF);
				params.add(OrderCode.OrderState.FINISH);
				break;
			case 7://被催单（“正常”、“催单次数!=0”）
				sBuffer.append(" AND orderState=? AND urgeTimes!=0");
				params.add(OrderCode.OrderState.NORMAL);
				break;
			case 8://已取消
				sBuffer.append(" AND orderState=?");
				params.add(OrderCode.OrderState.CANCEL);
				break;
			case 9://已过期
				sBuffer.append(" AND orderState=?");
				params.add(OrderCode.OrderState.EXPIRE);
				break;
			case 10://已删除
				sBuffer.append(" AND orderState=?");
				params.add(OrderCode.OrderState.DELETE);
				break;
			default:
				//默认不查询取消、过期、删除的订单
				sBuffer.append(" AND orderState!=? AND orderState!=? AND orderState!=?");
				params.add(OrderCode.OrderState.CANCEL);
				params.add(OrderCode.OrderState.EXPIRE);
				params.add(OrderCode.OrderState.DELETE);
				break;
		}
		if(!StrKit.isBlank(address)){
			sBuffer.append(" AND A.address LIKE ?");
			params.add("%"+address+"%");
		}
		if(!StrKit.isBlank(tel)){
			sBuffer.append(" AND A.tel=?");
			params.add(tel);
		}
		if(takeType!=0){
			sBuffer.append(" AND A.takeType=?");
			params.add(takeType);
		}
		if(payType!=0){
			sBuffer.append(" AND A.payType=?");
			params.add(payType);
		}
		
		sBuffer.append(" ORDER BY createDate DESC");
		//查询出符合条件的订单列表
		Page<Record> page = Db.paginate(pageNum,pageSize,"SELECT A.*,B.title logisticsTitle ",sBuffer.toString(),params.toArray());
		if(page.getList()!=null&&page.getList().size()!=0){
			//将各订单对应的订单商品查出
			return new Page<Record>(findGoodsByOrder(page.getList()), page.getPageNumber(), page.getPageSize(), page.getTotalPage(), page.getTotalRow());
		}
		return page;
	}
	
	/**
	 * 为指定订单列表，查找出相应的商品清单并set给每个订单记录
	 * @param orderList
	 */
	private List<Record> findGoodsByOrder(List<Record> orderList){
		//拼接SQL
		StringBuffer sBuffer = new StringBuffer("SELECT orderNum,thumb,name,price,count,amount FROM t_bus_order_list WHERE orderNum IN (");
		for(Record order : orderList){
			sBuffer.append("'").append(order.getStr("orderNum")).append("',");
		}
		sBuffer.replace(sBuffer.lastIndexOf(","), sBuffer.length(), ")");
		sBuffer.append(" ORDER BY orderNum");
		//整理结果
		List<Record> goodsList = Db.find(sBuffer.toString());
		if(goodsList!=null){
			//先将查出来的所有的订单商品，按订单号归类
			Map<String,List<Record>> goodsMap = new HashMap<String,List<Record>>();
			for(Record goods : goodsList){
				if(goodsMap.containsKey(goods.getStr("orderNum"))){
					((List<Record>)goodsMap.get(goods.getStr("orderNum"))).add(goods);
				}else{
					goodsMap.put(goods.getStr("orderNum"), new ListKit<Record>().add(goods).getList());
				}
			}
			//遍历订单列表的同时，设置对应订单商品集
			for(Record order : orderList){
				order.set("goodsList", goodsMap.get(order.get("orderNum")));
			}
		}
		return orderList;
	}

	/**
	 * 根据订单号查询出订单
	 */
	public Order getOrderByOrderNum(String orderNum){
		String sql = "SELECT bo.*,dl.title logisticsTitle FROM t_bus_order bo LEFT JOIN t_dict_logistics dl ON bo.logisticsid=dl.id WHERE orderNum=?";
		return Order.me().findFirst(sql,orderNum);
	}

	/**
	 * 根据订单号查询出商品清单
	 */
	public List<Record> getGoodsListByOrderNum(String orderNum){
		String sql = "SELECT * FROM t_bus_order_list WHERE orderNum=?";
		return Db.find(sql, orderNum);
	}

	/**
	 * 根据订单号查询订单操作日志
	 */
	public List<Record> getLogListByOrderNum(String orderNum){
		String sql = "SELECT * FROM t_bus_order_log WHERE orderNum=? ORDER BY createDate";
		return Db.find(sql, orderNum);
	}

	/**
	 * 根据订单号查询订单评价信息
	 */
	public Record getEvalByOrderNum(String orderNum){
		String sql = "SELECT * FROM t_bus_order_eval WHERE orderNum=?";
		return Db.findFirst(sql, orderNum);
	}

	/**
	 * 更新
	 */
	public boolean update(Order order){
		return order.update();
	}
	
	/**
	 * 取消订单，保存日志，并保存退款单信息
	 * @param user 当前登录店铺管理员对象
	 * @param order 订单信息
	 * @param reason 取消原因
	 * @param payType 支付类型
	 * @param payState 支付状态
	 * @param orderNum 订单编号
	 * @throws Exception 
	 */
	@Before(Tx.class)
	public void cancelOrder(Admin user,Order order,String reason, int payType, int payState, String orderNum) throws Exception {
		//4.取消订单操作，并记录日志
		order.set("orderState", OrderCode.OrderState.CANCEL).set("reason", reason);
		if(order.update()){
			//4-1.判断该订单是否是“在线支付”和“已支付”
			if(payType == OrderCode.OrderPayType.ON_LINE && payState == OrderCode.OrderPayState.HAD_PAY) {
				//保存退款单信息
				//生成退款类订单的订单编号，以大写字母T开头
				String orderRefundNum = "T"+DatetimeKit.getFormatDate("yyyyMMddHHmmssSSS")+CommonKit.buildRandomNum(3);
				if(!new OrderRefund()
						.set("refundNum", orderRefundNum)
						.set("orderNum", orderNum)
						.set("type", OrderCode.RefundType.CANCEL_ORDER)
						.set("amount", order.getDouble("amount"))
						.save()){
					throw new Exception("生成退款单失败！");
				}
			}
			//4-2.订单取消成功，记录订单操作日志并返回最新订单
			log(order.getStr("orderNum"),OrderCode.OrderLogType.CANCEL,"管理员："+user.getStr("username"), OrderCode.OrderLogType.CANCEL_ACTION, "订单已取消");
			//查询订单的商品列表供页面展示
			order.put("goodsList", getGoodsListByOrderNum(order.getStr("orderNum")));
		} else {
			throw new Exception("订单取消失败！");
		}
	}
	
	/**
	 * 用户签收订单
	 */
	@Before(Tx.class)
	public void receiveOrder(Admin user,Order order) throws Exception {
		//更新订单状态
		order.set("orderState", OrderCode.OrderState.FINISH).set("signDate",DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
		if(order.update()){
			String orderNum = order.getStr("orderNum");
			int takeType = order.getInt("takeType");
			if(takeType==OrderCode.OrderTakeType.DELIVER){
				//将配送员订单状态从“配送中”修改为“已完成”
				Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE orderNum=? AND state=?",DispatchState.FINISH,orderNum,DispatchState.DISPATCHING);
				//门店配送，用户签收
				log(order.getStr("orderNum"),OrderCode.OrderLogType.SIGN,"管理员："+user.getStr("username"), OrderCode.OrderLogType.SIGN_ACTION, "商品已签收，感谢您在大实惠购物，欢迎再次光临！");
			}else if(takeType==OrderCode.OrderTakeType.TAKE_SELF){
				//将配送员订单状态从“待取货”修改为“已完成”
				Db.update("UPDATE t_dict_store_seller_rel SET state=? WHERE orderNum=? AND state=?",DispatchState.FINISH,orderNum,DispatchState.WAIT_GET);
				//上门自取，用户取货
				log(order.getStr("orderNum"),OrderCode.OrderLogType.GET,"管理员："+user.getStr("username"), OrderCode.OrderLogType.GET_ACTION, "已取货，感谢您在大实惠购物，欢迎再次光临！");
			}
			//根据用户所购买的商品，进行分销处理
			rebate(order);
		}else{
			throw new Exception("订单接收失败！");
		}
	}
	
	private void rebate(Order order) throws Exception{
		String orderNum = order.getStr("orderNum");
		//查询订单商品列表
		List<Record> goodsList = Db.find("SELECT goodsid,amount,isRebate,percent1,percent2,percent3,percent4,percent5 FROM t_bus_order_list WHERE orderNum=?", order.getStr("orderNum"));
		
		//所购开启分销商品总额
		double totalAmount = 0;
		
		//分销日志SQL
		String logSql = "INSERT INTO t_log_rebate(orderNum,goodsid,amount,percent1,money1,percent2,money2,percent3,money3,percent4,money4,percent5,money5) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		//分销日志列表1
		List<Object[]> logParams1 = new ArrayList<Object[]>();
		//分销分成1（用户为普通用户，非黄金会员，也没有邀请用户，此时推荐人、DSH、社区享利）
		double[] rebate1 = new double[5];
		
		//分销日志列表2
		List<Object[]> logParams2 = new ArrayList<Object[]>();
		//分销分成2（用户为普通用户，非黄金会员，有邀请用户，此时推荐人、DSH、社区负责人、一级分销享利）
		double[] rebate2 = new double[5];
		
		//分销日志列表3
		List<Object[]> logParams3 = new ArrayList<Object[]>();
		//分销分成3（用户为黄金会员，也没有邀请用户，此时推荐人、DSH、社区负责人、二级分销享利）
		double[] rebate3 = new double[5];
		
		//分销日志列表4
		List<Object[]> logParams4 = new ArrayList<Object[]>();
		//分销分成4（用户为黄金会员，有邀请用户，此时推荐人、DSH、社区负责人、一级分销、二级分销享利）
		double[] rebate4 = new double[5];
		
		//遍历所有商品
		for(Record goods : goodsList){
			if(goods.getInt("isRebate")==1){
				int goodsid = goods.getInt("goodsid");
				//商品总金额
				double amount = goods.getDouble("amount");
				//商品设置“推荐人”返利比例
				double percent1 = DoubleKit.mul(goods.getDouble("percent1"),0.01);
				//商品设置“DSH”返利比例
				double percent2 = DoubleKit.mul(goods.getDouble("percent2"),0.01);
				//商品设置“社区负责人”返利比例
				double percent3 = DoubleKit.mul(goods.getDouble("percent3"),0.01);
				//商品设置“一级分销”返利比例
				double percent4 = DoubleKit.mul(goods.getDouble("percent4"),0.01);
				//商品设置“二级分销”返利比例
				double percent5 = DoubleKit.mul(goods.getDouble("percent5"),0.01);
				//累计本单中所有开启返利的商品的消费金额
				totalAmount = DoubleKit.add(totalAmount, amount);
				//用户为普通用户，非黄金会员，也没有邀请用户，此时推荐人、DSH、社区负责人享利
				rebate1[0] = DoubleKit.add(rebate1[0], DoubleKit.mul(amount,percent1));
				rebate1[1] = DoubleKit.add(rebate1[1], DoubleKit.mul(amount,percent2+percent4+percent5));
				rebate1[2] = DoubleKit.add(rebate1[2], DoubleKit.mul(amount,percent3));
				rebate1[3] = 0;
				rebate1[4] = 0;
				logParams1.add(new Object[]{orderNum,goodsid,amount,percent1,rebate1[0],percent2,rebate1[1],percent3,rebate1[2],percent4,rebate1[3],percent5,rebate1[4]});
				//用户为普通用户，非黄金会员，有邀请用户，此时推荐人、DSH、社区负责人、一级分销享利
				rebate2[0] = DoubleKit.add(rebate1[0], DoubleKit.mul(amount,percent1));
				rebate2[1] = DoubleKit.add(rebate1[1], DoubleKit.mul(amount,percent2+percent5));
				rebate2[2] = DoubleKit.add(rebate1[2], DoubleKit.mul(amount,percent3));
				rebate2[3] = DoubleKit.add(rebate1[3], DoubleKit.mul(amount,percent4));
				rebate2[4] = 0;
				logParams2.add(new Object[]{orderNum,goodsid,amount,percent1,rebate2[0],percent2,rebate2[1],percent3,rebate2[2],percent4,rebate2[3],percent5,rebate2[4]});
				//用户为黄金会员，也没有邀请用户，此时推荐人、DSH、社区负责人、二级分销享利
				rebate3[0] = DoubleKit.add(rebate1[0], DoubleKit.mul(amount,percent1));
				rebate3[1] = DoubleKit.add(rebate1[1], DoubleKit.mul(amount,percent2+percent4));
				rebate3[2] = DoubleKit.add(rebate1[2], DoubleKit.mul(amount,percent3));
				rebate3[3] = 0;
				rebate3[4] = DoubleKit.add(rebate1[4], DoubleKit.mul(amount,percent5));
				logParams3.add(new Object[]{orderNum,goodsid,amount,percent1,rebate3[0],percent2,rebate3[1],percent3,rebate3[2],percent4,rebate3[3],percent5,rebate3[4]});
				//用户为黄金会员，有邀请用户，此时推荐人、DSH、社区负责人、一级分销、二级分销享利
				rebate4[0] = DoubleKit.add(rebate1[0], DoubleKit.mul(amount,percent1));
				rebate4[1] = DoubleKit.add(rebate1[1], DoubleKit.mul(amount,percent2));
				rebate4[2] = DoubleKit.add(rebate1[2], DoubleKit.mul(amount,percent3));
				rebate4[3] = DoubleKit.add(rebate1[3], DoubleKit.mul(amount,percent4));
				rebate4[4] = DoubleKit.add(rebate1[4], DoubleKit.mul(amount,percent5));
				logParams4.add(new Object[]{orderNum,goodsid,amount,percent1,rebate4[0],percent2,rebate4[1],percent3,rebate4[2],percent4,rebate4[3],percent5,rebate4[4]});
			}
		}
		//如果有返利的商品消费金额为0，则不处理
		if(totalAmount==0)return;
		//用户ID
		int userid = order.getInt("userid");
		//查询相应下单的用户信息
		User user = User.me().findById(userid);
		//用户注册邀请人ID
		int inviteUserid = user.getInt("inviteUserid");
		if(user.getInt("level")==UserCode.UserLevel.LEVEL1){
			//查询用户以前购买并签收（在线支付）的有返利的商品所消费的金额（不包括本次消费的订单金额）
			Record history = Db.findFirst("SELECT IFNULL(SUM(amount),0) amount"
					+ " FROM t_bus_order_list bol WHERE isRebate=1 AND EXISTS("
					+ " SELECT 1"
					+ " FROM t_bus_order bo"
					+ " INNER JOIN t_bus_order_log bolog ON bo.orderNum=bolog.orderNum"
					+ " WHERE bolog.type=? AND bo.userid=? AND bo.orderNum=bol.orderNum AND bo.orderNum!=?)",
					OrderCode.OrderLogType.SIGN,userid,orderNum);
			if(history!=null){
				//加上本次所消费的金额
				totalAmount = DoubleKit.add(totalAmount, history.getDouble("amount"));
			}
			//用户为普通会员时
			double level2Line = SysConfig.getLevel2Line();
			if(totalAmount>=level2Line){
				//当本次所购的开启返利的商品的消费总金额达到黄金会员线的时候
				//1.更新用户会员等级为黄金会员，并生成唯一邀请码保存（根据用户ID生成，可逆）
				Db.update("UPDATE t_bus_user SET level=?,inviteCode=? WHERE id=? AND level=?",UserCode.UserLevel.LEVEL2,InviteCodeKit.toSerialCode(userid),userid,UserCode.UserLevel.LEVEL1);
				//2.记录用户会员等级变化日志
				Db.update("INSERT INTO t_bus_user_level_log(userid,fromLevel,toLevel,flag,`describe`,fromOrderNum) VALUES(?,?,?,?,?,?)",userid,UserCode.UserLevel.LEVEL1,UserCode.UserLevel.LEVEL2,UserCode.UserLevelLogFlag.CONSUME,"消费",orderNum);
			}
			if(inviteUserid==0){
				//如果用户注册时没有邀请人，则本次购物所有返利将由推荐人、DSH、社区负责人享利
				//1.记录分销日志1
				Db.batch(logSql, parseParams(logParams1), logParams1.size());
			}else{
				//如果用户注册时有邀请人，则本次购物所有返利将由推荐人、DSH、社区负责人、一级分销享利
				//1.更新一级分销用户的“实惠币金额”
				User inviteUser = User.me().findById(inviteUserid);
				if(inviteUser==null){
					throw new Exception("用户（用户ID："+userid+"）的注册邀请人（用户ID："+inviteUserid+"）找不到！");
				}
				//此处使用行锁定的方式更新，防止并发
				if(Db.update("UPDATE t_bus_user SET money=money+? WHERE id=? AND money=?",rebate2[3],inviteUserid,inviteUser.getDouble("money"))!=1){
					throw new Exception("用户（用户ID："+userid+"）的注册邀请人（用户ID："+inviteUserid+"）更新money时出错！");
				}
				//2.记录一级分销用户的“实惠币金额”变动日志（记录订单号）
				Db.update("INSERT INTO t_bus_user_value_log(userid,amount,type,flag,`describe`,fromUserid,fromOrderNum) VALUES(?,?,?,?,?,?,?)",inviteUserid,rebate2[3],1,UserCode.UserValueLogFlag.CONSUME_UNDER,"下线用户消费",userid,orderNum);
				//3.记录分销日志2
				Db.batch(logSql, parseParams(logParams2), logParams2.size());
			}
		}else if(user.getInt("level")==UserCode.UserLevel.LEVEL2){
			//用户为黄金会员时
			if(inviteUserid==0){
				//如果用户注册时没有邀请人，则本次购物所有返利将由推荐人、DSH、社区负责人、二级分销享利
				//1.更新二级分销用户（即当前用户）的“实惠币金额”
				//此处使用行锁定的方式更新，防止并发
				if(Db.update("UPDATE t_bus_user SET money=money+? WHERE id=? AND money=?",rebate3[4],userid,user.getDouble("money"))!=1){
					throw new Exception("用户（用户ID："+userid+"）更新money时出错！");
				}
				//2.记录二级分销用户（即当前用户）的“实惠币金额”变动日志（记录订单号）
				Db.update("INSERT INTO t_bus_user_value_log(userid,amount,type,flag,`describe`,fromOrderNum) VALUES(?,?,?,?,?,?)",userid,rebate3[4],1,UserCode.UserValueLogFlag.CONSUME_SELF,"消费",orderNum);
				//3.记录分销日志3
				Db.batch(logSql, parseParams(logParams3), logParams3.size());
			}else{
				//如果用户注册时有邀请人，则本次购物所有返利将由推荐人、DSH、社区负责人、一级分销、二级分销享利
				//1.更新一级分销用户和二级分销用户（即当前用户）的“实惠币金额”
				User inviteUser = User.me().findById(inviteUserid);
				if(inviteUser==null){
					throw new Exception("用户（用户ID："+userid+"）的注册邀请人（用户ID："+inviteUserid+"）找不到！");
				}
				//此处使用行锁定的方式更新，防止并发
				if(Db.update("UPDATE t_bus_user SET money=money+? WHERE id=? AND money=?",rebate4[3],inviteUserid,inviteUser.getDouble("money"))!=1){
					throw new Exception("用户（用户ID："+userid+"）的注册邀请人（用户ID："+inviteUserid+"）更新money时出错！");
				}
				if(Db.update("UPDATE t_bus_user SET money=money+? WHERE id=? AND money=?",rebate4[4],userid,user.getDouble("money"))!=1){
					throw new Exception("用户（用户ID："+userid+"）更新money时出错！");
				}
				//2.记录一级分销用户和二级分销用户（即当前用户）的“实惠币金额”变动日志（记录订单号）
				Db.update("INSERT INTO t_bus_user_value_log(userid,amount,type,flag,`describe`,fromUserid,fromOrderNum) VALUES(?,?,?,?,?,?,?)",inviteUserid,rebate4[3],1,UserCode.UserValueLogFlag.CONSUME_UNDER,"下线用户消费",userid,orderNum);
				Db.update("INSERT INTO t_bus_user_value_log(userid,amount,type,flag,`describe`,fromOrderNum) VALUES(?,?,?,?,?,?)",userid,rebate4[4],1,UserCode.UserValueLogFlag.CONSUME_SELF,"消费",orderNum);
				//3.记录分销日志4
				Db.batch(logSql, parseParams(logParams4), logParams4.size());
			}
		}
	}
	
	/**
	 * 将list<object[]>集合转换为object[][]
	 * @param params list<object[]>集合
	 */
	private Object[][] parseParams(List<Object[]> params){
		if(params!=null){
			Object[][] result = new Object[params.size()][];
			for(int index = 0;index<params.size();index++){
				result[index] = params.get(index);
			}
			return result;
		}
		return null;
	}
	
	/**
	 * 记录操作日志
	 */
	public void log(String orderNum, int type, String user, String action, String content){
		Db.update("INSERT INTO t_bus_order_log(orderNum,type,user,action,content) VALUES(?,?,?,?,?)",orderNum,type,user,action,content);
	}
}
