package cn.com.dashihui.web.service;

import java.util.List;

import cn.com.dashihui.web.dao.Brand;
import cn.com.dashihui.web.dao.Category;
import cn.com.dashihui.web.dao.City;
import cn.com.dashihui.web.dao.Logistics;
import cn.com.dashihui.web.dao.Store;

public class APIService {
	
	public List<Brand> getAllBrand(){
		return Brand.me().find("SELECT id,name FROM t_dict_brand");
	}
	
	public List<City> getAllProvince(){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=1");
	}
	
	public List<City> getAllCityByProvince(int provinceid){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=2 AND parentid=?",provinceid);
	}
	
	public List<City> getAllAreaByCity(int cityid){
		return City.me().find("SELECT id,name FROM t_dict_city WHERE type=3 AND parentid=?",cityid);
	}
	
	public List<Category> getCategoryByParent(int parentid){
		return Category.me().find("SELECT categoryId id,categoryName name FROM t_dict_category WHERE categoryFatherId=? ORDER BY categoryNo",parentid);
	}
	
	public List<Store> getStoreAll(){
		return Store.me().find("SELECT A.id,A.title FROM t_dict_store A ORDER BY A.createDate DESC");
	}
	
	public List<Logistics> getLogistics(){
		return Logistics.me().find("SELECT A.* FROM t_dict_logistics A WHERE A.state=1");
	}
}
