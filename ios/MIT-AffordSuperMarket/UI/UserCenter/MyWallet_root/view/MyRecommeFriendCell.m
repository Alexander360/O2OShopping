//
//  MyRecommeFriendCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#define kLeftSpace 15
#define ktopSpace 5
#import "MyRecommeFriendCell.h"

#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
@interface MyRecommeFriendCell()
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *userName;
@property(nonatomic,strong)UILabel *moenyLable;
@end
@implementation MyRecommeFriendCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubView];
    }
    return self;
}
-(void)setSubViewWithTimeText:(NSString *)timeText UserName:(NSString *)userName MoneyText:(NSString *)moneyText
{
    CGSize userNameSize =[userName sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    
//    NSString *string = [userName substringWithRange:NSMakeRange(4,4)];
//    //字符串的替换
//    userName = [userName stringByReplacingOccurrencesOfString:string withString:@"****"];
    _userName.frame = CGRectMake(kLeftSpace, ktopSpace, userNameSize.width, HightScalar(30));
    _userName.text =userName;
    
     CGSize titelSize =[timeText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    _timeLabel.frame = CGRectMake(kLeftSpace, CGRectGetMaxY(self.userName.frame), titelSize.width, CGRectGetHeight(self.userName.bounds));
    _timeLabel.text =timeText;
    
    NSString *shihuibi =[NSString stringWithFormat:@"+%@ 实惠币",moneyText];
     CGSize moenySize =[shihuibi sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    _moenyLable.frame = CGRectMake(VIEW_WIDTH-kLeftSpace-moenySize.width, (HightScalar(67)-moenySize.height)/2, moenySize.width,moenySize.height);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:shihuibi];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(16)] range:NSMakeRange(0, moneyText.length+1)];
    _moenyLable.attributedText = str;
}
#pragma mark - 初始化视图
-(void)setSubView
{
    //时间
    _timeLabel =[[UILabel alloc]initWithFrame:CGRectZero];
    _timeLabel.font =[UIFont systemFontOfSize:FontSize(16)];
    _timeLabel.textColor =[UIColor colorWithHexString:@"#999999"];
    _timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.timeLabel];
    
    //好友账号名
    _userName =[[UILabel alloc]initWithFrame:CGRectZero];
    _userName.font =[UIFont systemFontOfSize:FontSize(16)];
    _userName.textColor =[UIColor colorWithHexString:@"#555555"];
    _userName.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.userName];
    //实惠币
    _moenyLable =[[UILabel alloc]initWithFrame:CGRectZero];
    _moenyLable.font =[UIFont systemFontOfSize:FontSize(14)];
    _moenyLable.textColor =[UIColor colorWithHexString:@"#e71f18"];
    _moenyLable.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.moenyLable];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
