//
//  UserInfoModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserInfoModel
 Created_Date： 20151108
 Created_People： GT
 Function_description： 用户登录返回信息
 ***************************************/

#import <Foundation/Foundation.h>

@interface UserInfoModel : NSObject
@property(nonatomic, strong)NSString *token;//用户注册token
@property(nonatomic, strong)NSString *nickName;//用户昵称
@property(nonatomic, strong)NSString *xiaoquID;//小区ID
@property(nonatomic, strong)NSString *xiaoquName;//小区名字
@property(nonatomic, strong)NSString *userName;
@property(nonatomic, strong)NSString *userPassword;
@property(nonatomic, strong)NSString *userImage;
@property(nonatomic, strong)NSString *userLevel;//会员等级
@property(nonatomic, strong)NSString *userMoney;//实惠币
@property(nonatomic, strong)NSString *userInvitecode;//邀请码
@property(nonatomic, strong)NSString *userInviteuserID;//所受邀请用户ID
@end
