//
//  SureOrderHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SureOrderHeaderView.h"
#define ktopSpace 15
#define kleftSpace 15
#define kVSpace 10
#define kitemHeight 44
//view
#import "CheckBoxView.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
//untils
#import "NSString+TextSize.h"
#import "ManagerGlobeUntil.h"
#import "FL_Button.h"
#import "Global.h"
//model
#import "UserInfoModel.h"
@interface SureOrderHeaderView ()<UIGestureRecognizerDelegate>

//无地址视图
@property(nonatomic, strong)UIView *noAddressView;
@property(nonatomic, strong)UIButton *selectXiaoQuBtn;//选择小区
@property(nonatomic, strong)UILabel *xiaoQuAddressLabel;//显示小区label
//有地址视图
@property(nonatomic, strong)UIView *hadAddressView;
@property(nonatomic, strong)UILabel *hadAddressNameLabel;
@property(nonatomic, strong)UILabel *hadAddressAddressLabel;
//公共视图
@property(nonatomic, strong)UIButton *markInfoBtn;//备注信息btn
@property(nonatomic, strong)FL_Button *selectTimeBtn;//选择时间button
@property(nonatomic, strong)NSString *userTel; //用户电话号码
@property(nonatomic,strong)UILabel *peiSongIntroduceLabel;//营业说明/配送说明
@property (nonatomic,strong)UILabel * peiSongTypeViewContentLabel;
@end
@implementation SureOrderHeaderView
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress withSureOrderType:(SureOrderHeaderType)orderType {
    self = [super initWithFrame:frame];
    if (self) {
        self.payType = @"1";
        self.sendProductType = @"1";
        self.sexType = @"1";
        [self setupSubViewWithHadOrdersAddressStatus:isHadOrdersAddress withSureOrderType:orderType];
        //隐藏键盘手势
        UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
        tabGesture.delegate = self;
        [self addGestureRecognizer:tabGesture];
      
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress {
    return [self initWithFrame:frame hadOrdersAddressStatus:isHadOrdersAddress withSureOrderType:kproductSureOrderHeader];
}
- (void)setupSubViewWithHadOrdersAddressStatus:(BOOL)isHadOrdersAddress withSureOrderType:(SureOrderHeaderType)orderType {
    if (isHadOrdersAddress) {//有地址
        [self setupHadAddressView];
    } else {//无地址
        [self setupNoAddressView];
    }
    [self setupCommonViewWithHadOrdersAddressStatus:isHadOrdersAddress withSureOrderType:orderType];
}
//有地址视图
- (void)setupHadAddressView {
    
    
    _hadAddressView = [[UIView alloc]init];
    _hadAddressView.frame = CGRectMake(0
                                       , 15
                                       , CGRectGetWidth(self.bounds)
                                       , 90);
    _hadAddressView.backgroundColor = [UIColor colorWithHexString:@"#faeee6"];
    [self addSubview:_hadAddressView];
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_hadAddressView.bounds), CGRectGetHeight(_hadAddressView.bounds))];
    backgroundImage.image = [UIImage imageNamed:@"shouHuoAddress_bg"];
    [_hadAddressView addSubview:backgroundImage];
   // name
    _hadAddressNameLabel = [self setLabelWithTitle:@"袁姗姗   女士  15093457921" fontSize:15 textColor:@"#000000"];
    _hadAddressNameLabel.frame = CGRectMake(kleftSpace
                                            , 14
                                            , CGRectGetWidth(self.bounds) - kleftSpace - 44
                                            , 30);
    _hadAddressNameLabel.textAlignment = NSTextAlignmentLeft;
    [self.hadAddressView addSubview:_hadAddressNameLabel];
    //address
    UIImage *locationImage = [UIImage imageNamed:@"dingweiImage"];
    UIImageView *locationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_hadAddressNameLabel.frame)
                                                                                 , CGRectGetMaxY(_hadAddressNameLabel.frame) + 5
                                                                                  , locationImage.size.width - 4
                                                                                  , locationImage.size.height - 6) ];
    locationImageView.image = locationImage;
    [self.hadAddressView addSubview:locationImageView];
    _hadAddressAddressLabel = [self setLabelWithTitle:@"郑州市上街区宏盛新城1号楼2单元1203" fontSize:15 textColor:@"#000000"];
    _hadAddressAddressLabel.frame = CGRectMake(CGRectGetMaxX(locationImageView.frame) + 7
                                               , CGRectGetMaxY(_hadAddressNameLabel.frame) + 2
                                               , CGRectGetWidth(_hadAddressNameLabel.bounds) - 7
                                               , CGRectGetHeight(_hadAddressNameLabel.bounds));
    [self.hadAddressView addSubview:_hadAddressAddressLabel];
    
    //修改地址button
    UIButton *moditAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    moditAddressBtn.frame = CGRectMake(CGRectGetWidth(_hadAddressView.bounds) - 44
                                       , (90 - 44)/2
                                       , 44
                                       , 44);
    [moditAddressBtn setImage:[UIImage imageNamed:@"icon_eidt"] forState:UIControlStateNormal];
    [moditAddressBtn addTarget:self action:@selector(modifiedAddressEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.hadAddressView addSubview:moditAddressBtn];

}
//无地址视图
- (void)setupNoAddressView {
    //父视图
    _noAddressView = [[UIView alloc]initWithFrame:CGRectMake(0, ktopSpace, CGRectGetWidth(self.bounds), 225 -kitemHeight -1)];
    _noAddressView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [self addSubview:_noAddressView];
    
    UIImage *sepImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 2, 1)];
    NSString *usreNameTitle = @"联系人：";
    CGSize userNameTitleLabelSize = [usreNameTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userNameTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                           , 0
                                                                           , userNameTitleLabelSize.width
                                                                           , kitemHeight)];
    userNameTitleLabel.text = usreNameTitle;
    userNameTitleLabel.font = [UIFont systemFontOfSize:15];
    userNameTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
    [_noAddressView addSubview:userNameTitleLabel];
    
    _userNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                      , CGRectGetMinY(userNameTitleLabel.frame)
                                                                      , CGRectGetWidth(self.bounds) - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                      , CGRectGetHeight(userNameTitleLabel.bounds))];
    _userNameTextField.font = [UIFont systemFontOfSize:15];
    _userNameTextField.placeholder = @"请输入联系人姓名";
    _userNameTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [_noAddressView addSubview:_userNameTextField];
    
    UIImageView *firstSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                             , CGRectGetMaxY(_userNameTextField.frame)
                                                                             , CGRectGetWidth(self.bounds) - 2*kleftSpace
                                                                             , 1)];
    firstSepLine.image = sepImage;
    [_noAddressView addSubview:firstSepLine];
    
    //性别
    CheckBoxView *sexCheckBoxView = [[CheckBoxView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                                  , CGRectGetMaxY(firstSepLine.frame)
                                                                                  , CGRectGetWidth(firstSepLine.frame)
                                                                                  , CGRectGetHeight(_userNameTextField.bounds))
                                                           withiTitles:@[@"男士",@"女士"]
                                                           normalImage:[UIImage imageNamed:@"checkBoxNormalImage"]
                                                          slectedImage:[UIImage imageNamed:@"checkBoxSelectdImage"]];
    sexCheckBoxView.tag = 100;
    sexCheckBoxView.delegate = (id<CheckBoxViewDelegate>)self;
    [_noAddressView addSubview:sexCheckBoxView];
    
    UIImageView *secondSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                              , CGRectGetMaxY(sexCheckBoxView.frame)
                                                                              , CGRectGetWidth(firstSepLine.bounds)
                                                                              , CGRectGetHeight(firstSepLine.bounds))];
    secondSepLine.image = sepImage;
    [_noAddressView addSubview:secondSepLine];
    
    //电话
    NSString *userTelePhoneTitle = @"电话：";
    CGSize userTelePhoneTitleSize = [userTelePhoneTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userTelePhoneTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                                , CGRectGetMaxY(secondSepLine.frame)
                                                                                , userTelePhoneTitleSize.width
                                                                                , CGRectGetHeight(userNameTitleLabel.bounds))];
    userTelePhoneTitleLabel.text = userTelePhoneTitle;
    userTelePhoneTitleLabel.font = [UIFont systemFontOfSize:15];
    userTelePhoneTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
    [_noAddressView addSubview:userTelePhoneTitleLabel];
    
    _userTelePhoneTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                           , CGRectGetMinY(userTelePhoneTitleLabel.frame)
                                                                           , CGRectGetWidth(self.bounds) - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                           , CGRectGetHeight(userTelePhoneTitleLabel.bounds))];
    _userTelePhoneTextField.font = [UIFont systemFontOfSize:15];
    // 获取用户电话
     UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    _userTel = model.userName;
    _userTelePhoneTextField.text =self.userTel;
    _userTelePhoneTextField.placeholder = @"请输入联系人电话";
    _userTelePhoneTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [_noAddressView addSubview:_userTelePhoneTextField];
    
    UIImageView *thridSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(secondSepLine.frame)
                                                                             , CGRectGetMaxY(_userTelePhoneTextField.frame)
                                                                             , CGRectGetWidth(secondSepLine.bounds)
                                                                             , CGRectGetHeight(secondSepLine.bounds))];
    thridSepLine.image = sepImage;
    [_noAddressView addSubview:thridSepLine];
    
    //详细地址
    NSString *userAddressTitle = @"详细地址：";
    CGSize userAddressTitleSize = [userAddressTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                              , CGRectGetMaxY(thridSepLine.frame)
                                                                              , userAddressTitleSize.width
                                                                              , CGRectGetHeight(userNameTitleLabel.bounds))];
    userAddressTitleLabel.text = userAddressTitle;
    userAddressTitleLabel.font = [UIFont systemFontOfSize:15];
    userAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [_noAddressView addSubview:userAddressTitleLabel];
    
    _userAddressTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userAddressTitleLabel.frame)
                                                                         , CGRectGetMinY(userAddressTitleLabel.frame)
                                                                         , VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(userAddressTitleLabel.bounds)
                                                                         , CGRectGetHeight(userAddressTitleLabel.bounds))];
    _userAddressTextField.font = [UIFont systemFontOfSize:15];
    _userAddressTextField.placeholder = @"街道/小区/门牌号等详细信息";
    _userAddressTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [_noAddressView addSubview:_userAddressTextField];

//    //小区
//    NSString *xiaoquTitle = @"小区：";
//    CGSize xiaoquTitleSize = [xiaoquTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, kitemHeight)];
//    UILabel *xiaoquTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userTelePhoneTitleLabel.frame)
//                                                                         , CGRectGetMaxY(thridSepLine.frame)
//                                                                         , xiaoquTitleSize.width
//                                                                         , CGRectGetHeight(userTelePhoneTitleLabel.bounds))];
//    xiaoquTitleLabel.text = xiaoquTitle;
//    xiaoquTitleLabel.font = [UIFont systemFontOfSize:15];
//    xiaoquTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
//    [_noAddressView addSubview:xiaoquTitleLabel];
//    UIImage *dingWeiImage = [UIImage imageNamed:@"dingweiImage"];
//    UIImageView *dingWeiImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(xiaoquTitleLabel.frame) + 8
//                                                                                 ,(CGRectGetHeight(xiaoquTitleLabel.bounds) - dingWeiImage.size.height)/2 + CGRectGetMinY(xiaoquTitleLabel.frame)
//                                                                                 , dingWeiImage.size.width
//                                                                                 , dingWeiImage.size.height)];
//    dingWeiImageView.image = dingWeiImage;
//    [_noAddressView addSubview:dingWeiImageView];
//    
//    _xiaoQuAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(dingWeiImageView.frame) + 8
//                                                                   , CGRectGetMinY(xiaoquTitleLabel.frame)
//                                                                   , CGRectGetWidth(self.bounds) - 2*kleftSpace - xiaoquTitleSize.width - 8*2 - 44 - CGRectGetWidth(dingWeiImageView.bounds)
//                                                                   , CGRectGetHeight(xiaoquTitleLabel.bounds))];
////    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
//    _xiaoQuAddressLabel.textColor = [UIColor colorWithHexString:@"#000000"];
//    _xiaoQuAddressLabel.text = model.xiaoquName;
////    _xiaoQuAddressLabel.attributedText = [self dealWithLableFirstFontSize:15 secondFontSize:13 firstTextColor:@"#000000" secondTextColor:@"#a9a9a9" firstRang:NSMakeRange(0, 4) content:@"鸿盛新城-许昌路与洛宁路"];
//    _xiaoQuAddressLabel.textAlignment = NSTextAlignmentLeft;
//    [_noAddressView addSubview:_xiaoQuAddressLabel];
    
    //下订单不允许修改小区
//    _selectXiaoQuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_selectXiaoQuBtn addTarget:self action:@selector(selectXiaoQuEvent) forControlEvents:UIControlEventTouchUpInside];
//    [_selectXiaoQuBtn setImage:[UIImage imageNamed:@"changeXiaoQuImage"] forState:UIControlStateNormal];
//    _selectXiaoQuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//    _selectXiaoQuBtn.frame = CGRectMake(CGRectGetWidth(self.bounds) - kleftSpace - 44
//                                        , CGRectGetMinY(_xiaoQuAddressLabel.frame)
//                                        , 44
//                                        , CGRectGetHeight(_xiaoQuAddressLabel.bounds));
//    [_noAddressView addSubview:_selectXiaoQuBtn];
    
//    UIImageView *fourthSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
//                                                                              , CGRectGetMaxY(xiaoquTitleLabel.frame)
//                                                                              , CGRectGetWidth(firstSepLine.bounds)
//                                                                              , CGRectGetHeight(firstSepLine.bounds))];
//    fourthSepLine.image = sepImage;
//    [_noAddressView addSubview:fourthSepLine];
//    //楼号-单元-门牌号
//    NSString *detailAddressTitle = @"楼号-单元-门牌号：";
//    CGSize detailAddressTitleSize = [detailAddressTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(160, kitemHeight)];
//    UILabel *detailAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(xiaoquTitleLabel.frame)
//                                                                                , CGRectGetMaxY(fourthSepLine.frame)
//                                                                                , detailAddressTitleSize.width
//                                                                                , CGRectGetHeight(xiaoquTitleLabel.bounds))];
//    detailAddressTitleLabel.text = detailAddressTitle;
//    detailAddressTitleLabel.font = [UIFont systemFontOfSize:15];
//    detailAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
//    detailAddressTitleLabel.textAlignment = NSTextAlignmentLeft;
//    [_noAddressView addSubview:detailAddressTitleLabel];
//    
//    _detailAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_detailAddressBtn addTarget:self action:@selector(selectDetailAddressEvent) forControlEvents:UIControlEventTouchUpInside];
//    _detailAddressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//    
//    _detailAddressBtn.titleLabel.font = [UIFont systemFontOfSize:15];
//    [_detailAddressBtn setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
//    _detailAddressBtn.frame = CGRectMake(CGRectGetMaxX(detailAddressTitleLabel.frame)
//                                         , CGRectGetMinY(detailAddressTitleLabel.frame)
//                                         , CGRectGetWidth(self.bounds) - 2*kleftSpace - detailAddressTitleSize.width
//                                         , CGRectGetHeight(detailAddressTitleLabel.bounds));
//    [_noAddressView addSubview:_detailAddressBtn];
//    [self setDetailAddressTitle:@"请选择"];
    
}

//公共视图
- (void)setupCommonViewWithHadOrdersAddressStatus:(BOOL)isHadOrdersAddress  withSureOrderType:(SureOrderHeaderType)orderType{
    NSArray *payTypes = @[@"在线支付",@"货到付款"];;
    CGFloat y = 0;
    if (isHadOrdersAddress) {
        y = CGRectGetMaxY(self.hadAddressView.frame);
    } else {
        y = CGRectGetMaxY(self.noAddressView.frame);
    }
    
    if (orderType == kserviceSureorderHeader) {//服务订单确认
    //服务时间
        UIView *serviceTimeView = [[UIView alloc]initWithFrame:CGRectMake(0, y + 15
                                                                     , VIEW_WIDTH
                                                                      , 44)];
        [serviceTimeView.layer setBorderWidth:0.7];
        [serviceTimeView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        serviceTimeView.backgroundColor = [UIColor whiteColor];
        [self addSubview:serviceTimeView];
        
        //标题
        UILabel *serviceTimeTitle = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace, 0, 80, CGRectGetHeight(serviceTimeView.bounds))];
        serviceTimeTitle.text = @"服务时间";
        serviceTimeTitle.textAlignment = NSTextAlignmentLeft;
        serviceTimeTitle.font = [UIFont systemFontOfSize:15];
       // serviceTimeTitle.textColor = [UIColor colorWithHexString:@"#555555"];
        serviceTimeTitle.backgroundColor = [UIColor clearColor];
        [serviceTimeView addSubview:serviceTimeTitle];
        
        _selectTimeBtn = [FL_Button fl_shareButton];
        [_selectTimeBtn setBackgroundColor:[UIColor clearColor]];
        [_selectTimeBtn setImage:[UIImage imageNamed:@"right_arrow"] forState:UIControlStateNormal];
        _selectTimeBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_selectTimeBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        _selectTimeBtn.status = FLAlignmentStatusRight;
        [_selectTimeBtn setTitle:@"请选择服务时间" forState:UIControlStateNormal];
        [_selectTimeBtn addTarget:self action:@selector(selectServiceTimeEvent) forControlEvents:UIControlEventTouchUpInside];
        _selectTimeBtn.frame = CGRectMake(CGRectGetMaxX(serviceTimeTitle.frame) + kleftSpace
                                          , CGRectGetMinY(serviceTimeTitle.frame)
                                          , VIEW_WIDTH - CGRectGetMaxX(serviceTimeTitle.frame) - 2*kleftSpace
                                          , CGRectGetHeight(serviceTimeTitle.bounds));
        [serviceTimeView addSubview:_selectTimeBtn];
        y = CGRectGetMaxY(serviceTimeView.frame);
        
        payTypes = @[@"在线支付",@"服务后付款"];
        
    }
    //付款方式
    CheckBoxView *payTypeCheckBoxView = [[CheckBoxView alloc]initWithFrame:CGRectMake(0
                                                                                      , y + 15
                                                                                      , CGRectGetWidth(self.bounds)
                                                                                      , 89)
                                                                withTitles:payTypes
                                                          withCheckBoxType:kleftTitleAndRightImage
                                                               normalImage:[UIImage imageNamed:@"checkBoxNormalImage"]
                                                              slectedImage:[UIImage imageNamed:@"checkBoxSelectdImage"]];
    payTypeCheckBoxView.backgroundColor = [UIColor whiteColor];
    payTypeCheckBoxView.delegate = (id<CheckBoxViewDelegate>)self;
    [payTypeCheckBoxView.layer setBorderWidth:0.7];
    [payTypeCheckBoxView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    payTypeCheckBoxView.tag = 101;
    [self addSubview:payTypeCheckBoxView];
    
    if (orderType == kproductSureOrderHeader) {//商品确认订单
        //配送方式
        UIView *peiSongTypeView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                                      , CGRectGetMaxY(payTypeCheckBoxView.frame)+15
                                                                      , CGRectGetWidth(self.bounds)
                                                                      , CGRectGetHeight(payTypeCheckBoxView.bounds)+44)];
        
        peiSongTypeView.backgroundColor= [UIColor whiteColor];
        [peiSongTypeView.layer setBorderWidth:0.7];
        [peiSongTypeView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        [self addSubview:peiSongTypeView];

        //配送方式
        CheckBoxView *peiSongTypeCheckBoxView = [[CheckBoxView alloc]initWithFrame:CGRectMake(CGRectGetMinX(payTypeCheckBoxView.frame), 0,CGRectGetWidth(payTypeCheckBoxView.bounds),CGRectGetHeight(payTypeCheckBoxView.bounds))withTitles:@[@"门店配送",@"上门自取"] withCheckBoxType:kleftTitleAndRightImage normalImage:[UIImage imageNamed:@"checkBoxNormalImage"] slectedImage:[UIImage imageNamed:@"checkBoxSelectdImage"]];
        
        peiSongTypeCheckBoxView.backgroundColor = [UIColor whiteColor];
        peiSongTypeCheckBoxView.delegate = (id<CheckBoxViewDelegate>)self;
        peiSongTypeCheckBoxView.tag = 102;
        [peiSongTypeView addSubview:peiSongTypeCheckBoxView];
      
        //配送说明 /营业时间
        XiaoQuAndStoreInfoModel *model =[[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
        NSString * peiSongIntroduce =@"配送说明";
        CGSize peiSongIntroduceSize = [peiSongIntroduce sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, 44)];
        _peiSongIntroduceLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace, CGRectGetMaxY(peiSongTypeCheckBoxView.frame), peiSongIntroduceSize.width, 44)];
        _peiSongIntroduceLabel.font = [UIFont systemFontOfSize:15];
        _peiSongIntroduceLabel.text = peiSongIntroduce;
        _peiSongIntroduceLabel.textColor = [UIColor colorWithHexString:@"#000000"];
        [peiSongTypeView addSubview:_peiSongIntroduceLabel];
        
        _peiSongTypeViewContentLabel = [[UILabel alloc]initWithFrame:CGRectMake(2*kleftSpace + peiSongIntroduceSize.width, CGRectGetMinY(_peiSongIntroduceLabel.frame), CGRectGetWidth(self.bounds) - 3*kleftSpace - peiSongIntroduceSize.width - 4, CGRectGetHeight(_peiSongIntroduceLabel.bounds))];
        _peiSongTypeViewContentLabel.textAlignment = NSTextAlignmentRight;
        _peiSongTypeViewContentLabel.font = [UIFont systemFontOfSize:15];
        NSString *peiSongIntroduceContent =model.storePeiSongIntroduce;
        _peiSongTypeViewContentLabel.textColor =[UIColor colorWithHexString:@"#da411d"];
        _peiSongTypeViewContentLabel.text = peiSongIntroduceContent;
        [peiSongTypeView addSubview:_peiSongTypeViewContentLabel];

    
        //配送设置
        UIView *peiSongView = [[UIView alloc]initWithFrame:CGRectMake(0
                                                                      , CGRectGetMaxY(peiSongTypeView.frame)+15
                                                                      , CGRectGetWidth(self.bounds)
                                                                      , 89)];
    
        peiSongView.backgroundColor= [UIColor whiteColor];
        [peiSongView.layer setBorderWidth:0.7];
        [peiSongView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        [self addSubview:peiSongView];
        
    
        //配送时间
        NSString *peisongTimeTitle = @"送达时间";
        CGSize peiSongTimeTitleSize = [peisongTimeTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, 44)];
        UILabel *peisongTimeTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace, 0, peiSongTimeTitleSize.width, 44)];
        peisongTimeTitleLabel.font = [UIFont systemFontOfSize:15];
        peisongTimeTitleLabel.text = peisongTimeTitle;
        peisongTimeTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
        [peiSongView addSubview:peisongTimeTitleLabel];
        
        UILabel *peisontTimeContentLabel = [[UILabel alloc]initWithFrame:CGRectMake(2*kleftSpace + peiSongTimeTitleSize.width
                                                                                    , CGRectGetMinY(peisongTimeTitleLabel.frame)
                                                                                    , CGRectGetWidth(self.bounds) - 2*kleftSpace - peiSongTimeTitleSize.width - 4
                                                                                    , CGRectGetHeight(peisongTimeTitleLabel.bounds))];
        peisontTimeContentLabel.textAlignment = NSTextAlignmentRight;
        NSString *peisonTimeContent = @"立即配送（25分钟内送达）";
        NSMutableAttributedString *peisongTimeContentAttStr = [self dealWithLableFirstFontSize:15 secondFontSize:15 firstTextColor:@"#999999" secondTextColor:@"#da411d" firstRang:NSMakeRange(0, 4) content:peisonTimeContent];
        peisontTimeContentLabel.attributedText = peisongTimeContentAttStr;
        [peiSongView addSubview:peisontTimeContentLabel];
        
        //分割线
        UIImage *sepLine = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"]
                                                   frame:CGRectMake(0
                                                                    ,0
                                                                    ,CGRectGetWidth(self.bounds) - kleftSpace
                                                                    ,0.6)];
        UIImageView *sepLineView = [[UIImageView alloc]initWithImage:sepLine];
        sepLineView.frame = CGRectMake(kleftSpace
                                       , CGRectGetMaxY(peisontTimeContentLabel.frame)
                                       , CGRectGetWidth(self.bounds) - kleftSpace
                                       , 0.6);
        [peiSongView addSubview:sepLineView];
        //配送备注
        
        NSString *peisongMarkTitle = @"订单备注";
        CGSize peisongMarkTitleSize = [peisongMarkTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, 44)];
        UILabel *peisongMarkTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(peisongTimeTitleLabel.frame)
                                                                                  , CGRectGetMaxY(sepLineView.frame)
                                                                                  , peisongMarkTitleSize.width
                                                                                  , CGRectGetHeight(peisongTimeTitleLabel.bounds))];
        peisongMarkTitleLabel.font = [UIFont systemFontOfSize:15];
        peisongMarkTitleLabel.text = peisongMarkTitle;
        peisongMarkTitleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
        [peiSongView addSubview:peisongMarkTitleLabel];
        
        _markInfoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString *markInfoTitle = @"可输入特殊要求(选填)";
        UIImage *markInfoImage = [UIImage imageNamed:@"right_arrow"];
        _markInfoBtn.frame = CGRectMake(CGRectGetMaxX(peisongMarkTitleLabel.frame) + 12
                                        , CGRectGetMinY(peisongMarkTitleLabel.frame)
                                        , CGRectGetWidth(self.bounds) - 2*kleftSpace  - peisongMarkTitleSize.width - 12
                                        , CGRectGetHeight(peisongMarkTitleLabel.bounds));
        [_markInfoBtn setTitle:markInfoTitle forState:UIControlStateNormal];
        [_markInfoBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        [_markInfoBtn setImage:markInfoImage forState:UIControlStateNormal];
        _markInfoBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _markInfoBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        CGSize titleSize = [markInfoTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(CGRectGetWidth(self.bounds) - 2*kleftSpace - 12 - peisongMarkTitleSize.width
                                                                                                       , kitemHeight)];
        _markInfoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -titleSize.width);
        _markInfoBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0,  markInfoImage.size.width + 10);
        [_markInfoBtn addTarget:self action:@selector(addMarkInfoEvent:) forControlEvents:UIControlEventTouchUpInside];
        [peiSongView addSubview:_markInfoBtn];
    }
}

- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
- (void)updateViewWithReceivedAddressStatus:(BOOL)isHadReceivedAddress {
    [self.hadAddressView removeFromSuperview];
    [self.noAddressView removeFromSuperview];
    [self setupCommonViewWithHadOrdersAddressStatus:isHadReceivedAddress withSureOrderType:kproductSureOrderHeader];
}
#pragma mark -- CheckBoxViewDelegate
- (void)selectedItemWithIndex:(NSInteger)buttonTag withCheckBoxView:(CheckBoxView*)checkboxView {
    XiaoQuAndStoreInfoModel *model =[[ManagerGlobeUntil sharedManager]getxiaoQuAndStoreInfo];
    if (checkboxView.tag == 100) {//性别选择
        if (buttonTag == 0) {//男
            self.sexType = @"1";
        } else {//女
            self.sexType = @"2";
        }
        
    } else if (checkboxView.tag == 101) {//支付方式
        if (buttonTag == 0) {//在线支付
            self.payType = @"1";
        } else {//货到付款
             self.payType = @"2";
        }
    } else if (checkboxView.tag == 102) {//配送方式
        if (buttonTag == 0) {//送货
            self.sendProductType = @"1";
            self.peiSongIntroduceLabel.text =@"配送说明";
            self.peiSongTypeViewContentLabel.text = model.storePeiSongIntroduce;
        } else {//自取
             self.sendProductType = @"2";
            self.peiSongIntroduceLabel.text =@"营业时间";
            self.peiSongTypeViewContentLabel.text = model.storeYingYeTime;
        }
    }
}
#pragma mark -- button Action
- (void)selectXiaoQuEvent  {
    if (_delegate && [_delegate respondsToSelector:@selector(selectXiaoqu)]) {
        [_delegate selectXiaoqu];
    }
}

- (void)selectDetailAddressEvent {
    [self hideKeyboard];
    if (_delegate && [_delegate respondsToSelector:@selector(selectDetailAddress)]) {
        [_delegate selectDetailAddress];
    }
}

- (void)addMarkInfoEvent:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(addMarkInfo)]) {
        [_delegate addMarkInfo];
    }
    
}
- (void)modifiedAddressEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(modifiedAddress)]) {
        [_delegate modifiedAddress];
    }
}

- (void)selectServiceTimeEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(selectServiceTime)]) {
        [_delegate selectServiceTime];
    }
}
#pragma mark -- UIGestureRecognizerDelegate
- (void)hideKeyboard {
    if ([self.userNameTextField isFirstResponder])
    {
        [self.userNameTextField resignFirstResponder];
    }
    if ([self.userTelePhoneTextField isFirstResponder])
    {
        [self.userTelePhoneTextField resignFirstResponder];
    }
    if ([self.userAddressTextField isFirstResponder]) {
        [self.userAddressTextField resignFirstResponder];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.userNameTextField.isFirstResponder || self.userTelePhoneTextField.isFirstResponder || self.userAddressTextField.isFirstResponder) {
        return YES;
    }
    return NO;
}
#pragma mark -- 对字符串进行处理


- (void)setDetailAddressTitle:(NSString*)title {
//    UIImage *btnImage = [UIImage imageNamed:@"right_arrow"];
//    [_detailAddressBtn setImage:btnImage forState:UIControlStateNormal];
//    [_detailAddressBtn setTitle:title forState:UIControlStateNormal];
//    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(200, kitemHeight)];
//    _detailAddressBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -titleSize.width);
//    _detailAddressBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0,  btnImage.size.width + 10);
//    [_detailAddressBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    _userAddressTextField.text = title;
}

- (NSMutableAttributedString *)dealWithLableFirstFontSize:(CGFloat)firstFontSize secondFontSize:(CGFloat)secondFontSize firstTextColor:(NSString*)firstTextColor secondTextColor:(NSString*)secondTextColor firstRang:(NSRange)range content:(NSString*)content{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:content];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:firstTextColor] range:range];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:secondTextColor] range:NSMakeRange(range.length, content.length -range.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:firstFontSize] range:range];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:secondFontSize] range:NSMakeRange(range.length, content.length - range.length)];
    return str;
}

#pragma mark -- 赋值操作
- (void)resetMarkContent:(NSString*)markContent {
    UIImage *markInfoImage = [UIImage imageNamed:@"right_arrow"];
    NSString *title = @"";
    _markInfoBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if (markContent.length == 0) {
        title = @"可输入特殊要求(选填)";
    } else {
        title = markContent;
       
    }
   CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(CGRectGetWidth(self.markInfoBtn.bounds), kitemHeight)];
    CGFloat maxTitleWidth = CGRectGetWidth(self.markInfoBtn.bounds) - 2*markInfoImage.size.width;
    CGFloat leftSpace = 0;
    if (titleSize.width < maxTitleWidth ) {
        leftSpace = maxTitleWidth - titleSize.width - 10;
    }
    _markInfoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, (CGRectGetWidth(self.markInfoBtn.bounds) - markInfoImage.size.width), 0, 0);
    _markInfoBtn.titleEdgeInsets = UIEdgeInsetsMake(0,leftSpace, 0, markInfoImage.size.width);
    [self.markInfoBtn setTitle:title forState:UIControlStateNormal];
}

- (void)resetServiceDate:(NSString*)serviceDate {
    [_selectTimeBtn setTitle:serviceDate forState:UIControlStateNormal];
}

- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address {
    NSString *nameAndTelNumStr = [NSString stringWithFormat:@"%@    %@",name,telNumber];
    self.hadAddressNameLabel.text = nameAndTelNumStr;
    self.hadAddressAddressLabel.text = address;
}

@end
