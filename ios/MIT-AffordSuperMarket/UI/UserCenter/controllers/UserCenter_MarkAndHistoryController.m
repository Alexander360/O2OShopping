//
//  UserCenter_MarkAndHistoryController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "UserCenter_MarkAndHistoryController.h"
#import "Strore_detailController.h"
#import "Service_DetailController.h"
#import "Strore_Root_Controller.h"
#import "Home_Root_Controller.h"
//untils
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "ManagerHttpBase.h"
#import "Global.h"
#import "AppDelegate.h"
#import "EmptyPageView.h"
//view
#import "MyMarkAndHistory_Cell.h"

#import "FMDBManager.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "NSString+Conversion.h"
#import "ManagerGlobeUntil.h"
//vendor
#import "MJRefresh.h"
#import "RDVTabBarController.h"
#import "MTA.h"
@interface UserCenter_MarkAndHistoryController ()<UIAlertViewDelegate,EmptyPageViewDelegate>
{
    NSInteger _requestPage;//当前请求页
    NSInteger _totalCount;//数据总条数
    
}
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)UIView *bottomView;
@property(nonatomic, strong)NSString *rightNavItemTitle;
@property(nonatomic)BOOL isEditing;
@property(nonatomic,strong)EmptyPageView *emptyView;
@end

@implementation UserCenter_MarkAndHistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    _requestPage = 1;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    _dataSource = [[NSMutableArray alloc]init];
    [self addBackNavItem];
    [self setupSubView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if (self.markOrHistoyType == khistoryType) {
         [self loadHistoryListData];
    } else if (self.markOrHistoyType == kmarkType) {
        [self loadCollectionListDataWithPage:_requestPage requestCount:kRequestLimit];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"关注和历史"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"关注和历史"];
}
#pragma mark -- request data
//获取浏览历史数据
- (void)loadHistoryListData {
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSString *shopID=[NSString stringTransformObject:mode.storeID];
    NSArray *historyAry=[[FMDBManager sharedManager]HistoryQueryDataWithShopID:shopID];  
    self.dataSource=[historyAry mutableCopy];
    if (self.dataSource.count == 0) {
        [self changeRightButtonShow];
        self.emptyView.hidden=NO;
        self.tableView.hidden=YES;
    } else {
        [self changeRightButtonShow];
        self.emptyView.hidden=YES;
        self.tableView.hidden=NO;
        [self.tableView reloadData];
    }
    
}
//获取关注数据
- (void)loadCollectionListDataWithPage:(NSInteger)page  requestCount:(NSInteger)count {
    __weak typeof(self)weakSelf = self;
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    //请求参数说明： SIGNATURE  设备识别码
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:userInfoMode.token forKey:@"TOKEN"];
    [parameter setObject:xiaoQuAndStoreModel.storeID forKey:@"STOREID"];
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [parameter setObject:[NSNumber numberWithInteger:page] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:count] forKey:@"PAGESIZE"];
    [manager parameters:parameter customPOST:@"user/collectionList" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                _totalCount = [[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"TOTALROW"]] integerValue];
                
                [weakSelf.dataSource removeAllObjects];
                
                [weakSelf.dataSource addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LIST"]];
                if (weakSelf.dataSource.count == 0) {
                    [weakSelf changeBottomViewShowWithEditStatus:NO];
                    weakSelf.emptyView.hidden=NO;
                    weakSelf.tableView.hidden=YES;
                }else{
                    weakSelf.emptyView.hidden=YES;
                    weakSelf.tableView.hidden=NO;
                }
                [weakSelf changeRightButtonShow];
                [weakSelf.tableView reloadData];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        [weakSelf endRefresh];
        
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [weakSelf endRefresh];
    }];
}

//取消收藏
- (void)LoadCancelCollectionProductionDataWithProductID:(NSArray*)goodsIDs {
    //请求参数说明： SIGNATURE  设备识别码
    //      TOKEN    用户签名
    //      GOODSID         商品ID
    NSMutableArray *idArr = [[NSMutableArray alloc]init];
    for (NSDictionary *dataDic in goodsIDs) {
        [idArr addObject:[dataDic objectForKey:@"ID"]];
    }
   NSString *goodIDs=[idArr componentsJoinedByString:@","];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringTransformObject:mode.token] forKey:@"TOKEN"];
    [parameter setObject:goodIDs forKey:@"GOODSID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/cancelCollect" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                // msg = @"取消关注";
                if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
                    [weakSelf loadCollectionListDataWithPage:_requestPage requestCount:kRequestLimit];
                }
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
       
        
    } failure:^(bool isFailure) {
        
        
    }];
}
//黑条效果
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section+1 ==self.dataSource.count)
    {
        return 0;
    }else{
        return HightScalar(12);
    }
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    UIView *views = [[UIView alloc]init];
    views.backgroundColor = [UIColor clearColor];
    return views;
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    MyMarkAndHistory_Cell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[MyMarkAndHistory_Cell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        [cell.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        [cell.layer setBorderWidth:0.7];
    }
    if (self.markOrHistoyType == khistoryType) {
        if (self.dataSource.count !=0) {
        [cell updataWithDataDic:[self.dataSource objectAtIndex:indexPath.section]];
        }
    } else if (self.markOrHistoyType == kmarkType) {
        if (self.dataSource.count !=0) {
          [cell updataCollectionDataDic:[self.dataSource objectAtIndex:indexPath.section]];
        }
        
    }
    
    return cell;
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return HightScalar(153);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //if ([[ManagerHttpBase sharedManager] reachabilityManager].reachable) {
        
        if (_isEditing==NO) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            Strore_detailController *detailVc=[[Strore_detailController alloc]init];
            Service_DetailController *serviceDetailVc=[[Service_DetailController alloc]init];
            NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.section];
            if (self.markOrHistoyType == khistoryType) {
                if ([[dataDic objectForKey:@"goodID"] length]!=0) {
                    detailVc.goodsID = [NSString stringTransformObject:[dataDic objectForKey:@"goodID"]];
                    detailVc.isSelf = [NSString stringTransformObject:[dataDic objectForKey:@"isProprietary"]];
                     [self.navigationController pushViewController:detailVc animated:YES];
                }else if([[dataDic objectForKey:@"serviceshopID"] length]!=0){
                    serviceDetailVc.shopID=[dataDic objectForKey:@"serviceshopID"];
                    serviceDetailVc.shopName =[dataDic objectForKey:@"name"];
                     [self.navigationController pushViewController:serviceDetailVc animated:YES];
                }
                
            }else if (self.markOrHistoyType == kmarkType) {
                detailVc.goodsID = [NSString stringTransformObject:[dataDic objectForKey:@"ID"]];
                  detailVc.isSelf = [NSString stringTransformObject:[dataDic objectForKey:@"ISSELF"]];
                [self.navigationController pushViewController:detailVc animated:YES];
            }
            
           
        }
   // }
}

#pragma mark - 上拉/下拉  更新数据
-(void)headerRereshing {
    if (self.markOrHistoyType == kmarkType) {
        if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
            _requestPage = 1;
            [self loadCollectionListDataWithPage:_requestPage requestCount:kRequestLimit];
        }else{
            [self endRefresh];
        }
        
    }
   
}

-(void)footerRereshing
{
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        if (self.markOrHistoyType == kmarkType) {
            if (_requestPage*kRequestLimit < _totalCount) {
                _requestPage++;
                [self loadCollectionListDataWithPage:_requestPage requestCount:kRequestLimit];
            } else {
                [self endRefresh];
            }
        }
    }else{
        [self endRefresh];
    }
    
    
}
- (void)endRefresh
{
    [self.tableView headerEndRefreshing];
    [self.tableView footerEndRefreshing];
}

#pragma mark -- button Action
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)operationMyMark:(id)sender {
    UIButton *button = (UIButton*)sender;
      NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
     if (selectedRows.count > 0) {
    if (button.tag == 100) {//加入购物车
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否加入购物车?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=1;
        [alert show];
       
    } else if(button.tag == 101) {//取消关注
       
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否取消关注?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=2;
         [alert show];
        
    }

        
     }else{
          [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"还未选中商品~" inView:self.view];
     }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1) {
        if (buttonIndex == 1){
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        
            if (selectedRows.count > 0) {
            XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
            for (NSIndexPath *selectionIndex in selectedRows)
            {

                NSDictionary*dic=[self.dataSource objectAtIndex:selectionIndex.section];
                
                NSString *shopID=[NSString stringTransformObject:[NSString stringTransformObject:mode.storeID]];
                NSString *goodID=[NSString stringTransformObject:[dic objectForKey:@"ID"]];
                NSString *name =[NSString stringTransformObject:[dic objectForKey:@"NAME"]];
                UIImage *image =[UIImage createImageWithImageUrlString:[NSString stringTransformObject:[dic objectForKey:@"THUMB"]]];
                if (image==nil) {
                    image=[UIImage imageNamed:@"productDetail_Default"];
                }
                NSData *picture =UIImagePNGRepresentation(image);
                NSString *type=[NSString stringTransformObject:[dic objectForKey:@"TYPE"]];
                NSString *price=[NSString stringTransformObject:[dic objectForKey:@"SELLPRICE"]];
                NSString *oldprice=[NSString stringTransformObject:[dic objectForKey:@"MARKETPRICE"]];
                NSMutableArray *goodSqlite=[[FMDBManager sharedManager]QueryDataWithShopID:shopID goodID:goodID];
                if (goodSqlite.count ==0) { //购物车没数据 先存
                    NSString *buynum= @"1";
                    NSString *ischoose=@"1";
                    NSString *isProprietary =[NSString stringTransformObject:[dic objectForKey:@"ISSELF"]];
                    if ([isProprietary isEqualToString:@"1"]) {
                        shopID =@"";
                    }
                [[FMDBManager sharedManager]InsertWithshopID:shopID goodID:goodID name:name Picture:picture type:type price:price oldprice:oldprice buynum:buynum ischoose:ischoose isProprietary:isProprietary];
                }else{//购物车有数据 更新数据
//
                [[FMDBManager sharedManager]modifyShopCarDataWithshopID:shopID goodID:goodID name:name type:type price:price oldprice:oldprice];
                }
              }
            }
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"加入成功" inView:self.view];
        }
    }
    if (alertView.tag==2) {
        if (buttonIndex == 1){
        NSMutableArray * selectedGoodIDs=[[NSMutableArray alloc]init];
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        for (NSIndexPath *selectionIndex in selectedRows)
        {
            
            NSDictionary*dic=[self.dataSource objectAtIndex:selectionIndex.section];
            
            [selectedGoodIDs addObject:dic];
            }
        [self LoadCancelCollectionProductionDataWithProductID:selectedGoodIDs];
        }
    }
    if(alertView.tag==3){
        if (buttonIndex == 1){
        [self.dataSource removeAllObjects];
        [self changeRightButtonShow];
        [[FMDBManager sharedManager]deleteAll:@"t_history"];
        self.emptyView.hidden=NO;
        self.tableView.hidden=YES;
        [self.tableView reloadData];
      }
    }
    
}

- (void)rigthBtnAction:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
    _isEditing=button.selected;
    if (self.markOrHistoyType == khistoryType) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否清空记录?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=3;
        [alert show];
        
    } else {
        [ button setTitle:button.selected ? @"完成" : @"编辑" forState:UIControlStateNormal];
        if (self.dataSource.count!=0) {
            [self.tableView setEditing:button.selected animated:YES];
            [self changeBottomViewShowWithEditStatus:button.selected];
        } else {
            [self changeRightButtonShow];
             [self changeBottomViewShowWithEditStatus:NO];
        }
       
    }
    
}

- (void)changeBottomViewShowWithEditStatus:(BOOL)isEdit {
    CGRect bottomViewFrame = self.bottomView.frame;
    CGRect tableViewFrame =  self.tableView.frame;
    if (isEdit) {
        bottomViewFrame.origin.y = CGRectGetHeight(self.view.bounds) - 64;
        tableViewFrame.size.height = CGRectGetHeight(self.view.bounds) - 64 ;
    } else  {
        bottomViewFrame.origin.y = CGRectGetHeight(self.view.bounds);
        tableViewFrame.size.height = CGRectGetHeight(self.view.bounds);
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.bottomView.frame = bottomViewFrame;
        self.tableView.frame = tableViewFrame;
    }];
    
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds)
                                                              , CGRectGetHeight(self.view.bounds) - 64)
                                             style:UITableViewStylePlain];
    _tableView.delegate = (id<UITableViewDelegate>)self;
    _tableView.dataSource = (id<UITableViewDataSource>)self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
    _tableView.allowsMultipleSelectionDuringEditing = YES;
    if (self.markOrHistoyType == kmarkType) {
        //添加下拉刷新
        [_tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
        //添加上拉加载更多
        [_tableView addFooterWithTarget:self action:@selector(footerRereshing)];
    }
    //数据为空时显示的View
    [self.view addSubview:_tableView];

    if (self.markOrHistoyType==kmarkType) {
       _emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64) EmptyPageViewType:KMarkView];
    }else if(self.markOrHistoyType==khistoryType)
    {
       _emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64) EmptyPageViewType:kHistoyView];
    }
    _emptyView.backgroundColor=[UIColor whiteColor];
    _emptyView.hidden=YES;
    _emptyView.delegate =self;
     [self.view addSubview:_emptyView];
    
}
//立即逛逛 跳转
-(void)buttonClick
{
   
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(action) userInfo:nil repeats:NO];
    
   
}
-(void)action
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    Home_Root_Controller *homeVC = [[[appDelegate.tabBarController.viewControllers firstObject] childViewControllers] firstObject];
    [homeVC.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
    [appDelegate selectedControllerAtTabBarIndex:0];
}
#pragma mark -- setter method
- (void)setNavItemTitle:(NSString *)navItemTitle {
    self.title = navItemTitle;
}

- (void)changeRightButtonShow{
    if (self.dataSource.count == 0) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        if (!self.navigationItem.rightBarButtonItem) {
            _rightNavItemTitle = @"";
            if (self.markOrHistoyType == kmarkType) {
                _rightNavItemTitle = @"编辑";
            } else if (self.markOrHistoyType == khistoryType) {
                _rightNavItemTitle = @"清空";
            }
        [self addRightNavItemWithTitle:_rightNavItemTitle];
        }
    }
}
- (void)setMarkOrHistoyType:(MarkOrHistoyType)markOrHistoyType {
//    _rightNavItemTitle = @"";
//    if (markOrHistoyType == kmarkType) {
//        _rightNavItemTitle = @"编辑";
//    } else if (markOrHistoyType == khistoryType) {
//        _rightNavItemTitle = @"清空";
//    }
//    [self addRightNavItemWithTitle:_rightNavItemTitle];
    _markOrHistoyType = markOrHistoyType;
}
#pragma mark -- getter method
- (UIView*)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)
                                                             , CGRectGetWidth(self.view.bounds)
                                                             , 64)];
        _bottomView.backgroundColor = [UIColor clearColor];
        NSArray *titles = @[@"加入购物车",@"取消关注"];
        CGFloat buttonWidth = (CGRectGetWidth(self.view.bounds) - 1)/titles.count;
        UIImage *normalImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#ffffff"] frame:CGRectMake(0, 0, buttonWidth, 64)];
        UIImage *hightImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#efefef"] frame:CGRectMake(0, 0, buttonWidth, 64)];
        for (NSInteger i = 0; i< [titles count]; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake((buttonWidth + 1)*i, 0, buttonWidth, 64);
            [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            [button setBackgroundImage:normalImage forState:UIControlStateNormal];
            [button setBackgroundImage:hightImage forState:UIControlStateHighlighted];
            [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:16];
            button.tag = 100 + i;
            [button addTarget:self action:@selector(operationMyMark:) forControlEvents:UIControlEventTouchUpInside];
           
            [_bottomView addSubview:button];
        }
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
