package com.dashihui.afford.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dashihui.afford.AffordApp;
import com.dashihui.afford.R;
import com.dashihui.afford.business.BusinessCommon;
import com.dashihui.afford.business.BusinessOrder;
import com.dashihui.afford.business.BusinessUser;
import com.dashihui.afford.business.entity.EtySendToUI;
import com.dashihui.afford.common.base.BaseActivity;
import com.dashihui.afford.common.constants.AffConstans;
import com.dashihui.afford.common.constants.CommConstans;
import com.dashihui.afford.sqlite.SqliteShoppingCart;
import com.dashihui.afford.thirdapi.greedsqlite.ShoppingCart;
import com.dashihui.afford.ui.activity.my.AtyMyAddress;
import com.dashihui.afford.ui.adapter.AdapterSettlement;
import com.dashihui.afford.ui.model.ModelDoAdd;
import com.dashihui.afford.ui.widget.WgtAlertDialog;
import com.dashihui.afford.util.list.UtilList;
import com.dashihui.afford.util.number.UtilNumber;
import com.dashihui.afford.util.preferences.UtilPreferences;
import com.dashihui.afford.util.toast.UtilToast;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.List;
import java.util.Map;

public class AtySettlement extends BaseActivity {

    @ViewInject(R.id.left_back)
    private ImageButton mLeftBack;
    @ViewInject(R.id.ibtn_userinfo)
    private ImageButton mIbtEditAddr;
    private BusinessUser mBllUser;
    private BusinessOrder mBllOrder;
    @ViewInject(R.id.noAddress)
    private TextView mNoAddress;
    @ViewInject(R.id.top_address)
    private LinearLayout mLytTopaddress;
    @ViewInject(R.id.addAddress)
    private LinearLayout mLytAddAddress;


    @ViewInject(R.id.username)
    private TextView mUserName;//用户名字
    @ViewInject(R.id.usersex)
    private TextView mUserSex;//性别
    @ViewInject(R.id.userphone)
    private TextView mUserPhone;//用户电话
    @ViewInject(R.id.useraddr)
    private TextView mUserAddr;//用户地址

    /*************************/
    @ViewInject(R.id.payType)
    private RadioGroup mPayType;//支付方式
    @ViewInject(R.id.takeType)
    private RadioGroup takeType;//配送方式
    @ViewInject(R.id.delivery_remark)
    private TextView mRemarkTv;

    @ViewInject(R.id.goods_price)
    private TextView mGoodsPrice;//商品金额
    @ViewInject(R.id.stilpay_money)
    private TextView mPayMoney;//需付款
    @ViewInject(R.id.txtViewPrice)
    private TextView mPrice;//实付款

    @ViewInject(R.id.txtViewSettlement)
    private TextView mTxtViewSettlement;//提交订单

    @ViewInject(R.id.lyt_sendexplain)//配送说明
    private LinearLayout mLytSendExplain;
    @ViewInject(R.id.lyt_sendtime)//营业时间
    private LinearLayout mLytOpenExplain;

    @ViewInject(R.id.send_time)//配送说明时间
    private TextView mSendTime;
    @ViewInject(R.id.sendtime)//营业时间
    private TextView mOpenTime;

    @ViewInject(R.id.listView)
    private ListView mListView;


    private AdapterSettlement mAdapterSettlement;
    private List<ShoppingCart> mShopCartList;

    private ModelDoAdd mModelDoAdd;
    private String mOrderInfo;//从FragmentOrderState获取再来一单的订单信息
    public final static String ORDERTAG = "orderTag";
    private BusinessCommon mBllCommon;
    private Map<String, Object> mMapObject;
    private List<Map<String, Object>> mListMaps;
    private int mNoAddressType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_settlement);
        ViewUtils.inject(this);
        mLytSendExplain.setVisibility(View.VISIBLE);
        mLytOpenExplain.setVisibility(View.GONE);
        mBllUser = new BusinessUser(this);
        mModelDoAdd = new ModelDoAdd();
        mBllOrder = new BusinessOrder(this);
        mBllCommon = new BusinessCommon(this);
        //获取默认地址
        mBllUser.addresslist();
        //支付方式初始化
        mModelDoAdd.setPaytype("1");
        //送货方式初始化
        mModelDoAdd.setTaketype("1");
        //初始化数据
        initData();
        //支付方式
        initPayTypeView();
        //配送方式
        initTakeTypeView();

    }

    //支付方式
    public void initPayTypeView() {
        mPayType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                /****&&  设置支付方式   &&******/
                switch (checkedId) {
                    case R.id.confirm_weixin_cBoxAgree://线上支付
                        mModelDoAdd.setPaytype("1");
                        LogUtils.e("onPayTypeCheckedChanged======线上支付========>" + checkedId);
                        break;
                    case R.id.confirm_delivery_cBoxAgree://货到付款
                        mModelDoAdd.setPaytype("2");
                        LogUtils.e("onPayTypeCheckedChanged======货到付款=======>" + checkedId);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    //配送方式
    public void initTakeTypeView() {
        takeType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                LogUtils.e("onTakeTypeCheckedChanged======AtySettlement=========>" + checkedId);
                switch (checkedId) {
                    case R.id.confirm_store_cBoxAgree://门店配送
                        mModelDoAdd.setTaketype("1");

                        mLytSendExplain.setVisibility(View.VISIBLE);
                        mLytOpenExplain.setVisibility(View.GONE);
                        if (!"null".equals(mMapObject.get("DELIVERYDES") + "")) {
                            mSendTime.setText(mMapObject.get("DELIVERYDES") + "");
                        } else {
                            mSendTime.setText("无");
                        }

                        LogUtils.e("onTakeTypeCheckedChanged======门店配送=======>" + checkedId);
                        break;
                    case R.id.confirm_myself_cBoxAgree://上门自取
                        mModelDoAdd.setTaketype("2");
                        mLytSendExplain.setVisibility(View.GONE);
                        mLytOpenExplain.setVisibility(View.VISIBLE);
                        mOpenTime.setText(mMapObject.get("BEGINTIME") + "—" + mMapObject.get("ENDTIME"));

                        LogUtils.e("onTakeTypeCheckedChanged======上门自取=======>" + checkedId);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        LogUtils.e("是否登录================>" + AffordApp.isLogin());
        //是否登录
        if (!AffordApp.isLogin()) {
            //没有登录
            LogUtils.e("没有登录======isLogin==========>");
            Intent intent = new Intent(AtySettlement.this, AtyLogin.class);
            intent.putExtra(CommConstans.Login.INTENT_KEY, CommConstans.Login.INTENT_VALUE_SETTLEMENT);
            startActivity(intent);
            finish();
        }
        /****&&  设置店铺ID   &&******/
        if (AffordApp.getInstance().getEntityLocation() != null && AffordApp.getInstance().getEntityLocation().getSTORE() != null) {
            mModelDoAdd.setStoreid(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");
        } else {
            LogUtils.e("null===Error====AtySettlement===>" + AffordApp.LOG_PHONE);
            finish();
        }

        //获取默认地址
        mBllUser.defaultAddress();
        LogUtils.e("onResume=======storeID==========>" + AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");
        mBllCommon.storeDetail(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "");
        LogUtils.e("登录======isLogin==========>");
        //获取用户地址列表
//        mNoAddressType = UtilNumber.IntegerValueOf(getIntent().getStringExtra(AtySettlementAddress.NOADDRESS));
//        LogUtils.e("onResume==========mNoAddressType=========>" + mNoAddressType);
//        if (mNoAddressType != 100) {
//            mBllUser.addresslist();
//        }
        //解除禁止多次提交
        mTxtViewSettlement.setEnabled(true);
        super.onResume();
    }


    @Override
    public void onSuccess(EtySendToUI beanSendUI) {
        if (beanSendUI != null) {
            switch (beanSendUI.getTag()) {
                case AffConstans.BUSINESS.TAG_USER_DEFAULTADDRESS://默认地址
                    LogUtils.e("onSuccess======默认地址========>" + beanSendUI.getInfo());
                    Map<String, String> defaultAddr = (Map<String, String>) beanSendUI.getInfo();
                    mNoAddress.setVisibility(View.GONE);
                    mLytTopaddress.setVisibility(View.VISIBLE);
                    mUserName.setText(defaultAddr.get("LINKNAME") + "");//用户名字
                    String sex = defaultAddr.get("SEX") + "";
                    /****&&  设置性别   &&******/
                    if ("1".equals(sex)) {
                        mUserSex.setText("先生 ");//性别
                        mModelDoAdd.setSex("先生 ");
                    } else {
                        mUserSex.setText("女士");//性别
                        mModelDoAdd.setSex("女士");
                    }
                    mUserPhone.setText(defaultAddr.get("TEL") + "");//用户电话
                    if (defaultAddr.get("ADDRESS") == null){
                        mBaseUtilAty.startActivity(AtySettlementAddress.class);
                    }else {
                        mUserAddr.setText(defaultAddr.get("ADDRESS") + "");//用户地址
                    }
                    break;
                case AffConstans.BUSINESS.TAG_ORDER_SAVA://提交订单
                    Map<String, Object> order = (Map<String, Object>) beanSendUI.getInfo();
                    LogUtils.e("onSuccess======提交订单========>" + order);
                    if (order != null) {
                        LogUtils.e("onSuccess===== mModelDoAdd.getPaytype()=支付方式========>" + mModelDoAdd.getPaytype());
                        if ("1".equals(mModelDoAdd.getPaytype())) {
                            Intent mapIntent = new Intent(this, AtySettlementOrder.class);
                            mapIntent.putExtra(AtySettlementOrder.ORDER_PRICE, order.get("AMOUNT") + "");
                            mapIntent.putExtra(AtySettlementOrder.ORDER_CODE, order.get("ORDERNUM") + "");
                            //传输一个支付标志，区分商品、服务、家政订单支付
                            mapIntent.putExtra(AtySettlementOrder.ORDER_PAY_TYPE, CommConstans.ORDER.ORDER_PAY);
                            startActivity(mapIntent);

                            //删除购物车里已经购买的商品
                            List<ShoppingCart> shopCartList = SqliteShoppingCart.getInstance(this).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "", true);
                            for (int i = 0; i < shopCartList.size(); i++) {
                                SqliteShoppingCart.getInstance(this).deleteShoppingCart(shopCartList.get(i));
                            }
                            //存储本地
                            UtilPreferences.putInt(AtySettlement.this, AtyShoppingCart.SHOPCART_NUM, 0);
                            UtilPreferences.putString(AtySettlement.this, AtyShoppingCart.SHOPCART_PRICE, "0");
                            sendShopChartBroadcast();
                            finish();
                        } else if ("2".equals(mModelDoAdd.getPaytype())) {
                            Intent mapIntent = new Intent(this, AtyOrdertDetail.class);
                            mapIntent.putExtra(AtyOrdertDetail.TABCODE, 1);
                            mapIntent.putExtra(AtySettlementOrder.ORDER_PRICE, order.get("AMOUNT") + "");
                            mapIntent.putExtra(AtySettlementOrder.ORDER_CODE, order.get("ORDERNUM") + "");
                            startActivity(mapIntent);
                            UtilToast.show(this, "订单提交成功！", Toast.LENGTH_SHORT);

                            //删除购物车里已经购买的商品
                            List<ShoppingCart> shopCartList = SqliteShoppingCart.getInstance(this).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "", true);

                            for (int i = 0; i < shopCartList.size(); i++) {
                                SqliteShoppingCart.getInstance(this).deleteShoppingCart(shopCartList.get(i));
                            }
                            //存储本地
                            UtilPreferences.putInt(AtySettlement.this, AtyShoppingCart.SHOPCART_NUM, 0);
                            UtilPreferences.putString(AtySettlement.this, AtyShoppingCart.SHOPCART_PRICE, 0 + "");
                            sendShopChartBroadcast();

                            finish();
                        } else {
                            LogUtils.e("onSuccess======请选择支付方式========>" + mModelDoAdd.getPaytype());
                        }

                    } else {
                        LogUtils.e("onSuccess======提交订单===error=====>" + order);
                    }
                    break;
                case AffConstans.BUSINESS.TAG_STORE_DETAIL://获取商铺详情
                    LogUtils.e("onSuccess==============beanSendUI.getInfo()============>" + beanSendUI.getInfo());
                    mMapObject = (Map<String, Object>) beanSendUI.getInfo();

                    mLytSendExplain.setVisibility(View.VISIBLE);
                    mLytOpenExplain.setVisibility(View.GONE);
                    LogUtils.e("onSuccess=========mMapObject=========>" + mMapObject.get("DELIVERYDES") + "");
                    if (!"null".equals(mMapObject.get("DELIVERYDES") + "")) {
                        mSendTime.setText(mMapObject.get("DELIVERYDES") + "");
                    } else {
                        mSendTime.setText("无");
                    }
                    break;
                case AffConstans.BUSINESS.TAG_USER_ADDRESSLIST://收货地址列表
                    mListMaps = (List<Map<String, Object>>) beanSendUI.getInfo();
                    if (UtilList.isEmpty(mListMaps)) {
                        mBaseUtilAty.startActivity(AtySettlementAddress.class);
                    }
                    break;

                default:
                    LogUtils.e("onSuccess===default===========>" + beanSendUI);
                    break;
            }
        } else {
            LogUtils.e("onSuccess======AtyHome=========>" + beanSendUI);
        }
        //去提交订单按钮恢复
        mTxtViewSettlement.setEnabled(true);
    }

    @Override
    public void onFailure(EtySendToUI beanSendUI) {
        if (beanSendUI != null) {
            switch (beanSendUI.getTag()) {
                case AffConstans.BUSINESS.TAG_USER_DEFAULTADDRESS://默认地址
                    LogUtils.e("onFailure======默认地址========>" + beanSendUI.getInfo());
                    /**********  设置地址  **********/
                    mUserAddr.setText("");
                    /**********  收货人姓名 **********/
                    mUserName.setText("");
                    /**********  收货人电话 **********/
                    mUserPhone.setText("");
                    mNoAddress.setVisibility(View.VISIBLE);
                    mLytTopaddress.setVisibility(View.GONE);
                    break;
                case AffConstans.BUSINESS.TAG_ORDER_SAVA://提交订单
                    LogUtils.e("onFailure======提交订单========>" + beanSendUI.getInfo());
                    if (mDialog == null) {
                        mDialog = new WgtAlertDialog();
                    }
                    mDialog.show(this,
                            "确定",
                            beanSendUI.getInfo().toString(),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    AtySettlement.this.finish();
                                }
                            }, false, false);
                    break;
                default:
                    LogUtils.e("onFailure===default===========>" + beanSendUI);
                    break;
            }
        } else {
            LogUtils.e("onFailure======AtySettlement=========>" + beanSendUI);
        }
        //去提交订单按钮恢复
        mTxtViewSettlement.setEnabled(true);
    }

//    //支付方式
//    @OnRadioGroupCheckedChange(R.id.payType)
//    public void onPayTypeCheckedChanged(RadioGroup group, int checkedId) {
//        LogUtils.e("onPayTypeCheckedChanged======AtySettlement=========>" + checkedId);
//        /****&&  设置支付方式   &&******/
//        switch (checkedId) {
//            case R.id.confirm_weixin_cBoxAgree://线上支付
//                mModelDoAdd.setPaytype("1");
//                LogUtils.e("onPayTypeCheckedChanged======线上支付========>" + checkedId);
//                break;
//            case R.id.confirm_delivery_cBoxAgree://货到付款
//                mModelDoAdd.setPaytype("2");
//                LogUtils.e("onPayTypeCheckedChanged======货到付款=======>" + checkedId);
//                break;
//            default:
//                break;
//        }
//    }

//    //配送方式
//    @OnRadioGroupCheckedChange(R.id.takeType)
//    public void onTakeTypeCheckedChanged(RadioGroup group, int checkedId) {
//        LogUtils.e("onTakeTypeCheckedChanged======AtySettlement=========>" + checkedId);
//        switch (checkedId) {
//            case R.id.confirm_store_cBoxAgree://门店配送
//                mModelDoAdd.setTaketype("1");
//
//                mLytSendExplain.setVisibility(View.VISIBLE);
//                mLytOpenExplain.setVisibility(View.GONE);
//                mSendTime.setText(mMapObject.get("DELIVERYDES") + "");
//
//                LogUtils.e("onTakeTypeCheckedChanged======门店配送=======>" + checkedId);
//                break;
//            case R.id.confirm_myself_cBoxAgree://上门自取
//                mModelDoAdd.setTaketype("2");
//                mLytSendExplain.setVisibility(View.GONE);
//                mLytOpenExplain.setVisibility(View.VISIBLE);
//                mOpenTime.setText(mMapObject.get("BEGINTIME") + "—" + mMapObject.get("ENDTIME"));
//
//                LogUtils.e("onTakeTypeCheckedChanged======上门自取=======>" + checkedId);
//                break;
//            default:
//                break;
//        }
//    }

    /**
     * 提交订单
     *
     * @param v
     */
    @OnClick(R.id.txtViewSettlement)//提交订单
    public void onSettlementClick(View v) {
        mBllUser.addresslist();//再次获取地址列表
        LogUtils.e("=============提交订单=======1========>");
        //禁止多次提交
        mTxtViewSettlement.setEnabled(false);
        /**********  设置地址  **********/
        String address = mUserAddr.getText().toString().trim();
        /**********  收货人姓名 **********/
        String userName = mUserName.getText().toString().trim();
        /**********  收货人电话 **********/
        String phone = mUserPhone.getText().toString().trim();
        /**********  订单备注 **********/
        String describe = mRemarkTv.getText().toString().trim();

        if (!"".equals(address) && !"".equals(userName) && !"".equals(phone)) {
            mModelDoAdd.setAddress(address);
            mModelDoAdd.setLinkname(userName);
            mModelDoAdd.setTel(phone);

            mModelDoAdd.setDescribe(describe);

            mBllOrder.sava(mModelDoAdd);
            LogUtils.e("=============提交订单=======2========>");
        } else if (UtilList.isEmpty(mListMaps)) {
            mBaseUtilAty.startActivity(AtySettlementAddress.class);
        } else {
            if (mDialog == null) {
                mDialog = new WgtAlertDialog();
            }
            mDialog.show(this,
                    "确定",
                    "请选择收货地址！",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            mBaseUtilAty.startActivity(AtyMyAddress.class);
                        }
                    }, false, false);
            return;
        }
    }

    @OnClick(R.id.left_back)//返回
    public void onBackClick(View v) {
        onBackPressed();
    }

    @OnClick({(R.id.rlyt_address), (R.id.ibtn_userinfo)})
    public void onAddAdressClick(View v) {
        mBaseUtilAty.startActivity(AtyMyAddress.class);
    }

    /**
     * 动态设置listview高度
     *
     * @param listView
     */
    private void setlistViewHeigh(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        ((ViewGroup.MarginLayoutParams) params).setMargins(10, 10, 10, 10);
        listView.setLayoutParams(params);

    }

    @OnClick(R.id.lyt_remark)
    public void onLytRemarkClick(View v) {
        Intent intent = new Intent(AtySettlement.this, AtySettlementRemark.class);
        intent.putExtra("remark",mRemarkTv.getText()+"");
        startActivityForResult(intent, CommConstans.ADDRESS.MARS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CommConstans.ADDRESS.MARS:
                if (data != null && data.getExtras().size() > 0) {
                    Bundle _remBunlde = data.getExtras();
                    String remText = _remBunlde.getString(AtySettlementRemark.REMARK);

                    mRemarkTv.setText(remText);
                } else {
                    mRemarkTv.setText("");
                    mRemarkTv.setHint("备注（可选）");
                }
                break;
            default:
                break;
        }
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if (AffordApp.getInstance().getEntityLocation() != null && AffordApp.getInstance().getEntityLocation().getSTORE() != null) {
            mShopCartList = SqliteShoppingCart.getInstance(this).getListShoppingCartByShopID(AffordApp.getInstance().getEntityLocation().getSTORE().getID() + "", true);
            mAdapterSettlement = new AdapterSettlement(this, mShopCartList);
            mListView.setAdapter(mAdapterSettlement);
            setlistViewHeigh(mListView);

            double _allNum = 0;
            double _allPrice = 0d;
            String goodsID = "";
            String count = "";
            for (int i = 0; i < mShopCartList.size(); i++) {
                double num = UtilNumber.DoubleValueOf(mShopCartList.get(i).getBuynum() + "");
                double price = UtilNumber.DoubleValueOf(mShopCartList.get(i).getSellprice() + "");
                double numPrice = UtilNumber.DoubleValueOf(num * price + "");
                if (i == (mShopCartList.size() - 1)) {
                    goodsID += mShopCartList.get(i).getID() + "";
                    count += UtilNumber.IntegerValueOf(mShopCartList.get(i).getBuynum()) + "";
                } else {
                    goodsID += mShopCartList.get(i).getID() + ",";
                    count += UtilNumber.IntegerValueOf(mShopCartList.get(i).getBuynum()) + ",";
                }
                _allNum += num;
                _allPrice += numPrice;
            }
            if (_allNum != 0) {
                mModelDoAdd.setGoodsid(goodsID);
                mModelDoAdd.setCount(count);
                mModelDoAdd.setAmount(UtilNumber.DoubleValueOf(_allPrice + "") + "");
                mGoodsPrice.setText("" + UtilNumber.DoubleValueOf(_allPrice + "") + "");//商品金额
                mPayMoney.setText("" + UtilNumber.DoubleValueOf(_allPrice + "") + "");//需付款
                mPrice.setText("实付款：￥" + UtilNumber.DoubleValueOf(_allPrice + "") + "");//实付款
            }
        } else {
            LogUtils.e("null异常============================>");
            finish();
        }
    }
}
