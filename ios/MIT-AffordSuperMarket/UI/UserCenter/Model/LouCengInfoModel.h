//
//  LouCengInfoModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： LouCengInfoModel
 Created_Date： 20151108
 Created_People： GT
 Function_description： 楼层信息
 ***************************************/

#import <Foundation/Foundation.h>

@interface LouCengInfoModel : NSObject
@property(nonatomic, strong)NSString *ID;
@property(nonatomic, strong)NSString *name;
@property(nonatomic)NSInteger danYuanIndex;//针对选择不同单元，更新房间
@end
