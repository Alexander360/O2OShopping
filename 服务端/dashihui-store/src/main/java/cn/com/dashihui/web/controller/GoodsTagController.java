package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.GoodsTag;
import cn.com.dashihui.web.service.GoodsTagService;

public class GoodsTagController extends BaseController{
	private static final Logger logger = Logger.getLogger(GoodsTagController.class);
	private GoodsTagService service = Duang.duang(GoodsTagService.class);

    public void index(){
        render("index.jsp");
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize, getStoreid()));
	}
	
	public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortAd(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }

	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：名称为空，2：代码为空，3：代码已存在
	 */
	public void doAdd(){
		//标签名
		String tagName = getPara("tagName");
		//标签代码
		String code = getPara("code");
		//是否显示
		int isShow = getParaToInt("isShow",1);
		//是否可用
		int enabled = getParaToInt("enabled",1);
		if(StrKit.isBlank(tagName)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(2);
			return;
		}else if(service.findByCode(getStoreid(),code)!=null){
			renderResult(3);
			return;
		}else{
			//保存
			if(service.addTag(new GoodsTag()
				.set("tagName", tagName)
				.set("code", code)
				.set("isShow", isShow)
				.set("storeid", getStoreid())
				.set("enabled", enabled))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：名称为空，2：代码为空，3：代码已存在
	 */
	public void doEdit(){
		//标签ID
		String tagid = getPara("tagid");
		//标签名
		String tagName = getPara("tagName");
		//标签代码
		String code = getPara("code");
		//是否显示
		int isShow = getParaToInt("isShow",1);
		//是否可用
		int enabled = getParaToInt("enabled",1);
		if(StrKit.isBlank(tagid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(tagName)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(2);
			return;
		}
		GoodsTag findByCode = service.findByCode(getStoreid(),code);
		if(findByCode!=null&&findByCode.getInt("id")!=Integer.valueOf(tagid)){
			renderResult(3);
			return;
		}else{
			//更新
			if(service.editTag(new GoodsTag()
				.set("id", tagid)
				.set("tagName", tagName)
				.set("code", code)
				.set("isShow", isShow)
				.set("enabled", enabled))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delTag(id)){
			logger.info("删除标签【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
