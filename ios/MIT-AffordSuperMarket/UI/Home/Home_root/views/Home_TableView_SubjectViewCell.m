//
//  Home_TableView_SubjectViewCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_TableView_SubjectViewCell.h"
#import "Home_TableView_SubjectCollectionCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "FL_Button.h"
#define ksectionHeaderHight HightScalar(46)
#define kcontentViewHight HightScalar(216)
#define ksectionHeaderLeftSpace 17
@interface Home_TableView_SubjectViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
{
}
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)UILabel *remindLabel;
@property(nonatomic, strong)UICollectionView *collectionView;
@property(nonatomic, strong)UIButton *moreButton;
@property(nonatomic, strong)NSString *listTag;
@property(nonatomic, strong)NSString *listName;
@end
@implementation Home_TableView_SubjectViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
        [self setUpSubview];
    }
    return self;
}
- (void)updateViewWithData:(NSDictionary*)dataDic{

    
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:[dataDic objectForKey:@"LIST"]];
    self.frame = CGRectMake(0, 0, VIEW_WIDTH, ksectionHeaderHight + kcontentViewHight);
    NSString *remindLabelText = [NSString stringWithFormat:@"%@",[NSString stringTransformObject:[dataDic objectForKey:@"TAGNAME"]]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:remindLabelText];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#e9432a"] range:NSMakeRange(0,1)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Verdana-BoldItalic" size:FontSize(20)] range:NSMakeRange(0, 1)];
   
    self.remindLabel.attributedText = str;
    
    self.listTag =[NSString stringTransformObject:[dataDic objectForKey:@"TAGCODE"]];
    self.listName =[NSString stringTransformObject:[dataDic objectForKey:@"TAGNAME"]];
    [self.collectionView reloadData];
    
}
#pragma mark -- 初始化视图
- (void)setUpSubview {
    [self.contentView addSubview:self.remindLabel];
    [self.contentView addSubview:self.collectionView];
    [self.contentView addSubview:self.moreButton];
}

#pragma mark -- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Home_TableView_SubjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_TableView_SubjectCollectionCell" forIndexPath:indexPath];
    [cell updateViewWithData:[self.dataSource objectAtIndex:indexPath.row]];
    cell.backgroundColor =[UIColor whiteColor];
    if (indexPath.row == self.dataSource.count - 1) {
        cell.leftSepLine.hidden = YES;
    } else {
        cell.leftSepLine.hidden = NO;
    }
    return cell;
}

#pragma mark -- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic =[self.dataSource objectAtIndex:indexPath.row];
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath:goodID:isSelf:)]) {
        [_delegate selectedItemAtIndexPath:indexPath goodID:[NSString stringTransformObject:[dic objectForKey:@"ID"]]isSelf:[NSString stringTransformObject:[dic objectForKey:@"ISSELF"]]];
    }
    
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 0.6;
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 1;
}

#pragma mark --buttonClick
-(void)moreButtonClick:(UIButton *)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(moreButtonSelectedWithListByTag:listName:)]) {
        [_delegate moreButtonSelectedWithListByTag:_listTag listName:self.listName];
    }
}
#pragma mark -- 初始化视图
- (UICollectionView*)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumInteritemSpacing = 8;
        layout.minimumLineSpacing = 8;
        layout.itemSize = CGSizeMake(VIEW_WIDTH/3 + 8 , kcontentViewHight);
        layout.sectionInset = UIEdgeInsetsMake(0, 8, 0, 0);
        UICollectionView *collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.remindLabel.frame)
                                                                                             , VIEW_WIDTH,kcontentViewHight)
                                                             collectionViewLayout:layout];
        collectionview.backgroundColor = [UIColor whiteColor];
        [collectionview registerClass:[Home_TableView_SubjectCollectionCell class] forCellWithReuseIdentifier:@"Home_TableView_SubjectCollectionCell"];
        [collectionview registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Home_TableView_SubjectCollectionCell"];
        collectionview.delegate = self;
        collectionview.dataSource = self;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = collectionview;
    }
    return _collectionView;
}

- (UILabel*)remindLabel {
    if (!_remindLabel) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(ksectionHeaderLeftSpace, 0, VIEW_WIDTH-160, ksectionHeaderHight)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:FontSize(17)];
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor colorWithHexString:@"#555555"];
        _remindLabel = label;
    }
    return _remindLabel;
}
-(UIButton *)moreButton
{
    if (!_moreButton) {
        UIButton * button =[[FL_Button alloc]initWithAlignmentStatus:FLAlignmentStatusRight];
        button.frame =CGRectMake(VIEW_WIDTH-100
                                 , CGRectGetMinY(self.remindLabel.frame),
                                 100
                                 , CGRectGetHeight(self.remindLabel.bounds));
        [button setTitle:@"查看更多 >" forState:UIControlStateNormal];
        button.titleLabel.font =[UIFont systemFontOfSize:FontSize(13)];
        [button addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        _moreButton =button;
    }
    return _moreButton;
}

@end
