package com.dashihui.afford.ui.model;

/**
 *
 * Created by NiuFC on 2015/11/25.
 */
public class ModelDoAdd {
    /**
     *
     * @param storeid 商铺id
     * @param goodsid 商品id
     * @param count 数量
     * @param paytype 支付方式
    1：在线支付，2：货到付款
     * @param taketype 收货方式
    1：送货，2：自取
     * @param linkname 收货人姓名
     * @param sex 性别
     * @param tel 电话
     * @param address  地址
     * @param describe 订单备注
     */
    private String storeid;// 商铺id
    /***商品数量“，”号分隔***/
     private String goodsid ="";//  商品id
    /***商品数量“，”号分隔***/
     private String count ="" ;// 数量
     private String paytype;//  支付方式1：在线支付，2：货到付款
     private String taketype;//  收货方式 1：送货，2：自取
     private String linkname;//  收货人姓名
     private String sex ;// 性别
     private String tel;//  电话
     private String address;//  社区
     private String describe ;// 订单备注
    private String amount;//总价格

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(String goodsid) {
        this.goodsid = goodsid;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getTaketype() {
        return taketype;
    }

    public void setTaketype(String taketype) {
        this.taketype = taketype;
    }

    public String getLinkname() {
        return linkname;
    }

    public void setLinkname(String linkname) {
        this.linkname = linkname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
