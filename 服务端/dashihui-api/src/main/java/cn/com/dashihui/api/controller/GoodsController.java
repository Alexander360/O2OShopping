package cn.com.dashihui.api.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Duang;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.interceptor.WhenLoginInterceptor;
import cn.com.dashihui.api.service.GoodsService;
import cn.com.dashihui.kit.CommonKit;

public class GoodsController extends BaseController{
	//使用Duang.duang进行封装，使普通类具有事务的功能
	private GoodsService service = Duang.duang(GoodsService.class);
	
	/**
	 * 查询各优惠类型的一条商品信息
	 * @param STOREID 店铺ID
	 */
	public void topByType(){
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else{
			int storeid = Integer.valueOf(storeidStr);
			Record type2 = service.topByType(storeid,2);
			Record type3 = service.topByType(storeid,3);
			Record type4 = service.topByType(storeid,4);
    		renderSuccess(ResultMap.newInstance().put("TYPE2",type2).put("TYPE3", type3).put("TYPE4", type4));
		}
	}
	
	/**
	 * 分页加载商品列表
	 * @param APIVERSION 服务端接口版本号，以此判断来兼容旧版本接口
	 * @param STOREID 店铺ID
	 * @param CATEGORYCODE 类别代码，1：蔬菜水果，2：酒水饮料，3：生活百货，4：粮油调料，5：营养早餐
	 * @param CATEGORYONCODE 一级类别代码，V1.3.1以后用
	 * @param CATEGORYTWCODE 二级类别代码，V1.3.1以后用
	 * @param TYPE 优惠类型，1：普通，2：推荐，3：限量，4：一元购
	 * @param ORDERBY 排序，1：默认排序，2：销量从高到低，3：价格从低到高，4：价格从高到低，V1.3.1以后用
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 */
	public void list(){
		String apiVersion = getPara("APIVERSION");
		String storeidStr = getPara("STOREID");
		String categoryonCodeStr = getPara("CATEGORYONCODE");
		String categorytwCodeStr = getPara("CATEGORYTWCODE");
		int type = getParaToInt("TYPE",0);
		int orderBy = getParaToInt("ORDERBY",1);
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		int isSelf = getParaToInt("ISSELF",0);
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		//严格判断一级分类代码
		Prop prop = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
		if(!categoryonCodeStr.equals(prop.get("one1"))
				&&!categoryonCodeStr.equals(prop.get("one2"))
				&&!categoryonCodeStr.equals(prop.get("one3"))
				&&!categoryonCodeStr.equals(prop.get("one4"))
				&&!categoryonCodeStr.equals(prop.get("one5"))){
			renderFailed("参数CATEGORYCODE不正确");
			return;
		}
		if(apiVersion.equals(PropKit.get("api.version.133"))){
			Page<Record> page = service.findByPage_v133(Integer.valueOf(storeidStr),categoryonCodeStr,categorytwCodeStr,type,pageNum, pageSize,orderBy,isSelf);
			renderSuccess(ResultMap.newInstance()
					.put("PAGENUMBER", page.getPageNumber())
					.put("PAGESIZE", page.getPageSize())
					.put("TOTALPAGE", page.getTotalPage())
					.put("TOTALROW", page.getTotalRow())
					.put("LIST", page.getList()));
		} else {
			//1.3.3版本前代码，2016-04-14
			Page<Record> page = service.findByPage(Integer.valueOf(storeidStr),categoryonCodeStr,categorytwCodeStr,type,pageNum, pageSize,orderBy);
			renderSuccess(ResultMap.newInstance()
					.put("PAGENUMBER", page.getPageNumber())
					.put("PAGESIZE", page.getPageSize())
					.put("TOTALPAGE", page.getTotalPage())
					.put("TOTALROW", page.getTotalRow())
					.put("LIST", page.getList()));
		}
	}
	
	/**
	 * 商品标签列表<br/>
	 * 客户端首页用来查询所有商品标签，以及各标签下18条商品数据
	 * @param STOREID 店铺ID
	 */
	public void tags(){
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else{
			renderSuccess(service.getAllTag(Integer.valueOf(storeidStr),18));
		}
	}
	
	/**
	 * 按标签查询商品列表
	 * @param STOREID 店铺ID
	 * @param TAGCODE 标签代码
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 */
	public void listByTag(){
		String storeidStr = getPara("STOREID");
		String tagCode = getPara("TAGCODE");
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(tagCode)){
			renderFailed("参数TAGCODE不能为空");
    		return;
		}
		Page<Record> page = service.findPageByTag(Integer.valueOf(storeidStr), tagCode, pageNum, pageSize);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
	}
	
	/**
	 * 查询精品推荐商品列表
	 * @param STOREID 店铺ID
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 */
	public void listByRecom(){
		String storeidStr = getPara("STOREID");
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		Page<Record> page = service.findPageByRecom(Integer.valueOf(storeidStr), pageNum, pageSize);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
	}
    
	/**
	 * 商品详情
	 * @param STOREID 店铺ID
	 * @param GOODSID 商品ID
	 */
	@Before(WhenLoginInterceptor.class)
	public void detail(){
		String storeidStr = getPara("STOREID");
		String goodsidStr = getPara("GOODSID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(goodsidStr)){
    		renderFailed("参数GOODSID不能为空");
    		return;
		}else{
			//查询商品详情信息
			Record goods;
			if(getCurrentUser()!=null){
				//如果当前有用户登录，则查询商品详情的同时，查询该用户与该商品的收藏关系
				goods = service.findDetail(getCurrentUser().getInt("id"),Integer.valueOf(storeidStr), Integer.valueOf(goodsidStr));
			}else{
				//如果当前没有用户登录，则只查询商品详情
				goods = service.findDetail(Integer.valueOf(storeidStr), Integer.valueOf(goodsidStr));
			}
			if(goods!=null){
				//查询商品的图片集，并遍历拼成数组形式，如：["/goods/xxx1.jpg","goods/xxx2.jpg"]
				List<Record> images = service.findImages(Integer.valueOf(goodsidStr));
				List<String> strList = new ArrayList<String>();
				if(images!=null&&images.size()!=0){
					for(Record image : images){
						strList.add(image.getStr("THUMB"));
					}
				}
				//设置给商品详情信息
				goods.set("IMAGES", strList);
				renderSuccess(goods);
			}else{
	    		renderFailed("商品不存在");
			}
		}
	}
	
	/**
	 * 商品详情描述
	 * @param STOREID 店铺ID
	 * @param GOODSID 商品ID
	 */
	public void detailDescribe(){
		String storeidStr = getPara("STOREID");
		String goodsidStr = getPara("GOODSID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(goodsidStr)){
    		renderFailed("参数GOODSID不能为空");
    		return;
		}else{
			Record describe = service.findDetailDescribe(Integer.valueOf(storeidStr), Integer.valueOf(goodsidStr));
			renderSuccess(ResultMap.newInstance().put("CONTEXT", CommonKit.ifNull(describe.get("describe"),"")));
			return;
		}
	}
}
