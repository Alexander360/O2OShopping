//
//  MyOrderController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
#import "MyOrderController.h"
//untils
#import "UIColor+Hex.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
#import "AppDelegate.h"
//vendor
#import "RDVTabBarController.h"
#import "MyOrderDetail_rootController.h"
#import "MJRefresh.h"

//view
#import "MyOrderCell.h"
#import "EmptyPageView.h"
//model
#import "MyOrderModel.h"
//controller
#import "WeiXinPayController.h"
#import "SureOrderController.h"
#import "MTA.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "Home_Root_Controller.h"
@interface MyOrderController ()<UITableViewDelegate,UITableViewDataSource,MyOrderCellDelegate,EmptyPageViewDelegate>
{
    NSString *_orderType;
    NSString *_otherCuiDanTime;
}
@property (nonatomic)NSInteger totalCount;
@property (nonatomic)NSInteger currentPage;
@property (nonatomic,strong)UITableView *orderTableView;
@property (nonatomic,strong)EmptyPageView *emptyView;
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic,assign)NSInteger tag;
@property (nonatomic,strong)NSString *homeMake;

@end

@implementation MyOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.currentPage = 1;
    _dataSource = [[NSMutableArray alloc]init];
    [self addBackNavItem];
    [self setUpSuberView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
         [self loadTeHuiDataWithRequestPage:self.currentPage requestCount:kRequestLimit];
    }
   
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"全部订单"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"全部订单"];
}
#pragma mark -- request Data
//商品订单列表和服务订单列表
-(void)loadTeHuiDataWithRequestPage:(NSInteger)requestPage requestCount:(NSInteger)requestCount {
    
    //    SIGNATURE			设备识别码
    //    TOKEN			用户签名
    //    PAGENUM			页数
    //    PAGESIZE			一页显示订单数目
    //    FLAG			订单状态，
    //    0：全部，1:待付款，
    //    2：待服务，3：已完成
   NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/list":@"user/serviceOrderList";
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    
    XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:userInfoMode.token forKey:@"TOKEN"];
    [parameter setObject:xiaoQuAndStoreModel.storeID forKey:@"STOREID"];
    [parameter setObject:[NSNumber numberWithInteger:requestPage] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:requestCount] forKey:@"PAGESIZE"];
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [parameter setObject:_orderType forKey:@"FLAG"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            if ([state isEqualToString:@"0"]) {//请求成功
                _totalCount = [[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"TOTALROW"]] integerValue];
                if (weakSelf.currentPage == 1) {//请求第一页清空数据
                    [weakSelf.dataSource removeAllObjects];
                }
                [weakSelf.dataSource addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LIST"]];
                
                [weakSelf noDataViewIsShow];
                [_orderTableView reloadData];
                
            } else {//服务端返回错误
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        [weakSelf endRefresh];
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [weakSelf endRefresh];
    }];

}

//取消订单
-(void)cancelOrderWithTag:(NSInteger )tag homeMakeType:(NSString *)homeMakeType//取消订单
{
//    SIGNATURE			设备识别码
//    TOKEN			用户签名
//    ORDERNUM			订单号
//    REASON			取消订单理由
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/cancel":@"service/order/cancel";
    if (homeMakeType.length !=0) {
        requertKey=@"ser/order/cancel";
    }
   
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[dataDic objectForKey:@"ORDERNUM"] forKey:@"ORDERNUM"];
    [paramerters setObject:@"" forKey:@"REASON"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
        if ([state isEqualToString:@"0"]) {//请求成功
            
            [self loadTeHuiDataWithRequestPage:weakSelf.currentPage requestCount:kRequestLimit];
            
            
        }else{
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:[dataDic objectForKey:@"MSG"] inView:weakSelf.view];

        }
        
    } failure:^(bool isFailure) {
        
    }];
    
}
//删除订单

-(void)deleteOrderWithTag:(NSInteger )tag homeMakeType:(NSString *)homeMakeType
{
    /*
     SIGNATURE			设备识别码
     TOKEN			    用户签名
     ORDERNUM			订单号
     **/
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/delete":@"service/order/delete";
    
    if (homeMakeType.length !=0) {
        requertKey =@"ser/order/delete";
    }
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[dataDic objectForKey:@"ORDERNUM"] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
//        NSLog(@"%@",[dataDic objectForKey:@"MSG"]);
        if ([state isEqualToString:@"0"]) {//请求成功
            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"删除成功" inView:weakSelf.view];
                [weakSelf loadTeHuiDataWithRequestPage:weakSelf.currentPage requestCount:kRequestLimit];
        }
        
    } failure:^(bool isFailure) {
        
    }];
    
}
//请求催单数据
- (void)loadCuiOrderDataWithOrderNumber:(NSString*)orderNum homeMakeType:(NSString *)homeMakeType{
    
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/urge":@"service/order/urge";
    if (homeMakeType.length !=0) {
        requertKey =@"ser/order/urge";
    }
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:mode.token forKey:@"TOKEN"];
    [parameter setObject:orderNum forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                if (msg.length == 0) {
                    msg = @"催单成功";
                }
                _otherCuiDanTime = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"DATETIME"]];
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
    } failure:^(bool isFailure) {
       
        
    }];
}
//请求签收
- (void)loadOrderQianShouDataTag:(NSInteger )tag homeMakeType:(NSString *)homeMakeType {
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/receive":@"service/order/sign";
    if (homeMakeType.length !=0) {
        requertKey =@"ser/order/sign";
    }
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];

    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[dataDic objectForKey:@"ORDERNUM"] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"确认成功" inView:weakSelf.view];
                [weakSelf loadTeHuiDataWithRequestPage:weakSelf.currentPage requestCount:kRequestLimit];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {
        
    }];
}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellID =@"orderCell";
     MyOrderCell*cell=[tableView dequeueReusableCellWithIdentifier:cellID];
    if(!cell) {
        cell=[[MyOrderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID OrderType:kgoodOrderType];
        //取消cell点击状态
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate=self;
    cell.goPay.tag=indexPath.section;
    cell.deleteOrder.tag=indexPath.section;
    NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.section];
    if ([self.requestDataType isEqualToString:@"1"]) {////商品订单 是1 服务订单是 0
        [cell updateViewwithData:dataDic OrderType:kgoodOrderType];
    }else if ([self.requestDataType isEqualToString:@"0"]){////商品订单 是1 服务订单是 0
        [cell updateViewwithData:dataDic OrderType:kserviceOrderType];
        
    }
    //4.返回cell
    return cell;
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return HightScalar(220);
}
//黑条效果
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HightScalar(12);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *views = [[UIView alloc]init];
    views.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    return views;
}
//单元格选中事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.section];
    NSString *orderNum =[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
    MyOrderDetail_rootController *orderDetailRootVC = [[MyOrderDetail_rootController alloc]init];
    orderDetailRootVC.orderNum = orderNum;
    orderDetailRootVC.currentIndexOfController = 1;
    orderDetailRootVC.oldIndexOfController = 0;
    orderDetailRootVC.selectedItemIndex = 1;
    NSString * serviceType =[NSString stringTransformObject:[dataDic objectForKey:@"TYPE"]];
        
    if([serviceType isEqualToString:@"1"])
    {
       orderDetailRootVC.homeMakeType= @"1";
    }else{
        orderDetailRootVC.homeMakeType =@"";
    }
    
    orderDetailRootVC.requestDataType=self.requestDataType;
    [self.navigationController pushViewController:orderDetailRootVC animated:YES];
}
#pragma mark --MyOrderCellDelegate  
// 删除订单

-(void)selectedSecondItemWithTag:(NSInteger)tag title:(NSString *)title homeMakeType:(NSString *)homeMakeType
{
    _tag =tag;
    _homeMake =homeMakeType;
    if ([title isEqualToString:@"删除订单"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除订单?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=2;
        [alert show];
       
    } else if([title isEqualToString:@"取消订单"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否取消订单?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=1;
        [alert show];
    }
}
#pragma mark--UIAlertViewdelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag ==1){//取消订单
        if (buttonIndex == 1){
            [self cancelOrderWithTag:_tag homeMakeType:_homeMake];
        }
    }else if(alertView.tag == 2)//删除订单
    {
        if (buttonIndex == 1){
        [self deleteOrderWithTag:_tag homeMakeType:_homeMake];
        }
    }else if(alertView.tag ==100){//确认签收
        if (buttonIndex == 1){
            [self loadOrderQianShouDataTag:_tag homeMakeType:_homeMake];
        }
    }
    
    
}
//去支付 或者 再次购买
-(void)selectedFirstItemWithTag:(NSInteger)tag title:(NSString *)title homeMakeType:(NSString *)homeMakeType
{
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    if ([title isEqualToString:@"去支付"]) {
        WeiXinPayController *weixinpay=[[WeiXinPayController alloc]init];
        weixinpay.orderNum=[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
        weixinpay.totalPrice=[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]];
        weixinpay.homeMakeType =self.homeMakeType;
        weixinpay.requestDataType =self.requestDataType;
        weixinpay.goodIDs=@"";
        [self.navigationController pushViewController:weixinpay animated:YES];
    }else if([title isEqualToString:@"再次购买"]){
//    SureOrderController *sureOrderVC = [[SureOrderController alloc]init];
//    [self.navigationController pushViewController:sureOrderVC animated:YES];
        
    } else if ([title isEqualToString:@"催单"]) {//催单
        NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
        
        NSString *orderNum = [NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
       
        [self loadCuiOrderDataWithOrderNumber:orderNum homeMakeType:homeMakeType];
                
    }else if ([title isEqualToString:@"确认收货"]||[title isEqualToString:@"确认服务"]||[title isEqualToString:@"确认取货"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"是否%@？",title] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=100;
        _tag =tag;
        _homeMake =homeMakeType;
        [alert show];
        
    }
    
}
- (NSDate*)dateWithString:(NSString*)str {
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    return  [dateFormat dateFromString:str];
}
#pragma mark - 上拉/下拉  更新数据
-(void)headerRereshing {
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        self.currentPage = 1;
        [self loadTeHuiDataWithRequestPage:self.currentPage requestCount:kRequestLimit];
    }else{
        [self endRefresh];
    }
  
}

-(void)footerRereshing
{
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        if (self.currentPage*kRequestLimit < self.totalCount) {
            self.currentPage++;
            [self loadTeHuiDataWithRequestPage:self.currentPage requestCount:kRequestLimit];
        } else {
            [self endRefresh];
        }
    }else{
        [self endRefresh];
    }
   
}
- (void)endRefresh
{
    [self.orderTableView headerEndRefreshing];
    [self.orderTableView footerEndRefreshing];
}


#pragma mark -- 设置navigation标题
- (void)setMyOrderType:(MyOrderType)myOrderType {
    NSString *navitationTitle = @"";
    switch (myOrderType) {
        case kallOrderType:
            navitationTitle = @"全部订单";
            _orderType = @"0";
            break;
        case kwaitPayMoneyType:
            navitationTitle = @"待付款";
             _orderType = @"1";
            break;
        case kwaitSendProductType:
            if ([self.requestDataType isEqualToString:@"1"]) {
                navitationTitle = @"待发货";
                _orderType = @"2";

            }else if([self.requestDataType isEqualToString:@"0"])
            {
                navitationTitle = @"待服务";
                _orderType = @"2";
            }
                        break;
        case kwaitGetProductType:
            if ([self.requestDataType isEqualToString:@"1"]) {
                navitationTitle = @"待收货";
                _orderType = @"3";
            }else if([self.requestDataType isEqualToString:@"0"])
            {
                navitationTitle = @"已完成";
                _orderType = @"3";
            }
            break;
        default:
            break;
    }
    self.title = navitationTitle;
    _myOrderType = myOrderType;
}
//立即逛逛 跳转
-(void)buttonClick
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(action) userInfo:nil repeats:NO];
}
-(void)action
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if ([self.requestDataType isEqualToString:@"1"]) {//商品订单 是1 服务订单是 0
         Home_Root_Controller *homeVC = [[[appDelegate.tabBarController.viewControllers firstObject] childViewControllers] firstObject];
        [homeVC.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
        [appDelegate selectedControllerAtTabBarIndex:0];
        
    }else if([self.requestDataType isEqualToString:@"0"])//商品订单 是1 服务订单是 0
    {
        [appDelegate selectedControllerAtTabBarIndex:2];
    }
}
#pragma mark -- setup SubView
-(void)setUpSuberView
{
    _orderTableView =[[UITableView alloc]initWithFrame:CGRectMake(0
                                                                  , 0
                                                                  , self.view.bounds.size.width
                                                                  , self.view.bounds.size.height-64)
                                                 style:UITableViewStylePlain];
    _orderTableView.backgroundColor=[UIColor whiteColor];
    _orderTableView.delegate=self;
    _orderTableView.dataSource=self;
    //添加下拉刷新
    [_orderTableView addHeaderWithTarget:self action:@selector(headerRereshing)];
    //添加上拉加载更多
    [_orderTableView addFooterWithTarget:self action:@selector(footerRereshing)];
    UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 1)];
    _orderTableView.tableFooterView=footView;
    [self.view addSubview:_orderTableView];

    //数据为空时显示的View
    
    if ([self.requestDataType isEqualToString:@"1"]) {//商品订单 是1 服务订单是 0
         _emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64) EmptyPageViewType:kGoodsOrderRootView];
    }else if([self.requestDataType isEqualToString:@"0"])//商品订单 是1 服务订单是 0
    {
        _emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0,VIEW_WIDTH , VIEW_HEIGHT-64) EmptyPageViewType:kServiceOrderRootView];
    }
   
    _emptyView.backgroundColor=[UIColor whiteColor];
    _emptyView.hidden=YES;
    _emptyView.delegate =self;
     [self.view addSubview:_emptyView];
    }
//空白页面是否展示
- (void)noDataViewIsShow{
        if (self.dataSource.count==0)
        {
            self.emptyView.hidden=NO;
            self.orderTableView.hidden=YES;
        }else{
            self.emptyView.hidden=YES;
            self.orderTableView.hidden=NO;
        }
}
-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
