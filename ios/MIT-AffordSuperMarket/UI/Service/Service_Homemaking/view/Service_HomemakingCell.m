//
//  Service_HomemakingCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Service_HomemakingCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
@interface Service_HomemakingCell()

@property(nonatomic,strong)UIImageView *imageLogo;

@property(nonatomic,strong)UILabel *homeMakeTitle;

@property(nonatomic,strong)UILabel *homeMakeDetail;
@end
@implementation Service_HomemakingCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubView];
    }
    return self;
}
//初始化控件
-(void)setSubView
{
    _imageLogo=[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 70, 70)];
    _imageLogo.image =[UIImage imageNamed:@"place_image"];
    [self.contentView addSubview:_imageLogo];
    
    _homeMakeTitle =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imageLogo.frame)+10, CGRectGetMinY(_imageLogo.frame)+5,VIEW_WIDTH-CGRectGetWidth(_imageLogo.bounds)-30-10, 20)];
    _homeMakeTitle.text=@"日常保洁";
    _homeMakeTitle.textColor=[UIColor colorWithHexString:@"#555555"];
    _homeMakeTitle.font=[UIFont systemFontOfSize:14];
    [self.contentView addSubview:_homeMakeTitle];
    
    _homeMakeDetail =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_homeMakeTitle.frame), CGRectGetMaxY(_homeMakeTitle.frame)+5,CGRectGetWidth(_homeMakeTitle.bounds), 30)];
    _homeMakeDetail.text=@"适用于家具、家电、台面、地面、墙面、等区域的日常表面清洁。";
    _homeMakeDetail.textColor=[UIColor colorWithHexString:@"#999999"];
    _homeMakeDetail.font=[UIFont systemFontOfSize:12];
    _homeMakeDetail.numberOfLines=2;
    [self.contentView addSubview:_homeMakeDetail];
    
    
}
//更新cell数据
-(void)setSuberViewDataWithDictionary:(NSDictionary *)dictionary
{
    _imageLogo.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"imageName"]]];
    _homeMakeTitle.text=[dictionary objectForKey:@"title"];
    _homeMakeDetail.text =[dictionary objectForKey:@"detail"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
