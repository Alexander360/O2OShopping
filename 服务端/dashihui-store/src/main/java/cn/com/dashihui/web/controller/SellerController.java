package cn.com.dashihui.web.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.ValidateKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Seller;
import cn.com.dashihui.web.service.SellerService;

public class SellerController extends BaseController{
	private static final Logger logger = Logger.getLogger(SellerController.class);
	private SellerService service = new SellerService();

    public void index(){
        render("index.jsp");
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(getStoreid(), pageNum, pageSize));
	}

	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：用户名为空，2：用户名格式不正确，3：用户已经存在，4：密码为空，5：密码格式不正确，6：密码长度不正确，7：姓名为空，8：姓名过长，9：电话格式不正确
	 */
	public void doAdd(){
		//用户名
		String username = getPara("username");
		//密码
		String password = getPara("password");
		//姓名
		String name = getPara("name");
		//性别
		int sex = getParaToInt("sex");
		String tel = getPara("tel");
		if(StrKit.isBlank(username)){
			renderResult(1);
			return;
		}else if(!ValidateKit.Mobile(username)){
			renderResult(2);
			return;
		}else if(service.findExistsByUsername(username)){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(4);
			return;
		}else if(ValidateKit.Password_reg(password)){
			renderResult(5);
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderResult(6);
    		return;
		}else if(StrKit.isBlank(name)){
			renderResult(7);
			return;
		}else if(name.length()>50){
			renderResult(8);
			return;
		}else if(!StrKit.isBlank(tel)&&ValidateKit.Tel(tel)){
			renderResult(9);
			return;
		}else{
			//如果没有填写电话，则同用户名
			if(StrKit.isBlank(tel)){
				tel = username;
			}
			//保存
			if(service.addSeller(new Seller()
				.set("username", username)
				.set("password", CommonKit.encryptPassword(password))
				.set("name", name)
				.set("sex", sex)
				.set("tel", tel)
				.set("storeid", getStoreid()))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：用户名为空，2：用户名格式不正确，3：用户已经存在，4：姓名为空，5：姓名过长，6：电话格式不正确
	 */
	public void doEdit(){
		//用户ID
		String sellerid = getPara("sellerid");
		//用户名
		String username = getPara("username");
		//姓名
		String name = getPara("name");
		//性别
		int sex = getParaToInt("sex");
		String tel = getPara("tel");
		if(StrKit.isBlank(sellerid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(username)){
			renderResult(1);
			return;
		}else if(!ValidateKit.Mobile(username)){
			renderResult(2);
			return;
		}else if(service.findExistsByUsernameWithout(username,Integer.valueOf(sellerid))){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(4);
			return;
		}else if(name.length()>50){
			renderResult(5);
			return;
		}else if(!StrKit.isBlank(tel)&&ValidateKit.Tel(tel)){
			renderResult(6);
			return;
		}else{
			//如果没有填写电话，则同用户名
			if(StrKit.isBlank(tel)){
				tel = username;
			}
			//保存
			if(service.editSeller(new Seller()
				.set("id", sellerid)
				.set("username", username)
				.set("name", name)
				.set("sex", sex)
				.set("tel", tel))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEditPwd(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("editPwd.jsp");
	}
	
	/**
	 * 更新密码
	 * @return -1：异常，0：成功，1：密码为空，2：密码格式不正确，3：密码长度不正确
	 */
	public void doEditPwd(){
		//用户ID
		String sellerid = getPara("sellerid");
		//密码
		String password = getPara("password");
		if(StrKit.isBlank(sellerid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(1);
			return;
		}else if(ValidateKit.Password_reg(password)){
			renderResult(2);
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderResult(3);
    		return;
		}else{
			//更新
			if(service.editSeller(new Seller()
				.set("id", sellerid)
				.set("password", CommonKit.encryptPassword(password)))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delSeller(id)){
			logger.info("删除配送员【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
