//
//  BaiDuLocationCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "BaiDuLocationCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
@interface BaiDuLocationCell()
@property (nonatomic,strong) UILabel *baiduAddress;
@property (nonatomic,strong) UILabel *xiaoquName;
@end
@implementation BaiDuLocationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        self.backgroundColor =[UIColor whiteColor];
        [self setUpSuberView];
    }
    return self;
}

#pragma  mark--初始化控件
-(void)setUpSuberView
{
    _xiaoquName =[[UILabel alloc]initWithFrame:CGRectMake(HightScalar(18)
                                                          , HightScalar(14)
                                                          , VIEW_WIDTH
                                                          , HightScalar(24))];
    
    _xiaoquName.font=[UIFont systemFontOfSize:FontSize(16)];
    _xiaoquName.textColor=[UIColor colorWithHexString:@"#555555"];
    

    [self.contentView addSubview:_xiaoquName];
    _baiduAddress =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_xiaoquName.frame)
                                                            , CGRectGetMaxY(_xiaoquName.frame)
                                                            , CGRectGetWidth(_xiaoquName.bounds)
                                                            , HightScalar(25))];
    
    _baiduAddress.font=[UIFont systemFontOfSize:FontSize(13)];
    _baiduAddress.textColor=[UIColor colorWithHexString:@"#999999"];
    [self.contentView addSubview:_baiduAddress];
}
-(void)updateViewWithData:(BMKCloudPOIInfo *)poiInfo
{
    [self updateStoreTitle:poiInfo.title storeAddress:poiInfo.address];
}

- (void)updateStoreListCellWithDataDic:(NSDictionary*)dataDic {
    [self updateStoreTitle:[dataDic objectForKey:@"TITLE"] storeAddress:[dataDic objectForKey:@"ADDRESS"]];
}

- (void)updateStoreTitle:(NSString*)title storeAddress:(NSString*)address {
    _xiaoquName.text = title;
    _baiduAddress.text = address;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
