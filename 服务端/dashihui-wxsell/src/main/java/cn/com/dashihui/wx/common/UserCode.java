package cn.com.dashihui.wx.common;

/**
 * 用户会员等级
 */
public class UserCode {
	/**
	 * 用户会员等级
	 */
	public static class UserLevel{
		public final static int LEVEL1 = 1;
		public final static int LEVEL2 = 2;
	}
	/**
	 * 用户会员等级变化动作
	 */
	public static class UserLevelLogFlag{
		public final static int CONSUME = 1;
	}
	/**
	 * 用户money变化动作
	 */
	public static class UserValueLogFlag{
		public final static int CONSUME_SELF = 1;
		public final static int CONSUME_UNDER = 2;
		public final static int CONSUME_REDEEM = 3;
	}
}
