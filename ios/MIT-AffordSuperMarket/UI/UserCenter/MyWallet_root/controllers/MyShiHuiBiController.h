//
//  MyShiHuiBiController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyWalletViewController
 Created_Date： 20160504
 Created_People： JSQ
 Function_description： 我的实惠币
 ***************************************/
#import "BaseViewController.h"

@interface MyShiHuiBiController : BaseViewController
@property(nonatomic,strong)NSString *shiHuiBiNum;//实惠币金额
@end
