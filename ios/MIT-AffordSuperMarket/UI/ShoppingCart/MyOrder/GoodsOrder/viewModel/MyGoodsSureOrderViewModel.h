//
//  MyGoodsSureOrderViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/9.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ReceivedAddressOBJECT.h"
@interface MyGoodsSureOrderViewModel : NSObject
@property(nonatomic)NSInteger selectdGoodsType;//选中商品类型:0只包含便利店商品 1只包含自营商品 2包含便利店和自营商品
@property(nonatomic, strong)NSMutableArray *storeGoodsArr;//便利店商品
@property(nonatomic, strong)NSMutableArray *selfGoodsArr;//自营商品
@property(nonatomic, strong)ReceivedAddressOBJECT *receivedAddressInfo;//收货地址信息
@property(nonatomic)BOOL isHadRecivedAddress;//是否有地址
@property(nonatomic, strong)NSMutableArray *dataSource;//数据源
@property(nonatomic)CGFloat tableHeaderHight;//table头部高度
@property(nonatomic)CGFloat tableFooterHight;//table尾部高度
@property(nonatomic, strong)NSString *totalPrice;//总价格
@property(nonatomic, strong)NSString *storeGoodsPrice;//店铺商品总金额
@property(nonatomic, strong)NSString *selfGoodsPrice;//自营商品总金额
@property(nonatomic, strong)NSString *offsetTotalPrice;//抵消后的价格总价格
@property(nonatomic, strong)NSString *offsetStoreGoodsPrice;//抵消后的价格店铺商品总金额
@property(nonatomic, strong)NSString *offsetSelfGoodsPrice;//抵消后的价格自营商品总金额
@property(nonatomic, strong)NSString *storeGoodsID;//店铺商品IDS
@property(nonatomic, strong)NSString *selfGoodsID;//自营商品IDS
@property(nonatomic, strong)NSString *storeGoodsCount;//店铺商品数量
@property(nonatomic, strong)NSString *selfGoodsCount;//自营商品数量
@property(nonatomic, strong)NSString *shiHuiBiMoney;//实惠币数量
- (void)requestDatasSuccess:(void(^)(BOOL isSuccess))successs failure:(void(^)(NSString*failureInfo))failureInfo;
@end
