package cn.com.dashihui.api.common;

public class DispatchState {
	public final static int  UNACCEPT = 0;
	public final static int  WAIT_PACK = 1;
	public final static int  PACKING = 2;
	public final static int  PACK_FINISH = 3;
	public final static int  DISPATCHING = 4;
	public final static int  WAIT_GET = 5;
	public final static int  FINISH = 6;
}
