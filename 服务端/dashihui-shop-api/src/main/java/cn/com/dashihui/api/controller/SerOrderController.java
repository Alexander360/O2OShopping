package cn.com.dashihui.api.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.common.SerOrderCode;
import cn.com.dashihui.api.dao.SerOrder;
import cn.com.dashihui.api.dao.SerShop;
import cn.com.dashihui.api.service.SeOrderService;
import cn.com.dashihui.api.service.SerShopService;
import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.kit.SMSKit;
import cn.com.dashihui.kit.ValidateKit;


public class SerOrderController extends BaseController{
 
	private SeOrderService service = new SeOrderService();
	private SerShopService shopService = new SerShopService();
	
	/**
	 * 统计今日的营业额和订单总数
	 * @param ID 商家ID
	 */
	public void orderCount(){
		String shopid = getPara("ID");
		if(StrKit.isBlank(shopid)){
			renderFailed("参数ID不能为空");
			return;
		}
		String today = DatetimeKit.getFormatDate("yyyy-MM-dd")+" 00:00:00";
		Long orderCount = service.getOrderCount(Integer.valueOf(shopid).intValue(), today);
		double orderAmount = service.getOrderAmount(Integer.valueOf(shopid).intValue(), today);
		renderSuccess(ResultMap.newInstance()
				.put("ORDERCOUNT", orderCount)
				.put("ORDERAMOUNT", orderAmount));
	}
	
	/**
	 * 订单列表 上拉加载更多
	 * @param ID 商家ID
	 * @param STARTDATE 开始时间
	 * @param PAGENUM 页数
	 * @param PAGESIZE 一页显示订单数量
	 * @param FLAG 0：全部，1：待接单，2：待服务，3：完成
	 * @param direction 1：上拉加载旧数据，2：下拉加载新数据 
	 */
	public void page(){
		String shopid = getPara("ID"); 
		String pullDate = getPara("STARTDATE");
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		int flag = getParaToInt("FLAG",0);
		int direction = getParaToInt("DIRECTION",1);
		
		if(StrKit.isBlank(shopid)){
			renderFailed("参数ID不能为空");
			return;
		}/*else if(StrKit.isBlank(pullDate)){
			renderFailed("参数STARTDATE不能为空");
			return;
		}*/
		if(StrKit.isBlank(pullDate)){
			pullDate = DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss");
		}
		Page<Record> page = service.findByPage(Integer.valueOf(shopid).intValue(),pullDate,flag,direction,pageNum,pageSize);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
		return;
	}

	/**
	 * 订单详情
	 * @param ID 商家ID
	 * @param ORDERNUM 订单编号
	 */
	public void detail(){
		String shopid = getPara("ID");
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum,Integer.valueOf(shopid).intValue());
			if(order!=null){
				renderSuccess(order);
				return;
			}else{
				renderFailed("此订单不存在");
				return;
			}
		}
	}
	
	/**
	 * 接单
	 * @param ID 商家ID
	 * @param ORDERNUM 订单编号
	 */
	public void accept(){
		int shopid = getParaToInt("ID");
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum,shopid);
			if(order==null){
				renderFailed("此订单不存在");
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“未付款”时可操作
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.HAD_PAY||deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“店铺待接单”时可操作
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}
				SerShop shop = shopService.getShopById(shopid);
				//3.订单设为“商家确认”，并记录日志
				order.set("deliverState", SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT).set("proSerDeliverDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(order.update()){
					//记录操作日志
					service.log(orderNum, "商家："+shop.getStr("name"), "接单", "订单号："+orderNum+"，操作成功！");
					//向用户发送短信通知，需要判断如果用户的电话是手机号码
					if(ValidateKit.Mobile(order.getStr("tel"))){
						SMSKit.onShopAcceptToCustomer(order.getStr("tel"), order.getStr("proSerName"), order.getStr("proSerName"));
					}
					//向商家发送短信通知，需要判断如果商家的电话是手机号码
					if(!StrKit.isBlank(order.getStr("shopTel"))&&ValidateKit.Mobile(order.getStr("shopTel"))){
						SMSKit.onShopAcceptToShop(order.getStr("shopTel"), order.getStr("tel"), DatetimeKit.getFormatDate(order.getDate("serTime"), "yyyy年MM月dd日HH时mm分"), order.getStr("address"));
					}
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单操作失败");
				return;
			}
		}
	}
	
	/**
	 * 商家拒单操作
	 * @param ID 商家ID
	 * @param ORDERNUM 订单编号
	 * @param CONTENT  拒单理由
	 */
	public void refuse(){
		int shopid = getParaToInt("ID");
		String orderNum = getPara("ORDERNUM");
		String reason = getPara("CONTENT");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum,shopid);
			if(order==null){
				renderFailed("此订单不存在");
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“店铺已派单”时可操作
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.HAD_PAY||deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“店铺待接单”时可操作
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}
				SerShop shop = shopService.getShopById(shopid);
				//3.订单设为“商家拒单”，并记录日志
				order
				.set("deliverState", SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT)
				.set("orderState", SerOrderCode.OrderState.REFUSE)
				.set("refuseDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"))
				.set("refuseReason", reason);
				if(order.update()){
					//记录操作日志
					service.log(orderNum, "商家："+shop.getStr("name"), "拒单", "订单号："+orderNum+"，操作成功！");
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单操作失败");
				return;
			}
		}
	}
	
	/**
	 * 商家执行订单完成操作
	 * @param ID 商家ID
	 * @param ORDERNUM 订单数
	 */
	public void finish(){
		int shopid = getParaToInt("ID");
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
			return;
		}else{
			SerOrder order = service.getOrderByOrderNum(orderNum,shopid);
			if(order==null){
				renderFailed("此订单不存在");
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=SerOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许操作");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“商家已接单”时可操作
				if(payType==SerOrderCode.OrderPayType.ON_LINE){
					if(payState!=SerOrderCode.OrderPayState.HAD_PAY||deliverState!=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“商家已接单”时可操作
				if(payType==SerOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT){
						renderFailed("此订单当前不允许操作");
						return;
					}
				}
				SerShop shop = shopService.getShopById(shopid);
				//3.订单设为“商家完成订单”，并记录日志
				order.set("orderState", SerOrderCode.OrderState.FINISH).set("signDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(order.update()){
					//记录操作日志
					service.log(orderNum, "商家："+shop.getStr("name"), "服务完成", "订单号："+orderNum+"，操作成功！");
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单操作失败");
				return;
			}
		}
	}
}