//
//  RegisterTwoStepViewController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "RegisterTwoStepViewController.h"
#define kleftSpace 10
#define kHSpace 16
#define kVSpace 22
#define kCheckCodeTotalTime 60
#define kCheckCodeDuration 1
//vendor
#import "TPKeyboardAvoidingScrollView.h"
#import "SaveDataInKeychain.h"
#import "RDVTabBarController.h"
//untils
#import "UIColor+Hex.h"
#import "UITextField+Space.h"
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "AppDelegate.h"
//controllers
#import "Login_Controller.h"

@interface RegisterTwoStepViewController () {
    NSInteger _totalTime;//验证码有效总时间
}
@property(nonatomic, strong)TPKeyboardAvoidingScrollView *rootScrollView;
@property(nonatomic, strong)UITextField *phoneTextField;
@property(nonatomic, strong)UITextField *checkCodeTextField;
@property(nonatomic, strong)UITextField *passwordTextField;
@property(nonatomic, strong)UITextField *surePasswordTextField;
@property(nonatomic, strong)UITextField *invitationCodeTextField;
@property(nonatomic, strong)UIButton *refreshCheckCodeBtn;
@property(nonatomic, strong)UIButton *agreeItemsBtn;
@property(nonatomic, strong)UIButton *registerBtn;

@property (strong, nonatomic) NSTimer *timer;
@end

@implementation RegisterTwoStepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor colorWithHexString:@"#efeeee"];
    _totalTime = kCheckCodeTotalTime;
    [self addBackNavItem];
    [self setupSubView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    [self setNavgationItemTitle];
    [self changeSubViewStyle];
    [self isHideHadAccountButton];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
#pragma mark -- requestData 
//通过手机号获取验证码
- (void)loadCheckCodeRequestState:(void(^)(NSInteger state))successState {
    successState(0);
}
//注册请求
- (void)loadRegisterRequest {
    XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:self.phoneNum.length == 0 ? @"" : self.phoneNum forKey:@"PHONE"];
    [parameter setObject:self.passwordTextField.text forKey:@"PASSWORD"];
    [parameter setObject:self.checkCodeTextField.text forKey:@"CODE"];
    NSString *invitationCodeText= @"";
    if (self.invitationCodeTextField.text.length !=0) {
        invitationCodeText =self.invitationCodeTextField.text;
    }
   [parameter setObject:invitationCodeText forKey:@"INVITECODE"];//邀请码
    [parameter setObject:model.xiaoQuID.length == 0 ? @"" :model.xiaoQuID forKey:@"COMMUNITYID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/regist" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                [weakSelf requestSuccessShowVC];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }

    } failure:^(bool isFailure) {
        
        
    }];
}
//修改密码请求
- (void)loadModifiedPasswordRequest {
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:self.phoneNum.length == 0 ? @"" : self.phoneNum  forKey:@"PHONE"];
    [parameter setObject:self.checkCodeTextField.text forKey:@"CODE"];
    [parameter setObject:self.passwordTextField.text forKey:@"PWD"];
    [parameter setObject:self.surePasswordTextField.text forKey:@"VERIFYPWD"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/resetPwd" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                [weakSelf requestSuccessShowVC];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
        
    } failure:^(bool isFailure) {
        
        
    }];
}
//找回密码请求
- (void)loadFindPasswordRequest {
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:self.phoneNum.length == 0 ? @"" : self.phoneNum  forKey:@"PHONE"];
    [parameter setObject:self.checkCodeTextField.text forKey:@"CODE"];
    [parameter setObject:self.passwordTextField.text forKey:@"PWD"];
    [parameter setObject:self.surePasswordTextField.text forKey:@"VERIFYPWD"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/resetPwd" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf requestSuccessShowVC];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
        
    } failure:^(bool isFailure) {
        
        
    }];
}
//更改手机号码
- (void)loadModifiedPhoneNumberRequest {
    //[self requestSuccessShowVC];
}

- (void)requestSuccessShowVC{
    NSString *msg = @"";
    if (self.typeOfView == twoStepIsRegister) {
        msg = @"恭喜您注册成功";
    } else if (self.typeOfView == twoStepIsModifiedPassword || self.typeOfView == twoStepIsFindTheSecret) {
         msg = @"密码修改成功";
    }
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
    
}

#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //先判断栈里面是否有登陆页面，如果有直接跳转，如果没有生产登录对象跳转
    for (UIViewController *viewController in self.navigationController.childViewControllers) {
        if ([viewController isKindOfClass:[Login_Controller class]]) {
            ((Login_Controller*)viewController).userName = self.phoneNum;
            [self.navigationController popToViewController:viewController animated:YES];
            return ;
        }
    }
    Login_Controller *loginVC = [[Login_Controller alloc]init];
    loginVC.userName = self.phoneNum;
    [self.navigationController pushViewController:loginVC animated:YES];
}

#pragma mark -- button Action
//刷新验证码
- (void)refreshCheckCodeEvent:(id)sender {
    [self hideKeyboard];
    _totalTime = kCheckCodeTotalTime;
    if ([self isRightBeforeRequestCheckCode]) {
        //获取到验证码开始计时
        [self loadCheckCodeRequestState:^(NSInteger state) {
            
            if (state == 0) {
                [self beginTimer];
            }
        }];
    }
}

//同意协议条款
- (void)agreeItemsEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
}
//注册事件
- (void)registerEvent:(id)sender {
    switch (self.typeOfView)
    {
        case twoStepIsRegister:
            if ([self requestParamsIsRight])
            {
                [self loadRegisterRequest];
            }
            break;
        case twoStepIsModifiedPassword:
            if ([self requestParamsIsRight])
            {
                [self loadModifiedPasswordRequest];
            }
            break;
        case twoStepIsFindTheSecret:
            if ([self requestParamsIsRight])
            {
                [self loadFindPasswordRequest];
            }
            break;
            
        case TwoStepIsModifiedPhoneNumber:
            if ([self requestParamsIsRight])
            {
                [self loadModifiedPhoneNumberRequest];
            }
            break;
            
        default:
            break;
    }

}
- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//隐藏键盘
- (void)hideKeyboard
{
    if ([self.phoneTextField isFirstResponder])
    {
        [self.phoneTextField resignFirstResponder];
    }
    if ([self.checkCodeTextField isFirstResponder])
    {
        [self.checkCodeTextField resignFirstResponder];
    }
    if ([self.passwordTextField isFirstResponder])
    {
        [self.passwordTextField resignFirstResponder];
    }
    if ([self.surePasswordTextField isFirstResponder])
    {
        [self.surePasswordTextField resignFirstResponder];
    }
    if ([self.invitationCodeTextField isFirstResponder])
    {
        [self.invitationCodeTextField resignFirstResponder];
    }
}
#pragma mark -- 操作之前判断
//获取验证码之前的判断
- (BOOL)isRightBeforeRequestCheckCode
{
    //VerificationPhoneNumber * verificatPhoneNum = [[VerificationPhoneNumber alloc]init];
    //只对更改手机号进行特殊处理
    if (self.typeOfView == TwoStepIsModifiedPhoneNumber)
    {
        if (self.phoneTextField.text.length == 0)
        {
            //NSString * errorMsg = @"手机号码不能为空";
            //[[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            [self.phoneTextField becomeFirstResponder];
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return YES;
}
//确认之前判断
//确认之前判断
- (BOOL)requestParamsIsRight
{
    //VerificationPhoneNumber * verificatPhoneNum = [[VerificationPhoneNumber alloc]init];
    switch (self.typeOfView)
    {
        case twoStepIsRegister:
            //判断验证码，第一步输入手机号返回的验证码和输入验证码作比较
            //            if (![self.checkCode isEqualToString:self.checkCodeTextField.text])
            //            {
            //                NSString * errorMsg = @"验证码不正确，请重新输入";
            //                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            //                return NO;
            //
            //            }
            //else
            if (self.checkCodeTextField.text.length == 0)
            {
                NSString * errorMsg = @"验证码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.checkCodeTextField becomeFirstResponder];
                return NO;
            }
            if (self.passwordTextField.text.length == 0 )
            {
                NSString * errorMsg = @"密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.passwordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.surePasswordTextField.text.length == 0)
            {
                NSString * errorMsg = @"确认密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.passwordTextField.text.length < 6 || self.surePasswordTextField.text.length < 6)
            {
                NSString * errorMsg = @"密码设置不能少于六位";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(![self.passwordTextField.text isEqualToString:self.surePasswordTextField.text])
            {
                NSString * errorMsg = @"两次密码输入不一致";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
                
            }
            else if(!self.agreeItemsBtn.selected)
            {
                NSString * errorMsg = @"请阅读并同意《大实惠服务条款》";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                return NO;
            }
            else
            {
                return YES;
            }
            
            break;
        case twoStepIsModifiedPassword:
            //判断验证码，第一步输入手机号返回的验证码和输入验证码作比较
            //            if (![self.checkCode isEqualToString:self.checkCodeTextField.text])
            //            {
            //                NSString * errorMsg = @"验证码不正确，请重新输入";
            //                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            //                return NO;
            //
            //            }
            if (self.checkCodeTextField.text.length == 0)
            {
                NSString * errorMsg = @"验证码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.checkCodeTextField becomeFirstResponder];
                return NO;
            }
            if (self.passwordTextField.text.length == 0 )
            {
                NSString * errorMsg = @"密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.passwordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.surePasswordTextField.text.length == 0)
            {
                NSString * errorMsg = @"确认密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.passwordTextField.text.length < 6 || self.surePasswordTextField.text.length < 6)
            {
                NSString * errorMsg = @"密码设置不能少于六位";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(![self.surePasswordTextField.text isEqualToString:self.passwordTextField.text])
            {
                NSString * errorMsg = @"两次密码输入不一致";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            
            //            else if(![self.passwordTextField.text isEqualToString:self.rePasswordTextField.text])
            //            {
            //                NSString * errorMsg = @"两次密码输入不一致";
            //                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            //                return NO;
            //
            //            }
            else
            {
                return YES;
            }
            break;
        case twoStepIsFindTheSecret:
            //判断验证码，第一步输入手机号返回的验证码和输入验证码作比较
            //            if (![self.checkCode isEqualToString:self.checkCodeTextField.text])
            //            {
            //                NSString * errorMsg = @"验证码不正确，请重新输入";
            //                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            //                return NO;
            //
            //            }
            if (self.checkCodeTextField.text.length == 0)
            {
                NSString * errorMsg = @"验证码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                return NO;
            }
            if (self.passwordTextField.text.length == 0 )
            {
                NSString * errorMsg = @"密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.passwordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.surePasswordTextField.text.length == 0)
            {
                NSString * errorMsg = @"确认密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(self.passwordTextField.text.length < 6 || self.surePasswordTextField.text.length < 6)
            {
                NSString * errorMsg = @"密码设置不能少于六位";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
            }
            else if(![self.passwordTextField.text isEqualToString:self.surePasswordTextField.text])
            {
                NSString * errorMsg = @"两次密码输入不一致";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                //[self.rePasswordTextField becomeFirstResponder];
                return NO;
                
            }
            else
            {
                return YES;
            }
            break;
            
        case TwoStepIsModifiedPhoneNumber:
            //            if (![verificatPhoneNum isMobileNumber:self.phoneNumTextField.text])
            //            {
            //                NSString * errorMsg = @"手机号码格式不正确，请重新输入";
            //                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
            //                [UIView animateWithDuration:0.3 animations:^{
            //
            //                } completion:^(BOOL finished) {
            //                    [self.phoneNumTextField becomeFirstResponder];
            //                }];
            //                return NO;
            //            }
            //            else
            if (self.phoneTextField.text.length == 0)
            {
                NSString * errorMsg = @"手机号码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                return NO;
                
            }
            else if(self.checkCodeTextField.text.length == 0)
            {
                NSString * errorMsg = @"验证码不正确，请重新输入";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                [UIView animateWithDuration:0.3 animations:^{
                    
                } completion:^(BOOL finished) {
                    //[self.checkCodeTextField becomeFirstResponder];
                }];
                return NO;
                
            }
            else if (self.passwordTextField.text.length == 0 )
            {
                NSString * errorMsg = @"密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                [UIView animateWithDuration:0.3 animations:^{
                    
                } completion:^(BOOL finished) {
                    // [self.passwordTextField becomeFirstResponder];
                }];
                return NO;
            }
            else if (self.surePasswordTextField.text.length == 0)
            {
                NSString * errorMsg = @"确认密码不能为空";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                return NO;
            }
            
            else if(self.passwordTextField.text.length < 6 || self.surePasswordTextField.text.length < 6)
            {
                NSString * errorMsg = @"密码设置不能少于六位";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                return NO;
            }
            else if(![self.passwordTextField.text isEqualToString:self.surePasswordTextField.text])
            {
                NSString * errorMsg = @"两次密码输入不一致";
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
                [UIView animateWithDuration:0.3 animations:^{
                    
                } completion:^(BOOL finished) {
                    //[self.rePasswordTextField becomeFirstResponder];
                }];
                return NO;
                
            }
            else
            {
                return YES;
            }
            break;
        default:
            break;
    }
    
    return NO;
}
#pragma mark 定时器设置
- (void)beginTimer
{
    if (!_timer)
    {
        _timer = [NSTimer scheduledTimerWithTimeInterval:kCheckCodeDuration target:self selector:@selector(judgeIsOverTime)userInfo:nil repeats:YES];
        [self.timer fire];
    }
}

- (void)endTimer
{
    if (self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
        
    }
}

//判断是否超时
- (void)judgeIsOverTime
{
    _totalTime--;
    if (_totalTime < 1)
    {
        [self endTimer];
        [self setEnabledBackgrounOfgetCheckCode:YES];
    }
    else
    {
        [self setEnabledBackgrounOfgetCheckCode:NO];
    }
}

#pragma mark -- 初始化视图
//设置navigation标题
- (void)setNavgationItemTitle {
    NSString * title;
    switch (self.typeOfView)
    {
        case 0:
            title = @"新用户注册";
            break;
        case 1:
            title = @"密码重置";
            break;
        case 2:
            title = @"密码找回";
            break;
        case 3:
            title = @"变更手机号码";
            break;
            
        default:
            break;
    }
    self.title = title;
}
//改变子视图样式
- (void)changeSubViewStyle {
    if (self.typeOfView == TwoStepIsModifiedPhoneNumber) {
        [self setEnabledBackgrounOfgetCheckCode:YES];
    } else {
        self.checkCodeTextField.text = self.checkCode;
        [self beginTimer];
    }
}
//设置获取验证码按钮背景，及是否可点击
- (void)setEnabledBackgrounOfgetCheckCode:(BOOL)enabled
{
    self.refreshCheckCodeBtn.enabled  = enabled;
    if (enabled)//可点击
    {
        [UIView animateWithDuration:0.3 animations:^{
            
        } completion:^(BOOL finished) {
            [self.refreshCheckCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
            [self.refreshCheckCodeBtn setBackgroundImage:[[UIImage imageNamed:@"login_Btn"] stretchableImageWithLeftCapWidth:6 topCapHeight:6] forState:UIControlStateNormal];
            
        }];
    }
    else
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
        } completion:^(BOOL finished) {
            
            NSString * time = [NSString stringWithFormat:@"%ld秒后重新获取",(long)_totalTime];
            [self.refreshCheckCodeBtn setTitle:time forState:UIControlStateDisabled];
            // self.showTimeLabel.text = time;
            [self.refreshCheckCodeBtn setBackgroundImage:[[UIImage imageNamed:@"gray_button"] stretchableImageWithLeftCapWidth:6 topCapHeight:6] forState:UIControlStateDisabled];
            
        }];
        
        
    }
}
- (void)isHideHadAccountButton
{
    switch (self.typeOfView) {
        case twoStepIsRegister://注册第二步按钮不隐藏
            self.agreeItemsBtn.hidden = NO;
            self.phoneTextField.hidden = YES;
            [self.registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
            self.passwordTextField.placeholder =   @"密      码";
            self.surePasswordTextField.placeholder = @"确认密码";
            [self changeSubViewFrame];
            break;
        case twoStepIsModifiedPassword://修改密码第二步按钮隐藏
            self.agreeItemsBtn.hidden = YES;
            self.phoneTextField.hidden = YES;
            [self.registerBtn setTitle:@"确认" forState:UIControlStateNormal];
            self.passwordTextField.placeholder = @"新密码";
            self.surePasswordTextField.placeholder = @"确认新密码";
            [self changeSubViewFrame];
            break;
        case twoStepIsFindTheSecret://找回密码第二步按钮隐藏
            self.agreeItemsBtn.hidden = YES;
            self.phoneTextField.hidden = YES;
            [self.registerBtn setTitle:@"确认" forState:UIControlStateNormal];
            self.passwordTextField.placeholder =   @"新密码";
            self.surePasswordTextField.placeholder = @"确认密码";
            [self changeSubViewFrame];
            break;
        case TwoStepIsModifiedPhoneNumber://变更手机号第二步按钮隐藏
            self.phoneTextField.hidden = NO;
            self.agreeItemsBtn.hidden = YES;
            [self.registerBtn setTitle:@"确认" forState:UIControlStateNormal];
            self.passwordTextField.placeholder =   @"密       码";
            self.surePasswordTextField.placeholder = @"确认密码";
            [self recoverSubViewFrame];
            break;
        default:
            break;
    }
}

//修改子视图frame
- (void)changeSubViewFrame {//修改密码
    //验证码输入框frame
    self.checkCodeTextField.frame = CGRectMake(CGRectGetMinX(self.checkCodeTextField.frame)
                                               , CGRectGetMinY(self.phoneTextField.frame)
                                               , CGRectGetWidth(self.checkCodeTextField.frame)
                                               , CGRectGetHeight(self.checkCodeTextField.frame));
    //获取验证码按钮frame
    self.refreshCheckCodeBtn.frame = CGRectMake(CGRectGetMinX(self.refreshCheckCodeBtn.frame)
                                                , CGRectGetMinY(self.phoneTextField.frame)
                                                , CGRectGetWidth(self.refreshCheckCodeBtn.frame)
                                                , CGRectGetHeight(self.refreshCheckCodeBtn.frame));
    //密码输入框frame
    self.passwordTextField.frame = CGRectMake(CGRectGetMinX(self.passwordTextField.frame)
                                              , CGRectGetMaxY(self.checkCodeTextField.frame) + kVSpace
                                              , CGRectGetWidth(self.passwordTextField.frame)
                                              , CGRectGetHeight(self.passwordTextField.frame));
    //确认密码输入框frame
    self.surePasswordTextField.frame = CGRectMake(CGRectGetMinX(self.surePasswordTextField.frame)
                                                , CGRectGetMaxY(self.passwordTextField.frame) + kVSpace
                                                , CGRectGetWidth(self.surePasswordTextField.frame)
                                                , CGRectGetHeight(self.surePasswordTextField.frame));
    
    switch (self.typeOfView) {
        //注册页面
        case twoStepIsRegister:
        {
            _invitationCodeTextField.hidden=NO;
            self.agreeItemsBtn.frame = CGRectMake(CGRectGetMinX(self.invitationCodeTextField.frame)
                                                  , CGRectGetMaxY(self.invitationCodeTextField.frame) + 5
                                                  , CGRectGetWidth(self.agreeItemsBtn.bounds)
                                                  , CGRectGetHeight(self.agreeItemsBtn.bounds));

            self.registerBtn.frame = CGRectMake(CGRectGetMinX(self.registerBtn.frame)
                                                , CGRectGetMaxY(self.agreeItemsBtn.frame) + 5
                                                , CGRectGetWidth(self.registerBtn.frame)
                                                , CGRectGetHeight(self.registerBtn.frame));
        }
            break;
        //修改密码页面
        case twoStepIsModifiedPassword:
        {
            self.registerBtn.frame = CGRectMake(CGRectGetMinX(self.registerBtn.frame)
                                                , CGRectGetMaxY(self.surePasswordTextField.frame) + kVSpace
                                                , CGRectGetWidth(self.registerBtn.frame)
                                                , CGRectGetHeight(self.registerBtn.frame));
        }
            break;
        //找回密码页面
        case twoStepIsFindTheSecret:
        {
            self.registerBtn.frame = CGRectMake(CGRectGetMinX(self.registerBtn.frame)
                                                , CGRectGetMaxY(self.surePasswordTextField.frame) + kVSpace
                                                , CGRectGetWidth(self.registerBtn.frame)
                                                , CGRectGetHeight(self.registerBtn.frame));
        }
            break;
        default:
            break;
    }
}

- (void)recoverSubViewFrame//注册
{
    //验证码输入框frame
    self.checkCodeTextField.frame = CGRectMake(CGRectGetMinX(self.checkCodeTextField.frame)
                                               , CGRectGetMaxY(self.phoneTextField.frame) + kVSpace
                                               , CGRectGetWidth(self.checkCodeTextField.frame)
                                               , CGRectGetHeight(self.checkCodeTextField.frame));
    //获取验证码按钮frame
    self.refreshCheckCodeBtn.frame = CGRectMake(CGRectGetMinX(self.refreshCheckCodeBtn.frame)
                                            , CGRectGetMinY(self.checkCodeTextField.frame)
                                            , CGRectGetWidth(self.checkCodeTextField.bounds)
                                            , CGRectGetHeight(self.checkCodeTextField.bounds));
    //密码输入框frame
    self.passwordTextField.frame = CGRectMake(CGRectGetMinX(self.passwordTextField.frame)
                                              , CGRectGetMaxY(self.checkCodeTextField.frame) + kVSpace
                                              , CGRectGetWidth(self.passwordTextField.frame)
                                              , CGRectGetHeight(self.passwordTextField.frame));
    //确认密码输入框frame
    self.surePasswordTextField.frame = CGRectMake(CGRectGetMinX(self.surePasswordTextField.frame)
                                                , CGRectGetMaxY(self.passwordTextField.frame) + kVSpace
                                                , CGRectGetWidth(self.surePasswordTextField.bounds)
                                                , CGRectGetHeight(self.surePasswordTextField.bounds));
      //邀请码frame
    self.invitationCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.invitationCodeTextField.frame)
                                                , CGRectGetMaxY(self.surePasswordTextField.frame) + kVSpace
                                                , CGRectGetWidth(self.invitationCodeTextField.bounds)
                                                , CGRectGetHeight(self.invitationCodeTextField.bounds))];
    //修改密码按钮frame
    self.registerBtn.frame = CGRectMake(CGRectGetMinX(self.registerBtn.frame)
                                        , CGRectGetMaxY(self.surePasswordTextField.frame) + kVSpace
                                        , CGRectGetWidth(self.registerBtn.frame)
                                        , CGRectGetHeight(self.registerBtn.frame));
}
- (void)setupSubView {
    
    UIImage *textfieldBackgroundImage = [UIImage imageNamed:@"intPutBg"];
    textfieldBackgroundImage = [textfieldBackgroundImage stretchableImageWithLeftCapWidth:6 topCapHeight:6];
    //根视图容器
    _rootScrollView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0
                                                                                    , 0
                                                                                    , CGRectGetWidth(self.view.bounds)
                                                                                    , CGRectGetHeight(self.view.bounds))];
    _rootScrollView.scrollViewDelegate = (id<TPKeyboardAvoidingScrollViewDelegate>)self;
    _rootScrollView.showsHorizontalScrollIndicator = NO;
    _rootScrollView.showsVerticalScrollIndicator = NO;
    _rootScrollView.backgroundColor = [UIColor clearColor];
    _rootScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds)
                                             , CGRectGetHeight(self.view.bounds) - 64);
    [self.view addSubview:_rootScrollView];
    //头部指示图片
    UIImageView *leftImageView = [[UIImageView alloc]initWithFrame:CGRectMake(kleftSpace
                                                                              , 15
                                                                              , (CGRectGetWidth(self.view.bounds) - 2*kleftSpace - kHSpace)/2
                                                                              , 3)];
    leftImageView.image = [UIImage imageNamed:@"green_line"];
    leftImageView.hidden = YES;
    [_rootScrollView addSubview:leftImageView];
    
    UIImageView *rightImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftImageView.frame) + kHSpace
                                                                               , CGRectGetMinY(leftImageView.frame)
                                                                               , CGRectGetWidth(leftImageView.bounds)
                                                                               , CGRectGetHeight(leftImageView.bounds))];
    rightImageView.image = [UIImage imageNamed:@"blue_line"];
    rightImageView.hidden = YES;
    [_rootScrollView addSubview:rightImageView];
    //手机号文本框
    _phoneTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(leftImageView.frame)
                                                                   ,CGRectGetMaxY(leftImageView.frame) + kVSpace - 15
                                                                   ,CGRectGetWidth(self.view.bounds) - 2*kleftSpace
                                                                   , 44)];
    _phoneTextField.font = [UIFont systemFontOfSize:16];
    _phoneTextField.background = textfieldBackgroundImage;
    _phoneTextField.placeholder = @"输入新手机号码";
    [_phoneTextField setDirection:kLeftEdge edgeSpace:8];
    [_rootScrollView addSubview:_phoneTextField];
    //验证码输入框
    _checkCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(_phoneTextField.frame)
                                                                      , CGRectGetMaxY(_phoneTextField.frame) + kVSpace
                                                                      , CGRectGetWidth(leftImageView.bounds)
                                                                       , CGRectGetHeight(_phoneTextField.bounds))];
    _checkCodeTextField.font = [UIFont systemFontOfSize:16];
    _checkCodeTextField.background = textfieldBackgroundImage;
    _checkCodeTextField.placeholder = @"输入验证码";
    [_checkCodeTextField setDirection:kLeftEdge edgeSpace:8];
    [_rootScrollView addSubview:_checkCodeTextField];
    //获取验证码按钮
    _refreshCheckCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _refreshCheckCodeBtn.frame = CGRectMake(CGRectGetMaxX(_checkCodeTextField.frame) + kHSpace
                                            , CGRectGetMinY(_checkCodeTextField.frame)
                                            , CGRectGetWidth(_checkCodeTextField.bounds)
                                            , CGRectGetHeight(_checkCodeTextField.bounds));
    
    UIImage *refreshCheckCodeImage = [UIImage imageNamed:@"gray_button"];
    refreshCheckCodeImage = [refreshCheckCodeImage stretchableImageWithLeftCapWidth:6 topCapHeight:6];
    [_refreshCheckCodeBtn setBackgroundImage:refreshCheckCodeImage forState:UIControlStateNormal];
    [_refreshCheckCodeBtn addTarget:self action:@selector(refreshCheckCodeEvent:) forControlEvents:UIControlEventTouchUpInside];
    [_rootScrollView addSubview:_refreshCheckCodeBtn];
    
    //密码输入框
    _passwordTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(_checkCodeTextField.frame)
                                                                      , CGRectGetMaxY(_checkCodeTextField.frame) + kVSpace
                                                                      , CGRectGetWidth(_phoneTextField.bounds)
                                                                      , CGRectGetHeight(_phoneTextField.bounds))];
    _passwordTextField.font = [UIFont systemFontOfSize:16];
    _passwordTextField.placeholder = @"密   码";
    _passwordTextField.background = textfieldBackgroundImage;
    [_passwordTextField setDirection:kLeftEdge edgeSpace:8];
    _passwordTextField.secureTextEntry = YES;
    [_rootScrollView addSubview:_passwordTextField];
    //确认密码输入框
    _surePasswordTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(_passwordTextField.frame)
                                                                      , CGRectGetMaxY(_passwordTextField.frame) + kVSpace
                                                                      , CGRectGetWidth(_passwordTextField.bounds)
                                                                      , CGRectGetHeight(_passwordTextField.bounds))];
    _surePasswordTextField.font = [UIFont systemFontOfSize:16];
    _surePasswordTextField.placeholder = @"确认密码";
    _surePasswordTextField.background = textfieldBackgroundImage;
    [_surePasswordTextField setDirection:kLeftEdge edgeSpace:8];
    _surePasswordTextField.secureTextEntry = YES;
    [_rootScrollView addSubview:_surePasswordTextField];
    
    //邀请码
    _invitationCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMinX(_passwordTextField.frame)
                                                                          , CGRectGetMaxY(_passwordTextField.frame) + kVSpace
                                                                          , CGRectGetWidth(_passwordTextField.bounds)
                                                                          , CGRectGetHeight(_passwordTextField.bounds))];
    _invitationCodeTextField.font = [UIFont systemFontOfSize:16];
    _invitationCodeTextField.placeholder = @"邀请码（选填）";
//    _invitationCodeTextField.hidden
    _invitationCodeTextField.background = textfieldBackgroundImage;
    [_invitationCodeTextField setDirection:kLeftEdge edgeSpace:8];
    _invitationCodeTextField.hidden=YES;
    [_rootScrollView addSubview:_invitationCodeTextField];
    
    //同意条款按钮
    _agreeItemsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _agreeItemsBtn.frame = CGRectMake(CGRectGetMinX(_invitationCodeTextField.frame)
                                            , CGRectGetMaxY(_invitationCodeTextField.frame) + 5
                                            , CGRectGetWidth(_invitationCodeTextField.bounds)
                                            , CGRectGetHeight(_invitationCodeTextField.bounds));
    
    _agreeItemsBtn.backgroundColor = [UIColor clearColor];
    [_agreeItemsBtn setTitle:@"我已阅读并同意《大实惠服务条款》" forState:UIControlStateNormal];
    [_agreeItemsBtn setTitleColor:[UIColor colorWithHexString:@"#72706f"] forState:UIControlStateNormal];
    [_agreeItemsBtn setImage:[UIImage imageNamed:@"fxk"] forState:UIControlStateNormal];
    [_agreeItemsBtn setImage:[UIImage imageNamed:@"fxk_choose"] forState:UIControlStateSelected];
    _agreeItemsBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _agreeItemsBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
    _agreeItemsBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_agreeItemsBtn addTarget:self action:@selector(agreeItemsEvent:) forControlEvents:UIControlEventTouchUpInside];
    [_rootScrollView addSubview:_agreeItemsBtn];
    //注册按钮
    _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _registerBtn.frame = CGRectMake(CGRectGetMinX(_agreeItemsBtn.frame)
                                     , CGRectGetMaxY(_agreeItemsBtn.frame) + kVSpace
                                     , CGRectGetWidth(_agreeItemsBtn.bounds)
                                     , CGRectGetHeight(_agreeItemsBtn.bounds));
    [_registerBtn setTitle:@"立即注册" forState:UIControlStateNormal];
    UIImage *loginImage = [UIImage imageNamed:@"login_Btn"];
    loginImage = [loginImage stretchableImageWithLeftCapWidth:6 topCapHeight:6];
    [_registerBtn setBackgroundImage:loginImage forState:UIControlStateNormal];
    [_registerBtn addTarget:self action:@selector(registerEvent:) forControlEvents:UIControlEventTouchUpInside];
    [_rootScrollView addSubview:_registerBtn];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
