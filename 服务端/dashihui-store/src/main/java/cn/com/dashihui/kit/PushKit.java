package cn.com.dashihui.kit;

import java.util.List;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Logger;

/**
 * “个推”推送工具类，用来向客户端发起推送消息<br/>
 * 目前用于服务商家版客户端的推送
 * 参考：http://docs.igetui.com/pages/viewpage.action?pageId=1213590
 */
public class PushKit {
	
	private static final Logger logger = Logger.getLogger(PushKit.class);
	
	private static boolean isDebug(){
		Prop prop = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
		return prop.getBoolean("debug.push", true);
	}

	/**
	 * 向多个客户端推送消息
	 * @param cidList 多个客户端clientid集合
	 * @param title 推送消息标题
	 * @param text 摄像头消息内容
	 */
	public static void pushToMany(List<String> cidList,String title,String text){
		if(cidList!=null){
			for(String cid : cidList){
				pushToSingle(cid,title,text);
			}
		}
	}
	
	/**
	 * 向多个客户端推送消息
	 * @param cid 客户端clientid
	 * @param title 推送消息标题
	 * @param text 摄像头消息内容
	 */
	public static void pushToSingle(String cid,String title,String text){
		logger.info("向clientid："+cid+"推送消息：\n"+title+"\n"+text);
		if(!isDebug()){
			IGtPush push = new IGtPush(PropKit.get("getui.business.host"), PropKit.get("getui.business.appKey"), PropKit.get("getui.business.masterSecret"));
			SingleMessage message = new SingleMessage();
			message.setOffline(true);
			//离线有效时间，单位为毫秒，可选
			message.setOfflineExpireTime(24 * 3600 * 1000);
			message.setData(notificationTemplateDemo(title,text));
			message.setPushNetWorkType(0); //可选。判断是否客户端是否wifi环境下推送，1为在WIFI环境下，0为不限制网络环境。
			Target target = new Target();
			target.setAppId(PropKit.get("gt.appId"));
			target.setClientId(cid);
			IPushResult ret = null;
			try{
				ret = push.pushMessageToSingle(message, target);
			}catch(RequestException e){
				e.printStackTrace();
				ret = push.pushMessageToSingle(message, target, e.getRequestId());
			}
			if(ret != null){
				logger.info(ret.getResponse().toString()); 
			}else{
				logger.info("服务器响应异常"); 
			}
		}
	}

	/**
	 * 生成“个推”客户端部分能识别的消息体
	 */
	private static NotificationTemplate notificationTemplateDemo(String title,String text) {
	    NotificationTemplate template = new NotificationTemplate();
	    // 设置APPID与APPKEY
	    template.setAppId(PropKit.get("gt.appId"));
	    template.setAppkey(PropKit.get("gt.appKey"));
	    // 设置通知栏标题与内容
	    template.setTitle(title);
	    template.setText(text);
	    // 配置通知栏图标
	    template.setLogo("icon.png");
	    // 配置通知栏网络图标
	    template.setLogoUrl("");
	    // 设置通知是否响铃，震动，或者可清除
	    template.setIsRing(true);
	    template.setIsVibrate(true);
	    template.setIsClearable(true);
	    //1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
	    template.setTransmissionType(1);
	    return template;
	}
}
