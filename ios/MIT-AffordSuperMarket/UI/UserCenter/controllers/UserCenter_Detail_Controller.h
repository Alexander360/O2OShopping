//
//  UserCenter_Detail_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_Detail_Controller
 Created_Date： 20151108
 Created_People： GT
 Function_description： 个人中心明细
 ***************************************/

#import "BaseViewController.h"

@interface UserCenter_Detail_Controller : BaseViewController

@end
