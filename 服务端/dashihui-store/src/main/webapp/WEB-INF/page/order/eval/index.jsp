<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">直营订单评价</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<hr/>
				<div class="container-fluid"><div id="dataList" class="row"></div></div>
				<div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<div id="item{{item.id}}" class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-sm-6 col-md-4 col-lg-2"><a href="javascript:showOrder('{{item.orderNum}}')"><label>订单号：</label>{{item.orderNum}}</a></div>
				<div class="col-sm-6 col-md-4 col-lg-2"><label>评价时间：</label>{{item.createDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label>商品质量：</label>
					<i class="fa text-danger text-lg {{if item.eval1>=1}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval1>=2}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval1>=3}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval1>=4}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval1>=5}}fa-star{{else}}fa-star-o{{/if}}"></i>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label>配送速度：</label>
					<i class="fa text-danger text-lg {{if item.eval2>=1}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval2>=2}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval2>=3}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval2>=4}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval2>=5}}fa-star{{else}}fa-star-o{{/if}}"></i>
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<label>服务质量：</label>
					<i class="fa text-danger text-lg {{if item.eval3>=1}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval3>=2}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval3>=3}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval3>=4}}fa-star{{else}}fa-star-o{{/if}}"></i>
				    <i class="fa text-danger text-lg {{if item.eval3>=5}}fa-star{{else}}fa-star-o{{/if}}"></i>
				</div>
				<div class="col-sm-12 col-md-12 col-lg-12"><label>评价内容：</label>{{item.content}}</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(function(){
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/order/eval/page",{"pageSize":10},function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
});
var showOrderDialog;
function showOrder(orderNum){
	showOrderDialog = Kit.dialog("查看订单","${BASE_PATH}/order/detail?orderNum="+orderNum,{size:'size-wide',closable:true}).open();
}
</script>
</body>
</html>