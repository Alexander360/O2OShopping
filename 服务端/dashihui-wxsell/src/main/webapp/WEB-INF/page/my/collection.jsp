<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的关注</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_user_collection.css" />
	</head>
	<body>
		<header class="am-header am-header-default app-header" data-am-sticky>
			<h1 class="am-header-title">我的关注</h1>
			<div class="am-header-right am-header-nav">
				<a href="javascript:toEditModel();" class="normalModel"><i class="am-icon-edit"></i></a>
				<a href="javascript:doComplete();" class="editModel app-hide"><i class="am-icon-check"></i></a>
			</div>
		</header>
		<div class="emptyPart" style="display: none;"></div>
		<div class="notEmptyPart">
			<div class="app-cart-list" id="recomGoodsList"></div>
			
			<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
			<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:7%;bottom:50px;z-index:2;display:none;">+1</span>
			
			<div class="app-cart" id="btnCart">
				<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
				<a href="${BASE_PATH }/cart"><img src="${BASE_PATH}/static/app/img/breakfast_bottom_left.png" style="width:50px;height:50px;"/></a>
			</div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
				<div id="item{{goods.id}}" class="app-cart-item am-g">
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2 editModel app-hide" style="padding-top: 9%; display: none;">
						<a id="checkItem{{goods.id}}" href="javascript:doCheck({{goods.id}});" data-goodsid="{{goods.id}}" class="editModel checkbox checkboxitem"></a>
					</div>
					<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
						<a href="${BASE_PATH}/goods/detail/{{goods.id}}">
							<img data-original="${FTP_PATH}{{goods.thumb}}" class="am-img-thumbnail lazyload" width="100%">
						</a>
					</div>
					<div class="am-u-sm-7 am-u-md-6 am-u-lg-6" style="width:66%;">
						<div class="app-cart-item-title">
							{{if goods.isSelf==1}}
            				<img src="${BASE_PATH}/static/app/img/goods_self.png" height="15" style="margin-top:-4px;"/>
							{{goods.name}}
							{{else}}
							{{goods.name}}
							{{/if}}
						</div>
						<div class="app-cart-item-spec">{{goods.spec}}</div>
						<div style="width:80%;float:left;">
							<div class="am-vertical-align-bottom" style="color:red;"><i class="am-icon-rmb"></i>{{goods.sellPrice}}</div>
							<div class="app-item-price-del"><i class="am-icon-rmb"></i>{{goods.marketPrice}}</div>		
						</div>				
						<div style="width:15%;float:left;" id="btnAddToCart">
							<a onclick="javascript:addCart('{{goods.id}}','{{goods.isSelf}}','{{goods.name}}','{{goods.spec}}','{{goods.thumb}}','{{goods.sellPrice}}','{{goods.marketPrice}}');">
								<img src="${BASE_PATH }/static/app/img/add_car.png" style="width:30px;heigt:30px;padding-top:10px;"/>
							</a>
						</div>
						<div class="am-cf"></div>
					</div>
				</div>
				<div class="app-total am-g editModel app-hide">
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2" style="padding-top:15px;">
						<a id="checkAll" href="javascript:doCheckAll();" class="checkbox"></a>
					</div>
					<div class="am-fl app-m-l-10">
						<span class="am-fl">全选</span>
						<div class="am-cf"></div>
					</div>
					<div class="app-del-wrapper am-fr">
						<a href="javascript:doDelAll()" class="app-del-btn">删除</a>
					</div>
					<div class="am-cf"></div>
				</div>
				<hr/>
			{{/each}}
		</script>
		<script type="text/html" id="noData">
			<div>
				<div>
					<img src="${BASE_PATH}/static/app/img/shopping_cart.png" style="margin:0 auto;display:block;" width="140">
					<div class="am-text-center app-m-t-10">暂无关注商品，快去逛逛吧~</div>
					<a href="${BASE_PATH}/index" class="am-center am-radius app-m-t-10" style="width:120px;">立即逛逛</a>
				</div>
			</div>
		</script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1;
			//加载标识，表示当前是否有请求未完成，防止同时多个请求
			var loading = false;
			$(function(){
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#recomGoodsList").append("<div class=\"app-loading\">正在加载</div>");
	               		Kit.ajax.post("${BASE_PATH}/my/collectionList",{pageNum:pageNum+1,pageSize:10},function(result){
							$("#recomGoodsList").append(template("dataTpl",result.object));
							//amazeui要求在最后一个元素上添加am-u-end样式，否则默认为右浮动
							$(".app-goods-item","#recomGoodsList").removeClass("am-u-end").last().addClass("am-u-end");
							//图片延迟加载
							$("img.lazyload","#recomGoodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#recomGoodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							
							//没找到数据
							if(totalPage==0) {
								$(".am-header-right").addClass("app-hide").hide();
								$(".notEmptyPart").remove();
								$(".emptyPart").append(template("noData")).show();
							}
							//重置加载标识
							loading = false;
						});
	               	}
				});
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			
			//切换为编辑模式
			function toEditModel(){
				Kit.ui.toggle(".editModel",".normalModel");
				$("body").attr("class","app-navbar-body");
				$(".am-u-sm-7").removeAttr("style");
				$("#btnAddToCart,#btnCart").hide();
			}
			//完成编辑
			function doComplete(){
				Kit.ui.toggle(".normalModel",".editModel");
				$("body").removeAttr("class");
				$(".am-u-sm-7").attr("style", "width:66%;");
				$("#btnAddToCart,#btnCart").show();
			}
			//选中所有
			function doCheckAll(){
				var $all = $("#checkAll"), isCheckedAll = $all.hasClass("checked");
				if(isCheckedAll){
					$(".checkboxitem,#checkAll").removeClass("checked");
				}else{
					$(".checkboxitem,#checkAll").addClass("checked");
				}
			}
			//单选选中或取消
			function doCheck(goodsid){
				var $ck = $("#checkItem"+goodsid), isChecked = $ck.hasClass("checked"), $all = $("#checkAll");
				if(isChecked){
					$ck.removeClass("checked");
					$all.removeClass("checked");
				}else{
					$ck.addClass("checked");
					if($(".checkboxitem").not(".checked").length==0){
						$all.addClass("checked");
					}
				}
			}
			//删除选中的商品
			function doDelAll(){
				var $all = $(".checkboxitem.checked");
				if($all.length!=0){
					Kit.ui.confirm("确认要删除这"+$all.length+"种商品吗？",function(){
						var goodsids = new Array();
						$all.each(function(index,item){
							goodsids.push($(item).data("goodsid"));
						});
						//移除选中的商品
						Kit.ajax.post("${BASE_PATH}/my/cancelCollect",{goodsid:goodsids.join(",")},function(result){
							if(result.flag==0){
								//我的关注中没有其他商品时，刷新页面
								Kit.render.refresh();
							}
						});
					});
				} else {
					Kit.ui.alert("请选择您要删除的商品！");
				}
			}
			//添加到购物车
			function addCart(id,isSelf,name,spec,thumb,sellPrice,marketPrice){
				var goods = {
					"id":id,
					"isSelf":isSelf,
					"name":name,
					"spec":spec,
					"thumb":thumb,
					"sellPrice":sellPrice,
					"marketPrice":marketPrice
				};
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":50+"px","opacity":150}).hide();
					});
				});
			}
		</script>
	</body>
</html>