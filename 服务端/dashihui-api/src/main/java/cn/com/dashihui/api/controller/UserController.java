package cn.com.dashihui.api.controller;

import java.io.IOException;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Duang;
import com.jfinal.core.Const;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.CacheHolder;
import cn.com.dashihui.api.common.CacheKey;
import cn.com.dashihui.api.common.ResultField;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.common.ResultState;
import cn.com.dashihui.api.common.SysConfig;
import cn.com.dashihui.api.dao.User;
import cn.com.dashihui.api.dao.UserAddress;
import cn.com.dashihui.api.interceptor.AuthLoginInterceptor;
import cn.com.dashihui.api.service.UserService;
import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.FTPClientKit;
import cn.com.dashihui.kit.InviteCodeKit;
import cn.com.dashihui.kit.SMSKit;
import cn.com.dashihui.kit.ValidateKit;

/**
 * 用户操作对象<br/>包括登录、注册、个人信息管理、重置密码、收藏管理、收货地址管理操作
 * v1.3.1版本，新增加服务端接口按版本号管理，2016-01-25
 */
@Before(AuthLoginInterceptor.class)
public class UserController extends BaseController{
	//使用Duang.duang进行封装，使普通类具有事务的功能
	private UserService service = Duang.duang(UserService.class);
    
    /**
     * 注册时，生成并发送N位短信验证码
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
     * @return CONTENT 生成并发送的N位短信验证码
     */
	@Clear(AuthLoginInterceptor.class)
    public void sendRegCode(){
    	String clientid = getPara("SIGNATURE");
    	String msisdn = getPara("PHONE");
    	if(StrKit.isBlank(clientid)){
    		renderFailed("参数SIGNATURE不能为空");
    		return;
    	}else if(StrKit.isBlank(msisdn)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(service.findByUsername(msisdn)!=null){
    		renderResult(ResultState.STATE_USERNAME_EXISTS,"手机号已经存在，请直接登录");
    		return;
    	}
    	String randomNum = null;
    	String key = CacheHolder.key(CacheKey.CACHE_REGIST_SMS_VALID_CODE_KEY,clientid,msisdn);
    	//判断缓存中是否已经存在未过期的验证码
    	Object oldNum = CacheHolder.get(key);
		if(oldNum!=null){
			//如果有，则继续使用
			randomNum = String.valueOf(oldNum);
		}else{
			//否则生成新的验证码
			randomNum = CommonKit.randomNum();
		}
		if(SMSKit.sendRegCode(msisdn, randomNum)){
			CacheHolder.cache(key, randomNum, CacheHolder.TIME_FIVE_MINUTE);
			Prop debug = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
			if(debug.getBoolean("debug.sms")){
				//短信接口调试模式时，将短信验证码返回给客户端
				renderSuccess(ResultMap.newInstance().put("CODE",randomNum));
				return;
			}else{
				renderSuccess();
				return;
			}
		}
		renderFailed("验证码发送失败，请重新获取");
    }
	
	/**
	 * 用户注册
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 * @param PASSWORD 密码
	 * @param CODE 短信验证码
	 * @param COMMUNITYID 定位后获取的社区ID
	 * @return TOKEN 用户签名，在用户登录成功时生成
	 */
	@Clear(AuthLoginInterceptor.class)
    public void regist(){
    	String clientid = getPara("SIGNATURE");
    	String msisdn = getPara("PHONE");
    	String password = getPara("PASSWORD");
    	String code = getPara("CODE");
    	String communityid = getPara("COMMUNITYID");
    	String inviteCode = getPara("INVITECODE");
    	//验证各参数
    	if(StrKit.isBlank(msisdn)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(!ValidateKit.Mobile(msisdn)){
    		renderFailed("参数PHONE格式不是");
    		return;
    	}else if(service.findByUsername(msisdn)!=null){
    		renderResult(ResultState.STATE_USERNAME_EXISTS,"手机号已经存在，不能重复注册");
    		return;
    	}else if(StrKit.isBlank(password)){
    		renderFailed("参数PASSWORD不能为空");
    		return;
    	}else if(ValidateKit.Password_reg(password)){
    		renderFailed("密码只能填写数字、字母、下划线组合");
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderFailed("请保持密码长度在6-18之间");
    		return;
    	}else if(StrKit.isBlank(code)){
    		renderFailed("参数CODE不能为空");
    		return;
    	}else if(StrKit.isBlank(communityid)){
    		renderFailed("参数COMMUNITYID不能为空");
    		return;
    	}else if(!StrKit.isBlank(inviteCode)&&service.findUserInfoById(Long.valueOf(InviteCodeKit.codeToId(inviteCode)).intValue())==null){
    		renderFailed("邀请码不正确");
    		return;
    	}
    	//验证短信验证码
    	String cacheKey = CacheHolder.key(CacheKey.CACHE_REGIST_SMS_VALID_CODE_KEY,clientid,msisdn);
    	if(!CacheHolder.has(cacheKey)){
    		renderFailed("未获取短信验证码或验证码已超时，请重新获取");
    		return;
    	}else if(!CacheHolder.getStr(cacheKey).equals(code)){
    		renderFailed("验证码不正确");
    		return;
    	}
		//移除缓存中的验证码
		CacheHolder.remove(cacheKey);
    	//新建
    	User user = new User()
    			.set("username", msisdn)
    			.set("password", CommonKit.encryptPassword(password))
    			.set("nickName", CommonKit.hideMsisdn(msisdn))
    			.set("communityid", communityid)
    			.set("inviteUserid", Long.valueOf(InviteCodeKit.codeToId(inviteCode)).intValue());
    	//保存用户信息
    	if(service.save(user)){
    		//保存成功后，创建用户与Client之间的session，即登录操作
    		String sessionid = CommonKit.getUUID().concat(CommonKit.randomNum(4));
			if(service.login(sessionid, user.getInt("id"), clientid)){
				//登录成功后，将sessionid返回
	    		renderSuccess(ResultMap.newInstance().put(ResultField.FIELD_USER_LOGIN_TOKEN,sessionid).put("USER", service.findUserInfoById(user.getInt("id"))));
			}else{
				renderSuccess();
			}
			return;
    	}
    	renderFailed("注册失败");
    }
    
    /**
	 * 用户登录
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 * @param PASSWORD 密码
	 * @return TOKEN 用户签名，在用户登录成功时生成
	 */
	@Clear(AuthLoginInterceptor.class)
    public void login(){
    	String clientid = getPara("SIGNATURE");
    	String msisdn = getPara("PHONE");
    	String password = getPara("PASSWORD");
    	//验证各参数
    	if(StrKit.isBlank(msisdn)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(!ValidateKit.Mobile(msisdn)){
    		renderFailed("参数PHONE格式不是");
    		return;
    	}else if(StrKit.isBlank(password)){
    		renderFailed("参数PASSWORD不能为空");
    		return;
    	}else if(ValidateKit.Password_reg(password)){
    		renderFailed("密码只能填写数字、字母、下划线组合");
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderFailed("请保持密码长度在6-18之间");
    		return;
    	}
    	//查询指定手机号用户
    	User user = service.findByUsername(msisdn);
    	if(user==null||!CommonKit.passwordsMatch(password, user.getStr("password"))){
    		renderFailed("用户名或登录密码错误");
    		return;
    	}
		//创建用户与Client之间的session，即登录操作
		String sessionid = CommonKit.getUUID().concat(CommonKit.randomNum(4));
		if(service.login(sessionid, user.getInt("id"), clientid)){
			//登录成功后，将sessionid返回
    		renderSuccess(ResultMap.newInstance().put(ResultField.FIELD_USER_LOGIN_TOKEN,sessionid).put("USER", service.findUserInfoById(user.getInt("id"))));
    		return;
		}
		renderFailed("登录失败");
    }
	
	/**
	 * 查询当前登录用户对象的个人信息
	 */
	public void info(){
		//在AuthLoginInterceptor已经判断过当前登录情况，并已经查询了最新用户信息，在此只需要转而输出即可
		renderSuccess(service.findUserInfoById(getCurrentUser().getInt("id")));
	}
	
	/**
	 * 查询当前登录用户各状态信息<br/>
	 * 分别统计“收藏商品数量”、“待付款”、“待发货”、“待收货”、服务订单“待付款”、服务订单“待服务”六种状态的订单数量 
	 * @param APIVERSION 服务端接口版本号，以此判断来兼容旧版本接口
	 * @param STOREID 店铺ID，V1.3.1以后用
	 */
	public void state(){
		String apiVersion = getPara("APIVERSION");
		if(apiVersion.equals(PropKit.get("api.version.131"))){
			//1.3.1版本新增代码，2016-01-26
			String storeidStr = getPara("STOREID");
			if(StrKit.isBlank(storeidStr)){
	    		renderFailed("参数STOREID不能为空");
	    		return;
			}
			Record state = service.getUserState_v131(Integer.valueOf(storeidStr),getCurrentUser().getInt("id"));
			renderSuccess(ResultMap.newInstance()
					.put("COLLECTED",state.get("state1"))
					.put("NOPAY",state.get("state2"))
					.put("WAITDELIVER",state.get("state3"))
					.put("NOTAKE",state.get("state4"))
					.put("SERNOPAY",state.get("state5"))
					.put("SERNOSERVICE",state.get("state6")));
		}else if(apiVersion.equals(PropKit.get("api.version.132"))||apiVersion.equals(PropKit.get("api.version.133"))){
			//1.3.2版本新增代码，2016-03-16
			String storeidStr = getPara("STOREID");
			if(StrKit.isBlank(storeidStr)){
	    		renderFailed("参数STOREID不能为空");
	    		return;
			}
			Record state = service.getUserState_v132(Integer.valueOf(storeidStr),getCurrentUser().getInt("id"));
			renderSuccess(ResultMap.newInstance()
					.put("COLLECTED",state.get("state1"))
					.put("NOPAY",state.get("state2"))
					.put("NOTAKE",state.get("state3"))
					.put("NOEVAL",state.get("state4"))
					.put("SERNOPAY",state.get("state5"))
					.put("SERNOSERVICE",state.get("state6")));
		}
		return;
	}
    
	/**
	 * 我的钱包<br/>
	 * 查询用户的实惠币金额
	 */
	public void money(){
		User user = service.findUserInfoById(getCurrentUser().getInt("id"));
		Record record = service.findInviteUserCount(getCurrentUser().getInt("id"));
		renderSuccess(ResultMap.newInstance()
				.put("INVITECOUNT", record.get("inviteCount"))
				.put("MONEY",user.getDouble("MONEY")).getAttrs());
	}
	
	/**
	 * 会员奖励规则页面<br/>
	 * 不要求登录，任何人可看
	 */
	@Clear(AuthLoginInterceptor.class)
	public void rule(){
		try {
			setAttr("level2Line", SysConfig.getLevel2Line());
		} catch (Exception e) {
			e.printStackTrace();
		}
		render("rule.jsp");
	}
	
	/**
	 * 会员分享页面<br/>
	 * 只有黄金会员可看，同时还要展示会员的邀请码
	 */
	public void share(){
		setAttr("currentUser", getAttr(PropKit.get("constants.currentUserKey")));
		render("share.jsp");
	}
	
	/**
	 * 找回密码时发送手机验证码
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 */
	@Clear(AuthLoginInterceptor.class)
    public void sendResetPwdCode(){
    	String clientid = getPara("SIGNATURE");
    	String msisdn = getPara("PHONE");
    	if(StrKit.isBlank(msisdn)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(service.findByUsername(msisdn)==null){
    		renderFailed("手机号码未注册");
    		return;
    	}
    	String randomNum = null;
    	//判断缓存中是否已经存在未过期的验证码
    	Object oldNum = CacheHolder.get(CacheHolder.key(CacheKey.CACHE_RESETPWD_SMS_VALID_CODE_KEY,clientid,msisdn));
		if(oldNum!=null){
			//如果有，则继续使用
			randomNum = String.valueOf(oldNum);
		}else{
			//否则生成新的验证码
			randomNum = CommonKit.randomNum();
		}
		if(SMSKit.sendFindPwdCode(msisdn, randomNum)){
			CacheHolder.cache(CacheHolder.key(CacheKey.CACHE_RESETPWD_SMS_VALID_CODE_KEY,clientid,msisdn), randomNum, CacheHolder.TIME_FIVE_MINUTE);
			Prop debug = new Prop("realtime.properties",Const.DEFAULT_ENCODING);
			if(debug.getBoolean("debug.sms")){
				//短信接口调试模式时，将短信验证码返回给客户端
				renderSuccess(ResultMap.newInstance().put("CODE",randomNum));
				return;
			}else{
				renderSuccess();
				return;
			}
		}
		renderFailed("验证码发送失败，请重新获取");
    }

	/**
	 * 重置密码
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 * @param CODE 验证码
	 * @param PWD 新密码
	 * @param VERIFYPWD 确认密码
	 */
	@Clear(AuthLoginInterceptor.class)
	public void resetPwd(){
		String clientid = getPara("SIGNATURE");
    	String msisdn = getPara("PHONE");
    	String code = getPara("CODE");
    	String pwd = getPara("PWD");
    	String verifypwd = getPara("VERIFYPWD");
    	if(StrKit.isBlank(msisdn)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(StrKit.isBlank(code)){
    		renderFailed("参数CODE不能为空");
    		return;
    	}else if(service.findByUsername(msisdn)==null){
    		renderFailed("手机号未注册");
    		return;
    	}else if(StrKit.isBlank(pwd)){
    		renderFailed("密码为空");
    		return;
    	}else if(ValidateKit.Password_reg(pwd)){
    		renderFailed("密码只能填写数字、字母、下划线组合");
    		return;
    	}else if(pwd.length()<6||pwd.length()>18){
    		renderFailed("请保持密码长度在6-18之间");
    		return;
    	}else if(StrKit.isBlank(verifypwd)){
    		renderFailed("确认密码为空");
    		return;
    	}else if(!pwd.equals(verifypwd)){
    		renderFailed("两次密码输入不一致");
    		return;
    	}
		String cacheKey = CacheHolder.key(CacheKey.CACHE_RESETPWD_SMS_VALID_CODE_KEY,clientid,msisdn);
		if(!CacheHolder.has(cacheKey)){
			renderFailed("未获取短信验证码或验证码已超时，请重新获取");
			return;
		}else if(!CacheHolder.getStr(cacheKey).equals(code)){
			renderFailed("验证码不正确");
			return;
		}
		//移除缓存中的验证码
		CacheHolder.remove(cacheKey);
		if(service.updatePwdByUsername(CommonKit.encryptPassword(pwd), msisdn)){
			renderSuccess();
		}else{
			renderFailed("重置密码失败");
		}
	}
	
	/**
	 * 更新用户个人信息，可单独修改任一字段或全部修改
	 * @param NICKNAME 昵称
	 * @param SEX 性别
	 * @param AVATOR 头像
	 * @param COMMUNITYID 社区ID
	 * @param BUILDID 楼号ID
	 * @param UNITID 单元号ID
	 * @param FLOORID 楼层ID
	 * @param ROOMID 房间号ID
	 */
	public void update(){
		String nickName = getPara("NICKNAME");
		String sexStr = getPara("SEX");
		String avator = getPara("AVATOR");
		String communityidStr = getPara("COMMUNITYID");
		String buildidStr = getPara("BUILDID");
		String unitidStr = getPara("UNITID");
		String flooridStr = getPara("FLOORID");
		String roomidStr = getPara("ROOMID");
		User user = getCurrentUser();boolean isNeedUpdate = false;boolean isUpdateAvator = false;
		String avatorOld = user.getStr("avator");
		if(!StrKit.isBlank(nickName)){
			user.set("nickName", nickName);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(sexStr)&&(sexStr.equals("1")||sexStr.equals("2"))){
			user.set("sex", sexStr);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(avator)){
			user.set("avator", avator);isNeedUpdate = true;isUpdateAvator = true;
		}
		if(!StrKit.isBlank(communityidStr)){
			user.set("communityid", communityidStr);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(buildidStr)){
			user.set("buildid", buildidStr);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(unitidStr)){
			user.set("unitid", unitidStr);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(flooridStr)){
			user.set("floorid", flooridStr);isNeedUpdate = true;
		}
		if(!StrKit.isBlank(roomidStr)){
			user.set("roomid", roomidStr);isNeedUpdate = true;
		}
		if(isNeedUpdate){
			if(service.update(user)){
				//如果修改用户头像，则需要将旧头像图片删除
				if(isUpdateAvator&&StrKit.isBlank(avatorOld)){
					try {
						FTPClientKit.deleteFile(avatorOld);
					} catch (IOException e) {
						error("用户【"+user.getStr("username")+"】修改头像时，旧头像文件【"+avatorOld+"】删除失败");
						e.printStackTrace();
					}
				}
				renderSuccess();
			}else{
				renderFailed("更新信息失败");
			}
		}else{
			renderSuccess();
		}
	}
	
	/**
	 * 用户收藏指定商品
	 * @param STOREID 店铺ID
	 * @param GOODSID 商品ID
	 */
	public void doCollect(){
		String storeidStr = getPara("STOREID");
		String goodsidsStr = getPara("GOODSID");
		String isSelfsStr = getPara("ISSELF");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(goodsidsStr)){
			renderFailed("参数GOODSID不能为空");
			return;
		}
		String[] goodsids,isSelfs;
		if(goodsidsStr.indexOf(",")>=1&&isSelfsStr.indexOf(",")>=1){
			goodsids = goodsidsStr.split(",");
			isSelfs = isSelfsStr.split(",");
		}else{
			goodsids = new String[]{goodsidsStr};
			isSelfs = new String[]{isSelfsStr};
		}
		//批量收藏
		service.doCollect(Integer.valueOf(storeidStr),getCurrentUser().getInt("id"),goodsids,isSelfs);
		renderSuccess();
	}
	
	/**
	 * 用户取消收藏指定商品
	 * @param GOODSID 商品ID
	 */
	public void cancelCollect(){
		String goodsidsStr = getPara("GOODSID");
		if(StrKit.isBlank(goodsidsStr)){
    		renderFailed("参数GOODSID不能为空");
    		return;
		}
		String[] goodsids;
		if(goodsidsStr.indexOf(",")>=1){
			goodsids = goodsidsStr.split(",");
		}else{
			goodsids = new String[]{goodsidsStr};
		}
		//批量取消收藏
		service.cancelCollect(getCurrentUser().getInt("id"),goodsids);
		renderSuccess();
	}
	
	/**
	 * 分页查询用户收藏商品列表
	 * @param STOREID 店铺ID
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 */
	public void collectionList(){
		String storeidStr = getPara("STOREID");
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		Page<Record> page = service.findCollectionByPage(Integer.valueOf(storeidStr),getCurrentUser().getInt("id"),pageNum, pageSize);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
	}
	
	/**
	 * 查询用户收货地址列表
	 */
	public void addressList(){
		renderSuccess(service.findAddress(getCurrentUser().getInt("id")));
	}
	
	/**
	 * 增加收货地址
	 * @param LINKNAME 收货人姓名
	 * @param SEX 性别
	 * @param TEL 联系电话，座机或手机号
	 * @param COMMUNITYID 社区ID
	 * @param #BUILDID 楼号ID
	 * @param #UNITID 单元号ID
	 * @param #ROOMID 房间门牌号ID
	 * @param ADDRESS 详细地址
	 * @param ISDEFAULT 是否设置为默认收货地址，1：是，0：否
	 */
	public void addAddress(){
		String linkName = getPara("LINKNAME");
		String sexStr = getPara("SEX");
		String tel = getPara("TEL");
		String storeidStr = getPara("STOREID");
		String addressStr = getPara("ADDRESS");
		String isDefaultStr = getPara("ISDEFAULT","0");
		if(StrKit.isBlank(linkName)){
			renderFailed("参数LINKNAME不能为空");
			return;
		}else if(linkName.length()>20){
			renderFailed("参数LINKNAME过长");
			return;
		}else if(StrKit.isBlank(sexStr)){
			renderFailed("参数SEX不能为空");
			return;
		}else if(!sexStr.equals("1")&&!sexStr.equals("2")){
			renderFailed("参数SEX错误");
			return;
		}else if(StrKit.isBlank(tel)){
			renderFailed("参数TEL不能为空");
			return;
		}else if(!ValidateKit.Tel(tel)&&!ValidateKit.Mobile(tel)){
			renderFailed("参数TEL格式错误");
			return;
		}else if(StrKit.isBlank(storeidStr)){
			renderFailed("参数STOREID不能为空");
			return;
		}else if(StrKit.isBlank(addressStr)){
			renderFailed("参数ADDRESS不能为空");
			return;
		}else if(!StrKit.isBlank(isDefaultStr)&&!isDefaultStr.equals("1")&&!isDefaultStr.equals("0")){
			renderFailed("参数ISDEFAULT错误");
			return;
		}
		int userid = getCurrentUser().getInt("id");
		UserAddress address = new UserAddress()
				.set("userid", userid)
				.set("linkName", linkName)
				.set("sex", sexStr)
				.set("tel", tel)
				.set("storeid",storeidStr)
				.set("address",addressStr);
		if(service.addAddress(userid,address,Integer.valueOf(isDefaultStr))){
			renderSuccess(address);
		}else{
			renderFailed("添加收货地址失败");
		}
	}
	
	/**
	 * 删除收货地址
	 * @param ADDRESSID 收货地址ID
	 */
	public void delAddress(){
		String addressidStr = getPara("ADDRESSID");
		if(StrKit.isBlank(addressidStr)){
    		renderFailed("参数ADDRESSID不能为空");
    		return;
		}
		String[] addressids;
		if(addressidStr.indexOf(",")>=1){
			addressids = addressidStr.split(",");
		}else{
			addressids = new String[]{addressidStr};
		}
		//批量删除收货地址
		service.delAddress(getCurrentUser().getInt("id"),addressids);
		renderSuccess();
	}
	
	/**
	 * 修改收货地址
	 * @param ADDRESSID 收货地址ID
	 * @param LINKNAME 收货人姓名
	 * @param SEX 性别
	 * @param TEL 联系电话，座机或手机号
	 * @param COMMUNITYID 社区ID
	 * @param #BUILDID 楼号ID
	 * @param #UNITID 单元号ID
	 * @param #ROOMID 房间门牌号ID
	 * @param ADDRESS 详细地址
	 * @param ISDEFAULT 是否设置为默认收货地址，1：是，0：否
	 */
	public void updateAddress(){
		String addressidStr = getPara("ADDRESSID");
		String linkName = getPara("LINKNAME");
		String sexStr = getPara("SEX");
		String tel = getPara("TEL");
		String storeidStr = getPara("STOREID");
		String addressStr = getPara("ADDRESS");
		String isDefaultStr = getPara("ISDEFAULT","0");
		if(StrKit.isBlank(addressidStr)){
			renderFailed("参数ADDRESSID不能为空");
			return;
		}else if(StrKit.isBlank(linkName)){
			renderFailed("参数LINKNAME不能为空");
			return;
		}else if(linkName.length()>20){
			renderFailed("参数LINKNAME过长");
			return;
		}else if(StrKit.isBlank(sexStr)){
			renderFailed("参数SEX不能为空");
			return;
		}else if(!sexStr.equals("1")&&!sexStr.equals("2")){
			renderFailed("参数SEX错误");
			return;
		}else if(StrKit.isBlank(tel)){
			renderFailed("参数TEL不能为空");
			return;
		}else if(!ValidateKit.Tel(tel)&&!ValidateKit.Mobile(tel)){
			renderFailed("参数TEL格式错误");
			return;
		}else if(StrKit.isBlank(storeidStr)){
			renderFailed("参数STOREID不能为空");
			return;
		}else if(StrKit.isBlank(addressStr)){
			renderFailed("参数ADDRESS不能为空");
			return;
		}else if(!StrKit.isBlank(isDefaultStr)&&!isDefaultStr.equals("1")&&!isDefaultStr.equals("0")){
			renderFailed("参数ISDEFAULT错误");
			return;
		}
		int userid = getCurrentUser().getInt("id");
		UserAddress address = new UserAddress()
				.set("id", Integer.valueOf(addressidStr))
				.set("userid", userid)
				.set("linkName", linkName)
				.set("sex", sexStr)
				.set("tel", tel)
				.set("storeid",storeidStr)
				.set("address",addressStr);
		if(service.updateAddress(userid,address,Integer.valueOf(isDefaultStr))){
			renderSuccess(address);
		}else{
			renderFailed("更新收货地址失败");
		}
	}
	
	/**
	 * 查询指定ID的收货地址详情
	 * @param ADDRESSID 收货地址ID
	 */
	public void addressDetail(){
		String addressidStr = getPara("ADDRESSID");
		if(StrKit.isBlank(addressidStr)){
    		renderFailed("参数ADDRESSID不能为空");
    		return;
		}
		UserAddress address = service.findAddressById(Integer.valueOf(addressidStr));
		if(address!=null){
			renderSuccess(address);
		}else{
			renderFailed("没有收货地址");
		}
	}
	
	/**
	 * 查询当前登录用户的默认收货地址
	 */
	public void defaultAddress(){
		UserAddress address = service.findDefaultAddressByUser(getCurrentUser().getInt("id"));
		if(address!=null){
			renderSuccess(address);
		}else{
			renderFailed("没有收货地址");
		}
	}
	
	/**
	 * 查询用户服务订单列表
	 */
	public void serviceOrderList(){
		String storeidStr = getPara("STOREID");
		if(StrKit.isBlank(storeidStr)){
    		renderFailed("参数STOREID不能为空");
    		return;
		}
		int flag = getParaToInt("FLAG",0);
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		Page<Record> page = service.findServiceOrderByPage(Integer.valueOf(storeidStr).intValue(),getCurrentUser().getInt("id"),flag,pageNum,pageSize);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
		return;
	}
	
	/**
	 * 查询用户实惠币记录
	 */
	public void expenseRecord(){
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		int searchType = getParaToInt("SEARCHTYPE",0);
		Page<Record> page = service.findUserExpenseRecord(pageNum, pageSize, getCurrentUser().getInt("id"),searchType);
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
		return;
	}
}
