<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.web.common.SerOrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>订单管理</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">全部订单</h1>
			</div>
		</div>
		<!-- 搜索框 -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-9">
				<label class="search-label">订单编号：</label><input type="text" id="sOrderNum" value="" class="form-control search-input width200" maxlength="21">
				<label class="search-label">下单时间：</label><input type="text" id="sBeginDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<label class="search-label">至</label><input type="text" id="sEndDate" value="" class="form-control search-input width120 datetimepicker" data-format="yyyy-mm-dd" maxlength="10">
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-offset-3 col-lg-9">
				<label class="search-label">买家地址：</label><input type="text" id="sAddress" value="" class="form-control search-input width200" maxlength="100">
				<label class="search-label">买家电话：</label><input type="text" id="sTel" value="" class="form-control search-input width120" maxlength="13">
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-offset-3 col-lg-9 m-t-10">
				<label class="search-label">订单状态：</label>
				<div class="search-input">
					<select id="sState" class="selectpicker" data-width="150">
						<option value="0">全部</option>
						<option value="1">待付款</option>
						<option value="2">待接单</option>
						<option value="3">待派单/已拒单</option>
						<option value="4">待商家接单</option>
						<option value="5">待确认完成</option>
						<option value="6">已确认</option>
						<option value="7">无效</option>
					</select>
				</div>
				<label class="search-label">支付方式：</label>
				<div class="search-input">
					<select id="sPayType" class="selectpicker" data-width="150">
						<option value="0">全部</option>
						<option value="1">在线支付</option>
						<option value="2">服务后付款</option>
					</select>
				</div>
				<button class="btn btn-success search-btn" onclick="query();">确定</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6">
						<div class="text-muted single-line-text pull-left">共 <font color="#428bca" id="dataCount">0</font> 条记录</div>
					</div>
				</div>
				<div id="dataList" class="orderPage"></div>
		        <div class="row">
		        	<div class="col-lg-12">
		        		<ul id="dataPagination" class="pagination-sm pull-right"></ul>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	{{each list as item}}
	<div class="panel panel-default" id="item{{item.orderNum}}" data-id="{{item.orderNum}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-2"><label>订单号：</label>{{item.orderNum}}</div>
				<div class="col-lg-2"><label>下单时间：</label>{{item.startDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>
				{{if item.payState==<%=SerOrderCode.OrderPayState.HAD_PAY%>}}<div class="col-lg-2"><label>付款时间：</label>{{item.payDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
				{{if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}<div class="col-lg-2"><label>派单时间：</label>{{item.storeDeliverDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
				{{if item.deliverState==<%=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT%>}}<div class="col-lg-2"><label>商家接单：</label>{{item.proSerDeliverDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
				{{if item.orderState==2}}<div class="col-lg-2"><label>完成时间：</label>{{item.signDate | dateFormat:'yyyy-MM-dd hh:mm:ss'}}</div>{{/if}}
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="10%">服务商家</th>
					<th width="14%">服务项</th>
					<th width="10%">实收款（元）</th>
					<th width="10%">买家</th>
					<th width="15%">买家地址</th>
					<th width="10%">订单状态</th>
					<th width="15%">备注</th>
					<th width="6%">操作</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
					{{if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}
						<a href="javascript:void(0)" onclick="showShopDetail({{item.proSerid}})">{{item.proSerName}}</a>
					{{/if}}
					</td>
					<td>{{item.title}}</td>
					<td>
						￥ {{item.amount}}<br/>
						{{item.payType | flagTransform:1,'在线支付',2,'服务后付款'}}
					</td>
					<td>
						{{item.linkName}} {{item.sex}}<br/>
						{{item.tel}}
					</td>
					<td>
						{{item.address}}
					</td>
					{{if item.orderState==<%=SerOrderCode.OrderState.CANCEL%>}}
					<!-- 状态1：订单已取消 -->
					<td>已取消</td>
					{{else if item.orderState==<%=SerOrderCode.OrderState.FINISH%>}}
					<!-- 状态2：订单已完成 -->
					<td>已完成</td>
					{{else if item.orderState==<%=SerOrderCode.OrderState.WASTE%>}}
					<!-- 状态3：无效 -->
					<td>无效</td>
					{{else if item.payType==<%=SerOrderCode.OrderPayType.ON_LINE%>}}
					<!-- 状态4：在线支付 -->
						{{if item.payState==<%=SerOrderCode.OrderPayState.NO_PAY%>}}
						<!-- 状态4.1：待付款 -->
						<td>待付款</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT%>}}
						<!-- 状态4.2：待接单 -->
						<td>待接单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT%>}}
						<!-- 状态4.3：待派单 -->
						<td>待派单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}
						<!-- 状态4.4：待商家接单 -->
						<td>待商家接单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT%>}}
						<!-- 状态4.5：商家已接单 -->
						<td>待用户确认</td>
						{{/if}}
					{{else if item.payType==<%=SerOrderCode.OrderPayType.AFTER_SERVICE%>}}
					<!-- 状态5：服务后付款 -->
						{{if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT%>}}
						<!-- 状态5.1：待接单 -->
						<td>待接单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT%>}}
						<!-- 状态5.2：待派单 -->
						<td>待派单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}
						<!-- 状态5.3：待商家接单 -->
						<td>待商家接单</td>
						{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT%>}}
						<!-- 状态5.4：商家已接单 -->
						<td>待用户确认</td>
						{{/if}}
					{{/if}}
					<td>
						{{item.describe}}
					</td>
					<td>
						<button type="button" class="btn btn-sm btn-default" onclick="showOrder('{{item.orderNum}}')">查看订单</button><br/>
						{{if item.orderState==<%=SerOrderCode.OrderState.CANCEL%>}}
						<!-- 状态1：订单已取消 -->
						{{else if item.orderState==<%=SerOrderCode.OrderState.FINISH%>}}
						<!-- 状态2：订单已完成 -->
						{{else if item.orderState==<%=SerOrderCode.OrderState.WASTE%>}}
						<!-- 状态3：无效 -->
						{{else if item.payType==<%=SerOrderCode.OrderPayType.ON_LINE%>}}
						<!-- 状态4：在线支付 -->
							{{if item.payState==<%=SerOrderCode.OrderPayState.NO_PAY%>}}
							<!-- 状态4.1：待付款 -->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="wastOrder('{{item.orderNum}}')">设为无效</button>
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT%>}}
							<!-- 状态4.2：待接单 -->
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="acceptOrder('{{item.orderNum}}')">确认接单</button>
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT%>}}
							<!-- 状态4.3：待派单 -->
							<button type="button" class="btn btn-sm btn-warning m-t-5" onclick="toDeliver('{{item.orderNum}}')">立即派单</button>
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}
							<!-- 状态4.4：待商家接单 -->
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT%>}}
							<!-- 状态4.5：商家已接单 -->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="confirmOrder('{{item.orderNum}}')">用户确认</button>
							{{/if}}
						{{else if item.payType==<%=SerOrderCode.OrderPayType.AFTER_SERVICE%>}}
						<!-- 状态5：服务后付款 -->
							{{if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT%>}}
							<!-- 状态5.1：待接单 -->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="wastOrder('{{item.orderNum}}')">设为无效</button><br/>
							<button type="button" class="btn btn-sm btn-success m-t-5" onclick="acceptOrder('{{item.orderNum}}')">确认接单</button>
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT%>}}
							<!-- 状态5.2：待派单 -->
							<button type="button" class="btn btn-sm btn-warning m-t-5" onclick="toDeliver('{{item.orderNum}}')">立即派单</button>
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH%>}}
							<!-- 状态5.3：待商家接单 -->
							{{else if item.deliverState==<%=SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT%>}}
							<!-- 状态5.4：商家已接单 -->
							<button type="button" class="btn btn-sm btn-danger m-t-5" onclick="confirmOrder('{{item.orderNum}}')">用户确认</button>
							{{/if}}
						{{/if}}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	{{/each}}
</script>
<script type="text/javascript">
var dataPaginator;
$(query);
function query(){
	var params = {
		pageSize:10,
		orderNum:$("#sOrderNum").val(),
		beginDate:$("#sBeginDate").val(),
		endDate:$("#sEndDate").val(),
		address:$("#sAddress").val(),
		tel:$("#sTel").val(),
		state:$("#sState").val(),
		payType:$("#sPayType").val()
	};
	if(dataPaginator){
		dataPaginator.destroy();
	}
	dataPaginator = Kit.pagination("#dataPagination","${BASE_PATH}/ser/order/page",params,function(result){
		//设置显示最新的数据数量
		$("#dataCount").html(result.object.totalRow);
		//根据模板渲染数据并填充
		$("#dataList").empty().append(template("dataTpl",result.object));
	});
}
var shopDetailDialog;
function showShopDetail(shopid){
	shopDetailDialog = Kit.dialog("商家信息","${BASE_PATH}/ser/shop/detail/"+shopid,{closable:true}).open();
}
var showOrderDialog;
function showOrder(orderNum){
	showOrderDialog = Kit.dialog("查看订单","${BASE_PATH}/ser/order/detail?orderNum="+orderNum,{size:'size-wide',closable:true}).open();
}

function wastOrder(orderNum){
	Kit.confirm("提示","确定要设订单为无效吗？<input type='text' id='wastReason' class='form-control m-t-5' placeholder='请输入设为无效的理由'>",function(ref){
		var reason = $("#wastReason").val();
		$.post("${BASE_PATH}/ser/order/doWast",{'orderNum':orderNum,"reason":reason},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
function acceptOrder(orderNum){
	$.post("${BASE_PATH}/ser/order/doStroeAccept",{'orderNum':orderNum},function(result){
		if(result.flag==0){
			$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
		}else{
			Kit.alert("操作失败");return;
		}
    });
}
var deliverDialog;
function toDeliver(orderNum){
	deliverDialog = Kit.dialog("派单","${BASE_PATH}/ser/order/toDeliver?orderNum="+orderNum,{size:'size-wide',closable:true}).open();
}
function onDeliverSuccess(order){
	$("#item"+order.orderNum).replaceWith(template("dataTpl",{"list":[order]}));
	deliverDialog.close();
}
function confirmOrder(orderNum){
	Kit.confirm("提示",(payType==1?"确定已完成服务吗？":"确定已收款并完成服务吗？"),function(ref){
		$.post("${BASE_PATH}/order/doConfirm",{'orderNum':orderNum},function(result){
			if(result.flag==0){
				$("#item"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
			}else{
				Kit.alert("操作失败");return;
			}
			
		});
	});
}
</script>
</body>
</html>