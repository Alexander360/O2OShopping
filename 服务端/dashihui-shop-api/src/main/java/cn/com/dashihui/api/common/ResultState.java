package cn.com.dashihui.api.common;

public class ResultState {
	public static final String STATE_SUCCESS = "0";
	public static final String STATE_FAILED = "-1";
	
	/** 公共错误代码 */
	//没有携带手机终端注册TOKEN
	public static final String STATE_CLIENT_TOKEN_EXCEPTED = "901";
	//手机终端注册TOKEN错误
	public static final String STATE_CLIENT_TOKEN_ERROR = "902";
	//没有携带用户登录TOKEN，即没有登录
	public static final String STATE_USER_LOGIN_TOKEN_EXCEPTED = "903";
	//用户登录TOKEN错误
	public static final String STATE_USER_LOGIN_TOKEN_ERROR = "904";
	
	/**业务错误码*/
	//终端注册失败
	public static final String STATE_CLIENT_REGIST_FAILED = "101";
	//注册时判断用户名已经注册
	public static final String STATE_USERNAME_EXISTS = "102";
}
