//
//  MyGoodsOrderDetailHeadCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/16.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#define kLeftSpace 10
#define kRightSpace 10
#import "MyGoodsOrderDetailHeadCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
@interface MyGoodsOrderDetailHeadCell()
@property (nonatomic,strong)UILabel *orderNum;
@property (nonatomic,strong)UILabel *orderState;
@end

@implementation MyGoodsOrderDetailHeadCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //初始化视图
        [self setSubView];
    }
    return self;
}
-(void)setSubViewDateWithOredrNum:(NSString *)oredrNum OrderState:(NSString *)orderState{
    self.orderNum.text =[NSString stringWithFormat:@"订单号：%@",oredrNum];
    self.orderNum.frame =CGRectMake(kLeftSpace ,0, [self setLabelWidthWithNSString:self.orderNum.text Font:FontSize(16)].width, HightScalar(46));
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:orderState];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#0094dd"] range:NSMakeRange(0,str.length-1)];
        self.orderState.attributedText = str;
    self.orderState.frame =CGRectMake(VIEW_WIDTH -[self setLabelWidthWithNSString:self.orderState.text Font:FontSize(16)].width-kRightSpace, CGRectGetMinY(self.orderNum.frame), [self setLabelWidthWithNSString:self.orderState.text Font:FontSize(16)].width, CGRectGetHeight(self.orderNum.bounds));

}
#pragma mark --初始化控件
-(void)setSubView
{
    self.orderNum =[[UILabel alloc]initWithFrame:CGRectZero];
    self.orderNum.font =[UIFont systemFontOfSize:FontSize(16)];
    self.orderNum.textColor =[UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:self.orderNum];
    
    
    self.orderState =[[UILabel alloc]initWithFrame:CGRectZero];
    self.orderState.font =[UIFont systemFontOfSize:FontSize(16)];
    self.orderState.textColor =[UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:self.orderState];
    
}
-(CGSize)setLabelWidthWithNSString:(NSString*)lableText Font:(CGFloat)font
{
    CGSize maxSize = CGSizeMake(VIEW_WIDTH, HightScalar(46));
    CGSize curSize = [lableText boundingRectWithSize:maxSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                          attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:font] }
                                             context:nil].size;
    return curSize;
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
