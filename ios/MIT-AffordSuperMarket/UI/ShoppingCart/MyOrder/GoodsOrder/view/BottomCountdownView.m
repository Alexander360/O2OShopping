//
//  BottomCountdownView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
// label数量
#define labelCount 2
#define separateLabelCount 0
#define padding 5
#import "BottomCountdownView.h"
#import "UIColor+Hex.h"
#import "Global.h"
@interface BottomCountdownView (){
    // 定时器
    NSTimer *timer;
}
@property (nonatomic,strong)NSMutableArray *timeLabelArrM;
@property (nonatomic,strong)NSMutableArray *separateLabelArrM;
//// day
//@property (nonatomic,strong)UILabel *dayLabel;
//// hour
//@property (nonatomic,strong)UILabel *hourLabel;
// minues
@property (nonatomic,strong)UILabel *minuesLabel;
// seconds
@property (nonatomic,strong)UILabel *secondsLabel;

@end
@implementation BottomCountdownView
// 创建单例
+ (instancetype)shareCountDown{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[BottomCountdownView alloc] init];
    });
    return instance;
}
+ (instancetype)countDown{
    return [[self alloc] init];
}
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        //        [self addSubview:self.dayLabel];
        //        [self addSubview:self.hourLabel];
        [self addSubview:self.minuesLabel];
        [self addSubview:self.secondsLabel];
        
        for (NSInteger index = 0; index < separateLabelCount; index ++) {
            UILabel *separateLabel = [[UILabel alloc] init];
            separateLabel.text = @":";
            separateLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:separateLabel];
            [self.separateLabelArrM addObject:separateLabel];
        }
    }
    return self;
}

- (void)setBackgroundImageName:(NSString *)backgroundImageName{
    _backgroundImageName = backgroundImageName;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:backgroundImageName]];
    imageView.frame = self.bounds;
    [self addSubview:imageView];
    //    [self bringSubviewToFront:imageView];
}

// 拿到外界传来的时间戳
- (void)setTimestamp:(NSInteger)timestamp{
    _timestamp = timestamp;
   
}
-(void)timesBeginning
{
    if (_timestamp != 0) {
        timer =[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timer:) userInfo:nil repeats:YES];
    }
}
-(void)timesStop
{
    [timer invalidate];
    timer =  nil;
    self.minuesLabel.text =@"";
    self.secondsLabel.text =@"";
}
-(void)timer:(NSTimer*)timerr{
    _timestamp--;
    [self getDetailTimeWithTimestamp:_timestamp];
    if (_timestamp == 0) {
        [timer invalidate];
        timer = nil;
        // 执行block回调
//        self.timerStopBlock();
    }
}

- (void)getDetailTimeWithTimestamp:(NSInteger)timestamp{
    NSInteger ms = timestamp;
    NSInteger ss = 1;
    NSInteger mi = ss * 60;
    NSInteger hh = mi * 60;
    NSInteger dd = hh * 24;
    
    // 剩余的
    NSInteger day = ms / dd;// 天
    NSInteger hour = (ms - day * dd) / hh;// 时
    NSInteger minute = (ms - day * dd - hour * hh) / mi;// 分
    NSInteger second = (ms - day * dd - hour * hh - minute * mi) / ss;// 秒
    //    NSLog(@"%zd日:%zd时:%zd分:%zd秒",day,hour,minute,second);
    
    //    self.dayLabel.text = [NSString stringWithFormat:@"%zd天",day];
    //    self.hourLabel.text = [NSString stringWithFormat:@"%zd时",hour];
    ;
    NSMutableAttributedString *minutes = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%zd分钟",minute]];
    [minutes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(18)] range:NSMakeRange(0, minutes.length-2)];
    self.minuesLabel.attributedText =minutes;
    
    NSMutableAttributedString *seconds = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%zd秒",second]];
    [seconds addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(18)] range:NSMakeRange(0, seconds.length-1)];
    self.secondsLabel.attributedText =seconds;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 获得view的宽、高
    CGFloat viewW = self.frame.size.width;
    CGFloat viewH = self.frame.size.height;
    // 单个label的宽高
    CGFloat labelW = viewW / labelCount;
    CGFloat labelH = viewH;
        self.minuesLabel.frame = CGRectMake(0, 0, labelW, labelH);
        self.secondsLabel.frame = CGRectMake(labelW, 0, labelW, labelH);
  
    //    self.dayLabel.frame = CGRectMake(0, 0, labelW, labelH);
    //    self.hourLabel.frame = CGRectMake(labelW, 0, labelW, labelH);
//    self.minuesLabel.frame = CGRectMake(2 * labelW , 0, labelW, labelH);
//    self.secondsLabel.frame = CGRectMake(3 * labelW, 0, labelW, labelH);
    
//    for (NSInteger index = 0; index < self.separateLabelArrM.count ; index ++) {
//        UILabel *separateLabel = self.separateLabelArrM[index];
//        separateLabel.frame = CGRectMake((labelW - 1) * (index + 1), 0, 5, labelH);
//    }
}


#pragma mark - setter & getter

- (NSMutableArray *)timeLabelArrM{
    if (_timeLabelArrM == nil) {
        _timeLabelArrM = [[NSMutableArray alloc] init];
    }
    return _timeLabelArrM;
}

- (NSMutableArray *)separateLabelArrM{
    if (_separateLabelArrM == nil) {
        _separateLabelArrM = [[NSMutableArray alloc] init];
    }
    return _separateLabelArrM;
}

//- (UILabel *)dayLabel{
//    if (_dayLabel == nil) {
//        _dayLabel = [[UILabel alloc] init];
//        _dayLabel.textAlignment = NSTextAlignmentCenter;
////        _dayLabel.backgroundColor = [UIColor grayColor];
//    }
//    return _dayLabel;
//}
//
//- (UILabel *)hourLabel{
//    if (_hourLabel == nil) {
//        _hourLabel = [[UILabel alloc] init];
//        _hourLabel.textAlignment = NSTextAlignmentCenter;
////        _hourLabel.backgroundColor = [UIColor redColor];
//    }
//    return _hourLabel;
//}

- (UILabel *)minuesLabel{
    if (_minuesLabel == nil) {
        _minuesLabel = [[UILabel alloc] init];
        _minuesLabel.textAlignment = NSTextAlignmentCenter;
        _minuesLabel.textColor =[UIColor colorWithHexString:@"#000000"];
        _minuesLabel.font =[UIFont systemFontOfSize:FontSize(14)];
        //        _minuesLabel.backgroundColor = [UIColor orangeColor];
    }
    return _minuesLabel;
}

- (UILabel *)secondsLabel{
    if (_secondsLabel == nil) {
        _secondsLabel = [[UILabel alloc] init];
        _secondsLabel.textAlignment = NSTextAlignmentLeft;
        //        _secondsLabel.backgroundColor = [UIColor yellowColor];
        _secondsLabel.textColor =[UIColor colorWithHexString:@"#000000"];
        _secondsLabel.font =[UIFont systemFontOfSize:FontSize(14)];
    }
    return _secondsLabel;
}



@end
