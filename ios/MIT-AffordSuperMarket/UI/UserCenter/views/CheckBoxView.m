//
//  CheckBoxView.m
//  BooksListTest
//
//  Created by apple on 15/11/12.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "CheckBoxView.h"
#define kcheckBoxItemWidth 70
#define kleftSpace 15
#define ksepLineHight 1
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
@interface CheckBoxView () {

}
@property(nonatomic, strong)NSMutableArray *checkBoxviewsArr;
@end;
@implementation CheckBoxView

- (instancetype)initWithFrame:(CGRect)frame withTitles:(NSArray*)titles withCheckBoxType:(CheckBoxType)boxType normalImage:(UIImage*)normalImage slectedImage:(UIImage*)selectedImage {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        _checkBoxviewsArr = [[NSMutableArray alloc]init];
        [self setupSubViewWithTitle:titles withCheckBoxType:boxType normalImage:normalImage slectedImage:selectedImage];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame withiTitles:(NSArray*)titles normalImage:(UIImage*)normalImage slectedImage:(UIImage*)selectedImage {
    return [self initWithFrame:frame withTitles:titles withCheckBoxType:kleftImageAndRightTitle normalImage:normalImage slectedImage:selectedImage];
}
//初始化视图
- (void)setupSubViewWithTitle:(NSArray*)titles  withCheckBoxType:(CheckBoxType)boxType normalImage:(UIImage*)normalImage slectedImage:(UIImage*)selectedImage {
    if (boxType == kleftImageAndRightTitle) {
        CGFloat vSpace = (CGRectGetWidth(self.bounds) - titles.count * kcheckBoxItemWidth)/(titles.count +1);
        for (NSInteger i = 0; i < [titles count]; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(vSpace*(i+1) + kcheckBoxItemWidth*i
                                      , 0
                                      , kcheckBoxItemWidth
                                      , CGRectGetHeight(self.bounds));
            [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
            [button setImage:normalImage forState:UIControlStateNormal];
            button.tag = i+ 100;
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [button setImage:selectedImage forState:UIControlStateSelected];
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            button.titleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
            [button addTarget:self action:@selector(beatCheckBox:) forControlEvents:UIControlEventTouchUpInside];
            if (i == 0) {
                button.selected = YES;
            }
            [self addSubview:button];
            [self.checkBoxviewsArr addObject:button];
        }
    } else if (boxType == kleftTitleAndRightImage) {
        CGFloat hight = (CGRectGetHeight(self.bounds) - (titles.count - 1)*ksepLineHight)/titles.count;
        for (NSInteger j = 0; j < [titles count]; j++) {
            //标题
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                          , (hight + ksepLineHight)*j
                                                                          , CGRectGetWidth(self.bounds)
                                                                           , hight)];
            titleLabel.text = [titles objectAtIndex:j];
            titleLabel.font = [UIFont systemFontOfSize:15];
            titleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            [self addSubview:titleLabel];
            //分割线
            if (j != 0 || j != titles.count ) {
                UIImage *sepLine = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"]
                                                           frame:CGRectMake(0
                                                                            , 0
                                                                            , CGRectGetWidth(self.bounds) - kleftSpace
                                                                            , 0.6)];
                UIImageView *sepLineView = [[UIImageView alloc]initWithImage:sepLine];
                sepLineView.frame = CGRectMake(kleftSpace
                                               , CGRectGetMaxY(titleLabel.frame)
                                               , CGRectGetWidth(self.bounds) - kleftSpace
                                               , 0.6);
                [self addSubview:sepLineView];

            }
           //按钮
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(CGRectGetWidth(self.bounds) - hight
                                      , CGRectGetMinY(titleLabel.frame)
                                      , hight
                                      , hight);
            [button setImage:normalImage forState:UIControlStateNormal];
            [button setImage:selectedImage forState:UIControlStateSelected];
            button.tag = j + 100;
            [button addTarget:self action:@selector(beatCheckBox:) forControlEvents:UIControlEventTouchUpInside];
            if (j == 0) {
                button.selected = YES;
            }
            [self addSubview:button];
            [self.checkBoxviewsArr addObject:button];
            
            
        }
    }
   
}
- (void)beatCheckBox:(id)sender {
    UIButton *button = (UIButton*)sender;
    self.selectedItemIndex = button.tag - 100;
}

- (void)setSelectedItemIndex:(NSInteger)selectedItemIndex {
    if (_selectedItemIndex == selectedItemIndex) {
        return ;
    }
    UIButton *currentSelectedItem = (UIButton*)[self.checkBoxviewsArr objectAtIndex:selectedItemIndex];
    for (UIButton *btn in self.checkBoxviewsArr) {
        if ([btn isEqual:currentSelectedItem]) {
            btn.selected = YES;
        } else {
            btn.selected = NO;
        }
    }
    _selectedItemIndex = selectedItemIndex;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemWithIndex:withCheckBoxView:)]) {
        [_delegate selectedItemWithIndex:self.selectedItemIndex withCheckBoxView:self];
    }
}
@end
