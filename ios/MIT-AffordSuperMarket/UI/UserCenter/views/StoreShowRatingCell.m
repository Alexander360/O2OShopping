//
//  StoreShowRatingCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/19.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreShowRatingCell.h"
//untils
#import "NSString+TextSize.h"
#import "Global.h"
#import "UIColor+Hex.h"
//vendor
#import "LHRatingView.h"
#define kleftSpace 15
#define ktopSpace 15
#define khSpace 15
#define kvSpace 6
@interface StoreShowRatingCell()<ratingViewDelegate>
@property (nonatomic,strong)LHRatingView * goodsRatingView;//商品满意度
@property (nonatomic,strong)LHRatingView * peiSongRatingView;//配送速度满意度
@property (nonatomic,strong)LHRatingView * serviceRatingView;//服务质量满意度
@end
@implementation StoreShowRatingCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubView];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubView {
    NSString *goodsRatingTitle = @"商品满意度：";
    NSString *peiSongRatingTitle = @"配送速度满意度：";
    NSString *serviceRatingTitle = @"服务质量满意度：";
    CGSize titleSize = [serviceRatingTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(160, 30)];
    //商品满意度
    UILabel *goodsRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                              , ktopSpace
                                                                              , titleSize.width
                                                                              , 30)];
    goodsRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    goodsRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    goodsRatingTitleLabel.text = goodsRatingTitle;
    [self.contentView addSubview:goodsRatingTitleLabel];
    
    _goodsRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(goodsRatingTitleLabel.frame) + khSpace
                                                                     , CGRectGetMinY(goodsRatingTitleLabel.frame)
                                                                     , HightScalar(162)
                                                                     , CGRectGetHeight(goodsRatingTitleLabel.bounds))
                                                 starType:@"1"];
    _goodsRatingView.delegate = self;
    _goodsRatingView.userInteractionEnabled = NO;
    _goodsRatingView.ratingType = INTEGER_TYPE;
    [self.contentView addSubview:_goodsRatingView];
    
    //配送速度满意度
    UILabel *peiSongRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(goodsRatingTitleLabel.frame)
                                                                                , CGRectGetMaxY(goodsRatingTitleLabel.frame) + khSpace
                                                                                , CGRectGetWidth(goodsRatingTitleLabel.bounds)
                                                                                , CGRectGetHeight(goodsRatingTitleLabel.bounds))];
    peiSongRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    peiSongRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    peiSongRatingTitleLabel.text = peiSongRatingTitle;
    [self.contentView addSubview:peiSongRatingTitleLabel];
    
    _peiSongRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_goodsRatingView.frame)
                                                                       , CGRectGetMinY(peiSongRatingTitleLabel.frame)
                                                                       , CGRectGetWidth(_goodsRatingView.bounds)
                                                                       , CGRectGetHeight(_goodsRatingView.bounds))
                                                   starType:@"1"];
    _peiSongRatingView.delegate = self;
    _peiSongRatingView.userInteractionEnabled = NO;
    _peiSongRatingView.ratingType = INTEGER_TYPE;
    [self.contentView addSubview:_peiSongRatingView];
    
    //服务质量满意度
    UILabel *serviceRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(peiSongRatingTitleLabel.frame)
                                                                                , CGRectGetMaxY(peiSongRatingTitleLabel.frame) + khSpace
                                                                                , CGRectGetWidth(peiSongRatingTitleLabel.bounds)
                                                                                , CGRectGetHeight(peiSongRatingTitleLabel.bounds))];
    serviceRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    serviceRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    serviceRatingTitleLabel.text = serviceRatingTitle;
    [self.contentView addSubview:serviceRatingTitleLabel];
    
    _serviceRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_peiSongRatingView.frame)
                                                                       , CGRectGetMinY(serviceRatingTitleLabel.frame)
                                                                       , CGRectGetWidth(_peiSongRatingView.bounds)
                                                                       , CGRectGetHeight(_peiSongRatingView.bounds))
                                                   starType:@"1"];
    _serviceRatingView.delegate = self;
    _serviceRatingView.userInteractionEnabled = NO;
    _serviceRatingView.ratingType = INTEGER_TYPE;
    [self.contentView addSubview:_serviceRatingView];
}

- (void)updateViewWithData:(NSDictionary*)dataDic {
    _goodsRatingView.score = [[dataDic objectForKey:@"EVAL1"] floatValue];
    _peiSongRatingView.score = [[dataDic objectForKey:@"EVAL2"] floatValue];
    _serviceRatingView.score = [[dataDic objectForKey:@"EVAL3"] floatValue];
}
- (CGFloat)cellFactHight {
    return 30*3 + 2*ktopSpace + 2*khSpace;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
