<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		rules:{
			phonenumber: {
				isMobile:true
			}
		},
		messages:{
			trueName: {required: "请输入真实姓名"},
			phonenumber: {required: "请输入手机号码",isMobile: "请输入正确的手机号码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/auth/admin/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="${object.username}" class="form-control" disabled="disabled">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">真实姓名</label>
	    <div class="col-lg-9">
        	<input type="text" name="trueName" value="${object.trueName}" class="form-control" placeholder="请输入真实姓名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">手机号码</label>
	    <div class="col-lg-9">
        	<input type="text" name="phonenumber" value="${object.phonenumber}" class="form-control" placeholder="请输入手机号码" required maxlength="11">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">状态</label>
	    <div class="col-lg-9">
	    	<select name="enabled" class="selectpicker form-control">
				<option value="1" <c:if test="${object.enabled==1}">data-dropdown-selected="true"</c:if>>可用</option>
				<option value="0" <c:if test="${object.enabled==0}">selected</c:if>>禁用</option>
	        </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">备注</label>
	    <div class="col-lg-9">
        	<textarea name="remark" class="form-control" maxlength="20" rows="3">${object.remark}</textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="userid" value="${object.id}"/>
</form>