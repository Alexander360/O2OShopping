package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;


public class Category extends  Model<Category>{
	private static final long serialVersionUID = 1L;
	private static Category me = new Category();
	public static Category me(){
		return me;
	}
	
	//子级分类
	private boolean hasChildren = false;
	public List<Category> getChildren() {
		return get("children");
	}
	public void setChildren(List<Category> children) {
		put("children", children);
		hasChildren = true;
	}
	public boolean hasChildren(){
		return hasChildren;
	}
	public void addChild(Category child){
		List<Category> children = getChildren();
		if(children==null){
			children = new ArrayList<Category>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
	
	//子级店铺商品，只有四级分类才有子级店铺商品数据
	private boolean hasGoods = false;
	public List<Goods> getGoodsList() {
		return get("goodsList");
	}
	public void setGoodsList(List<Goods> goodsList) {
		put("goodsList", goodsList);
		hasGoods = true;
	}
	public boolean hasGoodsList(){
		return hasGoods;
	}
	public void addGoods(Goods goods){
		List<Goods> goodsList = getGoodsList();
		if(goodsList==null){
			goodsList = new ArrayList<Goods>();
		}
		goodsList.add(goods);
		setGoodsList(goodsList);
		hasGoods = true;
	}
}
