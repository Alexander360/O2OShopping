package cn.com.dashihui.api.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 一个简易的Map封装，可以实现new Map().put().put()....的设值形式，省事省代码
 * 用法举例：ResultMap.newInstance().put(key,value).put(key.value)....getAttrs();
 */
public class ResultMap {
	private Map<String,Object> attrs = new HashMap<String,Object>();
	
	private ResultMap() {
	}
	
	public static ResultMap newInstance(){ 
		return new ResultMap();
	}
	
	public ResultMap put(String key, Object value){
		this.attrs.put(key, value);
		return this;
	}
	
	public Map<String,Object> getAttrs(){
		return attrs;
	}
}
